"""
A class containing methods to extract statistics from log files.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import math

import numpy as np
import pandas as pd

from analysis.analysis_tools import AnalysisTools, ProblemType


class ExtractStats:
    @staticmethod
    def extract_baseline_class_data(res_file, models=None):
        """
        This method applies to classification problems.

        Given a log file containing results for a single loss, extract the best model stats, i.e., those for the\
        learning rate that resulted in the best performance.

        :param res_file:
        :param models: list of models to consider; if None, consider all models present in data
        :return: dict {k, v}: k = model, v = (best loss, best lr, (avg train W.F1, std train W.F1,
         avg test W.F1, std test W.F1), (avg train AP, std train AP, avg test AP, std test AP)
        """
        df = pd.read_csv(res_file)
        perf_per_loss = AnalysisTools.extract_perf_per_loss(df, ProblemType.CLASS)
        losses = list(perf_per_loss.keys())

        lrs = df.lr.unique()
        best_result_per_model = dict()
        if models is None:
            models = df.model.unique()

        for model in models:
            best_lr, best_stats, best_loss = -1, (-1, -1, -1, -1), ''
            for i in range(len(losses)):
                for lr in lrs:
                    stats = perf_per_loss[losses[i]][model][lr]
                    if stats[2] > best_stats[2]:
                        best_stats = stats
                        best_lr = lr
                        best_loss = losses[i]
            # Extract average/std over average precision values
            df_ap = df[(df.model == model) & (df.lr == best_lr)]
            train_ap_avg, train_ap_std = np.mean(df_ap.train_ap), np.std(df_ap.train_ap)
            test_ap_avg, test_ap_std = np.mean(df_ap.test_ap), np.std(df_ap.test_ap)
            best_ap = (train_ap_avg, train_ap_std, test_ap_avg, test_ap_std)
            best_result_per_model[model] = (best_loss, best_lr, best_stats, best_ap)

        return best_result_per_model

    @staticmethod
    def extract_baseline_reg_data(res_file, models=None):
        """
        This method applies to regression problems.

        Given a log file containing results for a single loss, extract the best model stats, i.e., those for the\
        learning rate that resulted in the best performance.

        :param res_file:
        :param models: list of models to consider; if None, consider all models present in data
        :return: dict {k, v}: k = model, v = (best loss, best lr, (avg train loss, std train loss,\
                avg test loss, std test loss),\
                (avg train MAE, std train MAE, avg test MAE, std test MAE),\
                (avg train SpearmanR, std train SpearmanR, avg test SpearmanR, std test SpearmanR)
        """
        df = pd.read_csv(res_file)
        perf_per_loss = AnalysisTools.extract_perf_per_loss(df, problem_type=ProblemType.REG)
        losses = list(perf_per_loss.keys())

        lrs = df.lr.unique()
        best_result_per_model = dict()
        if models is None:
            models = df.model.unique()

        for model in models:
            best_lr, best_stats, best_loss = -1, (math.inf, math.inf, math.inf, math.inf), ''
            for i in range(len(losses)):
                for lr in lrs:
                    stats = perf_per_loss[losses[i]][model][lr]
                    if stats[2] < best_stats[2]:
                        best_stats = stats
                        best_lr = lr
                        best_loss = losses[i]
            # Extract average/std over other metrics
            df_best = df[(df.model == model) & (df.lr == best_lr)]
            train_mae_avg, train_mae_std = np.mean(df_best.train_mae), np.std(df_best.train_mae)
            test_mae_avg, test_mae_std = np.mean(df_best.test_mae), np.std(df_best.test_mae)
            best_mae = (train_mae_avg, train_mae_std, test_mae_avg, test_mae_std)

            train_spearman_r_avg, train_spearman_r_std = np.mean(df_best.train_spearman_r), np.std(df_best.train_spearman_r)
            test_spearman_r_avg, test_spearman_r_std = np.mean(df_best.test_spearman_r), np.std(df_best.test_spearman_r)
            best_spearman_r = (train_spearman_r_avg, train_spearman_r_std, test_spearman_r_avg, test_spearman_r_std)

            best_result_per_model[model] = (best_loss, best_lr, best_stats, best_mae, best_spearman_r)

        return best_result_per_model

    @staticmethod
    def extract_dbl_baseline_class_data(res_file_ce, res_file_uce, models):
        """
        Given a log file containing CrossEntropyLoss results (or some other loss), and another file containing
        UnbalancedCrossEntropyLoss results (or some other loss), compare both files and for each model, extract
        the best model stats, i.e., those for the learning rate and loss that resulted in the best performance.
        !!! IT IS EXPECTED THAT EACH LOG FILE ONLY CONTAINS RESULTS FOR A SINGLE LOSS !!!

        :param res_file_ce:
        :param res_file_uce:
        :param models:
        :return: dict {k, v}: k = model, v = (best loss, best lr, (avg train W.F1, std train W.F1,
         avg test W.F1, std test W.F1), (avg train AP, std train AP, avg test AP, std test AP)
        """
        dfs = [pd.read_csv(f) for f in [res_file_ce, res_file_uce]]
        perf_per_loss = [AnalysisTools.extract_perf_per_loss(dfs[i], ProblemType.CLASS) for i in range(len(dfs))]
        losses = [list(ppl.keys())[0] for ppl in perf_per_loss]

        lrs = dfs[0].lr.unique()
        best_result_per_model = dict()
        for model in models:
            best_lr, best_stats, best_loss = -1, (-1, -1, -1, -1), ''
            for i in range(len(perf_per_loss)):
                for lr in lrs:
                    stats = perf_per_loss[i][losses[i]][model][lr]
                    if stats[2] > best_stats[2]:
                        best_stats = stats
                        best_lr = lr
                        best_loss = losses[i]
            # Extract average/std over average precision values
            idx_df = losses.index(best_loss)
            df = dfs[idx_df]
            df_ap = df[(df.model == model) & (df.lr == best_lr)]
            train_ap_avg, train_ap_std = np.mean(df_ap.train_ap), np.std(df_ap.train_ap)
            test_ap_avg, test_ap_std = np.mean(df_ap.test_ap), np.std(df_ap.test_ap)
            best_ap = (train_ap_avg, train_ap_std, test_ap_avg, test_ap_std)
            best_result_per_model[model] = (best_loss, best_lr, best_stats, best_ap)

        return best_result_per_model

    @staticmethod
    def extract_dbl_baseline_reg_data(res_file_mse, res_file_wmse, models):
        """
        Given a log file containing MSELoss results (or some other loss), and another file containing
        WeightedMSELoss results (or some other loss), compare both files and for each model, extract
        the best model stats, i.e., those for the learning rate and MAE that resulted in the best performance.
        !!! IT IS EXPECTED THAT EACH LOG FILE ONLY CONTAINS RESULTS FOR A SINGLE LOSS !!!

        :param res_file_mse:
        :param res_file_wmse:
        :param models:
        :return: dict {k, v}: k = model, v = (best loss, best lr, (avg train MAE, std train MAE,
         avg test MAE, std test MAE), (avg train MAE, std train MAE,
         avg test MAE, std test MAE), (avg train S.R, std train S.R, avg test S.R, std test S.R)\
         Yes, best MAE stats will be returned twice. This is an artefact that should be removed, but does not\
         hinder code execution in any way, and no time to fix this.
        """
        dfs = [pd.read_csv(f) for f in [res_file_mse, res_file_wmse]]
        perf_per_loss = [AnalysisTools.extract_perf_per_loss(dfs[i], problem_type=ProblemType.REG) for i in range(len(dfs))]
        losses = [list(ppl.keys())[0] for ppl in perf_per_loss]

        lrs = dfs[0].lr.unique()
        best_result_per_model = dict()
        for model in models:
            best_lr, best_stats, best_loss = -1, (math.inf, math.inf, math.inf, math.inf), ''
            for i in range(len(perf_per_loss)):
                for lr in lrs:
                    stats = perf_per_loss[i][losses[i]][model][lr]
                    if stats[2] < best_stats[2]:  # [2] == average test MAE
                        best_stats = stats
                        best_lr = lr
                        best_loss = losses[i]
            # Extract average/std over other metrics
            idx_df = losses.index(best_loss)
            df = dfs[idx_df]
            df_best = df[(df.model == model) & (df.lr == best_lr)]
            train_mae_avg, train_mae_std = np.mean(df_best.train_mae), np.std(df_best.train_mae)
            test_mae_avg, test_mae_std = np.mean(df_best.test_mae), np.std(df_best.test_mae)
            best_mae = (train_mae_avg, train_mae_std, test_mae_avg, test_mae_std)

            train_spearman_r_avg, train_spearman_r_std = np.mean(df_best.train_spearman_r), np.std(df_best.train_spearman_r)
            test_spearman_r_avg, test_spearman_r_std = np.mean(df_best.test_spearman_r), np.std(df_best.test_spearman_r)
            best_spearman_r = (train_spearman_r_avg, train_spearman_r_std, test_spearman_r_avg, test_spearman_r_std)

            best_result_per_model[model] = (best_loss, best_lr, best_stats, best_mae, best_spearman_r)

        return best_result_per_model
