"""
For a given pretrained model, compute the predictions per image over the FULL provided dataset, and check two things:
1) What is the rank of the correct label?
2) What is the distance between the correct label and the top predicted label?

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import os
from collections import Counter

import dill
import torch
import torchvision

from config import Config
from networks.data.annotation_buffer_in_dataset import AnnotationBufferINDatasetFactory
from networks.data.base_buffer_dataset import BaseBufferDataset
from networks.nn.imagenet.load_buffered_imagenet import LoadBufferedImageNet
from tools.annotation_dataset_tools import AnnotationEmotionMap
from lor_tools.sort.sort_tools import SortTools


class CheckTopNRanking:
    def __init__(self, ann_file: str, b_filter=False, base_dir=None):
        """

        :param ann_file: annotation file to use
        :param b_filter: filter annotations?
        :param base_dir: root dir for the images
        """
        self.base_dir = Config.DIR_IMAGES if base_dir is None else base_dir

        # Load annotations
        self.ann_file = ann_file
        self.anns = BaseBufferDataset.get_anns(ann_file=ann_file, b_filter=b_filter)
        self.dataset = AnnotationBufferINDatasetFactory.get_dataset(ann_file=ann_file, base_dir=base_dir)

    def process_model(self, model, device=torch.device('cuda')):
        """

        :param model: loaded PyTorch model
        :param device: what device to load the model on
        :return: (cnt_corr_idx, cnt_corr_dist); cnt_corr_idx = {k: v} with k = idx of correct emo, v = number of times;\
        cnt_corr_dist = {k: v} with k = distance between correct emo and max predicted emo, v = number of times
        """
        model.eval()

        # Load dataloader
        data_loader = torch.utils.data.DataLoader(self.dataset, batch_size=25, drop_last=False)

        img_preds = {}
        cnt_corr_idx = Counter()  # How often was correct emotion ranked X?
        cnt_corr_dist = Counter()  # How often was the distance between the top choice and correct choice = X?

        print(f"Parsing data...")
        nb_samples = len(data_loader.dataset)
        nb_batches = nb_samples // data_loader.batch_size
        with torch.no_grad():
            for batch_index, (data, img_paths) in enumerate(data_loader):
                preds = torch.nn.functional.softmax(model(data.to(device)), dim=1)
                for img_idx, img_path in enumerate(img_paths):
                    img_preds[img_path] = preds[img_idx]
                    _preds = preds[img_idx]
                    _preds, pred_idxs = SortTools.sort_together(_preds, list(range(8)), b_desc=True)
                    # Get annotation for image
                    ref_emo = self.anns[self.anns.image_path == img_path].emotion.iloc[0]
                    ref_emo_idx = AnnotationEmotionMap.EMOTION_LEAF_MAP[ref_emo]
                    # Index of correct (=annotated) emotion in predictions
                    pred_idx_correct = pred_idxs.index(ref_emo_idx)
                    cnt_corr_idx[pred_idx_correct] += 1
                    cnt_corr_dist[AnnotationEmotionMap.get_emo_distance(AnnotationEmotionMap.EMO8_LIST[ref_emo_idx],
                                                                        AnnotationEmotionMap.EMO8_LIST[pred_idxs[0]])] += 1

                print(
                    f"At batch {batch_index + 1}/{nb_batches};"
                    f" sample {(batch_index + 1) * data_loader.batch_size}/{nb_samples}...")
                print(cnt_corr_idx)
                print(cnt_corr_dist)

        return cnt_corr_idx, cnt_corr_dist


if __name__ == '__main__':
    checker = CheckTopNRanking(ann_file=Config.FILE_ANN_SINGLE, base_dir=Config.DIR_IMAGES)
    # _model_pp = transforms.Compose([ImgResize(width=800, height=600),
    #                                 ImageNetPreProcess(chain_type=ImageNetPreProcess.NORMALIZE)])

    date = '20231222'
    loss = 'UnbalancedCrossEntropyLoss'
    saved_models = [
        (torchvision.models.alexnet,
         os.path.join(Config.DIR_MODELS, 'BestModels', date, 'emo8_alexnet_lr=0.0001_loss=UnbalancedCrossEntropyLoss_sc=test_cuda.pth')),
        (torchvision.models.vgg16,
         os.path.join(Config.DIR_MODELS, 'BestModels', date, 'emo8_vgg16_lr=0.001_loss=UnbalancedCrossEntropyLoss_sc=test_cuda.pth')),
        # (torchvision.models.vgg19,
        #  os.path.join(Config.DIR_MODELS, 'BestModels', date, 'emo8_vgg19_lr=0.001_loss=UnbalancedCrossEntropyLoss_sc=test_cuda.pth')),
        (torchvision.models.resnet18,
         os.path.join(Config.DIR_MODELS, 'BestModels', date, 'emo8_resnet18_lr=0.001_loss=UnbalancedCrossEntropyLoss_sc=test_cuda.pth')),
        # (torchvision.models.resnet34,
        #  os.path.join(Config.DIR_MODELS, 'BestModels', date, 'emo8_resnet34_lr=0.001_loss=UnbalancedCrossEntropyLoss_sc=test_cuda.pth')),
        (torchvision.models.resnet50,
         os.path.join(Config.DIR_MODELS, 'BestModels', date, 'emo8_resnet50_lr=0.001_loss=UnbalancedCrossEntropyLoss_sc=test_cuda.pth')),
        (torchvision.models.resnet101,
         os.path.join(Config.DIR_MODELS, 'BestModels', date, 'emo8_resnet101_lr=0.001_loss=UnbalancedCrossEntropyLoss_sc=test_cuda.pth')),
        # (torchvision.models.googlenet,
        #  os.path.join(Config.DIR_MODELS, 'BestModels', date, 'emo8_googlenet_lr=0.001_loss=UnbalancedCrossEntropyLoss_sc=test_cuda.pth')),
        # (torchvision.models.inception_v3,
        #  os.path.join(Config.DIR_MODELS, 'BestModels', date, 'emo8_inception_v3_lr=0.0001_loss=UnbalancedCrossEntropyLoss_sc=test_cuda.pth')),
        # (torchvision.models.densenet121,
        #  os.path.join(Config.DIR_MODELS, 'BestModels', date, 'emo8_densenet121_lr=0.001_loss=UnbalancedCrossEntropyLoss_sc=test_cuda.pth')),
        (torchvision.models.densenet161,
         os.path.join(Config.DIR_MODELS, 'BestModels', date, 'emo8_densenet161_lr=0.001_loss=UnbalancedCrossEntropyLoss_sc=test_cuda.pth'))
    ]

    _device = torch.device('cuda')

    out_file = os.path.join(Config.DIR_ANALYSIS, 'top_n_ranking.dill')
    if os.path.isfile(out_file):
        results = dill.load(open(out_file, 'rb'))
    else:
        results = {}

    for tpl_idx, tpl in enumerate(saved_models):
        print('#'*90)
        print(f"At {tpl_idx+1} of {len(saved_models)}...")
        print('#'*90)
        if tpl[0].__name__ in results:
            print(f"Results for model class {tpl[0].__name__} already present in output file. Skipping...")
            continue

        _model, _pp = LoadBufferedImageNet.load(model=tpl[0],
                                                weights_file= tpl[1],
                                                device=_device)

        res = checker.process_model(model=_model, device=_device)
        results[tpl[0].__name__] = res
        # Save results to disk; do this after each iteration to not loose anything in case of crash or whatever
        dill.dump(results, open(out_file, 'wb'))

    nb_anns = float(len(checker.anns))
    tbl_rank = "\t\\begin{tabular}{@{}l|llllllll@{}}\n\t& 0 & 1 & 2 & 3 & 4 & 5 & 6 & 7 \\\\\n\t\\midrule\n"
    tbl_dist = "\t\\begin{tabular}{@{}l|lllll@{}}\n\t& 0 & 1 & 2 & 3 & 4 \\\\\n\t\\midrule\n"
    # for idx_k, k in enumerate(sorted(results.keys())):
    for idx_k, k in enumerate(['alexnet', 'vgg16', 'resnet18', 'resnet50', 'resnet101', 'densenet161']):
        v = results[k]
        print()
        print("#"*90)
        print(f"Model: {k}")
        print(f"Idx. of annotated emo: {v[0]}")
        print(f"Distance pred. vs ann. emo: {v[1]}")

        tbl_rank += f'\t{k}'
        for i in range(8):
            tbl_rank += f' & {100*v[0][i]/nb_anns:.1f}'
        if idx_k+1 < len(results):
            tbl_rank += ' \\\\'
        tbl_rank += '\n'

        tbl_dist += f'\t{k}'
        for i in range(5):
            tbl_dist += f' & {100*v[1][i]/nb_anns:.1f}'
        if idx_k+1 < len(results):
            tbl_dist += ' \\\\'
        tbl_dist += '\n'

    tbl_rank += '\t\\end{tabular}\n'
    tbl_dist += '\t\\end{tabular}\n'

    print(tbl_rank)

    print('\n\n')
    print(tbl_dist)

