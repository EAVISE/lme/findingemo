"""
Generate baseline + EmoNet arousal regression prediction results.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import os

import pandas as pd
import torch
from termcolor import cprint
from torch.nn import MSELoss
from torchvision import models

from config import Config
from networks.base_double_reg_trainer import BaseDoubleRegTrainer
from networks.configs.data_config import DataConfig
from networks.configs.net_config import NetConfig, StopCriterion
from networks.data.annotation_dbl_buffered_arousal_reg_dataset import \
    AnnotationDblBufferedArousalDoubleDatasetRegFactory
from networks.data.buffer_preds_dataset import BufferPredictionsDataset
from networks.nn.multimodal.experiment_7b import Exp7bMergerNetworkRegressor2
from tools.logger import Logger

if __name__ == '__main__':
    net_config = NetConfig(nb_outputs=1)
    net_config.b_allow_parallel = False
    net_config.b_use_manual_seed = False
    net_config.device = torch.device('cuda')
    net_config.max_epochs = 250
    net_config.dropout = 0.25
    net_config.hold_for_epochs = 3
    net_config.patience = 6
    net_config.batch_size = 50
    net_config.learning_rate = .0010
    net_config.log_interval = 5
    net_config.burn_in = 0
    net_config.stop_criterion = StopCriterion.TEST

    net_config.pretrained = True
    net_config.b_classifier_l1 = False
    net_config.buffered_in = True

    net_config.b_freeze = False  # Always set to "False" when using buffered features
    net_config.freeze_classifier = False
    net_config.imagenet_freeze_depth = 1
    net_config.change_last_layer = False  # Always set to "False" when using buffered features
    net_config.optimizer_class = torch.optim.Adam

    data_config = DataConfig()
    data_config.ann_file = Config.FILE_ANN_SINGLE

    models = [
        'CLIP',
        'DINOv2_B',
        'emonet',
        models.alexnet,
        models.vgg16, models.vgg19,
        models.resnet18, models.resnet34, models.resnet50, models.resnet101,
        # models.googlenet,
        # models.inception_v3,
        models.densenet121, models.densenet161,
        # 'vgg16_cc', 'alexnet_cc', 'resnet18_cc',
        # 'places365-feats_alexnet', 'places365-feats_resnet18',
        # 'places365-feats_resnet50', 'places365-feats_densenet161'
    ]

    # Logger params
    out_dir = os.path.join(Config.DIR_DATA, 'Baseline+Emo8 Arousal Reg Loss=MSE')
    data_config.logger_params['logfile'] = None
    data_config.logger_params['b_overwrite'] = False
    data_config.logger_params['b_create_out_dir'] = True
    data_config.file_results = os.path.join(out_dir, f"training_arousal{net_config.nb_outputs}_results.csv")
    Logger.open_regressor_results_csv(data_config.file_results)

    data_config.dataset_factory_params['ann_file'] = data_config.ann_file
    data_config.dataset_factory_params['max_samples'] = -1

    for net in models:
        for lr in [0.01, 0.001, 0.0001]:
            for run in range(10):
                net_config.imagenet = net
                if isinstance(net, str):
                    in_name = net
                else:
                    in_name = net.__name__

                net_config.network = Exp7bMergerNetworkRegressor2
                net_config.network_params = {'nb_stream1': 1,
                                             'nb_stream2': 8,
                                             'b_softmax1': False, 'b_softmax2': True,
                                             'dropout': net_config.dropout}

                net_config.model_name = net_config.network.__name__ + '+' + in_name + \
                                        '^_Emo8^'

                net_config.learning_rate = lr
                net_config.loss = MSELoss  # Reset loss
                net_config.loss_params = {}
                net_config.b_set_loss_weights = False
                net_config.set_default_lr_scheduler()

                if net_config.buffered_in:
                    data_config.dataset_factory = AnnotationDblBufferedArousalDoubleDatasetRegFactory
                    data_config.dataset_factory_params['buffered_feats_stream1'] = BufferPredictionsDataset
                    data_config.dataset_factory_params['buffered_feats_stream2'] = BufferPredictionsDataset
                    if in_name == 'CLIP':
                        data_config.dataset_factory_params['buffered_feats_stream1_params'] = {
                            'model': in_name.lower(), 'nb_targets': 1, 'target': 'aro',
                            'root_dir': Config.DIR_BUFFER_CLIP}
                        data_config.dataset_factory_params['buffered_feats_stream2_params'] = {
                            'model': in_name.lower(), 'nb_targets': 8, 'target': 'emo',
                            'root_dir': Config.DIR_BUFFER_CLIP}
                    elif in_name == 'DINOv2_B':
                        data_config.dataset_factory_params['buffered_feats_stream1_params'] = {
                            'model': in_name.lower(), 'nb_targets': 1, 'target': 'aro',
                            'root_dir': Config.DIR_BUFFER_DINOv2}
                        data_config.dataset_factory_params['buffered_feats_stream2_params'] = {
                            'model': in_name.lower(), 'nb_targets': 8, 'target': 'emo',
                            'root_dir': Config.DIR_BUFFER_DINOv2}
                    else:
                        data_config.dataset_factory_params['buffered_feats_stream1_params'] = {
                            'model': in_name.lower(), 'nb_targets': 1, 'target': 'aro'}
                        data_config.dataset_factory_params['buffered_feats_stream2_params'] = {
                            'model': in_name.lower(), 'nb_targets': 8, 'target': 'emo'}
                else:
                    raise NotImplementedError("Option isn't available.")

                print("=" * 60)
                print(f"Training model {net_config.model_name} with starting lr={lr}...")

                gen_file_name = f"arousal{net_config.nb_outputs}_{net_config.model_name}_lr={lr}_" + \
                                f"loss={net_config.loss.__name__}_sc={net_config.stop_criterion.value}"

                data_config.logger_params['logfile'] = os.path.join(out_dir, gen_file_name + ".txt")

                # Following is a mechanism to allow picking up were the process was interrupted, if need be.
                # Check if the log file already exists and is referenced in the CSV file containing
                # the results (meaning the training finished), in which case we can skip this.
                # 1. Check if log file exists
                logfile = data_config.logger_params['logfile']
                file_idx = run+1
                if file_idx > 1:
                    filename, ext = os.path.splitext(logfile)
                    logfile = f"{filename}_{file_idx}{ext}"

                if os.path.isfile(logfile):
                    # 2. Check if file is mentioned in CSV file
                    csv = pd.read_csv(data_config.file_results)

                    if logfile in csv.output_file.unique():
                        cprint(f"Skipping {logfile}", color='cyan')
                        continue

                trainer = BaseDoubleRegTrainer(net_config, data_config, b_default_init=True)
                best_stats = trainer.train(b_save_model=False, model_name="cnn3_nopeople_model_1")
                print("=" * 60)
                for k, v in best_stats.items():
                    print(f"{k}: {v}")
