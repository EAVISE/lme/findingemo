"""

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import os
from urllib import request

from termcolor import cprint

from config import Config


def download_img(url: str, file_path: str):
    """

    :param url: url to the image to be downloaded
    :param file_path: full path, including filename, where the image will be downloaded to
    :return:
    """
    req = request.Request(
        url=url,
        headers={
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'
        }
    )
    res = request.urlopen(req, timeout=10)

    # Create output directory, if it does not already exist
    file_dir = os.path.dirname(file_path)
    if not os.path.isdir(file_dir):
        os.makedirs(file_dir, exist_ok=True)

    # Download image
    with open(file_path, 'wb') as fout:
        fout.write(res.read())


if __name__ == '__main__':
    # Download again if file already exists?
    b_re_download = False
    # Path to the dataset_urls.txt file
    url_file = os.path.join(Config.DIR_DATA, 'dataset_urls.txt')

    # Local parent folder where the images will be downloaded; will be created if it does not yet exist.
    parent_folder = Config.DIR_IMAGES

    # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    # Just because an image does not download automatically does
    # not mean it is no longer available.
    # You can always try clicking on the links to see if they open
    # in your browser.
    # In case of HTTP 404's, Waybackmachine sometimes brings
    # solace.
    # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    at_line = 0
    not_found = []
    with open(url_file, 'r') as fin:
        for line in fin:
            at_line += 1
            line = line.strip()
            url, local_path = line.split(' --> ')
            img_path = os.path.join(parent_folder, *local_path.split('/'))
            # Check file does not yet exist in case b_re_download = False
            if not b_re_download and os.path.exists(img_path):
                cprint(f'\nNot (re)downloading existing image: {local_path}\nURL: {url}', color='yellow')
                continue

            # Download URL
            try:
                download_img(url=url, file_path=img_path)
            except Exception as e:
                cprint(f'\nCould not download image: {local_path}\nURL: {url}', color='cyan')
                cprint(f"Error: {e}", color='cyan')
                not_found.append(local_path)

            print(f"\rDownloaded {at_line} images. Not found: {len(not_found)}", end='', flush=True)

    print("\nDone!")
    print("Here is a list of images that were not found:")
    for img_path in not_found:
        print(img_path)
