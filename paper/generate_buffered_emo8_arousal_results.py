"""
Generate arousal prediction results for FindingEmo dataset paper using the buffered Emo8 predictions of the
ImageNet models.

For this task, we divide the 7-value scale up into 3 bins.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import os

import pandas as pd
import torch
from termcolor import cprint
from torch.nn import CrossEntropyLoss
from torchvision import models

from config import Config
from networks.base_trainer import BaseTrainer
from networks.configs.data_config import DataConfig
from networks.configs.net_config import NetConfig, StopCriterion
from networks.data.annotation_buffered_arousal_dataset import AnnotationBufferedArousalDatasetFactory
from networks.nn.losses.unbalanced_crossentropy_loss import UnbalancedCrossEntropyLoss
from tools.logger import Logger

if __name__ == '__main__':
    net_config = NetConfig(nb_outputs=3)
    net_config.b_allow_parallel = False
    net_config.b_use_manual_seed = False
    net_config.device = torch.device('cuda:0')
    net_config.max_epochs = 250
    net_config.dropout = 0.25
    net_config.hold_for_epochs = 3
    net_config.patience = 6
    net_config.batch_size = 50
    net_config.learning_rate = .0010
    net_config.log_interval = 5
    net_config.burn_in = 0
    net_config.stop_criterion = StopCriterion.TEST

    net_config.pretrained = True
    net_config.b_classifier_l1 = False
    net_config.network_params = {'nb_outputs': net_config.nb_outputs, 'dropout': net_config.dropout}
    # net_config.network_params = {'weights': models.VGG16_Weights.DEFAULT, 'progress': True}
    net_config.buffered_in = True

    net_config.b_freeze = False  # Always set to "False" when using buffered features
    net_config.freeze_classifier = False
    net_config.imagenet_freeze_depth = 1
    net_config.change_last_layer = False  # Always set to "False" when using buffered features
    net_config.optimizer_class = torch.optim.Adam
    net_config.loss = CrossEntropyLoss  # CrossEntropyLoss  # UnbalancedCrossEntropyLoss2  # UnbalancedCrossEntropyLoss - SoftF1Loss
    net_config.loss_params = {}

    data_config = DataConfig()
    data_config.ann_file = Config.FILE_ANN_SINGLE
    data_config.base_dir = Config.DIR_IMAGES
    # Logger params
    data_config.logger_params['logfile'] = None
    data_config.logger_params['b_overwrite'] = False

    models = [
        'CLIP',
        'DINOv2_B',
        'emonet',
        models.alexnet,
        models.vgg16, models.vgg19,
        models.resnet18, models.resnet34, models.resnet50, models.resnet101,
        # models.googlenet,
        # models.inception_v3,
        models.densenet121, models.densenet161,
        # 'vgg16_cc', 'alexnet_cc', 'resnet18_cc',
        'places365-feats_alexnet', 'places365-feats_resnet18',
        'places365-feats_resnet50', 'places365-feats_densenet161'
    ]

    # Logger params
    out_dir = os.path.join(Config.DIR_DATA, 'Buffered Emo8 Arousal Loss=CE')
    data_config.logger_params['b_overwrite'] = False
    data_config.logger_params['b_create_out_dir'] = True
    data_config.file_results = os.path.join(out_dir, f"training_arousal{net_config.nb_outputs}_results.csv")
    data_config.file_c_mtxs = os.path.join(out_dir, f"training_arousal{net_config.nb_outputs}_results_confusion_matrices.txt")
    Logger.open_results_csv(data_config.file_results)
    Logger.open_mtx_file(data_config.file_c_mtxs)

    data_config.dataset_factory_params['ann_file'] = data_config.ann_file
    data_config.dataset_factory_params['base_dir'] = data_config.base_dir
    data_config.dataset_factory_params['b_no_neutral'] = True
    data_config.dataset_factory_params['bins'] = [1, 4]
    data_config.dataset_factory_params['max_samples'] = -1

    for net in models:
        for lr in [0.1, 0.01, 0.001, 0.0001]:
            for run in range(10):
                net_config.model_name = net.__name__ + '-emo8'
                net_config.network = net_config.model_name

                net_config.learning_rate = lr
                net_config.loss = CrossEntropyLoss  # UnbalancedCrossEntropyLoss  # Reset loss
                net_config.set_default_lr_scheduler()

                if net_config.buffered_in:
                    data_config.dataset_factory = AnnotationBufferedArousalDatasetFactory
                    data_config.dataset_factory_params['model_name'] = net_config.model_name
                else:
                    raise NotImplementedError("Option isn't available.")

                print("=" * 60)
                print(f"Training model {net_config.model_name} with starting lr={lr}...")

                gen_file_name = f"arousal{net_config.nb_outputs}_{net_config.model_name}_lr={lr}_" + \
                                f"loss={net_config.loss.__name__}_sc={net_config.stop_criterion.value}"

                data_config.logger_params['logfile'] = os.path.join(out_dir, gen_file_name + ".txt")

                # Following is a mechanism to allow picking up were the process was interrupted, if need be.
                # Check if the log file already exists and is referenced in the CSV file containing
                # the results (meaning the training finished), in which case we can skip this.
                # 1. Check if log file exists
                logfile = data_config.logger_params['logfile']
                file_idx = run+1
                if file_idx > 1:
                    filename, ext = os.path.splitext(logfile)
                    logfile = f"{filename}_{file_idx}{ext}"

                if os.path.isfile(logfile):
                    # 2. Check if file is mentioned in CSV file
                    csv = pd.read_csv(data_config.file_results)

                    if logfile in csv.output_file.unique():
                        cprint(f"Skipping {logfile}", color='cyan')
                        continue

                trainer = BaseTrainer(net_config, data_config, b_set_loss_weights=True, b_default_init=True)
                best_stats = trainer.train(b_save_model=False, model_name="cnn3_nopeople_model_1")
                print("=" * 60)
                for k, v in best_stats.items():
                    print(f"{k}: {v}")
