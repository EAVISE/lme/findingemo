"""

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import os
from collections import defaultdict, Counter

import matplotlib
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

from analysis.analysis_tools import AnalysisTools, ProblemType
from annotations.filter_prolific_annotations import FilterProlificAnnotations
from config import Config
from paper.extract_stats import ExtractStats
from tools.annotation_dataset_tools import AnnotationEmotionMap


class CreateGraphs:
    LOSS_MAP = {'CrossEntropyLoss': 'CE',
                'UnbalancedCrossEntropyLoss': 'UCE',
                'MSELoss': 'MSE',
                'WeightedMSELoss': 'WMSE'}

    @classmethod
    def create_annotations_emo8_stats_plot(cls, ann_file: str,
                                           b_filter=False,
                                           b_remove_incomplete=True, b_use_alt_score=True):
        """
        Same as create_annotations_emo8_stats_table from create_latex_tables.py, but creates a plot instead of a table.

        :param ann_file: annotation csv file to use
        :param b_filter: do the annotations need to be filtered?
        :param b_remove_incomplete: filter parameter, see FilterProlificAnnotations.process
        :param b_use_alt_score: filter parameter, FilterProlificAnnotations.process
        :return:
        """
        annotations = pd.read_csv(ann_file, header=0, index_col=0)
        annotations.fillna('Undefined', inplace=True)
        if b_filter:
            annotations = FilterProlificAnnotations.process(annotations,
                                                            b_remove_incomplete=b_remove_incomplete,
                                                            b_use_alt_score=b_use_alt_score)

        emo8_anns = dict()
        for emo_idx, emo in enumerate(AnnotationEmotionMap.EMO8_LIST):
            emo_set = set(AnnotationEmotionMap.EMOTION_SETS[emo_idx])
            df_emo = annotations[annotations.emotion.isin(emo_set)]
            emo8_anns[emo] = df_emo

        emo8_stats = defaultdict(dict)
        for emo, emo_anns in emo8_anns.items():
            emo8_stats[emo]['nb_emo_anns'] = len(emo_anns)
            emo8_stats[emo]['avg_arousal'] = np.average(emo_anns.arousal.values)
            emo8_stats[emo]['std_arousal'] = np.std(emo_anns.arousal.values)
            emo8_stats[emo]['avg_valence'] = np.average(emo_anns.valence.values)
            emo8_stats[emo]['std_valence'] = np.std(emo_anns.valence.values)
            emo8_stats[emo]['avg_ambiguity'] = np.average(emo_anns.ambiguity.values)
            emo8_stats[emo]['std_ambiguity'] = np.std(emo_anns.ambiguity.values)

            print(f"Emo: {emo}")
            print(f"\tnb: {emo8_stats[emo]['nb_emo_anns']}")
            print(f"\tarousal: {emo8_stats[emo]['avg_arousal']:.3f} +/ {emo8_stats[emo]['std_arousal']:.3f}")
            print(f"\tvalence: {emo8_stats[emo]['avg_valence']:.3f} +/ {emo8_stats[emo]['std_valence']:.3f}")
            print(f"\tambiguity: {emo8_stats[emo]['avg_ambiguity']:.3f} +/ {emo8_stats[emo]['std_ambiguity']:.3f}")

        # Adapted from https://matplotlib.org/stable/gallery/units/bar_unit_demo.html?highlight=barchart
        N = 8  # 8 main emotions
        fig = plt.figure(figsize=(18, 9))

        ind = np.arange(N)  # the x locations for the groups
        width = 0.2  # the width of the bars

        # plt.suptitle("Emo8 annotation statistics")

        per_metric = []
        metrics = ['arousal', 'valence', 'ambiguity']
        for metric in metrics:
            avgs, stds = [], []

            for emo in AnnotationEmotionMap.EMO8_LIST:
                avgs.append(emo8_stats[emo][f'avg_{metric}'])
                stds.append(emo8_stats[emo][f'std_{metric}'])
            per_metric.append((avgs, stds))

        for idx, tpl in enumerate(per_metric):
            plt.bar(ind + ((-1 + idx) * width), tpl[0], width, bottom=0, yerr=tpl[1],
                         label=metrics[idx])

        plt.xticks(ind, labels=AnnotationEmotionMap.EMO8_LIST, rotation=90, fontsize=14)

        plt.legend(fontsize=12)
        # plt.set_ylim([0.0, 1.])

        plt.tight_layout()
        plt.show()

    @classmethod
    def create_emo8_piechart(cls, ann_file: str):
        """
        Create piechart showing relative importance of each emotion in annotations.

        :param ann_file:
        :return:
        """
        annotations = pd.read_csv(ann_file, header=0, index_col=0)
        annotations.fillna('Undefined', inplace=True)

        # ============================================================
        # Emotion Leaf pie chart
        # ============================================================
        emos = annotations.emotion.values
        emo_leafs = [AnnotationEmotionMap.EMOTION_LEAF_MAP_EXT[emos[i]] for i in range(len(emos))]
        emo_leaf_counts = Counter(emo_leafs)

        pie_x = [emo_leaf_counts[x] for x in range(8)]
        total = sum(pie_x)
        pie_x = [x / total for x in pie_x]
        pie_labels = AnnotationEmotionMap.EMO8_LIST

        fig, ax = plt.subplots()
        fig.set_size_inches(6, 4)
        ax.pie(x=pie_x, labels=[f"{100 * x:.1f}" for x in pie_x], textprops={'fontsize': 11})
        plt.legend(pie_labels, loc=(1, 0), fontsize=12)

        plt.tight_layout()
        plt.show()

    @classmethod
    def create_valence_arousal_corr_plt(cls, ann_file: str,
                                        b_filter=False,
                                        b_remove_incomplete=True, b_use_alt_score=True):
        """

        :param ann_file: annotation csv file to use
        :param b_filter: do the annotations need to be filtered?
        :param b_remove_incomplete: filter parameter, see FilterProlificAnnotations.process
        :param b_use_alt_score: filter parameter, FilterProlificAnnotations.process
        :return:
        """
        annotations = pd.read_csv(ann_file, header=0, index_col=0)
        annotations.fillna('Undefined', inplace=True)
        if b_filter:
            annotations = FilterProlificAnnotations.process(annotations,
                                                            b_remove_incomplete=b_remove_incomplete,
                                                            b_use_alt_score=b_use_alt_score)

        anns_keep = annotations[annotations.reject == 'Keep']
        anns_keep_val = anns_keep.valence.values
        anns_keep_aro = anns_keep.arousal.values
        tpls_val_aro = list(zip(anns_keep_val, anns_keep_aro))
        cnts_tpls_val_aro = Counter(tpls_val_aro)
        for k, v in cnts_tpls_val_aro.most_common():
            print(f"{k}: {v}")

        ks = sorted(cnts_tpls_val_aro.keys())
        xs, ys = [ks[i][0] for i in range(len(ks))], [ks[i][1] for i in range(len(ks))]
        sizes = [cnts_tpls_val_aro[k] for k in ks]
        c = [sizes[i]/max(sizes) for i in range(len(sizes))]

        plt.scatter(xs, ys, c=c, s=sizes, alpha=0.5, cmap='viridis')
        # plt.title("Valence-Arousal correlation", fontsize=14)
        plt.xlabel("Valence", fontsize=12)
        plt.ylabel("Arousal", fontsize=12)

        plt.xlim(-3.5, 3.5)
        plt.ylim(-0.5, 6.5)

        plt.tight_layout()

        plt.show()

    @staticmethod
    def create_hist_aro_val_amb(ann_file: str):
        """

        :return:
        """
        annotations = pd.read_csv(ann_file, header=0, index_col=0)
        annotations.fillna('Undefined', inplace=True)
        anns_keep = annotations[annotations.reject == 'Keep']

        val_aro = Counter(anns_keep.arousal)
        val_val = Counter(anns_keep.valence)
        val_amb = Counter(anns_keep.ambiguity)

        matplotlib.rc('xtick', labelsize=12)
        matplotlib.rc('ytick', labelsize=12)

        fig, axes = plt.subplots(nrows=1, ncols=3, sharey=True, figsize=(18, 6))
        ax_arousal, ax_valence, ax_ambiguity = axes[0], axes[1], axes[2]

        ax_arousal.bar(sorted(val_aro.keys()), [val_aro[k] for k in sorted(val_aro.keys())])
        ax_arousal.set_title("Arousal", fontsize=16)
        ax_arousal.set_xlabel("Value", fontsize=15)
        ax_arousal.set_ylabel("Nb. Annotations", fontsize=15)

        ax_valence.bar(sorted(val_val.keys()), [val_val[k] for k in sorted(val_val.keys())])
        ax_valence.set_xlabel("Value", fontsize=15)
        ax_valence.set_title("Valence", fontsize=15)
        # ax_valence.set_ylabel("Weighted F1")

        ax_ambiguity.bar(sorted(val_amb.keys()), [val_amb[k] for k in sorted(val_amb.keys())])
        ax_ambiguity.set_xlabel("Value", fontsize=15)
        ax_ambiguity.set_title("Ambiguity", fontsize=15)
        # ax_valence.set_ylabel("Weighted F1")

        plt.show()

    @staticmethod
    def create_barchart_emo8_beyond_baseline_per_lr(res_files: list, loss='UnbalancedCrossEntropyLoss'):
        """
        Create a graph for results per lr.

        :param res_files: list of tuples (path_to_csv, name)
        :param loss:
        :return:
        """
        dfs = [pd.read_csv(res_files[i][0]) for i in range(len(res_files))]
        perf_per_loss = [AnalysisTools.extract_perf_per_loss(dfs[i]) for i in range(len(dfs))]
        perf_for_loss = [perf_per_loss[i][loss] for i in range(len(perf_per_loss))]

        baseline_models = dfs[0].model.unique()
        model_map = defaultdict(list)
        to_remove = set()
        for i in range(0, len(dfs)):  # Yes, also include baseline models mapping onto themselves
            uq_models = list(dfs[i].model.unique())
            for model in baseline_models:
                b_found = False
                for m in uq_models:
                    if model in m:
                        model_map[model].append(m)
                        b_found = True
                        break
                if not b_found:
                    model_map[model].append(None)  # We only get here if no match was found
                    to_remove.add(model)

        for k in to_remove:
            del model_map[k]

        # Format is per_model[model_name][lr] = (train_avg, train_std, test_avg, test_std)
        # _per_model = [perf_per_loss[i][loss] for i in range(len(perf_per_loss))]

        # uq_lrs = sorted(set(df_baseline.lr.unique()).intersection(set(df_exp.lr.unique())), reverse=True)
        uq_lrs = [0.1, 0.01, 0.001, 0.0001]

        for lr in uq_lrs:
            # Adapted from https://matplotlib.org/stable/gallery/units/bar_unit_demo.html?highlight=barchart
            N = len(model_map)
            fig, axes = plt.subplots(nrows=2, ncols=1, sharex=True, figsize=(18, 9))
            ax_train, ax_test = axes[0], axes[1]

            ind = np.arange(N)  # the x locations for the groups
            width = 0.1  # the width of the bars

            # plt.suptitle(f'TestChart\nLoss = {loss}, lr={lr}')

            # This list will contain the data we want to plot, grouped per model group, i.e., 'baseline', 'Exp2...1a', etc.
            per_group = []

            # First, we add the baseline data
            # Cycle over "vgg16", "vgg19", etc.
            for i in range(len(perf_for_loss)):
                train_variant_avgs, train_variant_stds = [], []
                test_variant_avgs, test_variant_stds = [], []
                for model in sorted(model_map.keys()):
                    train_variant_avgs.append(perf_for_loss[i][model_map[model][i]][lr][0])
                    train_variant_stds.append(perf_for_loss[i][model_map[model][i]][lr][1])
                    test_variant_avgs.append(perf_for_loss[i][model_map[model][i]][lr][2])
                    test_variant_stds.append(perf_for_loss[i][model_map[model][i]][lr][3])
                per_group.append((train_variant_avgs, train_variant_stds, test_variant_avgs, test_variant_stds))
            nb_groups = len(per_group)

            model_variants = [e[1] for e in res_files]
            bar_colors = ['c', 'tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple', 'tab:brown']
            for idx, tpl in enumerate(per_group):
                ax_train.bar(ind + width / 2 + ((-nb_groups//2 + idx) * width), tpl[0], width, bottom=0, yerr=tpl[1],
                             label=f"{model_variants[idx]}", color=bar_colors[idx])
                ax_test.bar(ind + width / 2 + ((-nb_groups//2 + idx) * width), tpl[2], width, bottom=0, yerr=tpl[3],
                            label=f"{model_variants[idx]}", color=bar_colors[idx])

            plt.xticks(ind, labels=sorted(model_map.keys()), rotation=60)

            ax_train.legend(loc=2)
            ax_train.set_title("Train")
            ax_train.set_ylabel("Weighted F1")
            ax_train.set_ylim([0., 1.])

            ax_test.legend(loc=2)
            ax_test.set_title("Test")
            ax_test.set_ylabel("Weighted F1")
            ax_test.set_ylim([0., 1.])

            ax_train.grid(visible=True, axis='y')
            ax_test.grid(visible=True, axis='y')

            plt.tight_layout()
            # if out_dir is not None:
            #     file_name = f'baseline_vs_exp2_-_loss={loss}_lr={lr}.png'
            #     plt.savefig(os.path.join(out_dir, file_name))
            #     plt.close()
            # else:
            plt.show()

    @staticmethod
    def create_barchart_emo8_baseline(res_file_ce: str, res_file_uce: str, ignore_models=None):
        """

        :param res_file_ce: path to the CrossEntropyLoss results
        :param res_file_uce: path to the UnbalancedCrossEntropyLoss results
        :param ignore_models: list of models to ignore
        :return:
        """
        dfs = [pd.read_csv(f) for f in [res_file_ce, res_file_uce]]
        perf_per_loss = [AnalysisTools.extract_perf_per_loss(dfs[i], problem_type=ProblemType.CLASS) for i in range(len(dfs))]
        losses = [list(ppl.keys())[0] for ppl in perf_per_loss]
        loss_map = {'CrossEntropyLoss': 'CE',
                    'UnbalancedCrossEntropyLoss': 'UCE'}

        # models = sorted(dfs[0].model.unique())
        models = ['emonet', 'alexnet', 'alexnet_cc', 'places365-feats_alexnet',
                  'vgg16', 'vgg16_cc', 'vgg19',
                  'resnet18', 'resnet18_cc', 'places365-feats_resnet18', 'resnet34',
                  'resnet50', 'places365-feats_resnet50', 'resnet101',
                  'googlenet', 'inception_v3',
                  'densenet121', 'densenet161', 'places365-feats_densenet161', 'DINOv2_1', 'DINOv2_B', 'CLIP']
        model_names = ['emonet', 'alexnet', 'alexnet cc', 'alexnet p365',
                  'vgg16', 'vgg16 cc', 'vgg19',
                  'resnet18', 'resnet18 cc', 'resnet18 p365', 'resnet34',
                  'resnet50', 'resnet50 p365', 'resnet101',
                  'googlenet', 'inception_v3',
                  'densenet121', 'densenet161', 'densenet161 p365', 'DINOv2 (S)', 'DINOv2 (B)', 'CLIP']

        if ignore_models is not None:
            for m in ignore_models:
                try:
                    idx_m = models.index(m)
                    models.remove(models[idx_m])
                    model_names.remove(model_names[idx_m])
                except ValueError:
                    continue

        lrs = dfs[0].lr.unique()
        best_result_per_model = dict()
        for model in models:
            best_lr, best_stats, best_loss = -1, (-1, -1, -1, -1), ''
            for i in range(len(perf_per_loss)):
                for lr in lrs:
                    stats = perf_per_loss[i][losses[i]][model][lr]
                    if stats[2] > best_stats[2]:
                        best_stats = stats
                        best_lr = lr
                        best_loss = losses[i]
            # Extract average/std over average precision values
            idx_df = losses.index(best_loss)
            df = dfs[idx_df]
            df_ap = df[(df.model == model) & (df.lr == best_lr)]
            train_ap_avg, train_ap_std = np.mean(df_ap.train_ap), np.std(df_ap.train_ap)
            test_ap_avg, test_ap_std = np.mean(df_ap.test_ap), np.std(df_ap.test_ap)
            best_ap = (train_ap_avg, train_ap_std, test_ap_avg, test_ap_std)
            best_result_per_model[model] = (best_loss, best_lr, best_stats, best_ap)

        # Adapted from https://matplotlib.org/stable/gallery/units/bar_unit_demo.html?highlight=barchart
        N = len(best_result_per_model)
        fig, axes = plt.subplots(nrows=2, ncols=1, sharex=True, figsize=(18, 9))
        ax_train, ax_test = axes[0], axes[1]

        ind = np.arange(N)  # the x locations for the groups
        width = 0.25  # the width of the bars

        # plt.suptitle(f'Baseline Emo8 classification performance')

        # This list will contain the data we want to plot, grouped per model.
        per_model = []

        # First, add average Weighted F1, then average AP
        for idx in [2, 3]:
            train_avgs, train_stds = [], []
            test_avgs, test_stds = [], []
            for model in models:
                    train_avgs.append(best_result_per_model[model][idx][0])
                    train_stds.append(best_result_per_model[model][idx][1])
                    test_avgs.append(best_result_per_model[model][idx][2])
                    test_stds.append(best_result_per_model[model][idx][3])
            per_model.append((train_avgs, train_stds, test_avgs, test_stds))
        nb_groups = len(per_model)

        metrics = ['Weighted F1', 'Average Precision']
        bar_colors = ['tab:blue', 'tab:orange']
        for idx, tpl in enumerate(per_model):
            ax_train.bar(ind + width / 2 + ((-nb_groups//2 + idx) * width), tpl[0], width, bottom=0, yerr=tpl[1],
                         label=f"{metrics[idx]}", color=bar_colors[idx])
            ax_test.bar(ind + width / 2 + ((-nb_groups//2 + idx) * width), tpl[2], width, bottom=0, yerr=tpl[3],
                        label=f"{metrics[idx]}", color=bar_colors[idx])

        # ax_train.bar(ind, per_model_train_y, width, bottom=0, yerr=per_model_train_y_err)
        # ax_test.bar(ind, per_model_test_y, width, bottom=0, yerr=per_model_test_y_err)
        plt.xticks(ind, labels=model_names, rotation=60)

        lr_labels = [(f'{best_result_per_model[model][1]},'
                      f' {loss_map[best_result_per_model[model][0]]}') for model in models]
        ax_train.bar_label(ax_train.containers[1], labels=lr_labels, padding=10, rotation='vertical')

        ax_train.legend(loc=2)
        ax_train.set_title("Train")
        ax_train.set_ylabel("Weighted F1")
        ax_train.set_ylim([0., 1.])

        ax_test.legend(loc=2)
        ax_test.set_title("Test")
        ax_test.set_ylabel("Weighted F1")
        ax_test.set_ylim([0., 1.])

        ax_train.grid(visible=True, axis='y')
        ax_test.grid(visible=True, axis='y')

        plt.tight_layout()
        # if out_dir is not None:
        #     file_name = f'baseline_vs_exp2_-_loss={loss}_lr={lr}.png'
        #     plt.savefig(os.path.join(out_dir, file_name))
        #     plt.close()
        # else:
        plt.show()

    @classmethod
    def create_barchart_aro_val_regression(cls, res_file_aro_mse: str, res_file_val_mse: str,
                                           res_file_aro_wmse: str, res_file_val_wmse: str, ignore_models=None):
        """
        Create plot depicting MSE and Spearman R for Arousal and Valence as regression results.

        :param res_file_aro_mse: path to the arousal MSE loss results
        :param res_file_val_mse: path to the valence MSE loss results
        :param res_file_aro_wmse: path to the arousal WMSE loss results
        :param res_file_val_wmse: path to the valence WMSE loss results
        :param ignore_models: optional list of models to ignore
        :return:
        """
        models = ['emonet', 'alexnet', 'alexnet_cc', 'places365-feats_alexnet',
                  'vgg16', 'vgg16_cc', 'vgg19',
                  'resnet18', 'resnet18_cc', 'places365-feats_resnet18', 'resnet34',
                  'resnet50', 'places365-feats_resnet50', 'resnet101',
                  'googlenet', 'inception_v3',
                  'densenet121', 'densenet161', 'places365-feats_densenet161', 'DINOv2_1', 'CLIP']
        model_names = ['emonet', 'alexnet', 'alexnet cc', 'alexnet p365',
                  'vgg16', 'vgg16 cc', 'vgg19',
                  'resnet18', 'resnet18 cc', 'resnet18 p365', 'resnet34',
                  'resnet50', 'resnet50 p365', 'resnet101',
                  'googlenet', 'inception_v3',
                  'densenet121', 'densenet161', 'densenet161 p365', 'DINOv2_1', 'CLIP']

        if ignore_models is not None:
            for m in ignore_models:
                try:
                    idx_m = models.index(m)
                    models.remove(models[idx_m])
                    model_names.remove(model_names[idx_m])
                except ValueError:
                    continue

        aro_mse_best_res_per_model = ExtractStats.extract_baseline_reg_data(res_file_aro_mse, models=models)
        val_mse_best_res_per_model = ExtractStats.extract_baseline_reg_data(res_file_val_mse, models=models)
        aro_wmse_best_res_per_model = ExtractStats.extract_baseline_reg_data(res_file_aro_wmse, models=models)
        val_wmse_best_res_per_model = ExtractStats.extract_baseline_reg_data(res_file_val_wmse, models=models)

        aro_best_res_per_model = ExtractStats.extract_dbl_baseline_reg_data(res_file_aro_mse, res_file_aro_wmse, models=models)
        val_best_res_per_model = ExtractStats.extract_dbl_baseline_reg_data(res_file_val_mse, res_file_val_wmse, models=models)

        # Adapted from https://matplotlib.org/stable/gallery/units/bar_unit_demo.html?highlight=barchart
        N = len(models)

        # This list will contain the data we want to plot, grouped per model.
        per_model = []

        # First, add average Weighted F1, then average AP
        for best_result_per_model in [aro_best_res_per_model, val_best_res_per_model]:
            for idx in [3, 4]:  # 3 = MAE, 4 = SpearmanR
                train_avgs, train_stds = [], []
                test_avgs, test_stds = [], []
                for model in models:
                        train_avgs.append(best_result_per_model[model][idx][0])
                        train_stds.append(best_result_per_model[model][idx][1])
                        test_avgs.append(best_result_per_model[model][idx][2])
                        test_stds.append(best_result_per_model[model][idx][3])
                per_model.append((train_avgs, train_stds, test_avgs, test_stds))
        nb_groups = len(per_model)

        metrics = ['Arousal MAE', 'Arousal SpearmanR', 'Valence MAE', 'Valence SpearmanR']
        bar_colors = ['tab:orange', 'tab:green']
        ind = np.arange(N)  # the x locations for the groups
        width = 0.18  # the width of the bars

        # ############################################################
        # Create combined table
        # ############################################################
        fig, axes = plt.subplots(nrows=2, ncols=1, sharex=True, figsize=(18, 9))
        ax_train, ax_test = axes[0], axes[1]

        # plt.suptitle(f'Baseline Emo8/Arousal/Valence performance', fontsize=20)

        for idx, tpl in enumerate(per_model):
            if idx % 2 == 0:
                ax_train.bar(ind + width / 2 + ((-nb_groups//2 + idx) * width), tpl[0], width, bottom=0, yerr=tpl[1],
                             label=f"{metrics[idx]}", color=bar_colors[idx//2])
                ax_test.bar(ind + width / 2 + ((-nb_groups//2 + idx) * width), tpl[2], width, bottom=0, yerr=tpl[3],
                            label=f"{metrics[idx]}", color=bar_colors[idx//2])
            else:
                ax_train.bar(ind + width / 2 + ((-nb_groups//2 + idx) * width), tpl[0], width, bottom=0, yerr=tpl[1],
                             label=f"{metrics[idx]}", facecolor=(bar_colors[idx//2], 0.25), edgecolor=bar_colors[idx//2],
                             linewidth=1.5)
                ax_test.bar(ind + width / 2 + ((-nb_groups//2 + idx) * width), tpl[2], width, bottom=0, yerr=tpl[3],
                            label=f"{metrics[idx]}", facecolor=(bar_colors[idx//2], 0.25), edgecolor=bar_colors[idx//2],
                            linewidth=1.5)

        plt.xticks(ind, labels=model_names, rotation=60, fontsize=15)

        for idx, best_result_per_model in enumerate([aro_wmse_best_res_per_model, val_wmse_best_res_per_model]):
            lr_labels = [f'{best_result_per_model[model][1]}\n{CreateGraphs.LOSS_MAP[best_result_per_model[model][0]]}'
                         for model in models]
            ax_test.bar_label(ax_test.containers[1+(4*idx)], labels=lr_labels,
                              padding=5, rotation='vertical', fontsize=9)

        ax_train.legend(loc=2, ncols=2, fontsize=12)
        ax_train.set_title(f"Train, Loss=MSELoss")
        ax_train.set_ylabel("Weighted F1/Average Precision", fontsize=15)
        ax_train.set_ylim([0., 2.])

        # ax_test.legend(loc=2)
        ax_test.set_title("Test, Loss=MSELoss")
        ax_test.set_ylabel("Weighted F1/Average Precisions", fontsize=15)
        ax_test.set_ylim([0., 2.])
        plt.yticks(fontsize=12)

        ax_train.grid(visible=True, axis='y')
        ax_test.grid(visible=True, axis='y')

        plt.tight_layout()

        plt.show()

        # ############################################################
        # Create train table
        # ############################################################
        fig = plt.figure(figsize=(18, 6.5))

        plt.title(f'Baseline Arousal/Valence performance: Train data', fontsize=20)

        for idx, tpl in enumerate(per_model):
            if idx % 2 == 0:
                plt.bar(ind + width / 2 + ((-nb_groups // 2 + idx) * width), tpl[0], width, bottom=0, yerr=tpl[1],
                             label=f"{metrics[idx]}", color=bar_colors[idx // 2])
            else:
                plt.bar(ind + width / 2 + ((-nb_groups // 2 + idx) * width), tpl[0], width, bottom=0, yerr=tpl[1],
                             label=f"{metrics[idx]}", facecolor=(bar_colors[idx // 2], 0.25),
                             edgecolor=bar_colors[idx // 2],
                             linewidth=1.5)

        plt.xticks(ind, labels=model_names, rotation=60, fontsize=15)

        for idx, best_result_per_model in enumerate(
                [aro_best_res_per_model, val_best_res_per_model]):
            lr_labels = [(f'{best_result_per_model[model][1]}\n'
                          f' {cls.LOSS_MAP[best_result_per_model[model][0]]}') for model in models]
            plt.bar_label(fig.axes[0].containers[1 + (4 * idx)], labels=lr_labels,
                              padding=5, rotation='vertical', fontsize=9)

        plt.legend(loc=2, ncols=2, fontsize=12)
        plt.ylabel("Weighted F1/Average Precision", fontsize=15)
        plt.yticks(fontsize=12)
        plt.ylim([0., 2.])

        plt.grid(visible=True, axis='y')

        plt.tight_layout()

        plt.show()

        # ############################################################
        # Create test table
        # ############################################################
        fig = plt.figure(figsize=(18, 6.5))

        plt.title(f'Baseline Arousal/Valence performance: Test data', fontsize=20)

        for idx, tpl in enumerate(per_model):
            if idx % 2 == 0:
                plt.bar(ind + width / 2 + ((-nb_groups // 2 + idx) * width), tpl[2], width, bottom=0, yerr=tpl[3],
                        label=f"{metrics[idx]}", color=bar_colors[idx // 2])
            else:
                plt.bar(ind + width / 2 + ((-nb_groups // 2 + idx) * width), tpl[2], width, bottom=0, yerr=tpl[3],
                        label=f"{metrics[idx]}", facecolor=(bar_colors[idx // 2], 0.25),
                        edgecolor=bar_colors[idx // 2],
                        linewidth=1.5)

        plt.xticks(ind, labels=model_names, rotation=60, fontsize=15)

        for idx, best_result_per_model in enumerate(
                [aro_best_res_per_model, val_best_res_per_model]):
            lr_labels = [(f'{best_result_per_model[model][1]}\n'
                          f' {cls.LOSS_MAP[best_result_per_model[model][0]]}') for model in models]
            plt.bar_label(fig.axes[0].containers[1 + (4 * idx)], labels=lr_labels,
                          padding=5, rotation='vertical', fontsize=9)

        plt.legend(loc=2, ncols=2, fontsize=12)
        plt.ylabel("Weighted F1/Average Precision", fontsize=15)
        plt.ylim([0., 2.])
        plt.yticks(fontsize=12)

        plt.grid(visible=True, axis='y')

        plt.tight_layout()

        plt.show()

    @classmethod
    def create_barchart_all_baseline_class(cls, res_file_emo8_ce: str, res_file_emo8_uce: str,
                                           res_file_aro_ce: str, res_file_aro_uce: str,
                                           res_file_val_ce: str, res_file_val_uce: str,
                                           ignore_models=None):
        """
        Same as create_barchart_emo8_baseline, but also including arousal and valence classification in same plot.

        :param res_file_emo8_ce: path to the CrossEntropyLoss results
        :param res_file_emo8_uce: path to the UnbalancedCrossEntropyLoss results
        :param res_file_aro_ce: same for arousal
        :param res_file_aro_uce: same for arousal
        :param res_file_val_ce: same for valence
        :param res_file_val_uce: same for valence
        :param ignore_models: optional list of models to ignore
        :return:
        """
        # models = sorted(dfs[0].model.unique())
        models = ['emonet', 'alexnet', 'alexnet_cc', 'places365-feats_alexnet',
                  'vgg16', 'vgg16_cc', 'vgg19',
                  'resnet18', 'resnet18_cc', 'places365-feats_resnet18', 'resnet34',
                  'resnet50', 'places365-feats_resnet50', 'resnet101',
                  'googlenet', 'inception_v3',
                  'densenet121', 'densenet161', 'places365-feats_densenet161']
        model_names = ['emonet', 'alexnet', 'alexnet cc', 'alexnet p365',
                  'vgg16', 'vgg16 cc', 'vgg19',
                  'resnet18', 'resnet18 cc', 'resnet18 p365', 'resnet34',
                  'resnet50', 'resnet50 p365', 'resnet101',
                  'googlenet', 'inception_v3',
                  'densenet121', 'densenet161', 'densenet161 p365']

        if ignore_models is not None:
            for m in ignore_models:
                try:
                    idx_m = models.index(m)
                    models.remove(models[idx_m])
                    model_names.remove(model_names[idx_m])
                except ValueError:
                    continue

        emo8_best_res_per_model = ExtractStats.extract_dbl_baseline_class_data(res_file_emo8_ce, res_file_emo8_uce, models=models)
        aro_best_res_per_model = ExtractStats.extract_dbl_baseline_class_data(res_file_aro_ce, res_file_aro_uce, models=models)
        val_best_res_per_model = ExtractStats.extract_dbl_baseline_class_data(res_file_val_ce, res_file_val_uce, models=models)

        # Adapted from https://matplotlib.org/stable/gallery/units/bar_unit_demo.html?highlight=barchart
        N = len(models)

        # This list will contain the data we want to plot, grouped per model.
        per_model = []

        # First, add average Weighted F1, then average AP
        for best_result_per_model in [emo8_best_res_per_model, aro_best_res_per_model, val_best_res_per_model]:
            for idx in [2, 3]:
                train_avgs, train_stds = [], []
                test_avgs, test_stds = [], []
                for model in models:
                        train_avgs.append(best_result_per_model[model][idx][0])
                        train_stds.append(best_result_per_model[model][idx][1])
                        test_avgs.append(best_result_per_model[model][idx][2])
                        test_stds.append(best_result_per_model[model][idx][3])
                per_model.append((train_avgs, train_stds, test_avgs, test_stds))
        nb_groups = len(per_model)

        metrics = ['Emo8 W.F1', 'Emo8 AP', 'Arousal3 W.F1', 'Arousal3 AP', 'Valence3 W.F1', 'Valence3 AP']
        bar_colors = ['tab:blue', 'tab:orange', 'tab:green']
        ind = np.arange(N)  # the x locations for the groups
        width = 0.12  # the width of the bars

        # ############################################################
        # Create combined table
        # ############################################################
        fig, axes = plt.subplots(nrows=2, ncols=1, sharex=True, figsize=(18, 9))
        ax_train, ax_test = axes[0], axes[1]

        # plt.suptitle(f'Baseline Emo8/Arousal/Valence performance', fontsize=20)

        for idx, tpl in enumerate(per_model):
            if idx % 2 == 0:
                ax_train.bar(ind + width / 2 + ((-nb_groups//2 + idx) * width), tpl[0], width, bottom=0, yerr=tpl[1],
                             label=f"{metrics[idx]}", color=bar_colors[idx//2])
                ax_test.bar(ind + width / 2 + ((-nb_groups//2 + idx) * width), tpl[2], width, bottom=0, yerr=tpl[3],
                            label=f"{metrics[idx]}", color=bar_colors[idx//2])
            else:
                ax_train.bar(ind + width / 2 + ((-nb_groups//2 + idx) * width), tpl[0], width, bottom=0, yerr=tpl[1],
                             label=f"{metrics[idx]}", facecolor=(bar_colors[idx//2], 0.25), edgecolor=bar_colors[idx//2],
                             linewidth=1.5)
                ax_test.bar(ind + width / 2 + ((-nb_groups//2 + idx) * width), tpl[2], width, bottom=0, yerr=tpl[3],
                            label=f"{metrics[idx]}", facecolor=(bar_colors[idx//2], 0.25), edgecolor=bar_colors[idx//2],
                            linewidth=1.5)

        plt.xticks(ind, labels=model_names, rotation=60, fontsize=15)

        for idx, best_result_per_model in enumerate([emo8_best_res_per_model, aro_best_res_per_model, val_best_res_per_model]):
            lr_labels = [(f'{best_result_per_model[model][1]},'
                          f' {cls.LOSS_MAP[best_result_per_model[model][0]]}') for model in models]
            ax_test.bar_label(ax_test.containers[1+(4*idx)], labels=lr_labels,
                              padding=10, rotation='vertical', fontsize=12)

        ax_train.legend(loc=2, ncols=3, fontsize=12)
        ax_train.set_title("Train")
        ax_train.set_ylabel("Weighted F1/Average Precision", fontsize=15)
        ax_train.set_ylim([0., 1.])

        # ax_test.legend(loc=2)
        ax_test.set_title("Test")
        ax_test.set_ylabel("Weighted F1/Average Precisions", fontsize=15)
        ax_test.set_ylim([0., 1.])
        plt.yticks(fontsize=12)

        ax_train.grid(visible=True, axis='y')
        ax_test.grid(visible=True, axis='y')

        plt.tight_layout()

        plt.show()

        # ############################################################
        # Create train table
        # ############################################################
        fig = plt.figure(figsize=(18, 6.5))

        # plt.title(f'Baseline Emo8/Arousal/Valence performance: Train data', fontsize=20)

        for idx, tpl in enumerate(per_model):
            if idx % 2 == 0:
                plt.bar(ind + width / 2 + ((-nb_groups // 2 + idx) * width), tpl[0], width, bottom=0, yerr=tpl[1],
                             label=f"{metrics[idx]}", color=bar_colors[idx // 2])
            else:
                plt.bar(ind + width / 2 + ((-nb_groups // 2 + idx) * width), tpl[0], width, bottom=0, yerr=tpl[1],
                             label=f"{metrics[idx]}", facecolor=(bar_colors[idx // 2], 0.25),
                             edgecolor=bar_colors[idx // 2],
                             linewidth=1.5)

        plt.xticks(ind, labels=model_names, rotation=60, fontsize=15)

        for idx, best_result_per_model in enumerate(
                [emo8_best_res_per_model, aro_best_res_per_model, val_best_res_per_model]):
            lr_labels = [(f'{best_result_per_model[model][1]},'
                          f' {cls.LOSS_MAP[best_result_per_model[model][0]]}') for model in models]
            plt.bar_label(fig.axes[0].containers[1 + (4 * idx)], labels=lr_labels,
                              padding=10, rotation='vertical', fontsize=12)

        plt.legend(loc=2, ncols=3, fontsize=12)
        plt.ylabel("Weighted F1/Average Precision", fontsize=15)
        plt.yticks(fontsize=12)
        plt.ylim([0., 1.])

        plt.grid(visible=True, axis='y')

        plt.tight_layout()

        plt.show()

        # ############################################################
        # Create test table
        # ############################################################
        fig = plt.figure(figsize=(18, 6.5))

        # plt.title(f'Baseline Emo8/Arousal/Valence performance: Test data', fontsize=20)

        for idx, tpl in enumerate(per_model):
            if idx % 2 == 0:
                plt.bar(ind + width / 2 + ((-nb_groups // 2 + idx) * width), tpl[2], width, bottom=0, yerr=tpl[3],
                        label=f"{metrics[idx]}", color=bar_colors[idx // 2])
            else:
                plt.bar(ind + width / 2 + ((-nb_groups // 2 + idx) * width), tpl[2], width, bottom=0, yerr=tpl[3],
                        label=f"{metrics[idx]}", facecolor=(bar_colors[idx // 2], 0.25),
                        edgecolor=bar_colors[idx // 2],
                        linewidth=1.5)

        plt.xticks(ind, labels=model_names, rotation=60, fontsize=15)

        for idx, best_result_per_model in enumerate(
                [emo8_best_res_per_model, aro_best_res_per_model, val_best_res_per_model]):
            lr_labels = [(f'{best_result_per_model[model][1]},'
                          f' {cls.LOSS_MAP[best_result_per_model[model][0]]}') for model in models]
            plt.bar_label(fig.axes[0].containers[1 + (4 * idx)], labels=lr_labels,
                          padding=10, rotation='vertical', fontsize=12)

        plt.legend(loc=2, ncols=3, fontsize=12)
        plt.ylabel("Weighted F1/Average Precision", fontsize=15)
        plt.ylim([0., 1.])
        plt.yticks(fontsize=12)

        plt.grid(visible=True, axis='y')

        plt.tight_layout()

        plt.show()

    @classmethod
    def create_barchart_all_baseline_class_reg(cls, res_file_emo8_ce: str, res_file_emo8_uce: str,
                                               res_file_aro_mse: str, res_file_aro_wmse: str,
                                               res_file_val_mse: str, res_file_val_wmse: str,
                                               ignore_models=None,
                                               b_save=False,
                                               out_dir=None):
        """
        Same as create_barchart_emo8_baseline, but also including arousal and valence regression (!) in same plot.

        :param res_file_emo8_ce: path to the CrossEntropyLoss results
        :param res_file_emo8_uce: path to the UnbalancedCrossEntropyLoss results
        :param res_file_aro_mse: same for arousal
        :param res_file_aro_wmse: same for arousal
        :param res_file_val_mse: same for valence
        :param res_file_val_wmse: same for valence
        :param ignore_models: optional list of models to ignore
        :param b_save: save plots to disc; if True, plots will be saved in eps, png and svg formats.
        :param out_dir: output directory to use when b_save is True
        :return:
        """
        # models = sorted(dfs[0].model.unique())
        models = ['emonet', 'alexnet', 'alexnet_cc', 'places365-feats_alexnet',
                  'vgg16', 'vgg16_cc', 'vgg19',
                  'resnet18', 'resnet18_cc', 'places365-feats_resnet18', 'resnet34',
                  'resnet50', 'places365-feats_resnet50', 'resnet101',
                  'googlenet', 'inception_v3',
                  'densenet121', 'densenet161', 'places365-feats_densenet161', 'DINOv2_1', 'DINOv2_B', 'CLIP']
        model_names = ['emonet', 'alexnet', 'alexnet cc', 'alexnet p365',
                  'vgg16', 'vgg16 cc', 'vgg19',
                  'resnet18', 'resnet18 cc', 'resnet18 p365', 'resnet34',
                  'resnet50', 'resnet50 p365', 'resnet101',
                  'googlenet', 'inception_v3',
                  'densenet121', 'densenet161', 'densenet161 p365', 'DINOv2_1', 'DINOv2', 'CLIP']

        if ignore_models is not None:
            for m in ignore_models:
                try:
                    idx_m = models.index(m)
                    models.remove(models[idx_m])
                    model_names.remove(model_names[idx_m])
                except ValueError:
                    continue

        emo8_best_res_per_model = ExtractStats.extract_dbl_baseline_class_data(res_file_emo8_ce, res_file_emo8_uce, models=models)
        aro_best_res_per_model = ExtractStats.extract_dbl_baseline_reg_data(res_file_aro_mse, res_file_aro_wmse, models=models)
        val_best_res_per_model = ExtractStats.extract_dbl_baseline_reg_data(res_file_val_mse, res_file_val_wmse, models=models)

        # Adapted from https://matplotlib.org/stable/gallery/units/bar_unit_demo.html?highlight=barchart
        N = len(models)

        # This list will contain the data we want to plot, grouped per model.
        per_model = []

        # First, add average Weighted F1, then average AP
        for i, best_result_per_model in enumerate([emo8_best_res_per_model, aro_best_res_per_model, val_best_res_per_model]):
            idxs = [2, 3] if i == 0 else [3, 4]
            for idx in idxs:
                train_avgs, train_stds = [], []
                test_avgs, test_stds = [], []
                for model in models:
                        train_avgs.append(best_result_per_model[model][idx][0])
                        train_stds.append(best_result_per_model[model][idx][1])
                        test_avgs.append(best_result_per_model[model][idx][2])
                        test_stds.append(best_result_per_model[model][idx][3])
                per_model.append((train_avgs, train_stds, test_avgs, test_stds))
        nb_groups = len(per_model)

        metrics = ['Emo8 W.F1', 'Emo8 AP', 'Arousal MAE', 'Arousal S.R', 'Valence MAE', 'Valence S.R']
        bar_colors = ['#1f77b4', '#ff7f0e', '#2ca02c']
        bar_colors_alpha = ['#c7ddec', '#ffdec2', '#cae7ca']  # Use these colors instead of using alpha to be able to save to EPS format.
        ind = np.arange(N)  # the x locations for the groups
        width = 0.12  # the width of the bars

        # ############################################################
        # Create combined table
        # ############################################################
        fig, axes = plt.subplots(nrows=2, ncols=1, sharex=True, figsize=(18, 9))
        ax_train_class, ax_test_class = axes[0], axes[1]
        ax_train_reg, ax_test_reg = ax_train_class.twinx(), ax_test_class.twinx()

        plt.suptitle(f'Baseline Emo8/Arousal/Valence performance', fontsize=20)

        for idx, tpl in enumerate(per_model):
            ax_train = ax_train_class if idx in {0, 1, 3, 5} else ax_train_reg
            ax_test = ax_test_class if idx in {0, 1, 3, 5} else ax_test_reg
            if idx % 2 == 0:
                ax_train.bar(ind + width / 2 + ((-nb_groups//2 + idx) * width), tpl[0], width, bottom=0, yerr=tpl[1],
                             label=f"{metrics[idx]}", color=bar_colors[idx//2])
                ax_test.bar(ind + width / 2 + ((-nb_groups//2 + idx) * width), tpl[2], width, bottom=0, yerr=tpl[3],
                            label=f"{metrics[idx]}", color=bar_colors[idx//2])
            else:
                ax_train.bar(ind + width / 2 + ((-nb_groups//2 + idx) * width), tpl[0], width, bottom=0, yerr=tpl[1],
                             label=f"{metrics[idx]}", facecolor=(bar_colors_alpha[idx//2]), edgecolor=bar_colors[idx//2],
                             linewidth=1.5)
                ax_test.bar(ind + width / 2 + ((-nb_groups//2 + idx) * width), tpl[2], width, bottom=0, yerr=tpl[3],
                            label=f"{metrics[idx]}", facecolor=(bar_colors_alpha[idx//2]), edgecolor=bar_colors[idx//2],
                            linewidth=1.5)

        ax_test_class.set_xticks(ind, labels=model_names, rotation=60, fontsize=15)

        # for idx, best_result_per_model in enumerate([emo8_best_res_per_model, aro_best_res_per_model, val_best_res_per_model]):
        #     lr_labels = [(f'{best_result_per_model[model][1]},'
        #                   f' {cls.LOSS_MAP[best_result_per_model[model][0]]}') for model in models]
        #     ax_test_class.bar_label(ax_test_class.containers[1+(4*idx)], labels=lr_labels,
        #                       padding=10, rotation='vertical', fontsize=12)

        lines, labels = ax_train_class.get_legend_handles_labels()
        lines2, labels2 = ax_train_reg.get_legend_handles_labels()
        ax_train_class.legend(lines[:2] + [lines2[0]] + [lines[2]] + [lines2[1]] + [lines[3]],
                              labels[:2] + [labels2[0]] + [labels[2]] + [labels2[1]] + [labels[3]],
                              loc=1, ncols=3, fontsize=12)

        ax_train_class.set_title("Train")
        ax_train_class.set_ylabel("W.F1 / AP / S.R", fontsize=15)
        ax_train_reg.set_ylabel("MAE", fontsize=15)
        ax_train_class.set_ylim([0., 1.])
        ax_train_reg.set_ylim([0., 2.])
        ax_train_reg.set_yticks(ticks=np.arange(0, 2.1, 0.4), labels=[f'{x:.2f}' for x in np.arange(0, 2.1, 0.4)])

        # ax_test.legend(loc=2)
        ax_test_class.set_title("Test")
        ax_test_class.set_ylabel("W.F1 / AP / S.R", fontsize=15)
        ax_test_reg.set_ylabel("MAE", fontsize=15)
        ax_test_class.set_ylim([0., 1.])
        ax_test_reg.set_ylim([0., 2.])
        ax_test_reg.set_yticks(ticks=np.arange(0, 2.1, 0.4), labels=[f'{x:.2f}' for x in np.arange(0, 2.1, 0.4)])

        ax_train_class.tick_params(axis='y', which='major', labelsize=12)
        ax_train_reg.tick_params(axis='y', which='major', labelsize=12)
        ax_test_class.tick_params(axis='y', which='major', labelsize=12)
        ax_test_reg.tick_params(axis='y', which='major', labelsize=12)

        ax_train_class.grid(visible=True, axis='y')
        ax_test_class.grid(visible=True, axis='y')

        plt.tight_layout()

        if b_save:
            plt.savefig(os.path.join(out_dir, 'BarChart_Baseline.png'))
            plt.savefig(os.path.join(out_dir, 'BarChart_Baseline.eps'))
            plt.savefig(os.path.join(out_dir, 'BarChart_Baseline.svg'))

        plt.show()

        # ############################################################
        # Create train table
        # ############################################################
        fig, ax_class = plt.subplots(figsize=(18, 6.5))
        ax_reg = ax_class.twinx()

        # plt.title(f'Baseline Emo8/Arousal/Valence performance: Train data', fontsize=20)

        for idx, tpl in enumerate(per_model):
            ax = ax_class if idx in {0, 1, 3, 5} else ax_reg
            if idx % 2 == 0:
                ax.bar(ind + width / 2 + ((-nb_groups // 2 + idx) * width), tpl[0], width, bottom=0, yerr=tpl[1],
                       label=f"{metrics[idx]}", color=bar_colors[idx // 2])
            else:
                ax.bar(ind + width / 2 + ((-nb_groups // 2 + idx) * width), tpl[0], width, bottom=0, yerr=tpl[1],
                       label=f"{metrics[idx]}", facecolor=(bar_colors_alpha[idx // 2]),
                       edgecolor=bar_colors[idx // 2], linewidth=1.5)

        ax_class.set_xticks(ind, labels=model_names, rotation=60, fontsize=15)

        for idx, best_result_per_model in enumerate(
                [emo8_best_res_per_model, aro_best_res_per_model, val_best_res_per_model]):
            ax = ax_class  # if idx == 0 else ax_reg
            lr_labels = [(f'{best_result_per_model[model][1]},'
                          f' {cls.LOSS_MAP[best_result_per_model[model][0]]}') for model in models]
            text = ax.bar_label(ax.containers[3+(2*idx)], labels=lr_labels,
                         padding=10, rotation='vertical', fontsize=10)
            for t in text:
                t.xy = (t.xy[0]+0.014, t.xy[1])

        lines, labels = ax_class.get_legend_handles_labels()
        lines2, labels2 = ax_reg.get_legend_handles_labels()
        plt.legend(lines[:2] + [lines2[0]] + [lines[2]] + [lines2[1]] + [lines[3]],
                   labels[:2] + [labels2[0]] + [labels[2]] + [labels2[1]] + [labels[3]],
                   loc=2, ncols=3, fontsize=12)

        ax_class.set_ylabel("W.F1 / AP / S.R", fontsize=15)
        ax_reg.set_ylabel("MAE", fontsize=15)
        ax_class.set_ylim([0., 1.])
        ax_reg.set_ylim([0., 2.])
        ax_reg.set_yticks(ticks=np.arange(0, 2.1, 0.4), labels=[f'{x:.2f}' for x in np.arange(0, 2.1, 0.4)])

        ax_class.tick_params(axis='y', which='major', labelsize=12)
        ax_reg.tick_params(axis='y', which='major', labelsize=12)
        plt.grid(visible=True, axis='y', alpha=0.5)

        plt.tight_layout()

        if b_save:
            plt.savefig(os.path.join(out_dir, 'BarChart_Baseline_Train.png'))
            plt.savefig(os.path.join(out_dir, 'BarChart_Baseline_Train.eps'))
            plt.savefig(os.path.join(out_dir, 'BarChart_Baseline_Train.svg'))

        plt.show()

        # ############################################################
        # Create test table
        # ############################################################
        fig, ax_class = plt.subplots(figsize=(18, 6.5))
        ax_reg = ax_class.twinx()

        # plt.title(f'Baseline Emo8/Arousal/Valence performance: Test data', fontsize=20)

        for idx, tpl in enumerate(per_model):
            ax = ax_class if idx in {0, 1, 3, 5} else ax_reg
            if idx % 2 == 0:
                ax.bar(ind + width / 2 + ((-nb_groups // 2 + idx) * width), tpl[2], width, bottom=0, yerr=tpl[3],
                       label=f"{metrics[idx]}", color=bar_colors[idx // 2])
            else:
                ax.bar(ind + width / 2 + ((-nb_groups // 2 + idx) * width), tpl[2], width, bottom=0, yerr=tpl[3],
                       label=f"{metrics[idx]}", facecolor=(bar_colors_alpha[idx // 2]),
                       edgecolor=bar_colors[idx // 2], linewidth=1.5)

        ax_class.set_xticks(ind, labels=model_names, rotation=60, fontsize=15)

        for idx, best_result_per_model in enumerate(
                [emo8_best_res_per_model, aro_best_res_per_model, val_best_res_per_model]):
            ax = ax_class  # if idx == 0 else ax_reg
            lr_labels = [(f'{best_result_per_model[model][1]},'
                          f' {cls.LOSS_MAP[best_result_per_model[model][0]]}') for model in models]
            text = ax.bar_label(ax.containers[3+(2*idx)], labels=lr_labels,
                                padding=10, rotation='vertical', fontsize=10)
            for t in text:
                t.xy = (t.xy[0]+0.014, t.xy[1])

        lines, labels = ax_class.get_legend_handles_labels()
        lines2, labels2 = ax_reg.get_legend_handles_labels()
        plt.legend(lines[:2] + [lines2[0]] + [lines[2]] + [lines2[1]] + [lines[3]],
                   labels[:2] + [labels2[0]] + [labels[2]] + [labels2[1]] + [labels[3]],
                   loc=2, ncols=3, fontsize=12)

        ax_class.set_ylabel("W.F1 / AP / S.R", fontsize=15)
        ax_reg.set_ylabel("MAE", fontsize=15)
        ax_class.set_ylim([0., 1.])
        ax_reg.set_ylim([0., 2.])
        ax_reg.set_yticks(ticks=np.arange(0, 2.1, 0.4), labels=[f'{x:.2f}' for x in np.arange(0, 2.1, 0.4)])

        ax_class.tick_params(axis='y', which='major', labelsize=12)
        ax_reg.tick_params(axis='y', which='major', labelsize=12)
        plt.grid(visible=True, axis='y', alpha=0.5)

        plt.tight_layout()

        if b_save:
            plt.savefig(os.path.join(out_dir, 'BarChart_Baseline_Test.png'))
            plt.savefig(os.path.join(out_dir, 'BarChart_Baseline_Test.eps'))
            plt.savefig(os.path.join(out_dir, 'BarChart_Baseline_Test.svg'))

        plt.show()

    @staticmethod
    def create_barchart_f1only_emo8_beyond_baseline(res_files: list, loss='UnbalancedCrossEntropyLoss',
                                                    ignore_models=None):
        """
        Create a graph with best results. Only plot Weighted F1.

        :param res_files: list of tuples (path_to_csv, name)
        :param loss:
        :param ignore_models: optional list of models to ignore
        :return:
        """
        dfs = [pd.read_csv(res_files[i][0]) for i in range(len(res_files))]
        perf_per_loss = [AnalysisTools.extract_perf_per_loss(dfs[i]) for i in range(len(dfs))]
        perf_for_loss = [perf_per_loss[i][loss] for i in range(len(perf_per_loss))]

        best_result_per_model = []
        for i in range(len(perf_for_loss)):
            temp_d = dict()
            for model, d in perf_for_loss[i].items():
                best_lr, best_stats = -1, (-1, -1, -1, -1)
                for lr, stats in d.items():
                    if stats[2] > best_stats[2]:
                        best_stats = stats
                        best_lr = lr
                    temp_d[model] = (best_lr, best_stats)
            best_result_per_model.append(temp_d)

        baseline_models = set(dfs[0].model.unique())
        if ignore_models is not None:
            for m in ignore_models:
                if m in baseline_models:
                    baseline_models.remove(m)

        model_map = defaultdict(list)
        to_remove = set()
        for i in range(0, len(dfs)):  # Yes, also include baseline models mapping onto themselves
            uq_models = list(dfs[i].model.unique())
            for model in baseline_models:
                b_found = False
                for m in uq_models:
                    if model in m:
                        model_map[model].append(m)
                        b_found = True
                        break
                if not b_found:
                    model_map[model].append(None)  # We only get here if no match was found
                    to_remove.add(model)

        for k in to_remove:
            del model_map[k]

        # Adapted from https://matplotlib.org/stable/gallery/units/bar_unit_demo.html?highlight=barchart
        N = len(model_map)
        fig, axes = plt.subplots(nrows=2, ncols=1, sharex=True, figsize=(18, 9))
        ax_train, ax_test = axes[0], axes[1]

        ind = np.arange(N)  # the x locations for the groups
        width = 0.1  # the width of the bars

        # plt.suptitle(f'Best Emo8 performance per model\nLoss = {loss}')

        # This list will contain the data we want to plot, grouped per model group, i.e., 'baseline', 'Exp2...1a', etc.
        per_group = []

        # First, we add the baseline data
        # Cycle over "vgg16", "vgg19", etc.
        for i in range(len(best_result_per_model)):
            train_variant_avgs, train_variant_stds = [], []
            test_variant_avgs, test_variant_stds = [], []
            for model in sorted(model_map.keys()):
                train_variant_avgs.append(best_result_per_model[i][model_map[model][i]][1][0])
                train_variant_stds.append(best_result_per_model[i][model_map[model][i]][1][1])
                test_variant_avgs.append(best_result_per_model[i][model_map[model][i]][1][2])
                test_variant_stds.append(best_result_per_model[i][model_map[model][i]][1][3])
            per_group.append((train_variant_avgs, train_variant_stds, test_variant_avgs, test_variant_stds))
        nb_groups = len(per_group)

        model_variants = [e[1] for e in res_files]
        bar_colors = ['c', 'tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple', 'tab:brown']
        for idx, tpl in enumerate(per_group):
            ax_train.bar(ind + width / 2 + ((-nb_groups//2 + idx) * width), tpl[0], width, bottom=0, yerr=tpl[1],
                         label=f"{model_variants[idx]}", color=bar_colors[idx])
            ax_test.bar(ind + width / 2 + ((-nb_groups//2 + idx) * width), tpl[2], width, bottom=0, yerr=tpl[3],
                        label=f"{model_variants[idx]}", color=bar_colors[idx])

        plt.xticks(ind, labels=sorted(model_map.keys()), rotation=60)

        for grp_idx in range(nb_groups):
            lr_labels = [best_result_per_model[grp_idx][model_map[model][grp_idx]][0] for model in sorted(model_map.keys())]
            ax_train.bar_label(ax_train.containers[1+2*grp_idx], labels=lr_labels, padding=10, rotation='vertical')

        ax_train.legend(loc=2)
        ax_train.set_title("Train")
        ax_train.set_ylabel("Weighted F1")
        ax_train.set_ylim([0., 1.])

        ax_test.legend(loc=2)
        ax_test.set_title("Test")
        ax_test.set_ylabel("Weighted F1")
        ax_test.set_ylim([0., 1.])

        ax_train.grid(visible=True, axis='y')
        ax_test.grid(visible=True, axis='y')

        plt.tight_layout()
        # if out_dir is not None:
        #     file_name = f'baseline_vs_exp2_-_loss={loss}_lr={lr}.png'
        #     plt.savefig(os.path.join(out_dir, file_name))
        #     plt.close()
        # else:
        plt.show()

    @staticmethod
    def create_barchart_emo8_beyond_baseline(res_files: list, ignore_models=None):
        """
        Create a graph with best results. Plot both Weighted F1 and Average Precision

        :param res_files: list of tuples (path_to_csv, name)
        :param ignore_models: optional list of models to ignore
        :return:
        """
        models = ['alexnet', 'vgg16', 'vgg19', 'resnet18', 'resnet50', 'resnet101',
                  'densenet161', 'DINOv2_B', 'CLIP']
        model_names = ['alexnet', 'vgg16', 'vgg19', 'resnet18', 'resnet50', 'resnet101',
                  'densenet161', 'DINOv2', 'CLIP']
        best_results_per_model = [ExtractStats.extract_baseline_class_data(f[0]) for f in res_files]
        # baseline_models = list(best_results_per_model[0].keys())
        if ignore_models is not None:
            for m in ignore_models:
                try:
                    idx_m = models.index(m)
                    models.remove(models[idx_m])
                    model_names.remove(model_names[idx_m])
                except ValueError:
                    continue

        model_map = defaultdict(list)
        to_remove = set()

        for i in range(0, len(best_results_per_model)):  # Yes, also include baseline models mapping onto themselves
            uq_models = list(best_results_per_model[i].keys())
            for model in models:
                b_found = False
                for m in uq_models:
                    if model in m:
                        model_map[model].append(m)
                        b_found = True
                        break
                if not b_found:
                    model_map[model].append(None)  # We only get here if no match was found
                    to_remove.add(model)

        for k in to_remove:
            del model_map[k]

        # Adapted from https://matplotlib.org/stable/gallery/units/bar_unit_demo.html?highlight=barchart
        # models = sorted(model_map.keys())
        N = len(models)

        ind = np.arange(N)  # the x locations for the groups
        width = 0.065  # the width of the bars

        # This list will contain the data we want to plot, grouped per model.
        per_model = []

        # First, add average Weighted F1, then average AP
        for idx_brpm, brpm in enumerate(best_results_per_model):
            for idx in [2, 3]:
                train_avgs, train_stds = [], []
                test_avgs, test_stds = [], []
                for model in models:
                        train_avgs.append(brpm[model_map[model][idx_brpm]][idx][0])
                        train_stds.append(brpm[model_map[model][idx_brpm]][idx][1])
                        test_avgs.append(brpm[model_map[model][idx_brpm]][idx][2])
                        test_stds.append(brpm[model_map[model][idx_brpm]][idx][3])
                per_model.append((train_avgs, train_stds, test_avgs, test_stds))
        nb_groups = len(per_model)

        metrics = []
        for rf in res_files:
            metrics += [f'{rf[1]} W.F1', f'{rf[1]} AP']
        # metrics = [f'Emo8 W.F1', 'Emo8 AP', 'Arousal3 W.F1', 'Arousal3 AP', 'Valence3 W.F1', 'Valence3 AP']
        bar_colors = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple', 'tab:brown', 'tab:pink']

        # ############################################################
        # Create combined table
        # ############################################################
        fig, axes = plt.subplots(nrows=2, ncols=1, sharex=True, figsize=(18, 9))
        ax_train, ax_test = axes[0], axes[1]

        # plt.suptitle(f'Beyond baseline Emo8 performance', fontsize=20)

        for idx, tpl in enumerate(per_model):
            if idx % 2 == 0:
                ax_train.bar(ind + width / 2 + ((-nb_groups//2 + idx) * width), tpl[0], width, bottom=0, yerr=tpl[1],
                             label=f"{metrics[idx]}", color=bar_colors[idx//2])
                ax_test.bar(ind + width / 2 + ((-nb_groups//2 + idx) * width), tpl[2], width, bottom=0, yerr=tpl[3],
                            label=f"{metrics[idx]}", color=bar_colors[idx//2])
            else:
                ax_train.bar(ind + width / 2 + ((-nb_groups//2 + idx) * width), tpl[0], width, bottom=0, yerr=tpl[1],
                             label=f"{metrics[idx]}", facecolor=(bar_colors[idx//2], 0.25), edgecolor=bar_colors[idx//2],
                             linewidth=1.5)
                ax_test.bar(ind + width / 2 + ((-nb_groups//2 + idx) * width), tpl[2], width, bottom=0, yerr=tpl[3],
                            label=f"{metrics[idx]}", facecolor=(bar_colors[idx//2], 0.25), edgecolor=bar_colors[idx//2],
                             linewidth=1.5)

        # ax_train.bar(ind, per_model_train_y, width, bottom=0, yerr=per_model_train_y_err)
        # ax_test.bar(ind, per_model_test_y, width, bottom=0, yerr=per_model_test_y_err)
        plt.xticks(ind, labels=model_names, rotation=60, fontsize=15)

        for idx_brpm, brpm in enumerate(best_results_per_model):
            lr_labels = [f'{brpm[model_map[model][idx_brpm]][1]}' for model in models]
            ax_test.bar_label(ax_test.containers[1+(4*idx_brpm)], labels=lr_labels,
                              padding=15, rotation='vertical', fontsize=12)

        ax_train.legend(loc=2, ncols=3, fontsize=12)
        ax_train.set_title("Train")
        ax_train.set_ylabel("Weighted F1/Average Precision", fontsize=15)
        ax_train.set_ylim([0., 1.])

        # ax_test.legend(loc=2)
        ax_test.set_title("Test")
        ax_test.set_ylabel("Weighted F1/Average Precisions", fontsize=15)
        ax_test.set_ylim([0., 1.])
        plt.yticks(fontsize=12)

        ax_train.grid(visible=True, axis='y')
        ax_test.grid(visible=True, axis='y')

        plt.tight_layout()
        plt.show()

        # ############################################################
        # Create train table
        # ############################################################
        fig = plt.figure(figsize=(18, 6))

        # plt.title(f'Beyond baseline Emo8 performance: Train data', fontsize=20)

        for idx, tpl in enumerate(per_model):
            if idx % 2 == 0:
                plt.bar(ind + width / 2 + ((-nb_groups // 2 + idx) * width), tpl[0], width, bottom=0, yerr=tpl[1],
                        label=f"{metrics[idx]}", color=bar_colors[idx // 2])
            else:
                plt.bar(ind + width / 2 + ((-nb_groups // 2 + idx) * width), tpl[0], width, bottom=0, yerr=tpl[1],
                        label=f"{metrics[idx]}", facecolor=(bar_colors[idx // 2], 0.25),
                        edgecolor=bar_colors[idx // 2], linewidth=1.5)

        plt.xticks(ind, labels=model_names, rotation=60, fontsize=15)

        for idx_brpm, brpm in enumerate(best_results_per_model):
            lr_labels = [f'{brpm[model_map[model][idx_brpm]][1]}' for model in models]
            plt.bar_label(fig.axes[0].containers[1 + (4 * idx_brpm)], labels=lr_labels,
                          padding=15, rotation='vertical', fontsize=12)

        plt.legend(loc=2, ncols=3, fontsize=12)
        plt.ylabel("Weighted F1/Average Precision", fontsize=15)
        plt.ylim([0., 1.])
        plt.yticks(fontsize=12)

        plt.grid(visible=True, axis='y')

        plt.tight_layout()
        plt.show()

        # ############################################################
        # Create test table
        # ############################################################
        fig = plt.figure(figsize=(18, 6))

        # plt.title(f'Beyond baseline Emo8 performance: Test data', fontsize=20)

        for idx, tpl in enumerate(per_model):
            if idx % 2 == 0:
                plt.bar(ind + width / 2 + ((-nb_groups // 2 + idx) * width), tpl[2], width, bottom=0, yerr=tpl[3],
                        label=f"{metrics[idx]}", color=bar_colors[idx // 2])
            else:
                plt.bar(ind + width / 2 + ((-nb_groups // 2 + idx) * width), tpl[2], width, bottom=0, yerr=tpl[3],
                        label=f"{metrics[idx]}", facecolor=(bar_colors[idx // 2], 0.25),
                        edgecolor=bar_colors[idx // 2], linewidth=1.5)

        plt.xticks(ind, labels=model_names, rotation=60, fontsize=15)

        for idx_brpm, brpm in enumerate(best_results_per_model):
            lr_labels = [f'{brpm[model_map[model][idx_brpm]][1]}' for model in models]
            plt.bar_label(fig.axes[0].containers[1 + (4 * idx_brpm)], labels=lr_labels,
                          padding=15, rotation='vertical', fontsize=12)
    
        plt.legend(loc=2, ncols=3, fontsize=12)
        plt.ylabel("Weighted F1/Average Precision", fontsize=15)
        plt.ylim([0., 1.])
        plt.yticks(fontsize=12)

        plt.grid(visible=True, axis='y')

        plt.tight_layout()
        plt.show()

    @staticmethod
    def create_barchart_f1only_aro_val_beyond_baseline(res_files: list, loss='UnbalancedCrossEntropyLoss', target='Arousal'):
        """
        Create a graph with best results. Only plot weighted F1 metric.

        :param res_files: list of tuples (path_to_csv, name)
        :param loss:
        :return:
        """
        dfs = [pd.read_csv(res_files[i][0]) for i in range(len(res_files))]
        perf_per_loss = [AnalysisTools.extract_perf_per_loss(dfs[i]) for i in range(len(dfs))]
        perf_for_loss = [perf_per_loss[i][loss] for i in range(len(perf_per_loss))]

        best_result_per_model = []
        for i in range(len(perf_for_loss)):
            temp_d = dict()
            for model, d in perf_for_loss[i].items():
                best_lr, best_stats = -1, (-1, -1, -1, -1)
                for lr, stats in d.items():
                    if stats[2] > best_stats[2]:
                        best_stats = stats
                        best_lr = lr
                    temp_d[model] = (best_lr, best_stats)
            best_result_per_model.append(temp_d)

        baseline_models = dfs[0].model.unique()
        model_map = defaultdict(list)
        to_remove = set()
        for i in range(0, len(dfs)):  # Yes, also include baseline models mapping onto themselves
            uq_models = list(dfs[i].model.unique())
            for model in baseline_models:
                b_found = False
                for m in uq_models:
                    if model in m:
                        model_map[model].append(m)
                        b_found = True
                        break
                if not b_found:
                    model_map[model].append(None)  # We only get here if no match was found
                    to_remove.add(model)

        for k in to_remove:
            del model_map[k]

        # Adapted from https://matplotlib.org/stable/gallery/units/bar_unit_demo.html?highlight=barchart
        N = len(model_map)
        fig, axes = plt.subplots(nrows=2, ncols=1, sharex=True, figsize=(18, 9))
        ax_train, ax_test = axes[0], axes[1]

        ind = np.arange(N)  # the x locations for the groups
        width = 0.1  # the width of the bars

        # plt.suptitle(f'Best {target} performance per model\nLoss = {loss}')

        # This list will contain the data we want to plot, grouped per model group, i.e., 'baseline', 'Exp2...1a', etc.
        per_group = []

        # First, we add the baseline data
        # Cycle over "vgg16", "vgg19", etc.
        for i in range(len(best_result_per_model)):
            train_variant_avgs, train_variant_stds = [], []
            test_variant_avgs, test_variant_stds = [], []
            for model in sorted(model_map.keys()):
                train_variant_avgs.append(best_result_per_model[i][model_map[model][i]][1][0])
                train_variant_stds.append(best_result_per_model[i][model_map[model][i]][1][1])
                test_variant_avgs.append(best_result_per_model[i][model_map[model][i]][1][2])
                test_variant_stds.append(best_result_per_model[i][model_map[model][i]][1][3])
            per_group.append((train_variant_avgs, train_variant_stds, test_variant_avgs, test_variant_stds))
        nb_groups = len(per_group)

        model_variants = [e[1] for e in res_files]
        bar_colors = ['c', 'tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple', 'tab:brown']
        for idx, tpl in enumerate(per_group):
            ax_train.bar(ind + width / 2 + ((-nb_groups//2 + idx) * width), tpl[0], width, bottom=0, yerr=tpl[1],
                         label=f"{model_variants[idx]}", color=bar_colors[idx])
            ax_test.bar(ind + width / 2 + ((-nb_groups//2 + idx) * width), tpl[2], width, bottom=0, yerr=tpl[3],
                        label=f"{model_variants[idx]}", color=bar_colors[idx])

        plt.xticks(ind, labels=sorted(model_map.keys()), rotation=60)

        for grp_idx in range(nb_groups):
            lr_labels = [best_result_per_model[grp_idx][model_map[model][grp_idx]][0] for model in sorted(model_map.keys())]
            ax_train.bar_label(ax_train.containers[1+2*grp_idx], labels=lr_labels, padding=10, rotation='vertical')

        ax_train.legend(loc=2)
        ax_train.set_title("Train")
        ax_train.set_ylabel("Weighted F1")
        ax_train.set_ylim([0., 1.])

        ax_test.legend(loc=2)
        ax_test.set_title("Test")
        ax_test.set_ylabel("Weighted F1")
        ax_test.set_ylim([0., 1.])

        ax_train.grid(visible=True, axis='y')
        ax_test.grid(visible=True, axis='y')

        plt.tight_layout()
        # if out_dir is not None:
        #     file_name = f'baseline_vs_exp2_-_loss={loss}_lr={lr}.png'
        #     plt.savefig(os.path.join(out_dir, file_name))
        #     plt.close()
        # else:
        plt.show()

    @classmethod
    def create_barchart_aro_val_beyond_baseline(cls, res_files: list, target='Arousal', ignore_models=None):
        """
        Create a graph with best results. Plot both Weighted F1 and Average Precision.

        :param res_files: list of tuples (path_to_csv, name)
        :param target: Arousal or Valence
        :param ignore_models: don't plot data for these models
        :return:
        """
        models = ['alexnet', 'vgg16', 'vgg19', 'resnet18', 'resnet50', 'resnet101',
                  'densenet161', 'DINOv2_B', 'CLIP']
        best_results_per_model = [ExtractStats.extract_baseline_class_data(f[0]) for f in res_files]
        # baseline_models = list(best_results_per_model[0].keys())
        baseline_models = models
        if ignore_models is not None:
            for m in ignore_models:
                if m in baseline_models:
                    baseline_models.remove(m)

        model_map = defaultdict(list)
        to_remove = set()
        for i in range(0, len(best_results_per_model)):  # Yes, also include baseline models mapping onto themselves
            uq_models = list(best_results_per_model[i].keys())
            for model in baseline_models:
                b_found = False
                for m in uq_models:
                    if model in m:
                        model_map[model].append(m)
                        b_found = True
                        break
                if not b_found:
                    model_map[model].append(None)  # We only get here if no match was found
                    to_remove.add(model)

        for k in to_remove:
            del model_map[k]

        # Adapted from https://matplotlib.org/stable/gallery/units/bar_unit_demo.html?highlight=barchart
        # models = sorted(model_map.keys())
        N = len(models)

        y_ticks = [0, 0.2, 0.4, 0.6, 0.8, 1.0]
        y_tick_labels = ['0.0', '0.2', '0.4', '0.6', '0.8', '1.0']

        fig, axes = plt.subplots(nrows=2, ncols=1, sharex=True, figsize=(18, 9))
        ax_train, ax_test = axes[0], axes[1]

        ind = np.arange(N)  # the x locations for the groups
        width = 0.07  # the width of the bars

        # plt.suptitle(f'Beyond baseline {target}3 performance', fontsize=20)

        # This list will contain the data we want to plot, grouped per model.
        per_model = []

        # First, add average Weighted F1, then average AP
        for idx_brpm, brpm in enumerate(best_results_per_model):
            for idx in [2, 3]:
                train_avgs, train_stds = [], []
                test_avgs, test_stds = [], []
                for model in models:
                        train_avgs.append(brpm[model_map[model][idx_brpm]][idx][0])
                        train_stds.append(brpm[model_map[model][idx_brpm]][idx][1])
                        test_avgs.append(brpm[model_map[model][idx_brpm]][idx][2])
                        test_stds.append(brpm[model_map[model][idx_brpm]][idx][3])
                per_model.append((train_avgs, train_stds, test_avgs, test_stds))
        nb_groups = len(per_model)

        metrics = []
        for f in res_files:
            metrics += [f'{f[1]} W.F1', f'{f[1]} AP']
        bar_colors = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple', 'tab:brown']
        for idx, tpl in enumerate(per_model):
            if idx % 2 == 0:
                ax_train.bar(ind + width / 2 + ((-nb_groups//2 + idx) * width), tpl[0], width, bottom=0, yerr=tpl[1],
                             label=f"{metrics[idx]}", color=bar_colors[idx//2])
                ax_test.bar(ind + width / 2 + ((-nb_groups//2 + idx) * width), tpl[2], width, bottom=0, yerr=tpl[3],
                            label=f"{metrics[idx]}", color=bar_colors[idx//2])
            else:
                ax_train.bar(ind + width / 2 + ((-nb_groups//2 + idx) * width), tpl[0], width, bottom=0, yerr=tpl[1],
                             label=f"{metrics[idx]}", facecolor=(bar_colors[idx//2],0.25), edgecolor=bar_colors[idx//2],
                             linewidth=1.5)
                ax_test.bar(ind + width / 2 + ((-nb_groups//2 + idx) * width), tpl[2], width, bottom=0, yerr=tpl[3],
                            label=f"{metrics[idx]}", facecolor=(bar_colors[idx//2],0.25), edgecolor=bar_colors[idx//2],
                             linewidth=1.5)

        # ax_train.bar(ind, per_model_train_y, width, bottom=0, yerr=per_model_train_y_err)
        # ax_test.bar(ind, per_model_test_y, width, bottom=0, yerr=per_model_test_y_err)
        plt.xticks(ind, labels=models, rotation=60, fontsize=15)

        for idx_brpm, brpm in enumerate(best_results_per_model):
            lr_labels = [f'{brpm[model_map[model][idx_brpm]][1]}' for model in models]
            ax_test.bar_label(ax_test.containers[1+(4*idx_brpm)], labels=lr_labels,
                              padding=10, rotation='vertical', fontsize=12)

        ax_train.legend(loc=2, ncols=2, fontsize=12)
        ax_train.set_title("Train")
        ax_train.set_ylabel("Weighted F1/Average Precision", fontsize=15)
        ax_train.set_ylim([0., 1.])
        ax_train.set_yticks(ticks=y_ticks, labels=y_tick_labels, fontsize=12)

        # ax_test.legend(loc=2)
        ax_test.set_title("Test")
        ax_test.set_ylabel("Weighted F1/Average Precisions", fontsize=15)
        ax_test.set_ylim([0., 1.])
        ax_test.set_yticks(ticks=y_ticks, labels=y_tick_labels, fontsize=12)

        ax_train.grid(visible=True, axis='y')
        ax_test.grid(visible=True, axis='y')

        plt.tight_layout()
        plt.show()

    @classmethod
    def create_barchart_aro_val_reg_beyond_baseline(cls, res_files: list, target='Arousal', ignore_models=None):
        """
        Create a graph with best results. Plot both MAE and Spearman R.

        :param res_files: list of tuples (path_to_csv, name)
        :param target: Arousal or Valence
        :param ignore_models: don't plot data for these models
        :return:
        """
        models = ['alexnet', 'vgg16', 'vgg19', 'resnet18', 'resnet50', 'resnet101',
                  'densenet161', 'DINOv2_B', 'CLIP']
        model_names =  ['alexnet', 'vgg16', 'vgg19', 'resnet18', 'resnet50', 'resnet101',
                  'densenet161', 'DINOv2', 'CLIP']
        best_results_per_model = [ExtractStats.extract_baseline_reg_data(f[0]) for f in res_files]
        # baseline_models = list(best_results_per_model[0].keys())
        if ignore_models is not None:
            for m in ignore_models:
                try:
                    idx_m = models.index(m)
                    models.remove(models[idx_m])
                    model_names.remove(model_names[idx_m])
                except ValueError:
                    continue

        model_map = defaultdict(list)
        to_remove = set()
        for i in range(0, len(best_results_per_model)):  # Yes, also include baseline models mapping onto themselves
            uq_models = list(best_results_per_model[i].keys())
            for model in models:
                b_found = False
                for m in uq_models:
                    if model in m:
                        model_map[model].append(m)
                        b_found = True
                        break
                if not b_found:
                    model_map[model].append(None)  # We only get here if no match was found
                    to_remove.add(model)

        for k in to_remove:
            del model_map[k]

        # Adapted from https://matplotlib.org/stable/gallery/units/bar_unit_demo.html?highlight=barchart
        # models = sorted(model_map.keys())
        N = len(models)

        y_ticks = [0, 0.4, 0.8, 1.2, 1.6, 2.0]
        y_tick_labels = ['0.0', '0.4', '0.8', '1.2', '1.6', '2.0']

        fig, axes = plt.subplots(nrows=2, ncols=1, sharex=True, figsize=(18, 9))
        ax_train_class, ax_test_class = axes[0], axes[1]
        ax_train_reg, ax_test_reg = ax_train_class.twinx(), ax_test_class.twinx()

        ax_train_class.grid(visible=True, axis='y', zorder=1)
        ax_test_class.grid(visible=True, axis='y', zorder=1)

        ind = np.arange(N)  # the x locations for the groups
        width = 0.07  # the width of the bars

        # plt.suptitle(f'Beyond baseline {target}3 performance', fontsize=20)

        # This list will contain the data we want to plot, grouped per model.
        per_model = []

        # First, add average Weighted F1, then average AP
        for idx_brpm, brpm in enumerate(best_results_per_model):
            for idx in [3, 4]:
                train_avgs, train_stds = [], []
                test_avgs, test_stds = [], []
                for model in models:
                        train_avgs.append(brpm[model_map[model][idx_brpm]][idx][0])
                        train_stds.append(brpm[model_map[model][idx_brpm]][idx][1])
                        test_avgs.append(brpm[model_map[model][idx_brpm]][idx][2])
                        test_stds.append(brpm[model_map[model][idx_brpm]][idx][3])
                per_model.append((train_avgs, train_stds, test_avgs, test_stds))
        nb_groups = len(per_model)

        metrics = []
        for f in res_files:
            metrics += [f'{f[1]} MAE', f'{f[1]} S.R']

        bar_colors = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple', 'tab:brown']
        # Hard-code the "transparent" colors, so that the grid bars can be hidden behind the bars
        bar_colors_alpha = [(0.776, 0.863, 0.925), (1., 0.871, 0.761), (0.792, 0.906, 0.792),
                            (0.957, 0.784, 0.788), (0.894, 0.847, 0.933), (0.886, 0.831, 0.820)]

        for idx, tpl in enumerate(per_model):
            ax_train = ax_train_class if idx in {0, 2, 4, 6} else ax_train_reg
            ax_test = ax_test_class if idx in {0, 2, 4, 6} else ax_test_reg
            if idx % 2 == 0:
                ax_train.bar(ind + width / 2 + ((-nb_groups//2 + idx) * width), tpl[0], width, bottom=0, yerr=tpl[1],
                             label=f"{metrics[idx]}", color=bar_colors[idx//2], zorder=2)
                ax_test.bar(ind + width / 2 + ((-nb_groups//2 + idx) * width), tpl[2], width, bottom=0, yerr=tpl[3],
                            label=f"{metrics[idx]}", color=bar_colors[idx//2], zorder=2)
            else:
                ax_train.bar(ind + width / 2 + ((-nb_groups//2 + idx) * width), tpl[0], width, bottom=0, yerr=tpl[1],
                             label=f"{metrics[idx]}", facecolor=bar_colors_alpha[idx//2], edgecolor=bar_colors[idx//2],
                             linewidth=1.5, zorder=2)
                ax_test.bar(ind + width / 2 + ((-nb_groups//2 + idx) * width), tpl[2], width, bottom=0, yerr=tpl[3],
                            label=f"{metrics[idx]}", facecolor=bar_colors_alpha[idx//2], edgecolor=bar_colors[idx//2],
                             linewidth=1.5, zorder=2)

        # ax_train.bar(ind, per_model_train_y, width, bottom=0, yerr=per_model_train_y_err)
        # ax_test.bar(ind, per_model_test_y, width, bottom=0, yerr=per_model_test_y_err)
        ax_test_class.set_xticks(ind, labels=model_names, rotation=45, fontsize=15, ha='right')

        for idx_brpm, brpm in enumerate(best_results_per_model):
            # ax = ax_class  # if idx == 0 else ax_reg
            # lr_labels = [(f'{best_result_per_model[model][1]},'
            #               f' {cls.LOSS_MAP[best_result_per_model[model][0]]}') for model in models]
            # text = ax.bar_label(ax.containers[3+(2*idx)], labels=lr_labels,
            #                     padding=10, rotation='vertical', fontsize=10)
            lr_labels = [f'{brpm[model_map[model][idx_brpm]][1]}' for model in models]
            text = ax_test_class.bar_label(ax_test_class.containers[1 + 2*idx_brpm], labels=lr_labels,
                              padding=10, rotation='vertical', fontsize=12)
            for t in text:
                t.xy = (t.xy[0]+0.006, t.xy[1])

        lines, labels = ax_train_class.get_legend_handles_labels()
        lines2, labels2 = ax_train_reg.get_legend_handles_labels()
        ax_train_class.legend(cls._flatten_list([[lines[i]] + [lines2[i]] for i in range(len(lines))]),
                              cls._flatten_list([[labels[i]] + [labels2[i]] for i in range(len(labels))]),
                              loc=1, ncols=4, fontsize=12)
        # ax_train_class.legend(loc=1, ncols=2, fontsize=12)

        ax_train_class.set_title("Train")
        ax_train_class.set_ylabel("MAE", fontsize=15)
        ax_train_reg.set_ylabel("S.R", fontsize=15)
        ax_train_class.set_ylim([0., 2.])
        ax_train_class.set_yticks(ticks=y_ticks, labels=y_tick_labels, fontsize=12)
        ax_train_reg.set_ylim([0., 1.])
        ax_train_reg.set_yticks(ticks=np.arange(0, 1.1, 0.2), labels=[f'{x:.2f}' for x in np.arange(0, 1.1, 0.2)], fontsize=12)

        ax_test_class.set_title("Test")
        ax_test_class.set_ylabel("MAE", fontsize=15)
        ax_test_reg.set_ylabel('S.R', fontsize=15)
        ax_test_class.set_ylim([0., 2.])
        ax_test_class.set_yticks(ticks=y_ticks, labels=y_tick_labels, fontsize=12)
        ax_test_reg.set_ylim([0., 1.])
        ax_test_reg.set_yticks(ticks=np.arange(0, 1.1, 0.2), labels=[f'{x:.2f}' for x in np.arange(0, 1.1, 0.2)], fontsize=12)

        plt.tight_layout()
        plt.show()

    @classmethod
    def _flatten_list(cls, list_of_lists):
        flat_list = []
        for row in list_of_lists:
            flat_list += row
        return flat_list


if __name__ == '__main__':
    if False:
        CreateGraphs.create_annotations_emo8_stats_plot(ann_file=Config.FILE_ANN_SINGLE, b_filter=False)

    if False:
        CreateGraphs.create_emo8_piechart(ann_file=Config.FILE_ANN_SINGLE)

    if False:
        CreateGraphs.create_valence_arousal_corr_plt(ann_file=Config.FILE_ANN_SINGLE, b_filter=False)

    if False:
        CreateGraphs.create_hist_aro_val_amb(ann_file=Config.FILE_ANN_SINGLE)

    # Emo8 baseline
    if False:
        res_file_ce = os.path.join(Config.DIR_DATA, 'Baseline Emo8 Loss=CE', 'training_emo8_results.csv')
        res_file_uce = os.path.join(Config.DIR_DATA, 'Baseline Emo8 Loss=UCE', 'training_emo8_results.csv')
        CreateGraphs.create_barchart_emo8_baseline(res_file_ce=res_file_ce, res_file_uce=res_file_uce,
                                                   ignore_models=['googlenet', 'inception_v3',
                                                                  'alexnet_cc', 'vgg16_cc', 'resnet18_cc']
                                                   )

    if False:
    # Arousal/Valence regression baseline
        aro_res_file_mse = os.path.join(Config.DIR_DATA, 'Baseline Arousal Reg Loss=MSE', 'training_arousal1_results.csv')
        val_res_file_mse = os.path.join(Config.DIR_DATA, 'Baseline Valence Reg Loss=MSE', 'training_valence1_results.csv')
        aro_res_file_wmse = os.path.join(Config.DIR_DATA, 'Baseline Arousal Reg Loss=WeightedMSE', 'training_arousal1_results.csv')
        val_res_file_wmse = os.path.join(Config.DIR_DATA, 'Baseline Valence Reg Loss=WeightedMSE', 'training_valence1_results.csv')
        CreateGraphs.create_barchart_aro_val_regression(res_file_aro_mse=aro_res_file_mse, res_file_val_mse=val_res_file_mse,
                                                        res_file_aro_wmse=aro_res_file_wmse, res_file_val_wmse=val_res_file_wmse,
                                                        ignore_models=['googlenet', 'inception_v3',
                                                                       'alexnet_cc', 'vgg16_cc', 'resnet18_cc'])

    # Emo8/Arousal/Valence baseline - all classification problems
    if False:
        emo8_res_file_ce = os.path.join(Config.DIR_DATA, 'Baseline Emo8 Loss=CE', 'training_emo8_results.csv')
        emo8_res_file_uce = os.path.join(Config.DIR_DATA, 'Baseline Emo8 Loss=UCE', 'training_emo8_results.csv')
        aro_res_file_ce = os.path.join(Config.DIR_DATA, 'Baseline Arousal Loss=CE', 'training_arousal3_results.csv')
        aro_res_file_uce = os.path.join(Config.DIR_DATA, 'Baseline Arousal Loss=UCE', 'training_arousal3_results.csv')
        val_res_file_ce = os.path.join(Config.DIR_DATA, 'Baseline Valence Loss=CE', 'training_valence3_results.csv')
        val_res_file_uce = os.path.join(Config.DIR_DATA, 'Baseline Valence Loss=UCE', 'training_valence3_results.csv')
        CreateGraphs.create_barchart_all_baseline_class(res_file_emo8_ce=emo8_res_file_ce, res_file_emo8_uce=emo8_res_file_uce,
                                                        res_file_aro_ce=aro_res_file_ce, res_file_aro_uce=aro_res_file_uce,
                                                        res_file_val_ce=val_res_file_ce, res_file_val_uce=val_res_file_uce,
                                                        ignore_models=['googlenet', 'inception_v3',
                                                                 'alexnet_cc', 'vgg16_cc', 'resnet18_cc'])

    # Emo8/Arousal/Valence baseline - Emo8=classification, Arousal/Valence = regression
    if False:
        emo8_res_file_ce = os.path.join(Config.DIR_DATA, 'Baseline Emo8 Loss=CE', 'training_emo8_results.csv')
        emo8_res_file_uce = os.path.join(Config.DIR_DATA, 'Baseline Emo8 Loss=UCE', 'training_emo8_results.csv')
        aro_res_file_mse = os.path.join(Config.DIR_DATA, 'Baseline Arousal Reg Loss=MSE', 'training_arousal1_results.csv')
        val_res_file_mse = os.path.join(Config.DIR_DATA, 'Baseline Valence Reg Loss=MSE', 'training_valence1_results.csv')
        aro_res_file_wmse = os.path.join(Config.DIR_DATA, 'Baseline Arousal Reg Loss=WeightedMSE', 'training_arousal1_results.csv')
        val_res_file_wmse = os.path.join(Config.DIR_DATA, 'Baseline Valence Reg Loss=WeightedMSE', 'training_valence1_results.csv')
        CreateGraphs.create_barchart_all_baseline_class_reg(
            res_file_emo8_ce=emo8_res_file_ce, res_file_emo8_uce=emo8_res_file_uce,
            res_file_aro_mse=aro_res_file_mse, res_file_aro_wmse=aro_res_file_wmse,
            res_file_val_mse=val_res_file_mse, res_file_val_wmse=val_res_file_wmse,
            ignore_models=['googlenet', 'inception_v3',
                           'alexnet_cc', 'vgg16_cc', 'resnet18_cc',
                           'vgg19', 'resnet34', 'densenet121', 'DINOv2_1'],
            b_save=True,
            out_dir='Path/To/Output/Directory')

    # Emo8 beyond baseline
    if False:
        res_filename = 'training_emo8_results.csv'
        # First entry should be baseline
        _res_files = [
            # (os.path.join(Config.DIR_DATA, 'Baseline Emo8 Loss=UCE', res_filename), 'imagenet'),
            (os.path.join(Config.DIR_DATA, 'Baseline Emo8 Loss=CE', res_filename), 'Baseline'),
            # (os.path.join(Config.DIR_DATA, 'ExpLogs', '2x_ImageNetPred Loss=UCE', res_filename), '2ximagenet'),
            # (os.path.join(Config.DIR_DATA, 'Buffered-Emo8+Aro3+Val3 Emo8 Loss=CE', res_filename), 'emo8+aro3+val3'),
            (os.path.join(Config.DIR_DATA, 'Baseline+EmoNet Emo8 Loss=CE', res_filename), 'Emo8+EmoNet'),
            (os.path.join(Config.DIR_DATA, 'Baseline+OIToFER Emo8 Loss=CE', res_filename), 'Emo8+OIToFER'),
            (os.path.join(Config.DIR_DATA, 'Baseline+Places365 Feats Emo8 Loss=CE', res_filename), 'Baseline(f)+P365(f)'),
            # (os.path.join(Config.DIR_DATA, 'Baseline+Places365 Feats 2Lyrs Emo8 Loss=CE', res_filename), 'imagenet(f)+p365(f) 2Lyrs'),
            (os.path.join(Config.DIR_DATA, 'Baseline+Places365 Preds Emo8 Loss=CE', res_filename), 'Emo8+P365'),
            (os.path.join(Config.DIR_DATA, 'Baseline+EmoNet+OIToFER Emo8 Loss=CE', res_filename), 'Emo8+EmoNet+OIToFER')
                     ]
        # CreateGraphs.create_barchart_f1only_emo8_beyond_baseline(res_files=_res_files, loss='CrossEntropyLoss',
        #                                                   ignore_models=['googlenet', 'inception_v3'])
        CreateGraphs.create_barchart_emo8_beyond_baseline(res_files=_res_files,
                                                          ignore_models=['googlenet', 'inception_v3', 'resnet34',
                                                                         'vgg19', 'densenet121'])

    # Arousal/Valence classification beyond baseline
    if False:
        res_filename = 'training_arousal3_results.csv'
        # First entry should be baseline
        _res_files = [(os.path.join(Config.DIR_DATA, 'Baseline Arousal Loss=CE', res_filename), 'Baseline'),
                      (os.path.join(Config.DIR_DATA, 'Buffered Emo8 Arousal Loss=CE', res_filename), 'Emo8'),
                      (os.path.join(Config.DIR_DATA, 'Baseline+OIToFER Arousal NEW Loss=CE', res_filename), 'Arousal3+OIToFER'),
                      (os.path.join(Config.DIR_DATA, 'Baseline+EmoNet Arousal NEW Loss=CE', res_filename),
                       'Arousal3+EmoNet'),
                      (os.path.join(Config.DIR_DATA, 'Baseline+IN-Emo8+OIToFER Arousal Loss=CE', res_filename),
                       'Arousal3+Emo8+OIToFER')
                      # (os.path.join(Config.DIR_DATA, 'Baseline+OIToFER Arousal Loss=CE', res_filename), 'imagenet+oitofer'),
                      # (os.path.join(Config.DIR_DATA, 'Baseline+EmoNet Arousal Loss=CE', res_filename), 'imagenet+emonet'),
                      # (os.path.join(Config.DIR_DATA, 'Baseline+EmoNet+OIToFER Arousal Loss=CE', res_filename), 'imagenet+emonet+oitofer'),
                      # (os.path.join(Config.DIR_DATA, 'Baseline+Places365 Feats Arousal Loss=CE', res_filename), 'imagenet(f)+p365(f)')
                      # (os.path.join(Config.DIR_DATA, 'Baseline+Places365 Feats 2Lyrs Emo8', res_filename), 'imagenet(f)+p365(f) 2Lyrs'),
                      # (os.path.join(Config.DIR_DATA, 'Baseline+Places365 Preds Emo8', res_filename), 'imagenet+p365'),
                      # (os.path.join(Config.DIR_DATA, 'Baseline+OIToFER+Places365 Emo8', res_filename), 'imagenet+oitofer+p365')]
                     ]
        # CreateGraphs.create_barchart_f1only_aro_val_beyond_baseline(res_files=_res_files, loss='CrossEntropyLoss', target='Arousal')
        CreateGraphs.create_barchart_aro_val_beyond_baseline(res_files=_res_files, target='Arousal')

        res_filename = 'training_valence3_results.csv'
        # First entry should be baseline
        _res_files = [(os.path.join(Config.DIR_DATA, 'Baseline Valence Loss=CE', res_filename), 'Baseline'),
                      (os.path.join(Config.DIR_DATA, 'Buffered Emo8 Valence Loss=CE', res_filename), 'Emo8'),
                      (os.path.join(Config.DIR_DATA, 'Baseline+OIToFER Valence NEW Loss=CE', res_filename), 'Valence3+OIToFER'),
                      (os.path.join(Config.DIR_DATA, 'Baseline+EmoNet Valence NEW Loss=CE', res_filename),
                       'Valence3+EmoNet'),
                      (os.path.join(Config.DIR_DATA, 'Baseline+IN-Emo8+OIToFER Valence Loss=CE', res_filename),
                       'Valence3+Emo8+OIToFER'),
                      #               (os.path.join(Config.DIR_DATA, 'Baseline+OIToFER Valence Loss=CE', res_filename), 'imagenet+oitofer'),
        #               (os.path.join(Config.DIR_DATA, 'Baseline+EmoNet Valence Loss=CE', res_filename), 'imagenet+emonet'),
        #               (os.path.join(Config.DIR_DATA, 'Baseline+EmoNet+OIToFER Valence Loss=CE', res_filename), 'imagenet+emonet+oitofer'),
        #               (os.path.join(Config.DIR_DATA, 'Baseline+Places365 Feats Valence Loss=CE', res_filename), 'imagenet(f)+p365(f)')
        #               # (os.path.join(Config.DIR_DATA, 'Baseline+Places365 Feats 2Lyrs Emo8', res_filename), 'imagenet(f)+p365(f) 2Lyrs'),
        #               # (os.path.join(Config.DIR_DATA, 'Baseline+Places365 Preds Emo8', res_filename), 'imagenet+p365'),
        #               # (os.path.join(Config.DIR_DATA, 'Baseline+OIToFER+Places365 Emo8', res_filename), 'imagenet+oitofer+p365')]
                     ]
        # CreateGraphs.create_barchart_f1only_aro_val_beyond_baseline(res_files=_res_files, loss='CrossEntropyLoss', target='Valence')
        CreateGraphs.create_barchart_aro_val_beyond_baseline(res_files=_res_files, target='Valence',
                                                            ignore_models=['googlenet', 'inception_v3'])

    # Arousal/Valence regression beyond baseline
    if False:
        res_filename = 'training_arousal1_results.csv'
        # First entry should be baseline
        _res_files = [(os.path.join(Config.DIR_DATA, 'Baseline Arousal Reg Loss=MSE', res_filename), 'Baseline'),
                      (os.path.join(Config.DIR_DATA, 'Baseline+Emo8 Arousal Reg Loss=MSE', res_filename), 'Aro1+Emo8'),
                      (os.path.join(Config.DIR_DATA, 'Baseline+OIToFER Arousal Reg Loss=MSE', res_filename), 'Aro1+OIToFER'),
                      (os.path.join(Config.DIR_DATA, 'Baseline+EmoNet Arousal Reg Loss=MSE', res_filename), 'Aro1+EmoNet'),
                      # (os.path.join(Config.DIR_DATA, 'FindingEmo Paper', 'Baseline+IN-Emo8+OIToFER Arousal Loss=CE', res_filename),
                      #  'Arousal3+Emo8+OIToFER')
                      # (os.path.join(Config.DIR_DATA, 'Baseline+OIToFER Arousal Loss=CE', res_filename), 'imagenet+oitofer'),
                      # (os.path.join(Config.DIR_DATA, 'Baseline+EmoNet Arousal Loss=CE', res_filename), 'imagenet+emonet'),
                      # (os.path.join(Config.DIR_DATA, 'Baseline+EmoNet+OIToFER Arousal Loss=CE', res_filename), 'imagenet+emonet+oitofer'),
                      # (os.path.join(Config.DIR_DATA, 'Baseline+Places365 Feats Arousal Loss=CE', res_filename), 'imagenet(f)+p365(f)')
                      # (os.path.join(Config.DIR_DATA, 'Baseline+Places365 Feats 2Lyrs Emo8', res_filename), 'imagenet(f)+p365(f) 2Lyrs'),
                      # (os.path.join(Config.DIR_DATA, 'Baseline+Places365 Preds Emo8', res_filename), 'imagenet+p365'),
                      # (os.path.join(Config.DIR_DATA, 'Baseline+OIToFER+Places365 Emo8', res_filename), 'imagenet+oitofer+p365')]
                     ]
        # CreateGraphs.create_barchart_f1only_aro_val_beyond_baseline(res_files=_res_files, loss='CrossEntropyLoss', target='Arousal')
        CreateGraphs.create_barchart_aro_val_reg_beyond_baseline(res_files=_res_files, target='Arousal',
                                                                 ignore_models=['googlenet', 'inception_v3',
                                                                                'vgg19', 'resnet34', 'densenet121'])

        res_filename = 'training_valence1_results.csv'
        # First entry should be baseline
        _res_files = [(os.path.join(Config.DIR_DATA, 'Baseline Valence Reg Loss=MSE', res_filename), 'Baseline'),
                      (os.path.join(Config.DIR_DATA, 'Baseline+Emo8 Valence Reg Loss=MSE', res_filename), 'Val1+Emo8'),
                      (os.path.join(Config.DIR_DATA, 'Baseline+OIToFER Valence Reg Loss=MSE', res_filename), 'Val1+OIToFER'),
                      (os.path.join(Config.DIR_DATA, 'Baseline+EmoNet Valence Reg Loss=MSE', res_filename), 'Val1+EmoNet'),
                      # (os.path.join(Config.DIR_DATA, 'Baseline+IN-Emo8+OIToFER Valence Loss=CE', res_filename),
                      #  'Valence3+Emo8+OIToFER'),
                      #               (os.path.join(Config.DIR_DATA, 'Baseline+OIToFER Valence Loss=CE', res_filename), 'imagenet+oitofer'),
        #               (os.path.join(Config.DIR_DATA, 'Baseline+EmoNet Valence Loss=CE', res_filename), 'imagenet+emonet'),
        #               (os.path.join(Config.DIR_DATA, 'Baseline+EmoNet+OIToFER Valence Loss=CE', res_filename), 'imagenet+emonet+oitofer'),
        #               (os.path.join(Config.DIR_DATA, 'Baseline+Places365 Feats Valence Loss=CE', res_filename), 'imagenet(f)+p365(f)')
        #               # (os.path.join(Config.DIR_DATA, 'Baseline+Places365 Feats 2Lyrs Emo8', res_filename), 'imagenet(f)+p365(f) 2Lyrs'),
        #               # (os.path.join(Config.DIR_DATA, 'Baseline+Places365 Preds Emo8', res_filename), 'imagenet+p365'),
        #               # (os.path.join(Config.DIR_DATA, 'Baseline+OIToFER+Places365 Emo8', res_filename), 'imagenet+oitofer+p365')]
                     ]
        # CreateGraphs.create_barchart_f1only_aro_val_beyond_baseline(res_files=_res_files, loss='CrossEntropyLoss', target='Valence')
        CreateGraphs.create_barchart_aro_val_reg_beyond_baseline(res_files=_res_files, target='Valence',
                                                            ignore_models=['googlenet', 'inception_v3',
                                                                           'vgg19', 'resnet34', 'densenet121'])
