"""
Code used to create the LaTeX tables used in the paper.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import math
import os
from collections import defaultdict

import numpy as np
import pandas as pd

from analysis.analysis_tools import AnalysisTools
from config import Config
from tools.annotation_dataset_tools import AnnotationEmotionMap


class CreateLaTeXTables:
    # ============================================================
    # Create table methods
    # ============================================================
    @classmethod
    def create_baseline_table_main(cls, res_file_emo8_ce: str, res_file_emo8_uce: str,
                                   res_file_aro_mse: str, res_file_aro_wmse: str,
                                   res_file_val_mse: str, res_file_val_wmse: str,
                                   ignore_models=None,
                                   metrics_class=None, metric_labels_class=None,
                                   metrics_reg=None, metric_labels_reg=None):
        """
        Create LaTeX code for table containing the baseline Emo8 classification and Arousal/Valence regression
        performance stats.

        :param res_file_emo8_ce: path to the CrossEntropyLoss results
        :param res_file_emo8_uce: path to the UnbalancedCrossEntropyLoss results
        :param res_file_aro_mse: path to the MSELoss results
        :param res_file_aro_wmse: path to the WeightedMSELoss results
        :param res_file_val_mse: same for valence
        :param res_file_val_wmse: same for valence
        :param ignore_models: optional list of models to ignore
        :param metrics_class: list of classification metrics to be included in the table;
         if None, metric_labels param will be ignored
        :param metric_labels_class: labels by which the classification metrics will be labeled in the table
        :param metrics_reg: list of regression metrics to be included in the table;
         if None, metric_labels param will be ignored
        :param metric_labels_reg: labels by which the regression metrics will be labeled in the table
        :return:
        """
        if metrics_class is None:
            metrics_class = ['file', 'lr', 'test_acc', 'test_f1', 'test_wf1', 'test_ap']
            metric_labels_class = ['Loss', 'Start LR', 'Accuracy', 'F1', 'Weighted F1', 'Avg.Prec.']

        if metrics_reg is None:
            metrics_reg = ['file', 'lr', 'test_mae', 'test_sr']
            metric_labels_reg = ['Loss', 'Start LR', 'MAE', 'Spearman R']

        losses_class = ['CE', 'UCE']
        losses_reg = ['MSE', 'WMSE']
        # models = sorted(dfs[0].model.unique())
        models = ['emonet', 'alexnet', 'alexnet_cc', 'places365-feats_alexnet',
                  'vgg16', 'vgg16_cc', 'vgg19',
                  'resnet18', 'resnet18_cc', 'places365-feats_resnet18', 'resnet34',
                  'resnet50', 'places365-feats_resnet50', 'resnet101',
                  'googlenet', 'inception_v3',
                  'densenet121', 'densenet161', 'places365-feats_densenet161', 'DINOv2_1', 'DINOv2_B', 'CLIP']
        model_names = ['emonet', 'alexnet', 'alexnet cc', 'alexnet p365',
                  'vgg16', 'vgg16 cc', 'vgg19',
                  'resnet18', 'resnet18 cc', 'resnet18 p365', 'resnet34',
                  'resnet50', 'resnet50 p365', 'resnet101',
                  'googlenet', 'inception_v3',
                  'densenet121', 'densenet161', 'densenet161 p365', 'DINOv2 (S)', 'DINOv2', 'CLIP']

        if ignore_models is not None:
            for m in ignore_models:
                try:
                    idx_m = models.index(m)
                    models.remove(models[idx_m])
                    model_names.remove(model_names[idx_m])
                except ValueError:
                    continue

        best_emo8_results = cls.extract_best_results_from_csvs_class(res_file_emo8_ce, res_file_emo8_uce)
        best_aro_results = cls.extract_best_results_from_csvs_reg(res_file_aro_mse, res_file_aro_wmse)
        best_val_results = cls.extract_best_results_from_csvs_reg(res_file_val_mse, res_file_val_wmse)

        label = "tb:baseline_models"
        nb_models = len(model_names)
        columns_after =  {1, 3, 4, 9, 11}  # Where to place column separators; placement AFTER column index, start = 0
        col_def = 'l|'  # leftmost column = metric names
        for i in range(nb_models):
            col_def += 'l'
            if i+1 in columns_after:  # +1, because we need to take into account the leftmost column
                col_def += '|'

        # beg = "\\begin{landscape}\n" \
        beg = "\\begin{table}[!htbp]\n" \
              "\t\\footnotesize\n"\
              "\t\\centering\n" \
              "\t\\caption{Emo8 classification and Arousal/Valence regression performance for baseline models.}\n" \
              "\t\\label{" + label + "}\n" \
              "\t\\begin{tabular}{@{}" + col_def +"@{}}\n"
        end = "\t\\end{tabular}\n" \
              "\\end{table}\n"
              # "\\end{landscape}"

        header = ''
        linebreaks = {len(metric_labels_class)+1, len(metric_labels_class) + len(metric_labels_reg)+2}  # Insert horizontal break after line...; counting from 1
        # blankline = {}
        lines = []
        line_idx = 0
        for idx, target in enumerate(['Emo8', 'Arousal', 'Valence']):
            line_idx += 1
            lines.append(f"\t\t&\\multicolumn{{{nb_models}}}{{c}}{{\emph{{{target}}}}} \\\\")
            metric_labels = metric_labels_class if idx == 0 else metric_labels_reg
            for idx, metric in enumerate(metric_labels):
                line_idx += 1
                lines.append("\t\t" + metric + " &")
                if line_idx in linebreaks:
                    # lines.append("\t\t\\midrule")
                    lines.append(f"\t\t\\multicolumn{{{nb_models+1}}}{{c}}{{\phantom{{-}}}} \\\\")

        for model_idx, model_name in enumerate(model_names):
            h_model_name = model_name  # model name to be used in the header
            if h_model_name.endswith('_cc'):
                parts = h_model_name.split('_')
                h_model_name = f'{parts[1]}_{parts[0]}'
            elif h_model_name.startswith('places365-feats'):
                h_model_name = h_model_name.replace('places365-feats', 'p365')
            header += ' & \\vtext{' + h_model_name.replace('_', '\\_') + '}'
            if model_idx+1 == nb_models:
                header += ' \\\\'

            # Add Emo8 results
            for line_idx, metric in enumerate(metrics_class):
                line_idx = line_idx + 1

                if metric == 'file':
                    lines[line_idx] += (f" {losses_class[best_emo8_results[models[model_idx]][metric]]}")
                elif metric == 'lr':
                    lines[line_idx] += (f" ${best_emo8_results[models[model_idx]][metric]}$")
                else:
                    a = f"{best_emo8_results[models[model_idx]][f'{metric}_avg']:.3f}"
                    b = f"{best_emo8_results[models[model_idx]][f'{metric}_std']:.3f}"
                    lines[line_idx] += (f" ${a[1:]}^{{{b[3:]}}}$")

                if model_idx+1 < nb_models:
                    lines[line_idx] += " &"
                else:
                    lines[line_idx] += " \\\\"

            # Add Arousal/Valence results
            offset = len(metric_labels_class) + 1 + 2
            for res_idx, best_results in enumerate([best_aro_results, best_val_results]):
                for line_idx, metric in enumerate(metrics_reg):
                    line_idx = line_idx + res_idx*len(metrics_reg) + offset + res_idx*2

                    if metric == 'file':
                        lines[line_idx] += (f" {losses_reg[best_results[models[model_idx]][metric]]}")
                    elif metric == 'lr':
                        lines[line_idx] += (f" ${best_results[models[model_idx]][metric]}$")
                    else:
                        a = f"{best_results[models[model_idx]][f'{metric}_avg']:5.3f}"
                        b = f"{best_results[models[model_idx]][f'{metric}_std']:5.3f}"
                        lines[line_idx] += (f" ${a}^{{{b[3:]}}}$")

                    if model_idx+1 < nb_models:
                        lines[line_idx] += " &"
                    else:
                        lines[line_idx] += " \\\\"

        table = beg
        table += header + "\t\t\\midrule\n"
        for l in lines:
            table += l + '\n'
        table += end

        print('\n\n')
        print(table)

    @classmethod
    def create_ce_vs_uce_table(cls, res_file_emo8_ce: str, res_file_emo8_uce: str,
                               res_file_aro_mse: str, res_file_aro_wmse: str,
                               res_file_val_mse: str, res_file_val_wmse: str,
                               ignore_models=None):
        """
        Create LaTeX code for table containing the baseline CrossEntropyLoss vs. UnbalancedCrossEntropyLoss.

        :param res_file_emo8_ce: path to the CrossEntropyLoss results
        :param res_file_emo8_uce: path to the UnbalancedCrossEntropyLoss results
        :param res_file_aro_mse: path to the MSELoss results
        :param res_file_aro_wmse: path to the WeightedMSELoss results
        :param res_file_val_mse: same for valence
        :param res_file_val_wmse: same for valence
        :param ignore_models: optional list of models to ignore
        :return:
        """
        metrics_class = ['lr', 'test_wf1', 'test_ap']
        metric_labels_class = ['Start LR', 'Weighted F1', 'Avg.Prec.']
        metrics_reg = ['lr', 'test_mae', 'test_sr']
        metric_labels_reg = ['Start LR', 'MAE', 'Spearman R']

        losses_class = ['CE', 'UCE']
        losses_reg = ['MSE', 'WMSE']

        # models = sorted(dfs[0].model.unique())
        models = ['emonet', 'alexnet', 'alexnet_cc', 'places365-feats_alexnet',
                  'vgg16', 'vgg16_cc', 'vgg19',
                  'resnet18', 'resnet18_cc', 'places365-feats_resnet18', 'resnet34',
                  'resnet50', 'places365-feats_resnet50', 'resnet101',
                  'googlenet', 'inception_v3',
                  'densenet121', 'densenet161', 'places365-feats_densenet161', 'DINOv2_1', 'CLIP']
        model_names = ['emonet', 'alexnet', 'alexnet cc', 'alexnet p365',
                  'vgg16', 'vgg16 cc', 'vgg19',
                  'resnet18', 'resnet18 cc', 'resnet18 p365', 'resnet34',
                  'resnet50', 'resnet50 p365', 'resnet101',
                  'googlenet', 'inception_v3',
                  'densenet121', 'densenet161', 'densenet161 p365', 'DINOv2', 'CLIP']

        if ignore_models is not None:
            for m in ignore_models:
                try:
                    idx_m = models.index(m)
                    models.remove(models[idx_m])
                    model_names.remove(model_names[idx_m])
                except ValueError:
                    continue

        # best_emo8_results = cls.extract_best_results_from_csvs_class(res_file_emo8_ce, res_file_emo8_uce)
        best_emo8_ce_results = cls.extract_best_results_from_csv_class(res_file_emo8_ce)
        best_emo8_uce_results = cls.extract_best_results_from_csv_class(res_file_emo8_uce)

        # best_aro_results = cls.extract_best_results_from_csvs_reg(res_file_aro_mse, res_file_aro_wmse)
        best_aro_mse_results = cls.extract_best_results_from_csv_reg(res_file_aro_mse)
        best_aro_wmse_results = cls.extract_best_results_from_csv_reg(res_file_aro_wmse)

        # best_val_results = cls.extract_best_results_from_csvs_reg(res_file_val_mse, res_file_val_wmse)
        best_val_mse_results = cls.extract_best_results_from_csv_reg(res_file_val_mse)
        best_val_wmse_results = cls.extract_best_results_from_csv_reg(res_file_val_wmse)

        # list_best_results = [best_emo8_results, best_aro_results, best_val_results]

        label = "tb:baseline_cs_vs_uce"
        nb_models = len(model_names)
        columns_after = {1, 3, 4, 9, 11}  # Where to place column separators; placement AFTER column index, start = 0
        col_def = 'cl|'  # leftmost column = loss, 2nd column = metric names
        for i in range(nb_models):
            col_def += 'l'
            if i+1 in columns_after:  # +1, because we need to take into account the leftmost column
                col_def += '|'

        # beg = "\\begin{landscape}\n" \
        beg = "\\begin{table}[!htbp]\n" \
              "\t\\footnotesize\n"\
              "\t\\centering\n" \
              "\t\\caption{CrossEntropyLoss vs. UnbalancedCrossEntropyLoss: Emo8, Arousal3 and Valence3 classification performance for baseline models.}\n" \
              "\t\\label{" + label + "}\n" \
              "\t\\begin{tabular}{@{}" + col_def +"@{}}\n"
        end = "\t\\end{tabular}\n" \
              "\\end{table}\n"
              # "\\end{landscape}"

        header = '&'
        linebreaks = {4, 11, 18}  # Insert horizontal break after line...; counting from 1
        lines = []
        line_idx = 0
        for idx, target in enumerate(['Emo8', 'Arousal', 'Valence']):
            line_idx += 1
            if line_idx > 1:
                lines.append(f"\t\t\\multicolumn{{{nb_models+2}}}{{c}}{{\phantom{{-}}}} \\\\")
            lines.append(f"\t\t&&\\multicolumn{{{nb_models}}}{{c}}{{\emph{{{target}}}}} \\\\")
            losses = losses_class if idx == 0 else losses_reg
            metric_labels = metric_labels_class if idx == 0 else metric_labels_reg
            for loss in losses:
                for idx, metric in enumerate(metric_labels):
                    line_idx += 1
                    if idx == 0:
                        lines.append(f'\\multirow{{3}}{{*}}{{\\vtext{{{loss}}}}} & ' + metric + ' &')
                    else:
                        lines.append("\t\t& " + metric + " &")
                    if line_idx in linebreaks:
                        lines.append("\t\t\\midrule")
                        # lines.append(f"\t\t\\multicolumn{{{nb_models+2}}}{{c}}{{\phantom{{-}}}} \\\\")

        for model_idx, model_name in enumerate(model_names):
            h_model_name = model_name  # model name to be used in the header
            if h_model_name.endswith('_cc'):
                parts = h_model_name.split('_')
                h_model_name = f'{parts[1]}_{parts[0]}'
            elif h_model_name.startswith('places365-feats'):
                h_model_name = h_model_name.replace('places365-feats', 'p365')
            header += ' & \\vtext{' + h_model_name.replace('_', '\\_') + '}'
            if model_idx+1 == nb_models:
                header += ' \\\\'

            # Add results
            list_results = [best_emo8_ce_results, best_emo8_uce_results,
                            best_aro_mse_results, best_aro_wmse_results,
                            best_val_mse_results, best_val_wmse_results]
            for res_idx, best_results in enumerate(list_results):
                metrics = metrics_class if res_idx < 2 else metrics_reg
                for line_idx, metric in enumerate(metrics):
                    line_idx = 1 + line_idx + res_idx*(len(metrics)) + (res_idx+1//2) + res_idx//2

                    if metric == 'lr':
                        lines[line_idx] += (f" ${best_results[models[model_idx]][metric]}$")
                    else:
                        _avg = best_results[models[model_idx]][f'{metric}_avg']
                        a = f"{_avg:.3f}"
                        b = f"{best_results[models[model_idx]][f'{metric}_std']:.3f}"
                        if metric == 'test_mae':
                            score = f"{a}^{{{b[3:]}}}"
                        else:
                            score = f"{a[1:]}^{{{b[3:]}}}"
                        # Following lines boldface both metrics for a single loss
                        # s = list_best_results[res_idx//2][models[model_idx]]
                        # if res_idx%2 == s['file'] and best_results[models[model_idx]]['lr'] == s['lr']:
                        #     lines[line_idx] += f" $\\mathbf{{{score}}}$"
                        # else:
                        #     lines[line_idx] += f" ${score}$"
                        # Following lines boldface each metric separately by comparing both losses
                        offset = 1 if res_idx % 2 == 0 else -1
                        _other_avg = list_results[res_idx + offset][models[model_idx]][f'{metric}_avg']
                        if metric == 'test_mae':
                            lines[line_idx] += f" $\\mathbf{{{score}}}$" if _other_avg > _avg else f" ${score}$"
                        else:
                            lines[line_idx] += f" $\\mathbf{{{score}}}$" if _other_avg < _avg else f" ${score}$"

                    if model_idx+1 < nb_models:
                        lines[line_idx] += " &"
                    else:
                        lines[line_idx] += " \\\\"

        table = beg
        table += header + "\t\t\n\\midrule\n"
        for l in lines:
            table += l + '\n'
        table += end

        print('\n\n')
        print(table)

    @classmethod
    def create_baseline_vs_oitofer_table(cls, res_file_emo8_bl: str, res_file_emo8_oitofer: str,
                                         res_file_aro_bl: str, res_file_aro_oitofer: str,
                                         res_file_val_bl: str, res_file_val_oitofer: str,
                                         ignore_models=None):
        """
        Create LaTeX code for table containing the baseline Arousal3 and Valence3 results vs.
         UnbalancedCrossEntropyLoss.

        :param res_file_emo8_bl: path to baseline Emo8 results
        :param res_file_emo8_oitofer: path to imagenet+oitofer Emo8 results
        :param res_file_aro_bl: path to baseline Arousal3 results
        :param res_file_aro_oitofer: path to imagenet+oitofer Arousal3 results
        :param res_file_val_bl: same for valence
        :param res_file_val_oitofer: same for valence
        :param ignore_models: optional list of models to ignore
        :return:
        """
        metrics = ['lr', 'test_acc', 'test_f1', 'test_wf1', 'test_ap']
        metric_labels = ['Start LR', 'Accuracy', 'F1','Weighted F1', 'Avg.Prec.']

        models = ['alexnet', 'vgg16', 'vgg19',
                  'resnet18', 'resnet34', 'resnet50', 'resnet101',
                  'densenet121', 'densenet161']

        if ignore_models is not None:
            for m in ignore_models:
                try:
                    idx_m = models.index(m)
                    models.remove(models[idx_m])
                except ValueError:
                    continue

        best_emo8_bl_results = cls.extract_best_results_from_csv_class(res_file_emo8_bl)
        best_emo8_oitofer_results = cls.extract_best_results_from_csv_class(res_file_emo8_oitofer)

        best_aro_bl_results = cls.extract_best_results_from_csv_class(res_file_aro_bl)
        best_aro_oitofer_results = cls.extract_best_results_from_csv_class(res_file_aro_oitofer)

        best_val_bl_results = cls.extract_best_results_from_csv_class(res_file_val_bl)
        best_val_oitofer_results = cls.extract_best_results_from_csv_class(res_file_val_oitofer)

        label = "tb:baseline_vs_oitofer_aro_val"
        nb_models = len(models)
        columns_after = {1, 3, 7}  # Where to place column separators; placement AFTER column index, start = 0
        col_def = 'cl|'  # leftmost column = baseline/oitofer, 2nd column = metric names
        for i in range(nb_models):
            col_def += 'l'
            if i + 1 in columns_after:  # +1, because we need to take into account the leftmost column
                col_def += '|'

        # beg = "\\begin{landscape}\n" \
        beg = "\\begin{table}[!htbp]\n" \
              "\t\\footnotesize\n" \
              "\t\\centering\n" \
              "\t\\caption{Baseline vs. +OIToFER: Emo8, Arousal3 and Valence3 classification performance.}\n" \
              "\t\\label{" + label + "}\n" \
                                     "\t\\begin{tabular}{@{}" + col_def + "@{}}\n"
        end = "\t\\end{tabular}\n" \
              "\\end{table}\n"
        # "\\end{landscape}"

        header = '&'
        linebreaks = {6, 17, 28}  # Insert horizontal break after line...; counting from 1
        lines = []
        line_idx = 0
        for target in ['Emo8', 'Arousal3', 'Valence3']:
            line_idx += 1
            if line_idx > 1:
                lines.append(f"\t\t\\multicolumn{{{nb_models+2}}}{{c}}{{\phantom{{-}}}} \\\\")
            lines.append(f"\t\t&&\\multicolumn{{{nb_models}}}{{c}}{{\emph{{{target}}}}} \\\\")
            for results in ['Baseline', '+OIToFER']:
                for idx, metric in enumerate(metric_labels):
                    line_idx += 1
                    if idx == 0:
                        lines.append(f'\\multirow{{5}}{{*}}{{\\vtext{{{results}}}}} & ' + metric + ' &')
                    else:
                        lines.append("\t\t& " + metric + " &")
                    if line_idx in linebreaks:
                        lines.append("\t\t\\midrule")

        model_map = defaultdict(list)
        bl_model_names = best_aro_bl_results.keys()
        for best_res in [best_aro_bl_results, best_aro_oitofer_results]:
            for k in bl_model_names:
                for k2 in best_res.keys():
                    if k in k2:
                        model_map[k].append(k2)
                        break

        for model_idx, model_name in enumerate(models):
            h_model_name = model_name  # model name to be used in the header

            header += ' & \\vtext{' + h_model_name.replace('_', '\\_') + '}'
            if model_idx + 1 == nb_models:
                header += ' \\\\\n'

            # Add results
            for res_idx, best_results in enumerate([best_emo8_bl_results, best_emo8_oitofer_results,
                                                    best_aro_bl_results, best_aro_oitofer_results,
                                                    best_val_bl_results, best_val_oitofer_results]):
                for line_idx, metric in enumerate(metrics):
                    line_idx = 1 + line_idx + (res_idx * len(metrics)) + res_idx + res_idx//2

                    if metric == 'lr':
                        lines[line_idx] += (f" ${best_results[model_map[models[model_idx]][res_idx%2]][metric]}$")
                    else:
                        a = f"{best_results[model_map[models[model_idx]][res_idx%2]][f'{metric}_avg']:.3f}"
                        b = f"{best_results[model_map[models[model_idx]][res_idx%2]][f'{metric}_std']:.3f}"
                        score = f"{a[1:]}^{{{b[3:]}}}"
                        lines[line_idx] += f" ${score}$"

                    if model_idx + 1 < nb_models:
                        lines[line_idx] += " &"
                    else:
                        lines[line_idx] += " \\\\"

        table = beg
        table += header + "\t\t\\midrule\n"
        for l in lines:
            table += l + '\n'
        table += end

        print('\n\n')
        print(table)

    @classmethod
    def create_baseline_vs_oitofer_class_reg_table(cls, res_file_emo8_bl: str, res_file_emo8_oitofer: str,
                                                   res_file_aro_bl: str, res_file_aro_oitofer: str,
                                                   res_file_val_bl: str, res_file_val_oitofer: str,
                                                   ignore_models=None):
        """
        Create LaTeX code for table containing the baseline results vs. +OIToFER results.\
        This table combines Emo8 classification and Arousal/Valence regression results.

        :param res_file_emo8_bl: path to baseline Emo8 results
        :param res_file_emo8_oitofer: path to imagenet+oitofer Emo8 results
        :param res_file_aro_bl: path to baseline Arousal1 results
        :param res_file_aro_oitofer: path to imagenet+oitofer Arousal1 results
        :param res_file_val_bl: same for valence
        :param res_file_val_oitofer: same for valence
        :param ignore_models: optional list of models to ignore
        :return:
        """
        metrics_class = ['lr', 'test_acc', 'test_f1', 'test_wf1', 'test_ap']
        metric_labels_class = ['Start LR', 'Accuracy', 'F1', 'Weighted F1', 'Avg.Prec.']
        metrics_reg = ['lr', 'test_mae', 'test_sr']
        metric_labels_reg = ['Start LR', 'MAE', 'Spearman R']

        losses_class = ['CE', 'UCE']
        losses_reg = ['MSE', 'WMSE']

        models = ['alexnet', 'vgg16', 'vgg19',
                  'resnet18', 'resnet34', 'resnet50', 'resnet101',
                  'densenet121', 'densenet161', 'DINOv2_B', 'CLIP']

        if ignore_models is not None:
            for m in ignore_models:
                try:
                    idx_m = models.index(m)
                    models.remove(models[idx_m])
                except ValueError:
                    continue

        best_emo8_bl_results = cls.extract_best_results_from_csv_class(res_file_emo8_bl)
        best_emo8_oitofer_results = cls.extract_best_results_from_csv_class(res_file_emo8_oitofer)

        best_aro_bl_results = cls.extract_best_results_from_csv_reg(res_file_aro_bl)
        best_aro_oitofer_results = cls.extract_best_results_from_csv_reg(res_file_aro_oitofer)

        best_val_bl_results = cls.extract_best_results_from_csv_reg(res_file_val_bl)
        best_val_oitofer_results = cls.extract_best_results_from_csv_reg(res_file_val_oitofer)

        label = "tb:baseline_vs_oitofer_aro_val"
        nb_models = len(models)
        columns_after = {1, 2, 5, 6}  # Where to place column separators; placement AFTER column index, start = 0
        col_def = 'cl|'  # leftmost column = baseline/oitofer, 2nd column = metric names
        for i in range(nb_models):
            col_def += 'l'
            if i + 1 in columns_after:  # +1, because we need to take into account the leftmost column
                col_def += '|'

        # beg = "\\begin{landscape}\n" \
        beg = "\\begin{table}[!htbp]\n" \
              "\t\\footnotesize\n" \
              "\t\\centering\n" \
              "\t\\caption{Baseline vs. +OIToFER: Emo8 classification and Arousal/Valence regression performance.}\n" \
              "\t\\label{" + label + "}\n" \
                                     "\t\\begin{tabular}{@{}" + col_def + "@{}}\n"
        end = "\t\\end{tabular}\n" \
              "\\end{table}\n"
        # "\\end{landscape}"

        # Preparing all the lines in the table: headers, and start of rows
        header = '&'
        linebreaks = {6, 15, 22}  # Insert horizontal break after line...; counting from 1
        lines = []
        line_idx = 0
        for idx_target, target in enumerate(['Emo8', 'Arousal', 'Valence']):
            line_idx += 1
            if line_idx > 1:
                lines.append(f"\t\t\\multicolumn{{{nb_models+2}}}{{c}}{{\\phantom{{-}}}} \\\\")
            lines.append(f"\t\t&&\\multicolumn{{{nb_models}}}{{c}}{{\\emph{{{target}}}}} \\\\")
            for results in ['Baseline', '+OIToFER']:
                metric_labels = metric_labels_class if idx_target == 0 else metric_labels_reg
                for idx, metric in enumerate(metric_labels):
                    line_idx += 1
                    if idx == 0:
                        lines.append(f'\\multirow{{{len(metric_labels)}}}{{*}}{{\\vtext{{{results}}}}} & ' + metric + ' &')
                    else:
                        lines.append("\t\t& " + metric + " &")
                    if line_idx in linebreaks:
                        lines.append("\t\t\\midrule")

        # Generate map from model names in one csv to other
        # Classification
        model_map_class = defaultdict(list)
        bl_model_names = best_emo8_bl_results.keys()
        for best_res in [best_emo8_bl_results, best_emo8_oitofer_results]:
            for k in bl_model_names:
                best_match = ''
                for k2 in best_res.keys():
                    if k == k2:
                        best_match = k2
                        break
                    elif k in k2:
                        best_match = k2
                if best_match:
                    print(f"Best match: {k} --> {best_match}")
                    model_map_class[k].append(best_match)
                else:
                    print(f"No match found for model {k}")

        # Regression
        model_map_reg = defaultdict(list)
        bl_model_names = best_aro_bl_results.keys()
        for best_res in [best_aro_bl_results, best_aro_oitofer_results]:
            for k in bl_model_names:
                best_match = ''
                for k2 in best_res.keys():
                    if k == k2:
                        best_match = k2
                        break
                    elif k in k2:
                        best_match = k2
                if best_match:
                    print(f"Best match: {k} --> {best_match}")
                    model_map_reg[k].append(best_match)
                else:
                    print(f"No match found for model {k}")

        # Complete the table rows
        for model_idx, model_name in enumerate(models):
            h_model_name = model_name  # model name to be used in the header

            header += ' & \\vtext{' + h_model_name.replace('_', '\\_') + '}'
            if model_idx + 1 == nb_models:
                header += ' \\\\\n'

            # Add results
            offset = 0
            for res_idx, best_results in enumerate([best_emo8_bl_results, best_emo8_oitofer_results,
                                                    best_aro_bl_results, best_aro_oitofer_results,
                                                    best_val_bl_results, best_val_oitofer_results]):
                metrics = metrics_class if res_idx < 2 else metrics_reg
                model_map = model_map_class if res_idx < 2 else model_map_reg
                if 0 < res_idx < 3:
                    offset += len(metrics_class)
                elif res_idx > 2:
                    offset += len(metrics_reg)

                for line_idx, metric in enumerate(metrics):
                    line_idx = 1 + line_idx + offset + 2*(res_idx//2)

                    if metric == 'lr':
                        lines[line_idx] += (f" ${best_results[model_map[models[model_idx]][res_idx%2]][metric]}$")
                    else:
                        a = f"{best_results[model_map[models[model_idx]][res_idx%2]][f'{metric}_avg']:.3f}"
                        b = f"{best_results[model_map[models[model_idx]][res_idx%2]][f'{metric}_std']:.3f}"
                        if metric == 'test_mae':
                            score = f"{a}^{{{b[3:]}}}"
                        else:
                            score = f"{a[1:]}^{{{b[3:]}}}"
                        # score = f"{a[1:]}^{{{b[3:]}}}"
                        lines[line_idx] += f" ${score}$"

                    if model_idx + 1 < nb_models:
                        lines[line_idx] += " &"
                    else:
                        lines[line_idx] += " \\\\"

                    if line_idx in {5, 16, 25}:
                        offset += 1

        table = beg
        table += header + "\t\t\\midrule\n"
        for l in lines:
            table += l + '\n'
        table += end

        print('\n\n')
        print(table)

    @classmethod
    def create_annotations_emo8_stats_table(cls, ann_file: str,
                                            b_remove_incomplete=True, b_use_alt_score=True):
        """

        :param ann_file:
        :return:
        """
        annotations = pd.read_csv(ann_file, header=0, index_col=0)
        annotations.fillna('Undefined', inplace=True)
        annotations = annotations[annotations.reject == 'Keep']

        emo8_anns = dict()
        for emo_idx, emo in enumerate(AnnotationEmotionMap.EMO8_LIST):
            emo_set = set(AnnotationEmotionMap.EMOTION_SETS[emo_idx])
            df_emo = annotations[annotations.emotion.isin(emo_set)]
            emo8_anns[emo] = df_emo

        emo8_stats = defaultdict(dict)
        for emo, emo_anns in emo8_anns.items():
            emo8_stats[emo]['nb_emo_anns'] = len(emo_anns)
            emo8_stats[emo]['avg_arousal'] = np.average(emo_anns.arousal.values)
            emo8_stats[emo]['std_arousal'] = np.std(emo_anns.arousal.values)
            emo8_stats[emo]['avg_valence'] = np.average(emo_anns.valence.values)
            emo8_stats[emo]['std_valence'] = np.std(emo_anns.valence.values)
            emo8_stats[emo]['avg_ambiguity'] = np.average(emo_anns.ambiguity.values)
            emo8_stats[emo]['std_ambiguity'] = np.std(emo_anns.ambiguity.values)
            
            print(f"Emo: {emo}")
            print(f"\tnb: {emo8_stats[emo]['nb_emo_anns']}")
            print(f"\tarousal: {emo8_stats[emo]['avg_arousal']:.3f} +/ {emo8_stats[emo]['std_arousal']:.3f}")
            print(f"\tvalence: {emo8_stats[emo]['avg_valence']:.3f} +/ {emo8_stats[emo]['std_valence']:.3f}")
            print(f"\tambiguity: {emo8_stats[emo]['avg_ambiguity']:.3f} +/ {emo8_stats[emo]['std_ambiguity']:.3f}")

        # Create table proper
        col_def = 'l|llllllll'  # leftmost column = metric names
        label = "tb:emo8_ann_stats"

        # beg = "\\begin{landscape}\n" \
        beg = "\\begin{table*}[!htbp]\n" \
              "\t\\centering\n" \
              "\t\\caption{Average annotation values for arousal, valence and ambiguity per \\red{main} emotion.}\n" \
              "\t\\label{" + label + "}\n" \
              "\t\\begin{tabular}{@{}" + col_def +"@{}}\n"
        end = "\t\\end{tabular}\n" \
              "\\end{table*}\n"
              # "\\end{landscape}"

        header = ''
        linebreaks = {}  # Insert horizontal break after line...; counting from 1
        lines = []
        metrics = ['nb_emo_anns', ('avg_arousal', 'std_valence'),
                   ('avg_valence', 'std_valence'), ('avg_ambiguity', 'std_ambiguity')]
        metric_labels = ['Nb.', 'Arousal', 'Valence', 'Ambiguity']
        line_idx = 0
        for idx, metric in enumerate(metric_labels):
            line_idx += 1
            lines.append("\t\t" + metric + " &")
            if line_idx in linebreaks:
                lines.append("\t\t\\midrule")

        for emo_idx, emo in enumerate(AnnotationEmotionMap.EMO8_LIST):
            line_idx = -1
            header += f' & \\vtext{{{emo}}}'
            if emo_idx+1 == len(AnnotationEmotionMap.EMO8_LIST):
                header += ' \\\\'

            for metric_idx, metric in enumerate(metrics):
                line_idx += 1

                if isinstance(metric, str):
                    lines[line_idx] += (f" \\phantom{{$-$}}${emo8_stats[emo][metric]}$")
                elif isinstance(metric, tuple):
                    a = f"{emo8_stats[emo][metric[0]]:.2f}"
                    b = f"{emo8_stats[emo][metric[1]]:.2f}"
                    lines[line_idx] += ((' ' if emo8_stats[emo][metric[0]] < 0 else ' \\phantom{$-$}')
                                        + f"${a}^{{{b}}}$")

                if emo_idx+1 < len(AnnotationEmotionMap.EMO8_LIST):
                    lines[line_idx] += " &"
                else:
                    lines[line_idx] += " \\\\"

        table = beg
        table += header + "\t\t\\midrule\n"
        for l in lines:
            table += l + '\n'
        table += end

        print('\n\n')
        print(table)

    @classmethod
    def create_annotations_emo24_stats_table(cls, ann_file: str):
        """

        :return:
        """
        annotations = pd.read_csv(ann_file, header=0, index_col=0)
        annotations.fillna('Undefined', inplace=True)
        ann_keep = annotations[annotations.reject == 'Keep']

        nb_anns = len(ann_keep)

        emo24_anns = dict()
        for emo_idx in range(24):
            emo = AnnotationEmotionMap.INV_EMOTION_MAP_EXT[emo_idx]
            df_emo = ann_keep[ann_keep.emotion == emo]
            emo24_anns[emo] = df_emo

        emo24_stats = defaultdict(dict)
        for emo, emo_anns in emo24_anns.items():
            emo24_stats[emo]['nb_emo_anns'] = len(emo_anns)
            emo24_stats[emo]['avg_arousal'] = np.average(emo_anns.arousal.values)
            emo24_stats[emo]['std_arousal'] = np.std(emo_anns.arousal.values)
            emo24_stats[emo]['avg_valence'] = np.average(emo_anns.valence.values)
            emo24_stats[emo]['std_valence'] = np.std(emo_anns.valence.values)
            emo24_stats[emo]['avg_ambiguity'] = np.average(emo_anns.ambiguity.values)
            emo24_stats[emo]['std_ambiguity'] = np.std(emo_anns.ambiguity.values)

            print(f"Emo: {emo}")
            print(f"\tnb: {emo24_stats[emo]['nb_emo_anns']}")
            print(f"\tarousal: {emo24_stats[emo]['avg_arousal']:.3f} +/ {emo24_stats[emo]['std_arousal']:.3f}")
            print(f"\tvalence: {emo24_stats[emo]['avg_valence']:.3f} +/ {emo24_stats[emo]['std_valence']:.3f}")
            print(f"\tambiguity: {emo24_stats[emo]['avg_ambiguity']:.3f} +/ {emo24_stats[emo]['std_ambiguity']:.3f}")

        emotion_rings = []
        for i in range(3):
            ring = []
            for idx_leaf in range(8):
                ring.append(AnnotationEmotionMap.EMOTION_SETS[idx_leaf][i])
            emotion_rings.append(ring)

        # Create table proper
        col_def = 'l|llllllll'  # leftmost column = metric names
        label = "tb:emo24_ann_stats"

        beg = "\\begin{table}[!htb]\n" \
              "\t\\centering\n" \
              "\t\\caption{Average annotation values for arousal, valence and ambiguity per emotion.}\n" \
              "\t\\label{" + label + "}\n" \
              "\t\\begin{tabular}{@{}" + col_def + "@{}}\n"
        end = "\t\\end{tabular}\n" \
              "\\end{table}"

        # header = ''
        linebreaks = {5, 10}  # Insert horizontal break after line...; counting from 1
        lines = []
        metrics = ['', 'nb_emo_anns', ('avg_arousal', 'std_valence'),
                   ('avg_valence', 'std_valence'), ('avg_ambiguity', 'std_ambiguity')]
        metric_labels = ['', 'Nb.', 'Arousal', 'Valence', 'Ambiguity']
        line_idx = 0
        for idx_ring in range(len(emotion_rings)):
            for idx, metric in enumerate(metric_labels):
                line_idx += 1
                lines.append("\t\t" + metric + " &")
                if line_idx in linebreaks:
                    lines.append("\t\t\\midrule")

        for idx_ring, ring in enumerate(emotion_rings):
            for idx_emo, emo in enumerate(ring):
                line_idx = -1 + (idx_ring*len(metrics)) + idx_ring
                # if emo_idx + 1 == len(AnnotationEmotionMap.EMO8_LIST):
                #     header += ' \\\\'

                for metric_idx, metric in enumerate(metrics):
                    line_idx += 1

                    # Emotion
                    if not metric:
                        lines[line_idx] += f' {emo}'
                    # Number of annotations
                    elif isinstance(metric, str):
                        nb = emo24_stats[emo][metric]
                        lines[line_idx] += (f" \\phantom{{$-$}}${nb}$ [{100*nb/nb_anns:.1f}\\%]")
                    # Metrics
                    elif isinstance(metric, tuple):
                        a = f"{emo24_stats[emo][metric[0]]:.2f}"
                        b = f"{emo24_stats[emo][metric[1]]:.2f}"
                        lines[line_idx] += ((' ' if emo24_stats[emo][metric[0]] < 0 else ' \\phantom{$-$}')
                                            + f"${a}^{{{b}}}$")

                    if idx_emo + 1 < len(ring):
                        lines[line_idx] += " &"
                    else:
                        lines[line_idx] += " \\\\"

        table = beg
        # table += header + "\t\t\\midrule\n"
        for l in lines:
            table += l + '\n'
        table += end

        print('\n\n')
        print(table)

    @classmethod
    def create_baseline_emo8_performance_per_class_table(cls, res_file_emo8_ce, res_file_emo8_uce, ignore_models=None):
        """

        :param res_file_emo8_ce:
        :param res_file_emo8_uce:
        :param ignore_models:
        :return:
        """
        res_files = [res_file_emo8_ce, res_file_emo8_uce]
        # Extract best model results
        losses = ['CE', 'UCE']
        loss_map = {'CE': 'CrossEntropyLoss', 'UCE': 'UnbalancedCrossEntropyLoss'}

        models = ['alexnet', 'alexnet_cc', 'places365-feats_alexnet',
                  'vgg16', 'vgg16_cc', 'vgg19',
                  'resnet18', 'resnet18_cc', 'places365-feats_resnet18', 'resnet34',
                  'resnet50', 'places365-feats_resnet50', 'resnet101',
                  'googlenet', 'inception_v3',
                  'densenet121', 'densenet161', 'places365-feats_densenet161', 'DINOv2_1', 'DINOv2_B', 'CLIP']
        model_names = ['alexnet', 'alexnet cc', 'alexnet p365',
                  'vgg16', 'vgg16 cc', 'vgg19',
                  'resnet18', 'resnet18 cc', 'resnet18 p365', 'resnet34',
                  'resnet50', 'resnet50 p365', 'resnet101',
                  'googlenet', 'inception v3',
                  'densenet121', 'densenet161', 'densenet161 p365', 'dino v2 (s)', 'DINOv2', 'CLIP']

        if ignore_models is not None:
            for m in ignore_models:
                try:
                    idx_m = models.index(m)
                    models.remove(models[idx_m])
                    model_names.remove(model_names[idx_m])
                except ValueError:
                    continue

        best_emo8_results = cls.extract_best_results_from_csvs_class(res_file_emo8_ce, res_file_emo8_uce)
        mtxs_ce = AnalysisTools.load_confusion_matrices(file_conf_mtxs=res_file_emo8_ce[:-4] + '_confusion_matrices.txt')
        mtxs_uce = AnalysisTools.load_confusion_matrices(file_conf_mtxs=res_file_emo8_uce[:-4] + '_confusion_matrices.txt')
        mtx_files = [mtxs_ce, mtxs_uce]

        # Extract Rec/Prec/F1 per class tables from log files
        res_per_model = dict()
        for model, results in best_emo8_results.items():
            res_file = res_files[results['file']]
            loss_name = loss_map[losses[results['file']]]
            df = pd.read_csv(res_file)
            sub_df = df[(df.model == model) & (df.lr == results['lr']) & (df.loss == loss_name)]
            processed_data =\
                cls._generate_per_class_stats(sub_df.datetime.values, mtx_files[results['file']])
            res_per_model[model] = processed_data

        for i, indxs in enumerate([(0, 1), (2, 3)]):
            print('#'*120)
            print('#'*120)
            # Make table
            col_def = 'l|llllllll'  # leftmost column = metric names
            label = "tb:baseline_perf_per_class_" + ('train' if i == 0 else 'test')

            # beg = "\\afterpage{\n" \
            #       "\\begin{landscape}\n" \
            beg = "\\begin{table}[!htbp]\n" +\
                  "\t\\footnotesize\n" +\
                  "\t\\centering\n" +\
                  "\t\\caption{" + \
                  ("Train results: " if i == 0 else "Test results: ") +\
                  "Emo8 Recall, Precision and F1 metrics per emotion leaf for baseline models.}\n" +\
                  "\t\\label{" + label + "}\n" +\
                  "\t\\begin{tabular}{@{}" + col_def +"@{}}\n"
            end = "\t\\end{tabular}\n" \
                  "\\end{table}\n"
                  # "\\end{landscape}\n" \
                  # "}"

            header = ''
            linebreaks = {8, 12, 32, 40}  # Insert horizontal break after line...; counting from 1
            lines = []
            line_idx = 0
            # models = models[:8]
            # model_names = model_names[:8]
            metrics = ['rec', 'prec', 'f1']
            metric_labels = ['Recall', 'Precision', 'F1']
            for target in model_names:
                line_idx += 1
                lines.append(f"\t\t& \\multicolumn{{8}}{{c}}{{\emph{{{target}}}}} \\\\")
                for idx, metric in enumerate(metric_labels):
                    line_idx += 1
                    lines.append("\t\t" + metric + " &")
                    if line_idx in linebreaks:
                        lines.append("\t\t\\midrule")

            for idx_model, model in enumerate(models):
                model_res = res_per_model[model]

                for emo_idx, emo in enumerate(AnnotationEmotionMap.EMO8_LIST):
                    line_idx = (idx_model * len(metrics)) + idx_model
                    for l in sorted(linebreaks):
                        if line_idx >= l:
                            line_idx += 1

                    if idx_model == 0:
                        header += f' & {emo}'
                        if emo_idx+1 == len(AnnotationEmotionMap.EMO8_LIST):
                            header += ' \\\\'

                    for metric_idx, metric in enumerate(metrics):
                        line_idx += 1

                        a = f"{model_res[indxs[0]][metric][emo_idx]:.2f}"
                        b = f"{model_res[indxs[1]][metric][emo_idx]:.2f}"
                        lines[line_idx] += f" ${a}^{{{b[1:]}}}$"

                        if emo_idx+1 < len(AnnotationEmotionMap.EMO8_LIST):
                            lines[line_idx] += " &"
                        else:
                            lines[line_idx] += " \\\\"

            table = beg
            table += header + "\n\t\t\\midrule\n"
            for l in lines:
                table += l + '\n'
            table += end

            print('\n\n')
            print(table)

    @classmethod
    def create_baseline_aro_val_performance_per_class_table(cls, res_file_aro3_ce, res_file_aro3_uce,
                                                            res_file_val3_ce, res_file_val3_uce,
                                                            ignore_models=None):
        """

        :param res_file_aro3_ce:
        :param res_file_aro3_uce:
        :param res_file_val3_ce:
        :param res_file_val3_uce:
        :param ignore_models:
        :return:
        """
        res_files_aro3 = [res_file_aro3_ce, res_file_aro3_uce]
        res_files_val3 = [res_file_val3_ce, res_file_val3_uce]
        res_files = [res_files_aro3, res_files_val3]

        # Extract best model results
        losses = ['CE', 'UCE']
        loss_map = {'CE': 'CrossEntropyLoss', 'UCE': 'UnbalancedCrossEntropyLoss'}

        models = ['alexnet', 'alexnet_cc', 'places365-feats_alexnet',
                  'vgg16', 'vgg16_cc', 'vgg19',
                  'resnet18', 'resnet18_cc', 'places365-feats_resnet18', 'resnet34',
                  'resnet50', 'places365-feats_resnet50', 'resnet101',
                  'googlenet', 'inception_v3',
                  'densenet121', 'densenet161', 'places365-feats_densenet161']
        model_names = ['alexnet', 'alexnet cc', 'alexnet p365',
                  'vgg16', 'vgg16 cc', 'vgg19',
                  'resnet18', 'resnet18 cc', 'resnet18 p365', 'resnet34',
                  'resnet50', 'resnet50 p365', 'resnet101',
                  'googlenet', 'inception v3',
                  'densenet121', 'densenet161', 'densenet161 p365']

        if ignore_models is not None:
            for m in ignore_models:
                try:
                    idx_m = models.index(m)
                    models.remove(models[idx_m])
                    model_names.remove(model_names[idx_m])
                except ValueError:
                    continue

        best_aro3_results = cls.extract_best_results_from_csvs_class(res_file_aro3_ce, res_file_aro3_uce)
        best_val3_results = cls.extract_best_results_from_csvs_class(res_file_val3_ce, res_file_val3_uce)

        mtxs_aro3_ce = AnalysisTools.load_confusion_matrices(file_conf_mtxs=res_file_aro3_ce[:-4] + '_confusion_matrices.txt')
        mtxs_aro3_uce = AnalysisTools.load_confusion_matrices(file_conf_mtxs=res_file_aro3_uce[:-4] + '_confusion_matrices.txt')
        mtx_files_aro3 = [mtxs_aro3_ce, mtxs_aro3_uce]
        mtxs_val3_ce = AnalysisTools.load_confusion_matrices(file_conf_mtxs=res_file_val3_ce[:-4] + '_confusion_matrices.txt')
        mtxs_val3_uce = AnalysisTools.load_confusion_matrices(file_conf_mtxs=res_file_val3_uce[:-4] + '_confusion_matrices.txt')
        mtx_files_val3 = [mtxs_val3_ce, mtxs_val3_uce]
        mtx_files = [mtx_files_aro3, mtx_files_val3]

        # Extract Rec/Prec/F1 per class tables from log files
        res_per_model_aro3, res_per_model_val3 = dict(), dict()
        res_per_model_dicts = [res_per_model_aro3, res_per_model_val3]
        for idx_best, best_res in enumerate([best_aro3_results, best_val3_results]):
            for model, results in best_res.items():
                res_file = res_files[idx_best][results['file']]
                loss_name = loss_map[losses[results['file']]]
                df = pd.read_csv(res_file)
                sub_df = df[(df.model == model) & (df.lr == results['lr']) & (df.loss == loss_name)]
                processed_data =\
                    cls._generate_per_class_stats(sub_df.datetime.values, mtx_files[idx_best][results['file']])
                res_per_model_dicts[idx_best][model] = processed_data

        for i, indxs in enumerate([(0, 1), (2, 3)]):
            print('#'*120)
            print('#'*120)
            # Make table
            col_def = 'l|lll|lll'  # leftmost column = metric names
            label = "tb:baseline_aro3_val3_perf_per_class_" + ("train" if i == 0 else "test")

            # beg = "\\afterpage{\n" \
            #       "\\begin{landscape}\n" \
            beg = "\\begin{table}[!htbp]\n" +\
                  "\\footnotesize" +\
                  "\t\\centering\n" +\
                  "\t\\caption{" + \
                  ("Train results: " if i == 0 else "Test results: ") +\
                  "Arousal3 and Valence3 Recall, Precision and F1 metrics per emotion leaf for baseline models.}\n" +\
                  "\t\\label{" + label + "}\n" +\
                  "\t\\begin{tabular}{@{}" + col_def +"@{}}\n"
            end = "\t\\end{tabular}\n" \
                  "\\end{table}\n"
                  # "\\end{landscape}\n" \
                  # "}"

            header = f' & \\multicolumn{{3}}{{c}}{{Arousal}} & \\multicolumn{{3}}{{c}}{{Valence}} \\\\ \n'
            linebreaks = {8, 16, 40}  # Insert horizontal break after line...; counting from 1
            lines = []
            line_idx = 0
            # models = models[:8]
            # model_names = model_names[:8]
            metrics = ['rec', 'prec', 'f1']
            metric_labels = ['Recall', 'Precision', 'F1']
            for target in model_names:
                line_idx += 1
                lines.append(f"\t\t&\\multicolumn{{6}}{{c}}{{\emph{{{target}}}}} \\\\")
                for idx, metric in enumerate(metric_labels):
                    line_idx += 1
                    lines.append("\t\t" + metric + " &")
                    if line_idx in linebreaks:
                        lines.append("\t\t\\midrule")

            for idx_model, model in enumerate(models):
                aro_res = res_per_model_aro3[model]
                val_res = res_per_model_val3[model]

                for col_idx, cols in enumerate([(aro_res, 'Low'), (aro_res, 'Medium'), (aro_res, 'High'),
                                                (val_res, 'Negative'), (val_res, 'Neutral'), (val_res, 'Positive')]):
                    line_idx = (idx_model * len(metrics)) + idx_model
                    for l in sorted(linebreaks):
                        if line_idx >= l:
                            line_idx += 1

                    if idx_model == 0:
                        header += f' & {cols[1]}'
                        if col_idx+1 == 6:
                            header += ' \\\\'

                    for metric_idx, metric in enumerate(metrics):
                        line_idx += 1

                        a = f"{cols[0][indxs[0]][metric][col_idx%3]:.2f}"
                        b = f"{cols[0][indxs[1]][metric][col_idx%3]:.2f}"
                        lines[line_idx] += f" ${a}^{{{b[1:]}}}$"

                        if col_idx+1 < 6:
                            lines[line_idx] += " &"
                        else:
                            lines[line_idx] += " \\\\"

            table = beg
            table += header + "\n\t\t\\midrule\n"
            for l in lines:
                table += l + '\n'
            table += end

            print('\n\n')
            print(table)

    @classmethod
    def _generate_per_class_stats(cls, datetimes, mtx_data):
        """

        :param datetimes: datetime keys to be used in mtx_data dictionary
        :param mtx_data: dictionary containing the confusion matrix/per class stats
        :return: avg_train_data, std_train_data, avg_test_data, std_test_data
        """
        train_data, test_data = mtx_data[datetimes[0]][1], mtx_data[datetimes[0]][3]
        for idx_dt, dt in enumerate(datetimes[1:]):
            data = mtx_data[dt]
            for k in ['prec', 'rec', 'f1']:
                if idx_dt == 0:
                    train_data[k] = np.stack((train_data[k], data[1][k]), axis=0)
                    test_data[k] = np.stack((test_data[k], data[3][k]), axis=0)
                else:
                    train_data[k] = np.concatenate((train_data[k], np.expand_dims(data[1][k], axis=0)),
                                                   axis=0)
                    test_data[k] = np.concatenate((test_data[k], np.expand_dims(data[3][k], axis=0)),
                                                   axis=0)

        avg_train_data, avg_test_data, std_train_data, std_test_data = dict(), dict(), dict(), dict()
        for k in ['prec', 'rec', 'f1']:
            avg_train_data[k] = np.average(train_data[k], axis=0)
            std_train_data[k] = np.std(train_data[k], axis=0)
            avg_test_data[k] = np.average(test_data[k], axis=0)
            std_test_data[k] = np.std(test_data[k], axis=0)

        return avg_train_data, std_train_data, avg_test_data, std_test_data

    # ============================================================
    # Helper methods
    # ============================================================
    @classmethod
    def extract_best_results_from_csv_class(cls, csv_file: str, b_silent=True):
        """
        Extract best results from CSV file containing classification results.

        :param csv_file:
        :param b_silent: print best model stats or not.
        :return:
        """
        csv = pd.read_csv(csv_file)
        # For each model architecture, we want to extract the average/std performance metrics for the
        # best starting lr
        best_performance = dict()

        # Go over model architectures
        for model_arch in sorted(csv.model.unique()):
            data_model = csv[csv.model == model_arch]
            best_lr = -1
            best_train_avg_loss, best_train_std_loss = math.inf, math.inf
            best_train_avg_acc, best_train_std_acc = -1, -1
            best_train_avg_f1, best_train_std_f1 = -1, -1
            best_train_avg_wf1, best_train_std_wf1 = -1, -1
            best_train_avg_ap, best_train_std_ap = -1, -1
            best_test_avg_loss, best_test_std_loss = math.inf, math.inf
            best_test_avg_acc, best_test_std_acc = -1, -1
            best_test_avg_f1, best_test_std_f1 = -1, -1
            best_test_avg_wf1, best_test_std_wf1 = -1, -1
            best_test_avg_ap, best_test_std_ap = -1, -1
            # Go over learning rates
            for lr in sorted(data_model.lr.unique()):
                data_model_lr = data_model[data_model.lr == lr]
                test_avg_wf1 = np.average(data_model_lr.test_weighted_f1.values)
                if test_avg_wf1 > best_test_avg_wf1:
                    best_lr = lr
                    # Train
                    best_train_avg_loss = np.average(data_model_lr.train_loss.values)
                    best_train_std_loss = np.std(data_model_lr.train_loss.values)
                    best_train_avg_wf1 = np.average(data_model_lr.train_weighted_f1.values)
                    best_train_std_wf1 = np.std(data_model_lr.train_weighted_f1.values)
                    best_train_avg_f1 = np.average(data_model_lr.train_macro_f1.values)
                    best_train_std_f1 = np.std(data_model_lr.train_macro_f1.values)
                    best_train_avg_acc = np.average(data_model_lr.train_acc.values)
                    best_train_std_acc = np.std(data_model_lr.train_acc.values)
                    best_train_avg_ap = np.average(data_model_lr.train_ap.values)
                    best_train_std_ap = np.std(data_model_lr.train_ap.values)
                    # Test
                    best_test_avg_loss = np.average(data_model_lr.test_loss.values)
                    best_test_std_loss = np.std(data_model_lr.test_loss.values)
                    best_test_avg_wf1 = test_avg_wf1
                    best_test_std_wf1 = np.std(data_model_lr.test_weighted_f1.values)
                    best_test_avg_f1 = np.average(data_model_lr.test_macro_f1.values)
                    best_test_std_f1 = np.std(data_model_lr.test_macro_f1.values)
                    best_test_avg_acc = np.average(data_model_lr.test_acc.values)
                    best_test_std_acc = np.std(data_model_lr.test_acc.values)
                    best_test_avg_ap = np.average(data_model_lr.test_ap.values)
                    best_test_std_ap = np.std(data_model_lr.test_ap.values)

            best_performance[model_arch] = {
                "lr": best_lr,
                "train_loss_avg": best_train_avg_loss, "train_loss_std": best_train_std_loss,
                "train_wf1_avg": best_train_avg_wf1, "train_wf1_std": best_train_std_wf1,
                "train_f1_avg": best_train_avg_f1, "train_f1_std": best_train_std_f1,
                "train_acc_avg": best_train_avg_acc, "train_acc_std": best_train_std_acc,
                "train_ap_avg": best_train_avg_ap, "train_ap_std": best_train_std_ap,
                "test_loss_avg": best_test_avg_loss, "test_loss_std": best_test_std_loss,
                "test_wf1_avg": best_test_avg_wf1, "test_wf1_std": best_test_std_wf1,
                "test_f1_avg": best_test_avg_f1, "test_f1_std": best_test_std_f1,
                "test_acc_avg": best_test_avg_acc, "test_acc_std": best_test_std_acc,
                "test_ap_avg": best_test_avg_ap, "test_ap_std": best_test_std_ap
            }

            if not b_silent:
                print(f"Model: {model_arch}")
                print(f"\tBest lr : {best_lr}")
                print(f"\tBest test acc: {best_test_avg_acc:.3f} +/- {best_test_std_acc:.3f}")
                print(f"\tBest test wf1: {best_test_avg_wf1:.3f} +/- {best_test_std_wf1:.3f}")
                print(f"\tBest test ap : {best_test_avg_ap:.3f} +/- {best_test_std_ap:.3f}")

        return best_performance

    @classmethod
    def extract_best_results_from_csv_reg(cls, csv_file: str, b_silent=True):
        """
        Extract best results from CSV file containing regression results.

        :param csv_file:
        :param b_silent: print best model stats or not.
        :return:
        """
        csv = pd.read_csv(csv_file)
        # For each model architecture, we want to extract the average/std performance metrics for the
        # best starting lr
        best_performance = dict()

        # Go over model architectures
        for model_arch in sorted(csv.model.unique()):
            data_model = csv[csv.model == model_arch]
            best_lr = -1
            best_train_avg_loss, best_train_std_loss = math.inf, math.inf
            best_train_avg_mae, best_train_std_mae = -1, -1
            best_train_avg_sr, best_train_std_sr = -1, -1
            best_train_avg_srp, best_train_std_srp = -1, -1
            best_test_avg_loss, best_test_std_loss = math.inf, math.inf
            best_test_avg_mae, best_test_std_mae = -1, -1
            best_test_avg_sr, best_test_std_sr = -1, -1
            best_test_avg_srp, best_test_std_srp = -1, -1
            # Go over learning rates
            for lr in sorted(data_model.lr.unique()):
                data_model_lr = data_model[data_model.lr == lr]
                test_loss = np.average(data_model_lr.test_loss.values)
                if test_loss < best_test_avg_loss:
                    best_test_avg_loss = test_loss
                    best_test_std_loss = np.std(data_model_lr.test_loss.values)
                    best_lr = lr
                    # Train
                    best_train_avg_loss = np.average(data_model_lr.train_loss.values)
                    best_train_std_loss = np.std(data_model_lr.train_loss.values)
                    best_train_avg_mae = np.average(data_model_lr.train_mae.values)
                    best_train_std_mae = np.std(data_model_lr.train_mae.values)
                    best_train_avg_sr = np.average(data_model_lr.train_spearman_r.values)
                    best_train_std_sr = np.std(data_model_lr.train_spearman_r.values)
                    best_train_avg_srp = np.average(data_model_lr.train_spearman_r_p.values)
                    best_train_std_srp = np.std(data_model_lr.train_spearman_r_p.values)
                    # Test
                    best_test_avg_mae = np.average(data_model_lr.test_mae.values)
                    best_test_std_mae = np.std(data_model_lr.test_mae.values)
                    best_test_avg_sr = np.average(data_model_lr.test_spearman_r.values)
                    best_test_std_sr = np.std(data_model_lr.test_spearman_r.values)
                    best_test_avg_srp = np.average(data_model_lr.test_spearman_r_p.values)
                    best_test_std_srp = np.std(data_model_lr.test_spearman_r_p.values)

            best_performance[model_arch] = {
                "lr": best_lr,
                "train_loss_avg": best_train_avg_loss, "train_loss_std": best_train_std_loss,
                "train_mae_avg": best_train_avg_mae, "train_mae_std": best_train_std_mae,
                "train_sr_avg": best_train_avg_sr, "train_sr_std": best_train_std_sr,
                "train_srp_avg": best_train_avg_srp, "train_srp_std": best_train_std_srp,
                "test_loss_avg": best_test_avg_loss, "test_loss_std": best_test_std_loss,
                "test_mae_avg": best_test_avg_mae, "test_mae_std": best_test_std_mae,
                "test_sr_avg": best_test_avg_sr, "test_sr_std": best_test_std_sr,
                "test_srp_avg": best_test_avg_srp, "test_srp_std": best_test_std_srp,
            }

            if not b_silent:
                print(f"Model: {model_arch}")
                print(f"\tBest lr : {best_lr}")
                print(f"\tBest test mae: {best_test_avg_mae:.3f} +/- {best_test_std_mae:.3f}")
                print(f"\tBest test Spearman R: {best_test_avg_sr:.3f} +/- {best_test_std_sr:.3f}")
                print(f"\tBest test Spearman R p : {best_test_avg_srp:.3f} +/- {best_test_std_srp:.3f}")

        return best_performance

    @classmethod
    def extract_best_results_from_csvs_class(cls, csv_file: str, csv_file2: str):
        """
        For same model, present in both CSV files, extract best result.
        Handy when you have two CSV files containing results for two different loss functions.

        :param csv_file:
        :param csv_file2:
        :return:
        """
        best_performance_1 = cls.extract_best_results_from_csv_class(csv_file, b_silent=True)
        best_performance_2 = cls.extract_best_results_from_csv_class(csv_file2, b_silent=True)

        common_models = set(best_performance_1.keys()).intersection(set(best_performance_2.keys()))
        best_performance = dict()
        for m in common_models:
            if best_performance_1[m]['test_wf1_avg'] >= best_performance_2[m]['test_wf1_avg']:
                best_performance[m] = best_performance_1[m]
                best_performance[m]['file'] = 0
            else:
                best_performance[m] = best_performance_2[m]
                best_performance[m]['file'] = 1

        return best_performance

    @classmethod
    def extract_best_results_from_csvs_reg(cls, csv_file: str, csv_file2: str):
        """
        For same model, present in both CSV files, extract best result.
        Handy when you have two CSV files containing results for two different loss functions.

        :param csv_file:
        :param csv_file2:
        :return:
        """
        best_performance_1 = cls.extract_best_results_from_csv_reg(csv_file, b_silent=True)
        best_performance_2 = cls.extract_best_results_from_csv_reg(csv_file2, b_silent=True)

        common_models = set(best_performance_1.keys()).intersection(set(best_performance_2.keys()))
        best_performance = dict()
        for m in common_models:
            if best_performance_1[m]['test_mae_avg'] <= best_performance_2[m]['test_mae_avg']:
                best_performance[m] = best_performance_1[m]
                best_performance[m]['file'] = 0
            else:
                best_performance[m] = best_performance_2[m]
                best_performance[m]['file'] = 1

        return best_performance


if __name__ == '__main__':
    if False:
        CreateLaTeXTables.create_annotations_emo8_stats_table(ann_file=Config.FILE_ANN_SINGLE)

    if False:
        CreateLaTeXTables.create_annotations_emo24_stats_table(ann_file=Config.FILE_ANN_SINGLE)

    if False:
        emo8_res_file_ce = os.path.join(Config.DIR_DATA, 'Baseline Emo8 Loss=CE', 'training_emo8_results.csv')
        emo8_res_file_uce = os.path.join(Config.DIR_DATA, 'Baseline Emo8 Loss=UCE', 'training_emo8_results.csv')
        aro_res_file_mse = os.path.join(Config.DIR_DATA, 'Baseline Arousal Reg Loss=MSE', 'training_arousal1_results.csv')
        aro_res_file_wmse = os.path.join(Config.DIR_DATA, 'Baseline Arousal Reg Loss=WeightedMSE', 'training_arousal1_results.csv')
        val_res_file_mse = os.path.join(Config.DIR_DATA, 'Baseline Valence Reg Loss=MSE', 'training_valence1_results.csv')
        val_res_file_wmse = os.path.join(Config.DIR_DATA, 'Baseline Valence Reg Loss=WeightedMSE', 'training_valence1_results.csv')

        _train_class_metrics = ['file', 'lr', 'train_acc', 'train_f1', 'train_wf1', 'train_ap']
        _train_class_metric_labels = ['Loss', 'Start LR', 'Accuracy', 'F1', 'Weighted F1', 'Avg.Prec.']
        _train_reg_metrics = ['file', 'lr', 'train_mae', 'train_sr']
        _train_reg_metric_labels = ['Loss', 'Start LR', 'MAE', 'Spearman R']

        _test_class_metrics = ['file', 'lr', 'test_acc', 'test_f1', 'test_wf1', 'test_ap']
        _test_class_metric_labels = ['Loss', 'Start LR', 'Accuracy', 'F1', 'Weighted F1', 'Avg.Prec.']
        _test_reg_metrics = ['file', 'lr', 'test_mae', 'test_sr']
        _test_reg_metric_labels = ['Loss', 'Start LR', 'MAE', 'Spearman R']

        # Train table
        if True:
            CreateLaTeXTables.create_baseline_table_main(res_file_emo8_ce=emo8_res_file_ce, res_file_emo8_uce=emo8_res_file_uce,
                                                         res_file_aro_mse=aro_res_file_mse, res_file_aro_wmse=aro_res_file_wmse,
                                                         res_file_val_mse=val_res_file_mse, res_file_val_wmse=val_res_file_wmse,
                                                         ignore_models=['googlenet', 'inception_v3',
                                                                        'alexnet_cc', 'vgg16_cc', 'resnet18_cc',
                                                                        'vgg19', 'resnet34', 'densenet121', 'DINOv2_1'],
                                                         metrics_class=_train_class_metrics,
                                                         metric_labels_class=_train_class_metric_labels,
                                                         metrics_reg=_train_reg_metrics,
                                                         metric_labels_reg=_train_reg_metric_labels)

        # Test table
        if True:
            CreateLaTeXTables.create_baseline_table_main(res_file_emo8_ce=emo8_res_file_ce, res_file_emo8_uce=emo8_res_file_uce,
                                                         res_file_aro_mse=aro_res_file_mse, res_file_aro_wmse=aro_res_file_wmse,
                                                         res_file_val_mse=val_res_file_mse, res_file_val_wmse=val_res_file_wmse,
                                                         ignore_models=['googlenet', 'inception_v3',
                                                                        'alexnet_cc', 'vgg16_cc', 'resnet18_cc',
                                                                        'vgg19', 'resnet34', 'densenet121', 'DINOv2_1'],
                                                         metrics_class=_test_class_metrics,
                                                         metric_labels_class=_test_class_metric_labels,
                                                         metrics_reg=_test_reg_metrics,
                                                         metric_labels_reg=_test_reg_metric_labels)

        # CE vs. UCE + MSE vs. WMSE
        if True:
            CreateLaTeXTables.create_ce_vs_uce_table(res_file_emo8_ce=emo8_res_file_ce, res_file_emo8_uce=emo8_res_file_uce,
                                                     res_file_aro_mse=aro_res_file_mse, res_file_aro_wmse=aro_res_file_wmse,
                                                     res_file_val_mse=val_res_file_mse, res_file_val_wmse=val_res_file_wmse,
                                                     ignore_models=['googlenet', 'inception_v3',
                                                                    'alexnet_cc', 'vgg16_cc', 'resnet18_cc',
                                                                    'vgg19', 'resnet34', 'densenet121', 'DINOv2_1'])

    if False:
        csv_emo8_baseline = os.path.join(Config.DIR_DATA, 'Baseline Emo8 Loss=CE',
                                         'training_emo8_results.csv')
        # csv_emo8_oitofer = os.path.join(Config.DIR_DATA, 'Baseline+OIToFER Emo8',
        #                                 'training_emo8_results.csv')
        # csv_emo8_p365_feats = os.path.join(Config.DIR_DATA, 'Baseline+Places365 Feats Emo8',
        #                              'training_emo8_results.csv')
        # csv_emo8_p365_preds = os.path.join(Config.DIR_DATA, 'Baseline+Places365 Preds Emo8',
        #                              'training_emo8_results.csv')

        a = CreateLaTeXTables.extract_best_results_from_csv_class(csv_emo8_baseline, b_silent=False)
        #print("="*120)
        #b = CreateLaTeXTables.extract_best_results_from_csv(csv_emo8_oitofer)
        #print("="*120)
        #c = CreateLaTeXTables.extract_best_results_from_csv(csv_emo8_p365_feats)
        #print("="*120)
        #d = CreateLaTeXTables.extract_best_results_from_csv(csv_emo8_p365_preds)

    # Create table containing Recall/Precision/F1 per emo8 for baseline models
    if True:
        emo8_res_file_ce = os.path.join(Config.DIR_DATA, 'Baseline Emo8 Loss=CE', 'training_emo8_results.csv')
        emo8_res_file_uce = os.path.join(Config.DIR_DATA, 'Baseline Emo8 Loss=UCE', 'training_emo8_results.csv')

        CreateLaTeXTables.create_baseline_emo8_performance_per_class_table(emo8_res_file_ce, emo8_res_file_uce,
                                                                           ignore_models=['googlenet', 'inception_v3',
                                                                        'alexnet_cc', 'vgg16_cc', 'resnet18_cc',
                                                                        'vgg19', 'resnet34', 'densenet121', 'DINOv2_1'])

    # Create baseline vs. OIToFER table; i.e. worst vs. best
    if False:
        emo8_res_file_bl = os.path.join(Config.DIR_DATA, 'Baseline Emo8 Loss=CE',
                                        'training_emo8_results.csv')
        emo8_res_file_oitofer = os.path.join(Config.DIR_DATA, 'Baseline+OIToFER Emo8 Loss=CE',
                                             'training_emo8_results.csv')
        aro_res_file_bl = os.path.join(Config.DIR_DATA, 'Baseline Arousal Reg Loss=MSE',
                                       'training_arousal1_results.csv')
        aro_res_file_oitofer = os.path.join(Config.DIR_DATA, 'Baseline+OIToFER Arousal Reg Loss=MSE',
                                            'training_arousal1_results.csv')
        val_res_file_bl = os.path.join(Config.DIR_DATA, 'Baseline Valence Reg Loss=MSE',
                                       'training_valence1_results.csv')
        val_res_file_oitofer = os.path.join(Config.DIR_DATA, 'Baseline+OIToFER Valence Reg Loss=MSE',
                                            'training_valence1_results.csv')

        CreateLaTeXTables.create_baseline_vs_oitofer_class_reg_table(res_file_emo8_bl=emo8_res_file_bl, res_file_emo8_oitofer=emo8_res_file_oitofer,
                                                           res_file_aro_bl=aro_res_file_bl, res_file_aro_oitofer=aro_res_file_oitofer,
                                                           res_file_val_bl=val_res_file_bl, res_file_val_oitofer=val_res_file_oitofer,
                                                           ignore_models=['googlenet', 'inception_v3',
                                                                          'alexnet_cc', 'vgg16_cc', 'resnet18_cc',
                                                                          'vgg19', 'resnet34', 'densenet121'])
