"""
Generic tools to load and process results files containing model training results and confusion matrices.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
from collections import defaultdict

import numpy as np
import pandas as pd


class ProblemType:
    CLASS = 'classification'
    REG = 'regression'


class AnalysisTools:
    # ==========================================================================================
    # Results files
    # ==========================================================================================
    @classmethod
    def extract_perf_per_loss(cls, df: str or pd.DataFrame, problem_type: ProblemType):
        """
        Extract performance per loss.
        Returns a dictionary containing per loss, and per model, the performance per learning rate.
        So: loss --> model --> lr --> performance

        :param df: path to results CSV file or already loaded results file as Pandas DataFrame.
        :param problem_type: classification or regression?
        :return: dict(loss, dict(model, dict(lr, perf))), with perf = (avg train perf, std train perf, avg test perf, std test perf)
        """
        if isinstance(df, str):
            df = pd.read_csv(df)

        uq_models = df.model.unique()

        per_loss = {}

        # Create graph per used loss function
        losses = df.loss.unique()
        for loss in losses:
            # Here, we will store the performance per model, per lr
            # So, format is per_model[model_name][lr] = (train_avg, train_std, test_avg, test_std)
            per_model = defaultdict(dict)

            df_loss = df[df.loss == loss]
            for model in uq_models:
                df_loss_model = df_loss[df_loss.model == model]
                if problem_type == ProblemType.CLASS:
                    perf_per_lr = cls.extract_loss_model_class_perf_from_df(df_loss_model)
                elif problem_type == ProblemType.REG:
                    perf_per_lr = cls.extract_loss_model_reg_perf_from_df(df_loss_model)
                else:
                    raise ValueError(f"Don't know what to do with problem_type={problem_type}.")
                per_model[model].update(perf_per_lr)

            per_loss[loss] = per_model

        return per_loss

    @classmethod
    def extract_loss_model_class_perf_from_df(cls, df_loss_model):
        """
        This method applies to classification problems.

        Extract performance per model for this particular DataFrame df_loss_model that we assume has already been filtered
        to contain only training stats pertaining to a specific loss and specific model combination.

        :param df_loss_model: pd.DataFrame containing training data for specific loss and specific model
        :return: dictionary containing average/std train/test performance per lr
        """
        perf_per_lr = {}
        lrs = sorted(df_loss_model.lr.unique())
        for lr in lrs:
            if lr == np.nan:
                raise ValueError
            df_loss_model_lr = df_loss_model[df_loss_model.lr == lr]
            train_wf1s = df_loss_model_lr.train_weighted_f1
            test_wf1s = df_loss_model_lr.test_weighted_f1

            perf = (np.average(train_wf1s), np.std(train_wf1s), np.average(test_wf1s), np.std(test_wf1s))
            perf_per_lr[lr] = perf

        return perf_per_lr

    @classmethod
    def extract_loss_model_reg_perf_from_df(cls, df_loss_model):
        """
        This method applies to regression problems.

        Extract performance per model for this particular DataFrame df_loss_model that we assume has already been filtered
        to contain only training stats pertaining to a specific loss and specific model combination.

        :param df_loss_model: pd.DataFrame containing training data for specific loss and specific model
        :return: dictionary containing average/std train/test performance per lr
        """
        perf_per_lr = {}
        lrs = sorted(df_loss_model.lr.unique())
        for lr in lrs:
            if lr == np.nan:
                raise ValueError
            df_loss_model_lr = df_loss_model[df_loss_model.lr == lr]
            train_loss = df_loss_model_lr.train_loss
            test_loss = df_loss_model_lr.test_loss

            perf = (np.average(train_loss), np.std(train_loss),
                    np.average(test_loss), np.std(test_loss))
            perf_per_lr[lr] = perf

        return perf_per_lr

    # ==========================================================================================
    # Confusion matrices
    # ==========================================================================================
    @classmethod
    def load_confusion_matrices(cls, file_conf_mtxs: str):
        """
        Load confusion matrices and performance tables stored in file_conf_mtxs into dictionary, with the timestamp as key,
        so entries can be matched to corresponding entries in \*_results.csv file.

        :param file_conf_mtxs:
        :return: dict(timestamp: (train_matrix, train_stats, test_matrix, test_stats)); the matrices are numpy arrays, the stats\
        are dict(metric: numpy array)
        """
        per_timestamp = {}

        # Load entire file into memory
        with open(file_conf_mtxs, 'r') as fin:
            contents = fin.read()
        # The start of a new entry is marked by 2 lines of '='s
        entry_start = "====================================================================================================\n" \
                      "====================================================================================================\n"
        entry_middle = "----------------------------------------------------------------------------------------------------\n"

        start_pos = contents.find(entry_start) + len(entry_start)
        b_continue = True
        while b_continue:
            # Extract timestamp
            ts, start_pos = cls._get_next_line(contents, start_pos)

            # Extract
            mid_point = contents.find(entry_middle, start_pos)
            train_data = contents[start_pos: mid_point]
            train_mtx, train_stats = cls._extract_mtx_and_table(train_data)
            start_pos = mid_point + len(entry_middle)

            end_pos = contents.find(entry_start, start_pos)
            if end_pos < 0:
                b_continue = False
                end_pos = len(contents)
            test_data = contents[start_pos: end_pos]
            if b_continue:
                start_pos = end_pos + len(entry_start)
            test_mtx, test_stats = cls._extract_mtx_and_table(test_data)

            per_timestamp[ts] = (train_mtx, train_stats, test_mtx, test_stats)

        return per_timestamp

    @classmethod
    def _get_next_line(cls, data, start_pos, b_return_parts=False):
        line_end = data.find('\n', start_pos)
        line = data[start_pos:line_end]
        if b_return_parts:
            return line, line_end+1, line.split()
        else:
            return line, line_end+1

    @classmethod
    def _extract_mtx_and_table(cls, text: str):
        # matrix, table = defaultdict(Counter), defaultdict(Counter)

        start_pos = 0
        # Next line contains model name and learning rate, but we don't need those
        line, start_pos = cls._get_next_line(text, start_pos)

        # We move on to the confusion matrix
        # First line contains header, and tells uw how many categories there are
        line, start_pos, parts = cls._get_next_line(text, start_pos, b_return_parts=True)
        nb_categories = len(parts) - 1  # Last column = number of predictions for class represented by row
        # Read next lines that constitute the confusion matrix
        matrix = np.zeros((nb_categories, nb_categories+1))
        for row_idx in range(nb_categories):
            line, start_pos, parts = cls._get_next_line(text, start_pos, b_return_parts=True)
            # cat = parts[0][:-1]
            # cat_vals = {x: int(parts[x + 1]) for x in range(nb_categories)}
            # matrix[cat].update(cat_vals)
            # matrix[cat]['#'] = int(parts[-1])
            matrix[row_idx] = [float(parts[x + 1]) for x in range(nb_categories+1)]
        # Next two lines are blank
        line, start_pos = cls._get_next_line(text, start_pos)
        line, start_pos = cls._get_next_line(text, start_pos)
        # We have arrived at the start of the prec/rec/f1 matrix
        # We can skip the header; we already know the number of categories
        line, start_pos = cls._get_next_line(text, start_pos)
        # Now, we read out the metrics; 3 consecutive lines containing prec, rec and f1
        table = {}
        for metric in ['prec', 'rec', 'f1']:
            line, start_pos, parts = cls._get_next_line(text, start_pos, b_return_parts=True)
            # Check metric corresponding to this line
            if metric != parts[0]:
                raise ValueError(f"Metric mismatch, expected {metric}, got {parts[0]}")
            # Read line and store values
            table[metric] = np.array([float(parts[x + 1]) for x in range(nb_categories)])

        return matrix, table
