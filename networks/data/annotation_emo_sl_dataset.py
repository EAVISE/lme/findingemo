"""
Annotation dataset to load Emotion annotations.

This is a dataset to be used for single label classification. Images that have been annotated
more than once, and for which the multiple annotations disagree, are ignored.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import os
from collections import defaultdict

import PIL
import numpy as np
import torch
from torchvision import transforms

from config import Config
from networks.data.annotation_dataset_tools import AnnotationEmotionMap, OneHot
from networks.data.base_dataset import BaseDataset
from networks.data.preprocessing.imagenet_preproc import ImageNetPreProcess
from networks.data.preprocessing.img_resize_preproc import ImgResize
from networks.data.split_train_test import SplitTrainTest


class AnnotationEmoSLDatasetFactory(BaseDataset):
    """
    Create PyTorch dataset train/test objects, based on emotion annotations of scraped images.

    SL = Single Label
    """
    @classmethod
    def get_train_test_datasets(cls,
                                ann_file=None,
                                b_filter=False,
                                base_dir=None,
                                b_preload=False,
                                b_to_gpu=False,
                                b_skip_normalizing=False,
                                b_no_uncertain=True,
                                b_no_neutral=False,
                                b_one_hot_target=False,
                                b_only_main_emos=False,
                                train_test_split=0.8,
                                max_samples=-1,
                                resize_width=800,
                                resize_height=600):
        """
        Read data and put it into datasets.

        :param ann_file: path to the annotation file to be used
        :param b_filter: filter annotations or not?
        :param base_dir: parent directory containing images to load
        :param b_preload: preload images to RAM
        :param b_to_gpu: send preloaded images from computer RAM to GPU RAM (b_preload needs to be True for this to work)
        :param b_skip_normalizing: don't load data in RAM to get mean/std stats; useful if you only need the paths to the images
        :param b_no_uncertain: by default only images annotated as "keep" are kept for training;
            if this bool is set to "true", also images annotated as "uncertain" will be considered.
        :param b_no_neutral: ignore images that were annotated with no emotion selected (i.e, "neutral" emotion).
        :param b_one_hot_target: targets should be returned as one-hot encoded tensors instead of indices
        :param b_only_main_emos: map 24 emotions to 8 main emotions
        :param train_test_split: ratio of images to be kept as training data
        :param max_samples: maximum number of samples to load
        :param resize_width: width images will be resized to
        :param resize_height: height images will be resized to
        """
        print(f"AnnotationEmoSLDatasetFactory: Reading data...")
        if base_dir is None:
            base_dir = Config.DIR_IMAGES

        ann = cls.get_anns(ann_file=ann_file, b_filter=b_filter)

        data = defaultdict(set)
        for i, row in ann.iterrows():
            # Check image file exists, else ignore
            full_path = os.path.join(base_dir, *row.image_path.split('/'))
            if not os.path.isfile(full_path):
                continue

            if row.reject == 'Reject' or (not b_no_uncertain and row.reject == 'Uncertain'):
                continue
            elif b_no_neutral and row.emotion == "Undefined":
                continue

            if b_only_main_emos:
                data[row.image_path].add(AnnotationEmotionMap.EMOTION_LEAF_MAP_EXT[row.emotion])
            else:
                data[row.image_path].add(AnnotationEmotionMap.EMOTION_MAP_EXT[row.emotion])
        data_files, labels = [], []
        for k, v in data.items():
            # Image has been annotated with more than one emotion? Then ignore.
            if len(v) > 1:
                continue
            data_files.append(k)
            labels.append(list(v)[0])

        train_img, train_target, test_img, test_target = SplitTrainTest.get_balanced_per_label(data_files, labels,
                                                                                               train_test_split=train_test_split)

        train_target = np.array(train_target)
        test_target = np.array(test_target)

        # Compute mean/std over train set for normalization
        # See https://kozodoi.me/python/deep%20learning/pytorch/tutorial/2021/03/08/image-mean-std.html
        img_resize = ImgResize(width=resize_width, height=resize_height)
        # if b_skip_normalizing:
        #     xs_means = torch.zeros(3)
        #     xs_stds = torch.zeros(3)
        # else:
        #     _xs_train_sum, _xs_train_sum_sq = torch.zeros(3), torch.zeros(3)
        #     for i, img_path in enumerate(train_img):
        #         if (i+1) % 10 == 0:
        #             print(f"\rComputing mean/std over pixels, processing image {i+1}/{len(train_img)}...", end='', flush=True)
        #         full_path = os.path.join(base_dir, *img_path.split('/'))
        #         try:
        #             with open(full_path, 'rb') as f:
        #                 img = PIL.Image.open(f)
        #                 img = img.convert('RGB')
        #             tf_img = img_resize(img)
        #             _xs_train_sum += tf_img.sum(dim=(1, 2))
        #             _xs_train_sum_sq += tf_img.square().sum(dim=(1, 2))
        #         except Exception as e:
        #             print(f"Error processing image:\n\t{full_path}\n"
        #                   f"Error: {e}\n------------------------------------------------------------\n")
        #             continue
        #     print(f"\rComputing mean/std over pixels, processing image {len(train_img)}/{len(train_img)}... done!")
        #     n = len(train_img)*600*800
        #     xs_means = _xs_train_sum/n
        #     xs_stds = (_xs_train_sum_sq/n - xs_means.square()).sqrt()
        #
        # transform = transforms.Compose([img_resize,
        #                                 transforms.Normalize(xs_means, xs_stds)])

        transform = transforms.Compose([img_resize,
                                        ImageNetPreProcess(chain_type=ImageNetPreProcess.NORMALIZE)])

        if b_one_hot_target:
            target_transform = transforms.Compose([OneHot(nb_classes=24 if b_no_neutral else 25)])
        else:
            target_transform = transforms.Compose([torch.as_tensor])

        data_train = AnnotationEmoSLDataset(xs=train_img, ys=train_target, b_preload=b_preload,
                                            transform=transform, target_transform=target_transform,
                                            b_to_gpu=b_to_gpu, base_dir=base_dir)
        data_test = AnnotationEmoSLDataset(xs=test_img, ys=test_target, b_preload=b_preload,
                                           transform=transform, target_transform=target_transform,
                                           b_to_gpu=b_to_gpu, base_dir=base_dir)

        return data_train, data_test


class AnnotationEmoSLDataset(torch.utils.data.Dataset):
    """
    PyTorch Dataset containing scraped image data.
    """
    def __init__(self, xs, ys, base_dir=None, transform=None, target_transform=None, b_preload=False, b_to_gpu=False):
        """

        :param xs: array containing the paths to the images to use
        :param ys: array containing the target values
        :param base_dir: base dir from which the image paths will be taken
        :param transform: transform to be applied to images
        :param target_transform: transform to be applied to the target values
        :param b_preload: preload images to RAM
        :param b_to_gpu: send preloaded images from computer RAM to GPU RAM (b_preload needs to be True for this to work)
        """
        if b_to_gpu and not b_preload:
            raise ValueError(f"If b_to_gpu=True, b_preload needs to be set to True as well. However p_preload={b_preload}.")

        if base_dir is None:
            self.base_dir = Config.DIR_IMAGES
        else:
            self.base_dir = base_dir

        if transform is None:
            self.transform = transforms.Compose([transforms.Resize((600, 800)), transforms.ToTensor()])
        else:
            self.transform = transform
        if target_transform is None:
            self.target_transform = transforms.Compose([torch.as_tensor])
        else:
            self.target_transform = target_transform

        self.b_preload = b_preload

        if b_preload:
            xs = torch.stack([self.transform(self.pil_loader(xs[i])) for i in range(len(xs))])
            ys = torch.stack([self.target_transform(ys[i]) for i in range(len(ys))])
            if b_to_gpu:
                xs.to('cuda')
                ys.to('cuda')

        self.xs = xs
        self.ys = ys

    def __getitem__(self, item: int) -> (torch.Tensor, torch.Tensor):
        """
        Retrieves an item for training.

        :param item: Item index.
        :return: Return item in the form (x, y), where x is a vector of features, and y are the ground truth values to
        be predicted.
        """
        if self.b_preload:
            return self.xs[item], self.ys[item]
        else:
            return self.transform(self.pil_loader(self.xs[item])), self.target_transform(self.ys[item])

    def __len__(self) -> int:
        """
        Return the length of the dataset.

        :return: The length of the dataset.
        """
        assert len(self.xs) == len(self.ys)
        return len(self.xs)

    def pil_loader(self, path: str) -> PIL.Image.Image:
        # open path as file to avoid ResourceWarning (https://github.com/python-pillow/Pillow/issues/835)
        full_path = os.path.join(self.base_dir, *path.split('/'))
        with open(full_path, 'rb') as f:
            img = PIL.Image.open(f)
            try:
                img = img.convert('RGB')
            except OSError:
                print(f"!!!Couldn't open image:\t{full_path}")
                exit()
            return img


if __name__ == '__main__':
    train, test = AnnotationEmoSLDatasetFactory.get_train_test_datasets(b_preload=False, b_to_gpu=False, max_samples=0)

    # for i in range(10):
    #     print(train.__getitem__(i))
