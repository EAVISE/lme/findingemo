"""

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
from collections import defaultdict
import random

from networks.data.annotation_dataset_tools import AnnotationDatasetTools


class SplitTrainTest:
    @staticmethod
    def get_balanced_per_label(data_files: list, labels: list, train_test_split=0.8):
        # Get indices of elements per label
        idxs_per_label = defaultdict(list)
        for i, e in enumerate(labels):
            idxs_per_label[e].append(i)

        # Randomly shuffle each list of indices and
        # split indices for each label into train/test split, and
        # group into list of indices for train/test data and
        # create list containing the corresponding labels
        train_data, train_labels, test_data, test_labels = [], [], [], []

        rng = random.SystemRandom()
        for k, v in idxs_per_label.items():
            rng.shuffle(v)
            split_idx = int(len(v)*train_test_split)
            train_data += v[:split_idx]
            train_labels += [k]*split_idx
            test_data += v[split_idx:]
            test_labels += [k]*(len(v)-split_idx)

        # Shuffle again, and we're done!
        train_data, train_labels = AnnotationDatasetTools.shuffle_data(train_data, train_labels)
        train_imgs = [data_files[i] for i in train_data]
        test_data, test_labels = AnnotationDatasetTools.shuffle_data(test_data, test_labels)
        test_imgs = [data_files[i] for i in test_data]

        return train_imgs, train_labels, test_imgs, test_labels
