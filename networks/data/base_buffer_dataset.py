"""
Base dataset object to load buffered features from a dill file.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
from networks.data.base_dataset import BaseDataset


class BaseBufferDataset(BaseDataset):
    def get_subset_for_imgs(self, img_paths: list):
        raise NotImplementedError

    def get_feats_for_img(self, img_path: str):
        raise NotImplementedError
