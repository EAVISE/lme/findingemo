"""
Transform values in [min, max] to fit in range [0, 1]

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import PIL
import torch
import torchvision


class LimitRange(object):
    """
    Resize image to pre-defined size and convert to torch.Tensor.

    The image will be resized such that it completely fits within the pre-defined size and will be further padded
    such as to fully obtain the pre-defined size.

    E.g., if the target dimensions are 800x600, and the image to be resized is 1000x600, the image will first be resized to
    800x480, and then its height will be further padded so that it becomes 800x600.
    """
    def __init__(self, min: float, max: float):
        if not isinstance(min, float) or not isinstance(max, float):
            raise TypeError("Input arguments should be of type 'float'; at least 1 one was not.")
        self.min = min
        self.max = max
        self.range = float(max - min)
        if self.range == 0:
            raise ValueError("It would appear your min and max values are equal, which is invalid.")

        print(f"LimitRange processor initialized with min={min}, max={max}, range={self.range}.")

    def __call__(self, value: int) -> torch.Tensor:
        """
        Map input value to range [0, 1].

        :param value: Input value.
        :return: the resized image, as torch.Tensor.
        """
        res = (value - self.min)/self.range

        return torch.as_tensor(res, dtype=torch.float32)

    def inverse_transform(self, value):
        return (value*self.range) + self.min
