"""
Some classes that can be used across different AnnotationDatasets.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import os
import random

import numpy as np
import torch


class AnnotationIntensityMap:
    INTENSITY_MAP = {'−−': -2,
                     '−' : -1,
                     '±' : 0,
                     '+' : 1,
                     '++': 2}


class AnnotationRKUMap:
    RKU_MAP = {'Reject': 0,
               'Keep': 1,
               'Uncertain': 2}
    INV_RKU_MAP = {v: k for k, v in RKU_MAP.items()}


class AnnotationEmotionMap:
    EMOTION_SETS = [["Serenity", "Joy", "Ecstasy"],
                    ["Acceptance", "Trust", "Admiration"],
                    ["Apprehension", "Fear", "Terror"],
                    ["Distraction", "Surprise", "Amazement"],
                    ["Pensiveness", "Sadness", "Grief"],
                    ["Boredom", "Disgust", "Loathing"],
                    ["Annoyance", "Anger", "Rage"],
                    ["Interest", "Anticipation", "Vigilance"]]

    EMO8_LIST = [l[1] for l in EMOTION_SETS]

    #: Mapping of emotion to unique index.
    EMOTION_MAP = {l: (i*3 + j) for i, k in enumerate(EMOTION_SETS) for j, l in enumerate(k)}
    #: Extended map, that also includes "Undefined".
    EMOTION_MAP_EXT = {"Undefined": len(EMOTION_MAP)}
    EMOTION_MAP_EXT.update(EMOTION_MAP)
    #: Mapping of emotion to "coordinate on emotion axis". Hereby, each leaf gets a unique index,
    #: and the three emotions in the index are at thirds of each leaf.
    #: E.g., "Joy" is in leaf 0, and is in the middle, so at position 0.66.
    #: In practice, this amounts to dividing the unique emo indices by 3, but that sounds way less cool.
    #: We use these numbers to compute distances between emotion annotations.
    EMOTION_DIST_MAP = {l: (i + 0.33*j) for i, k in enumerate(EMOTION_SETS) for j, l in enumerate(k)}
    #: Inversed extended emotion map: map index to emotion.
    INV_EMOTION_MAP_EXT = {v: k for k, v in EMOTION_MAP_EXT.items()}
    #: Leaf map: map emotion onto leaf index.
    EMOTION_LEAF_MAP = {l: i for i, k in enumerate(EMOTION_SETS) for j, l in enumerate(k)}
    #: Leaf map extended with "Undefined".
    EMOTION_LEAF_MAP_EXT = {"Undefined": len(EMOTION_LEAF_MAP)}
    EMOTION_LEAF_MAP_EXT.update(EMOTION_LEAF_MAP)
    #: Extended inversed emotion leaf map; have to do this the long way
    INV_EMOTION_LEAF_MAP_EXT = {}
    for i in range(1, 24, 3):
        INV_EMOTION_LEAF_MAP_EXT[i//3] = INV_EMOTION_MAP_EXT[i]
    INV_EMOTION_LEAF_MAP_EXT[len(INV_EMOTION_LEAF_MAP_EXT)] = 'Undefined'

    @classmethod
    def get_emo_distance(cls, emo1: str, emo2: str):
        """
        Compute "distance" between 2 emotions.

        :param emo1: string representation, i.e., name as it appears in EMOTION_SETS, of first emotion
        :param emo2: string representation, i.e., name as it appears in EMOTION_SETS, of second emotion
        :return:
        """
        idx_a = cls.EMOTION_DIST_MAP[emo1]
        idx_b = cls.EMOTION_DIST_MAP[emo2]
        diff_axis = np.abs(int(idx_a) - int(idx_b))
        if diff_axis > 4:
            diff_axis = 8 - diff_axis
        diff_inter = np.abs((idx_a % 1) - (idx_b % 1))

        return diff_axis + diff_inter


class OneHot(object):
    """
    Transform integer into one-hot encoded tensor.
    """
    def __init__(self, nb_classes: int):
        self.nb_classes = nb_classes

    def __call__(self, idx: int) -> torch.Tensor:
        """

        :param idx: target index to transform.
        :return: a one-hot encoded 1D torch.Tensor.
        """
        return torch.nn.functional.one_hot(torch.tensor(idx), self.nb_classes).float()


class Normalize1D(object):
    """
    Normalize a single number, e.g., a regression target value.
    """
    def __init__(self, mean: np.float64, std: np.float64):
        self.mean = mean
        self.std = std

        if self.std <= 0.:
            raise ValueError("StandardDeviation should be > 0.")

    def __call__(self, f: np.float64) -> torch.Tensor:
        """
        Normalize argument.

        :param f: Argument to normalize.
        :return: the normalized argument, as torch.Tensor.
        """
        f = (f-self.mean)/self.std

        return torch.as_tensor(f, dtype=torch.float32)

    def unnormalize(self, t: torch.Tensor):
        """
        Do reverse transform on normalized argument.

        :param t: tensor containing the argument(s)
        :return: unnormalized argument
        """
        t = t.detach().clone()
        t = (t * self.std) + self.mean

        return t


class AnnotationDatasetTools:
    @staticmethod
    def shuffle_data(data_files, labels):
        rng = random.SystemRandom()
        temp = list(zip(data_files, labels))
        rng.shuffle(temp)
        return zip(*temp)

    @classmethod
    def filter_missing_images(cls, base_dir: str, data_files: list):
        """
        Given a list of image paths, filter out those that correspond to missing images.
        !!! THE PROVIDED LIST IS CHANGED IN PLACE !!!

        :param base_dir: parent directory containing the images
        :param data_files:
        :return:
        """
        to_remove = []
        for img_path in data_files:
            full_path = os.path.join(base_dir, *img_path.split('/'))
            if not os.path.isfile(full_path):
                to_remove.append(img_path)

        for e in to_remove:
            data_files.remove(e)
        if to_remove:
            print(f"Removed {len(to_remove)} references to non-existing files.")


if __name__ == '__main__':
    print(AnnotationEmotionMap.INV_EMOTION_LEAF_MAP_EXT)
