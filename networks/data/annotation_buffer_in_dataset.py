"""
Annotation dataset that loads all annotations, to be used to buffer the ImageNet model predictions.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import os

import PIL
import torch
from torchvision import transforms

from config import Config
from networks.data.annotation_dataset_tools import AnnotationDatasetTools
from networks.data.base_dataset import BaseDataset
from networks.data.preprocessing.imagenet_preproc import ImageNetPreProcess
from networks.data.preprocessing.img_resize_preproc import ImgResize


class AnnotationBufferINDatasetFactory(BaseDataset):
    """
    Create PyTorch dataset train/test objects, based on "reject/keep/uncertain" annotations of scraped images.
    """
    @classmethod
    def get_dataset(cls,
                    ann_file=None,
                    b_filter=False,
                    base_dir=None,
                    filter_images: set=None,
                    train_test_split=0.8,
                    max_samples=-1,
                    resize_width=800,
                    resize_height=600):
        """
        Read data and put it into datasets.

        :param ann_file: annotation csv file to use
        :param b_filter: filter annotations?
        :param base_dir: parent directory containing images to load
        :param filter_images: a set containing the (relative) paths of images that should not be loaded
        :param train_test_split: ratio of images to be kept as training data.
        :param max_samples: maximum number of samples to load
        :param resize_width: width to which all images will be resized
        :param resize_height: height to which all images will be resized
        :return: dataset
        """
        print(f"AnnotationBufferINDatasetFactory: Reading data...")
        if base_dir is None:
            base_dir = Config.DIR_IMAGES

        if filter_images is None:
            filter_images = set()

        ann = cls.get_anns(ann_file=ann_file, b_filter=b_filter)

        data_files = list(set(ann.image_path.unique()).difference(filter_images))
        AnnotationDatasetTools.filter_missing_images(base_dir=base_dir, data_files=data_files)

        nb_imgs = len(data_files)
        if 0 < max_samples < nb_imgs:
            nb_imgs = max_samples
            data_files = data_files[:max_samples]
        max_train_idx = int(nb_imgs*train_test_split)
        train_img, test_img = [], []
        for i in range(nb_imgs):
            if i < max_train_idx:
                train_img.append(data_files[i])
            else:
                test_img.append(data_files[i])

        # Compute mean/std over train set for normalization
        # See https://kozodoi.me/python/deep%20learning/pytorch/tutorial/2021/03/08/image-mean-std.html
        img_resize = ImgResize(width=resize_width, height=resize_height)
        # if b_skip_normalizing:
        #     xs_means = torch.zeros(3)
        #     xs_stds = torch.zeros(3)
        # else:
        #     _xs_train_sum, _xs_train_sum_sq = torch.zeros(3), torch.zeros(3)
        #     for i, img_path in enumerate(train_img):
        #         if (i+1) % 10 == 0:
        #             print(f"\rComputing mean/std over pixels, processing image {i+1}/{len(train_img)}...", end='', flush=True)
        #         full_path = os.path.join(base_dir, *img_path.split('/'))
        #         try:
        #             with open(full_path, 'rb') as f:
        #                 img = PIL.Image.open(f)
        #                 img = img.convert('RGB')
        #             tf_img = img_resize(img)
        #             _xs_train_sum += tf_img.sum(dim=(1, 2))
        #             _xs_train_sum_sq += tf_img.square().sum(dim=(1, 2))
        #         except:
        #             print(f"!!! AnnotationBufferINDatasetFactory: file not found:\n\t{full_path}")
        #             continue
        #     print(f"\rComputing mean/std over pixels, processing image {len(train_img)}/{len(train_img)}... done!")
        #     n = len(train_img)*resize_width*resize_height
        #     xs_means = _xs_train_sum/n
        #     xs_stds = (_xs_train_sum_sq/n - xs_means.square()).sqrt()


        transform = transforms.Compose([img_resize, ImageNetPreProcess(chain_type=ImageNetPreProcess.NORMALIZE)])

        dataset = AnnotationBufferINDataset(xs=data_files, transform=transform, base_dir=base_dir)

        return dataset


class AnnotationBufferINDataset(torch.utils.data.Dataset):
    """
    PyTorch Dataset containing scraped image data.
    """

    def __init__(self, xs, base_dir=None, transform=None):
        """

        :param xs: array containing the paths to the images to use
        :param base_dir: base dir from which the image paths will be taken
        :param transform: transform to be applied to images
        """
        if base_dir is None:
            self.base_dir = Config.DIR_IMAGES
        else:
            self.base_dir = base_dir

        if transform is None:
            self.transform = transforms.Compose([transforms.Resize((600, 800)), transforms.ToTensor()])
        else:
            self.transform = transform
        self.target_transform = transforms.Compose([torch.as_tensor])

        self.xs = xs

    def __getitem__(self, item: int) -> (torch.Tensor, torch.Tensor):
        """
        Retrieves an item from dataset.

        :param item: Item index.
        :return: Return item.
        """
        img_path = self.xs[item]
        return self.transform(self.pil_loader(img_path)), img_path

    def __len__(self) -> int:
        """
        Return the length of the dataset.

        :return: The length of the dataset.
        """
        return len(self.xs)

    def pil_loader(self, path: str) -> PIL.Image.Image:
        # open path as file to avoid ResourceWarning (https://github.com/python-pillow/Pillow/issues/835)
        full_path = os.path.join(self.base_dir, *path.split('/'))
        with open(full_path, 'rb') as f:
            img = PIL.Image.open(f)
            return img.convert('RGB')

    def filter_images(self, filter_images: set):
        """
        Remove the image paths in filter_images from self.xs.

        :param filter_images:
        :return:
        """
        to_remove = []
        for i, e in enumerate(self.xs):
            if e in filter_images:
                to_remove.append(i)

        for i in to_remove[::-1]:
            del self.xs[i]


if __name__ == '__main__':
    dataset = AnnotationBufferINDatasetFactory.get_dataset(max_samples=10)

    for i in range(10):
        print(dataset.__getitem__(i))
