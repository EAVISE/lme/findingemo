"""
Generic dataset to hold buffered features, which are provided as a dictionary (k,v): k=image path, v=features.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import torch


class GenericBufferedFeatsDataset(torch.utils.data.Dataset):
    """
    PyTorch Dataset containing buffered ImageNet features.
    """

    def __init__(self, buffered_feats: dict):
        """

        :param buffered_feats: dictionary containing buffered ImageNet features
        """
        self.buffered_feats = buffered_feats
        self.xs = sorted(self.buffered_feats.keys())

    def __getitem__(self, item: int) -> (torch.Tensor, str):
        """
        Retrieves an item from the dataset.

        :param item: Item index.
        :return: Return item in the form (x, y), where x is a vector of features, and y is the image path
        """
        img_path = self.xs[item]
        return self.buffered_feats[img_path], img_path

    def __len__(self) -> int:
        """
        Return the length of the dataset.

        :return: The length of the dataset.
        """
        return len(self.xs)