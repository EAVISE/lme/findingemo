"""
Class to load precomputed ImageNet network features.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import os

import dill
import torchvision

from config import Config
from networks.data.base_buffer_dataset import BaseBufferDataset


class BufferINDataset(BaseBufferDataset):
    def __init__(self, in_model_name: str, b_classifier_l1=False, root_dir=None):
        super().__init__()
        if root_dir is None:
            root_dir = Config.DIR_BUFFER_IN
        if in_model_name is None:
            raise ValueError("Model name not specified.")

        print(f"Loading BufferINDataset for model {in_model_name}...", end='')
        if b_classifier_l1:
            if not in_model_name in {'alexnet', 'vgg16', 'vgg19'}:
                raise ValueError(f"You specified b_classifier_l1=True for a model that does not support this.\n"
                                 f"Specified model name={in_model_name}, but option is only available for alexnet, vgg16 and vgg19.")
            file_feats = os.path.join(root_dir, f"buffer_{in_model_name}_layer=class1.dill")
        else:
            file_feats = os.path.join(root_dir, f"buffer_{in_model_name}.dill")
        self.feats_per_img = dill.load(open(file_feats, 'rb'))
        print(f" done!\n\tLoaded file: {file_feats}")

    def get_subset_for_imgs(self, img_paths: list):
        """
        Get a subset of full dataset for image paths provided.

        :param img_paths:
        :return: dictionary containing subset of full dataset
        """
        return {x: self.feats_per_img[x] for x in img_paths}

    def get_feats_for_img(self, img_path: str):
        """
        Return the ImageNet feats for this particular image.

        :param img_path: relative image path, relative to the root image directory.
        :return: buffered features for this image
        """
        return self.feats_per_img[img_path]


if __name__ == '__main__':
    boid = BufferINDataset(in_model_name=torchvision.models.vgg16.__name__)
