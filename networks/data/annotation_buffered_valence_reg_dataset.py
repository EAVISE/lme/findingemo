"""

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
from collections import defaultdict

import numpy as np
import torch
import torchvision
from torchvision import transforms

from networks.data.base_buffer_dataset import BaseBufferDataset
from networks.data.buffer_clip_dataset import BufferCLIPDataset
from networks.data.buffer_dinov2_dataset import BufferDINOv2Dataset
from networks.data.buffer_en_dataset import BufferENDataset
from networks.data.buffer_in_dataset import BufferINDataset
from networks.data.buffer_places365_dataset import BufferPlaces365Dataset
from networks.data.preprocessing.limit_range_preproc import LimitRange
from networks.data.split_train_test import SplitTrainTest


class AnnotationBufferedValenceRegDatasetFactory(BaseBufferDataset):
    """
    Create PyTorch dataset train/test objects, based on "arousal" annotations of scraped images.
    """
    @classmethod
    def get_train_test_datasets(cls,
                                model_name,
                                b_classifier_l1=False,
                                ann_file=None,
                                b_filter=False,
                                b_no_uncertain=True,
                                min=-3.0,
                                max=3.0,
                                train_test_split=0.8,
                                buffered_feats_root_dir=None,
                                max_samples=-1):
        """
        Read data and put it into datasets.

        :param model_name: name of ImageNet model for which to use precomputed features, 'emonet' for EmoNet, \
         'places365' for Places365 or name ending in '_sin' for Stylized ImageNet
        :param b_classifier_l1: use buffered features of first classifier layer for AlexNet, VGG16 or VGG19, instead\
            of second classifier layer (=default)
        :param ann_file: path to the annotation file to be used
        :param b_filter: filter annotations or not?
        :param b_no_uncertain: by default only images annotated as "keep" are kept for training;
            if this bool is set to "true", also images annotated as "uncertain" will be considered.
        :param min: minimum range value of labels
        :param min: maximum range value of labels
        :param train_test_split: ratio of images to be kept as training data
        :param buffered_feats_root_dir: root directory where to look for the buffered features
        :param max_samples: maximum number of samples to load
        """
        print(f"AnnotationBufferedValenceRegDatasetFactory: Reading data...")

        ann = cls.get_anns(ann_file=ann_file, b_filter=b_filter)

        # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        # We have to be careful when loading the annotations. Some
        # images have been annotated more than once.
        data = defaultdict(set)
        for i, row in ann.iterrows():
            if row.reject == 'Reject' or (not b_no_uncertain and row.reject == 'Uncertain'):
                continue

            val = row.valence
            data[row.image_path].add(val)

        data_files, labels = [], []
        for k, v in data.items():
            # Image has been annotated with more than one valence value? Then ignore for now.
            if len(v) > 1:
                continue
            data_files.append(k)
            labels.append(list(v)[0])

        train_img, train_target, test_img, test_target = SplitTrainTest.get_balanced_per_label(data_files, labels,
                                                                                               train_test_split=train_test_split)

        train_target = np.asarray(train_target, dtype=np.float32)
        test_target = np.asarray(test_target, dtype=np.float32)

        # Load precomputed features
        if model_name == 'emonet':
            buffered_feats = BufferENDataset(root_dir=buffered_feats_root_dir).get_subset_for_imgs(img_paths=data_files)
        elif model_name.startswith('CLIP'):
            buffered_feats = BufferCLIPDataset(root_dir=buffered_feats_root_dir).get_subset_for_imgs(img_paths=data_files)
        elif model_name.startswith('DINOv2'):
            buffered_feats = BufferDINOv2Dataset(root_dir=buffered_feats_root_dir).get_subset_for_imgs(img_paths=data_files)
        elif model_name.startswith('places365'):
            parts = model_name.split('-')
            if len(parts) != 2:
                raise ValueError("When using a Places365 model, you should use the parameter format:\n"
                                 "'model_name=places365-xxx', where 'xxx' should replaced by the places365 model to use.\n"
                                 "E.g., 'model_name=places365-feats_alexnet'.")
            buffered_feats = BufferPlaces365Dataset(places365_model=parts[1],
                                                    root_dir=buffered_feats_root_dir).get_subset_for_imgs(img_paths=data_files)
        # Load precomputed ImageNet features
        else:
            buffered_feats = BufferINDataset(in_model_name=model_name,
                                             root_dir=buffered_feats_root_dir).get_subset_for_imgs(img_paths=data_files)
        buffered_feats_train = {x: buffered_feats[x] for x in train_img}
        buffered_feats_test = {x: buffered_feats[x] for x in test_img}

        target_transform = LimitRange(min=min, max=max)

        data_train = AnnotationBufferedValenceRegDataset(xs=train_img, ys=train_target,
                                                         buffered_feats=buffered_feats_train,
                                                         target_transform=target_transform)
        data_test = AnnotationBufferedValenceRegDataset(xs=test_img, ys=test_target,
                                                        buffered_feats=buffered_feats_test,
                                                        target_transform=target_transform)

        return data_train, data_test


class AnnotationBufferedValenceRegDataset(torch.utils.data.Dataset):
    def __init__(self, xs, ys, buffered_feats: dict, target_transform=None):
        """

        :param xs: array containing the paths to the images to use
        :param ys: array containing the target values
        :param buffered_feats: dictionary (k, v): k=image path, v=buffered features
        """
        self.buffered_feats = buffered_feats

        if target_transform is None:
            self.target_transform = transforms.Compose([torch.as_tensor])
        else:
            self.target_transform = target_transform

        self.xs = xs
        self.ys = ys

    def __getitem__(self, item: int) -> (torch.Tensor, torch.Tensor):
        """
        Retrieves an item from the dataset.

        :param item: Item index.
        :return: Return item in the form (x, y), where x is a vector of features, and y are the ground truth values to
        be predicted.
        """
        img_path = self.xs[item]
        return self.buffered_feats[img_path], self.target_transform(self.ys[item]).float()

    def __len__(self) -> int:
        """
        Return the length of the dataset.

        :return: The length of the dataset.
        """
        assert len(self.xs) == len(self.ys)
        return len(self.xs)


if __name__ == '__main__':
    train, test = AnnotationBufferedValenceRegDatasetFactory.get_train_test_datasets(
        model_name=torchvision.models.vgg16.__name__)
    for i in range(25):
        print(train.__getitem__(i))
