"""
Variant of annotation_arousal_dataset.py that uses 3 feature streams.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import os
from collections import defaultdict

import numpy as np
import torch
from lor_tools.search import binary_search
from torchvision.transforms import transforms

from config import Config
from networks.data.base_buffer_dataset import BaseBufferDataset
from networks.data.base_dataset import BaseDataset
from networks.data.split_train_test import SplitTrainTest


class AnnotationTrplBufferedArousalDatasetFactory(BaseDataset):
    """
    Create PyTorch dataset train/test objects, based on "arousal" annotations of scraped images.
    """
    @classmethod
    def get_train_test_datasets(cls,
                                buffered_feats_stream1: BaseBufferDataset,
                                buffered_feats_stream2: BaseBufferDataset,
                                buffered_feats_stream3: BaseBufferDataset,
                                buffered_feats_stream1_params=None,
                                buffered_feats_stream2_params=None,
                                buffered_feats_stream3_params=None,
                                ann_file=None,
                                b_filter=False,
                                base_dir=None,
                                b_no_uncertain=True,
                                b_no_neutral=False,
                                train_test_split=0.8,
                                bins=None,
                                max_samples=-1):
        """
        Read data and put it into datasets.

        :param buffered_feats_stream1: Class that will contain the buffered features for the first data stream
        :param buffered_feats_stream2: Class that will contain the buffered features for the second data stream
        :param buffered_feats_stream3: Class that will contain the buffered features for the third data stream
        :param buffered_feats_stream1_params: parameters to pass on to buffered_feats_stream1
        :param buffered_feats_stream2_params: parameters to pass on to buffered_feats_stream2
        :param buffered_feats_stream3_params: parameters to pass on to buffered_feats_stream3
        :param ann_file: path to the annotation file to be used
        :param b_filter: filter annotations or not?
        :param base_dir: parent directory containing images to load
        :param b_no_uncertain: by default only images annotated as "keep" are kept for training;
            if this bool is set to "true", also images annotated as "uncertain" will be considered.
        :param b_no_neutral: ignore images that were annotated with no emotion selected (i.e, "neutral" emotion).
        :param train_test_split: ratio of images to be kept as training data
        :param bins: list of bin edges; default = None = each arousal value is a category label
        :param max_samples: maximum number of samples to load
        """
        print(f"AnnotationTrplBufferedArousalDatasetFactory: Reading data...")
        if base_dir is None:
            base_dir = Config.DIR_IMAGES

        ann = cls.get_anns(ann_file=ann_file, b_filter=b_filter)

        data = defaultdict(set)
        for i, row in ann.iterrows():
            # Check image file exists, else ignore
            full_path = os.path.join(base_dir, *row.image_path.split('/'))
            if not os.path.isfile(full_path):
                continue

            if row.reject == 'Reject' or (not b_no_uncertain and row.reject == 'Uncertain'):
                continue
            elif b_no_neutral and row.emotion == "Undefined":
                continue

            aro = row.arousal
            if bins is None:
                data[row.image_path].add(aro)
            else:
                bin_idx = binary_search.BinarySearch.find(bins, aro, b_return_pos=True)
                if bin_idx < 0:
                    bin_idx = (-bin_idx)-1
                data[row.image_path].add(bin_idx)

        data_files, labels = [], []
        for k, v in data.items():
            # Image has been annotated with more than one valence value? Then ignore.
            if len(v) > 1:
                continue
            data_files.append(k)
            labels.append(list(v)[0])

        train_img, train_target, test_img, test_target = SplitTrainTest.get_balanced_per_label(
            data_files, labels, train_test_split=train_test_split)

        train_target = np.array(train_target)
        test_target = np.array(test_target)

        # Load precomputed features for first stream
        if buffered_feats_stream1_params is None:
            buffered_feats_stream1_params = dict()
        buffered_feats_stream1 = buffered_feats_stream1(**buffered_feats_stream1_params).get_subset_for_imgs(img_paths=data_files)
        buffered_feats_stream1_train = {x: buffered_feats_stream1[x] for x in train_img}
        buffered_feats_stream1_test = {x: buffered_feats_stream1[x] for x in test_img}

        # Load precomputed features for second stream
        if buffered_feats_stream2_params is None:
            buffered_feats_stream2_params = dict()
        buffered_feats_stream2 = buffered_feats_stream2(**buffered_feats_stream2_params).get_subset_for_imgs(img_paths=data_files)
        buffered_feats_stream2_train = {x: buffered_feats_stream2[x] for x in train_img}
        buffered_feats_stream2_test = {x: buffered_feats_stream2[x] for x in test_img}

        # Load precomputed features for third stream
        if buffered_feats_stream3_params is None:
            buffered_feats_stream3_params = dict()
        buffered_feats_stream3 = buffered_feats_stream3(**buffered_feats_stream3_params).get_subset_for_imgs(img_paths=data_files)
        buffered_feats_stream3_train = {x: buffered_feats_stream3[x] for x in train_img}
        buffered_feats_stream3_test = {x: buffered_feats_stream3[x] for x in test_img}

        target_transform = transforms.Compose([torch.as_tensor])

        data_train = AnnotationTrplBufferedArousalDataset(xs=train_img, ys=train_target, buffered_feats_1=buffered_feats_stream1_train,
                                                             buffered_feats_2=buffered_feats_stream2_train,
                                                             buffered_feats_3=buffered_feats_stream3_train,
                                                             target_transform=target_transform, base_dir=base_dir)
        data_test = AnnotationTrplBufferedArousalDataset(xs=test_img, ys=test_target, buffered_feats_1=buffered_feats_stream1_test,
                                                            buffered_feats_2=buffered_feats_stream2_test,
                                                            buffered_feats_3=buffered_feats_stream3_test,
                                                            target_transform=target_transform, base_dir=base_dir)

        return data_train, data_test


class AnnotationTrplBufferedArousalDataset(torch.utils.data.Dataset):
    """
    PyTorch Dataset containing scraped image data.
    """

    def __init__(self, xs, ys, buffered_feats_1: dict, buffered_feats_2: dict, buffered_feats_3: dict,
                 base_dir=None, transforms=None, target_transform=None):
        """

        :param xs: array containing the paths to the images to use
        :param ys: array containing the target values
        :param buffered_feats_1: dictionary containing buffered features for first stream
        :param buffered_feats_2: dictionary containing buffered features for second stream
        :param buffered_feats_3: dictionary containing buffered features for third stream
        :param base_dir: base dir from which the image paths will be taken
        :param transforms: transforms to be applied to images
        :param target_transform: transform to be applied to the target values
        """
        if base_dir is None:
            self.base_dir = Config.DIR_IMAGES
        else:
            self.base_dir = base_dir

        self.buffered_feats_1 = buffered_feats_1
        self.buffered_feats_2 = buffered_feats_2
        self.buffered_feats_3 = buffered_feats_3

        if target_transform is None:
            self.target_transform = transforms.Compose([torch.as_tensor])
        else:
            self.target_transform = target_transform

        self.xs = xs
        self.ys = ys

    def __getitem__(self, item: int) -> (torch.Tensor, torch.Tensor):
        """
        Retrieves an item from the dataset.

        :param item: Item index.
        :return: Return item in the form (s, t, u, v), where s are the pre-computed ImageNet features,
         t are the pre-computed OIToFER features, u is the image path, and v is the target
        be predicted.
        """
        img_path = self.xs[item]
        return (self.buffered_feats_1[img_path], self.buffered_feats_2[img_path], self.buffered_feats_3[img_path],
                img_path, self.target_transform(self.ys[item]))

    def __len__(self) -> int:
        """
        Return the length of the dataset.

        :return: The length of the dataset.
        """
        assert len(self.xs) == len(self.ys)
        return len(self.ys)
