"""

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import pandas as pd
from termcolor import cprint

from annotations.filter_prolific_annotations import FilterProlificAnnotations
from config import Config


class BaseDataset:
    @classmethod
    def get_anns(cls,
                 ann_file=None,
                 b_filter=False):
        """

        :param ann_file: path to the annotation file to be used
        :param b_filter: filter annotations or not?
        :return: Pandas DataFrame containing the loaded annotations
        """
        if ann_file is None:
            ann_file = Config.FILE_ANN_SINGLE

        # Load annotations
        ann = pd.read_csv(filepath_or_buffer=ann_file, header=0, index_col=0)
        ann.fillna('Undefined', inplace=True)
        if b_filter:
            cprint("Filering annotations!", color='red')
            ann = FilterProlificAnnotations.process(ann, b_remove_incomplete=True, b_use_alt_score=True)

        return ann
