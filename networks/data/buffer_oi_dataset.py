"""
Class to load precomputed YoLo v3 + OpenImages object detections.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import os

import pandas as pd

from config import Config
from networks.nn.buffer.buffer_oi import BufferOI
from tools.img_tools import ImageTools


class BufferOIDataset:
    def __init__(self, root_dir=None):
        if root_dir is None:
            root_dir = Config.DIR_BUFFER_OI
        self.root_dir = root_dir

        self.dfs = None
        self._load_dfs()

    def _load_dfs(self):
        self.dfs = {}
        print("Loading pre-computed YoLo + OI CSVs...", end='')
        self._parse_dir(d=self.root_dir)
        print(" done!")

    def _parse_dir(self, d: str):
        for l in os.listdir(d):
            full_path = os.path.join(d, l)

            # Load CSV files
            if l.endswith('.csv'):
                rel_path = full_path.replace(self.root_dir, '')
                self.dfs[rel_path] = pd.read_csv(full_path)
            # Recursively check dirs
            elif os.path.isdir(full_path):
                self._parse_dir(d=full_path)
            else:
                print(f"No idea what to do with this path: {full_path}")

    def get_csv_for_img(self, img_path: str, b_rebuffer=False):
        """
        Return the CSV for this particular image. This method will check whether the relative path, with the image extension
        replace by '.csv', is known as a key in the self.dfs dictionary containing all loaded CSV files.

        :param img_path: relative image path, relative to the root image directory.
        :param b_rebuffer: if no CSV is loaded for this image, launch buffer_oi and reload CSVs. Then, try again.
        :return:
        """
        csv_path = ImageTools.get_csv_for_img(img_path)
        if csv_path in self.dfs:
            return self.dfs[csv_path].copy(deep=True)
        else:
            if b_rebuffer:
                print(f"BufferOIDataset: No CSV for {csv_path}")
                BufferOI.buffer_oi(out_dir=self.root_dir)
                self._load_dfs()
                return self.get_csv_for_img(img_path, b_rebuffer=False)
            else:
                return None


if __name__ == '__main__':
    boid = BufferOIDataset()
