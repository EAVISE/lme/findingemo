"""
Variant of annotation_emo_sl_dataset.py that uses pre-computed features.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import os
from collections import defaultdict

import numpy as np
import torch
import torchvision.models
from torchvision.transforms import transforms

from config import Config
from networks.data.annotation_dataset_tools import AnnotationEmotionMap, OneHot
from networks.data.base_dataset import BaseDataset
from networks.data.buffer_en_dataset import BufferENDataset
from networks.data.buffer_in_dataset import BufferINDataset
from networks.data.buffer_places365_dataset import BufferPlaces365Dataset
from networks.data.split_train_test import SplitTrainTest


class AnnotationBufferedEmoSLDatasetFactory(BaseDataset):
    """
    Create PyTorch dataset train/test objects, based on emotion annotations of scraped images.
    """

    @classmethod
    def get_train_test_datasets(cls,
                                model_name,
                                b_classifier_l1=False,
                                ann_file=None,
                                b_filter=False,
                                base_dir=None,
                                b_no_uncertain=True,
                                b_no_neutral=False,
                                b_one_hot_target=False,
                                b_only_main_emos=False,
                                train_test_split=0.8,
                                max_samples=-1):
        """
        Read data and put it into datasets.

        :param model_name: name of ImageNet model for which to use precomputed features, 'emonet' for EmoNet, \
         'places365' for Places365 or name ending in '_sin' for Stylized ImageNet
        :param b_classifier_l1: use buffered features of first classifier layer for AlexNet, VGG16 or VGG19, instead\
            of second classifier layer (=default)
        :param ann_file: path to the annotation file to be used
        :param b_filter: filter annotations or not?
        :param base_dir: parent directory containing images to load
        :param b_no_uncertain: by default only images annotated as "keep" are kept for training;\
            if this bool is set to "true", also images annotated as "uncertain" will be considered.
        :param b_no_neutral: ignore images that were annotated with no emotion selected (i.e, "neutral" emotion).
        :param b_one_hot_target: targets should be returned as one-hot encoded tensors instead of indices
        :param b_only_main_emos: map 24 emotions to 8 main emotions
        :param train_test_split: ratio of images to be kept as training data
        :param max_samples: maximum number of samples to load
        """
        print(f"AnnotationBufferedEmoSLDatasetFactory: Reading data...")
        if base_dir is None:
            base_dir = Config.DIR_IMAGES

        ann = cls.get_anns(ann_file=ann_file, b_filter=b_filter)

        data = defaultdict(set)
        for i, row in ann.iterrows():
            # Check image file exists, else ignore
            full_path = os.path.join(base_dir, *row.image_path.split('/'))
            if not os.path.isfile(full_path):
                continue

            if row.reject == 'Reject' or (not b_no_uncertain and row.reject == 'Uncertain'):
                continue
            elif b_no_neutral and row.emotion == "Undefined":
                continue

            if b_only_main_emos:
                data[row.image_path].add(AnnotationEmotionMap.EMOTION_LEAF_MAP_EXT[row.emotion])
            else:
                data[row.image_path].add(AnnotationEmotionMap.EMOTION_MAP_EXT[row.emotion])

        data_files, labels = [], []
        for k, v in data.items():
            # Image has been annotated with more than one emotion? Then ignore.
            if len(v) > 1:
                continue
            data_files.append(k)
            labels.append(list(v)[0])

        # # Shuffle data
        # data_files, labels = AnnotationDatasetTools.shuffle_data(data_files, labels)
        #
        # nb_imgs = len(data_files)
        # if 0 < max_samples < nb_imgs:
        #     nb_imgs = max_samples
        # max_train_idx = int(nb_imgs * train_test_split)
        # train_img, train_target, test_img, test_target = [], [], [], []
        # for i in range(nb_imgs):
        #     if i < max_train_idx:
        #         train_img.append(data_files[i])
        #         train_target.append(labels[i])
        #     else:
        #         test_img.append(data_files[i])
        #         test_target.append(labels[i])
        train_img, train_target, test_img, test_target = SplitTrainTest.get_balanced_per_label(data_files, labels,
                                                                                               train_test_split=train_test_split)

        train_target = np.array(train_target)
        test_target = np.array(test_target)

        # Load precomputed features
        if model_name == 'emonet':
            buffered_feats = BufferENDataset().get_subset_for_imgs(img_paths=data_files)
        elif model_name.startswith('places365'):
            parts = model_name.split('-')
            if len(parts) != 2:
                raise ValueError("When using a Places365 model, you should use the parameter format:\n"
                                 "'model_name=places365-xxx', where 'xxx' should replaced by the places365 model to use.\n"
                                 "E.g., 'model_name=places365-feats_alexnet'.")
            buffered_feats = BufferPlaces365Dataset(places365_model=parts[1]).get_subset_for_imgs(img_paths=data_files)
        # Load precomputed ImageNet features
        else:
            buffered_feats = BufferINDataset(in_model_name=model_name).get_subset_for_imgs(img_paths=data_files)
        buffered_feats_train = {x: buffered_feats[x] for x in train_img}
        buffered_feats_test = {x: buffered_feats[x] for x in test_img}

        if b_one_hot_target:
            if b_only_main_emos:
                target_transform = transforms.Compose([OneHot(nb_classes=8 if b_no_neutral else 9)])
            else:
                target_transform = transforms.Compose([OneHot(nb_classes=24 if b_no_neutral else 25)])
        else:
            target_transform = transforms.Compose([torch.as_tensor])

        data_train = AnnotationBufferedEmoSLDataset(xs=train_img, ys=train_target, buffered_feats=buffered_feats_train,
                                                    target_transform=target_transform, base_dir=base_dir)
        data_test = AnnotationBufferedEmoSLDataset(xs=test_img, ys=test_target, buffered_feats=buffered_feats_test,
                                                   target_transform=target_transform, base_dir=base_dir)

        return data_train, data_test


class AnnotationBufferedEmoSLDataset(torch.utils.data.Dataset):
    """
    PyTorch Dataset containing buffered ImageNet features.
    """

    def __init__(self, xs, ys, buffered_feats: dict, base_dir=None, target_transform=None):
        """

        :param xs: array containing the paths to the images to use
        :param ys: array containing the target values
        :param buffered_feats: dictionary containing buffered ImageNet features
        :param base_dir: base dir from which the image paths will be taken
        :param target_transform: transform to be applied to the target values
        """
        if base_dir is None:
            self.base_dir = Config.DIR_IMAGES
        else:
            self.base_dir = base_dir

        self.buffered_feats = buffered_feats

        if target_transform is None:
            self.target_transform = transforms.Compose([torch.as_tensor])
        else:
            self.target_transform = target_transform

        self.xs = xs
        self.ys = ys

    def __getitem__(self, item: int) -> (torch.Tensor, torch.Tensor):
        """
        Retrieves an item from the dataset.

        :param item: Item index.
        :return: Return item in the form (x, y), where x is a vector of features, and y are the ground truth values to
        be predicted.
        """
        img_path = self.xs[item]
        return self.buffered_feats[img_path], self.target_transform(self.ys[item])

    def __len__(self) -> int:
        """
        Return the length of the dataset.

        :return: The length of the dataset.
        """
        assert len(self.xs) == len(self.ys)
        return len(self.ys)


if __name__ == '__main__':
    # train, test = AnnotationBufferedEmoSLDatasetFactory.get_train_test_datasets(
    #     model_name='emonet', max_samples=20)
    train, test = AnnotationBufferedEmoSLDatasetFactory.get_train_test_datasets(
        ann_file=Config.FILE_ANN_SINGLE, base_dir=Config.DIR_IMAGES,
        model_name=torchvision.models.vgg16.__name__, max_samples=20, b_only_main_emos=True)

    for i in range(10):
        print(train.__getitem__(i))
