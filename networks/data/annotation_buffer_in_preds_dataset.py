"""
Annotation dataset that loads all annotations, to be used to buffer the ImageNet model predictions.
Used by networks.nn.buffer.buffer_in_predictions.py

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""

import torch

from config import Config
from networks.data.annotation_dataset_tools import AnnotationDatasetTools
from networks.data.base_dataset import BaseDataset
from networks.data.buffer_en_dataset import BufferENDataset
from networks.data.buffer_in_dataset import BufferINDataset


class AnnotationBufferINPredictionsDatasetFactory(BaseDataset):
    """
    Create PyTorch dataset train/test objects, based on emotion annotations of scraped images.
    """

    @classmethod
    def get_dataset(cls,
                    model_name,
                    ann_file=None,
                    b_filter=False,
                    base_dir=None,
                    max_samples=-1):
        """
        Read data and put it into datasets.

        :param model_name: name of ImageNet model for which to use precomputed features, or 'emonet' for EmoNet
        :param ann_file: annotation csv file to use
        :param b_filter: filter annotations?
        :param base_dir: parent directory containing images to load
        :param max_samples: maximum number of samples to load
        """
        print(f"AnnotationBufferINPredictionsDatasetFactory: Reading data...")
        if base_dir is None:
            base_dir = Config.DIR_IMAGES

        ann = cls.get_anns(ann_file=ann_file, b_filter=b_filter)

        data_files = list(ann.image_path.unique())
        AnnotationDatasetTools.filter_missing_images(base_dir=base_dir, data_files=data_files)

        if 0 < max_samples < len(data_files):
            data_files = data_files[:max_samples]

        # Load precomputed EmoNet features
        if model_name == 'emonet':
            buffered_feats = BufferENDataset().get_subset_for_imgs(img_paths=data_files)
        # Load precomputed ImageNet features
        else:
            buffered_feats = BufferINDataset(in_model_name=model_name).get_subset_for_imgs(img_paths=data_files)

        data = AnnotationBufferINPredictionsDataset(xs=data_files, buffered_feats=buffered_feats, base_dir=base_dir)

        return data


class AnnotationBufferINPredictionsDataset(torch.utils.data.Dataset):
    """
    PyTorch Dataset containing buffered ImageNet features.
    """

    def __init__(self, xs, buffered_feats: dict, base_dir=None):
        """

        :param xs: array containing the paths to the images to use
        :param buffered_feats: dictionary containing buffered ImageNet features
        :param base_dir: base dir from which the image paths will be taken
        """
        if base_dir is None:
            self.base_dir = Config.DIR_IMAGES
        else:
            self.base_dir = base_dir

        self.buffered_feats = buffered_feats

        self.xs = xs

    def __getitem__(self, item: int) -> (torch.Tensor, torch.Tensor):
        """
        Retrieves an item from the dataset.

        :param item: Item index.
        :return: Return item in the form (x, y), where x is a vector of features, and y is the corresponding image path.
        """
        img_path = self.xs[item]
        return self.buffered_feats[img_path], img_path

    def __len__(self) -> int:
        """
        Return the length of the dataset.

        :return: The length of the dataset.
        """
        assert len(self.xs) == len(self.buffered_feats)
        return len(self.xs)
