"""
Class to load precomputed ImageNet network features.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import os

import dill
import torchvision

from config import Config
from networks.data.base_buffer_dataset import BaseBufferDataset


class BufferINPredictionsDataset(BaseBufferDataset):
    def __init__(self, imagenet_model, nb_targets=8, target='emo', root_dir=None):
        super().__init__()
        if root_dir is None:
            root_dir = Config.DIR_BUFFER_IN
        if imagenet_model is None:
            raise ValueError("Model name not specified.")

        if isinstance(imagenet_model, str):
            model_name = imagenet_model
        else:
            model_name = imagenet_model.__name__

        print(f"Loading BufferINPredictionsDataset for model {model_name}...", end='')
        file_feats = os.path.join(root_dir, f"buffer_{model_name}_{target}{nb_targets}.dill")
        self.feats_per_img = dill.load(open(file_feats, 'rb'))
        print(" done!")

    def get_subset_for_imgs(self, img_paths: list):
        """
        Get a subset of full dataset for image paths provided.

        :param img_paths:
        :return: dictionary containing subset of full dataset
        """
        return {x: self.feats_per_img[x] for x in img_paths}

    def get_feats_for_img(self, img_path: str):
        """
        Return the ImageNet feats for this particular image.

        :param img_path: relative image path, relative to the root image directory.
        :return: buffered features for this image
        """
        return self.feats_per_img[img_path]


if __name__ == '__main__':
    boid = BufferINPredictionsDataset(imagenet_model=torchvision.models.vgg16)
