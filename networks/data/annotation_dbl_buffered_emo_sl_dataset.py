"""
Variant of annotation_emo_sl_double_dataset.py that uses pre-computed ImageNet features and OIToFER features.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import os
from collections import defaultdict

import numpy as np
import torch
import torchvision.models
from torchvision import transforms

from config import Config
from networks.data.annotation_dataset_tools import AnnotationEmotionMap, OneHot
from networks.data.base_buffer_dataset import BaseBufferDataset
from networks.data.base_dataset import BaseDataset
from networks.data.buffer_en_dataset import BufferENDataset
from networks.data.buffer_in_preds_dataset import BufferINPredictionsDataset
from networks.data.split_train_test import SplitTrainTest


class AnnotationDblBufferedEmoSLDoubleDatasetFactory(BaseDataset):
    """
    Create PyTorch dataset train/test objects, based on emotion annotations of scraped images.
    """
    @classmethod
    def get_train_test_datasets(cls,
                                buffered_feats_stream1: BaseBufferDataset,
                                buffered_feats_stream2: BaseBufferDataset,
                                buffered_feats_stream1_params=None,
                                buffered_feats_stream2_params=None,
                                ann_file=None,
                                b_filter=False,
                                base_dir=None,
                                b_no_uncertain=True,
                                b_no_neutral=False,
                                b_one_hot_target=False,
                                b_only_main_emos=False,
                                train_test_split=0.8,
                                max_samples=-1):
        """
        Read data and put it into datasets.

        :param buffered_feats_stream1: Class that will contain the buffered features for the first data stream
        :param buffered_feats_stream2: Class that will contain the buffered features for the second data stream
        :param buffered_feats_stream1_params: parameters to pass on to buffered_feats_stream1
        :param buffered_feats_stream2_params: parameters to pass on to buffered_feats_stream2
        :param ann_file: annotation csv file to use
        :param b_filter: filter annotations?
        :param base_dir: parent directory containing images to load
        :param b_no_uncertain: by default only images annotated as "keep" are kept for training;
            if this bool is set to "true", also images annotated as "uncertain" will be considered.
        :param b_no_neutral: ignore images that were annotated with no emotion selected (i.e, "neutral" emotion).
        :param b_one_hot_target: targets should be returned as one-hot encoded tensors instead of indices
        :param b_only_main_emos: map 24 emotions to 8 main emotions
        :param train_test_split: ratio of images to be kept as training data
        :param max_samples: maximum number of samples to load
        """
        print(f"AnnotationDblBufferedEmoSLDoubleDatasetFactory: Reading data...")
        if base_dir is None:
            base_dir = Config.DIR_IMAGES

        ann = cls.get_anns(ann_file=ann_file, b_filter=b_filter)

        data = defaultdict(set)
        for i, row in ann.iterrows():
            # Check image file exists, else ignore
            full_path = os.path.join(base_dir, *row.image_path.split('/'))
            if not os.path.isfile(full_path):
                continue

            if row.reject == 'Reject' or (not b_no_uncertain and row.reject == 'Uncertain'):
                continue
            elif b_no_neutral and row.emotion == "Undefined":
                continue

            if b_only_main_emos:
                data[row.image_path].add(AnnotationEmotionMap.EMOTION_LEAF_MAP_EXT[row.emotion])
            else:
                data[row.image_path].add(AnnotationEmotionMap.EMOTION_MAP_EXT[row.emotion])
        data_files, labels = [], []
        for k, v in data.items():
            # Image has been annotated with more than one emotion? Then ignore.
            if len(v) > 1:
                continue
            data_files.append(k)
            labels.append(list(v)[0])

        # Below is the old way of getting train/test data; does not take samples per label into account
        # # Shuffle data
        # data_files, labels = AnnotationDatasetTools.shuffle_data(data_files, labels)
        #
        # nb_imgs = len(data_files)
        # if 0 < max_samples < nb_imgs:
        #     nb_imgs = max_samples
        # max_train_idx = int(nb_imgs * train_test_split)
        # train_img, train_target, test_img, test_target = [], [], [], []
        # for i in range(nb_imgs):
        #     if i < max_train_idx:
        #         train_img.append(data_files[i])
        #         train_target.append(labels[i])
        #     else:
        #         test_img.append(data_files[i])
        #         test_target.append(labels[i])

        train_img, train_target, test_img, test_target = SplitTrainTest.get_balanced_per_label(data_files, labels,
                                                                                               train_test_split=train_test_split)

        train_target = np.array(train_target)
        test_target = np.array(test_target)

        # Load precomputed features for first stream
        if buffered_feats_stream1_params is None:
            buffered_feats_stream1_params = dict()
        buffered_feats_stream1 = buffered_feats_stream1(**buffered_feats_stream1_params).get_subset_for_imgs(img_paths=data_files)
        buffered_feats_stream1_train = {x: buffered_feats_stream1[x] for x in train_img}
        buffered_feats_stream1_test = {x: buffered_feats_stream1[x] for x in test_img}

        # Load precomputed features for second stream
        if buffered_feats_stream2_params is None:
            buffered_feats_stream2_params = dict()
        buffered_feats_stream2 = buffered_feats_stream2(**buffered_feats_stream2_params).get_subset_for_imgs(img_paths=data_files)
        buffered_feats_stream2_train = {x: buffered_feats_stream2[x] for x in train_img}
        buffered_feats_stream2_test = {x: buffered_feats_stream2[x] for x in test_img}

        if b_one_hot_target:
            if b_only_main_emos:
                target_transform = transforms.Compose([OneHot(nb_classes=8 if b_no_neutral else 9)])
            else:
                target_transform = transforms.Compose([OneHot(nb_classes=24 if b_no_neutral else 25)])
        else:
            target_transform = transforms.Compose([torch.as_tensor])

        data_train = AnnotationDblBufferedEmoSLDoubleDataset(xs=train_img, ys=train_target, buffered_feats_1=buffered_feats_stream1_train,
                                                             buffered_feats_2=buffered_feats_stream2_train,
                                                             target_transform=target_transform, base_dir=base_dir)
        data_test = AnnotationDblBufferedEmoSLDoubleDataset(xs=test_img, ys=test_target, buffered_feats_1=buffered_feats_stream1_test,
                                                            buffered_feats_2=buffered_feats_stream2_test,
                                                            target_transform=target_transform, base_dir=base_dir)

        return data_train, data_test


class AnnotationDblBufferedEmoSLDoubleDataset(torch.utils.data.Dataset):
    """
    PyTorch Dataset containing scraped image data.
    """

    def __init__(self, xs, ys, buffered_feats_1: dict, buffered_feats_2: dict,
                 base_dir=None, transforms=None, target_transform=None):
        """

        :param xs: array containing the paths to the images to use
        :param ys: array containing the target values
        :param buffered_feats_1: dictionary containing buffered features for first stream
        :param buffered_feats_2: dictionary containing buffered features for second stream
        :param base_dir: base dir from which the image paths will be taken
        :param transforms: transforms to be applied to images
        :param target_transform: transform to be applied to the target values
        """
        if base_dir is None:
            self.base_dir = Config.DIR_IMAGES
        else:
            self.base_dir = base_dir

        self.buffered_feats_1 = buffered_feats_1
        self.buffered_feats_2 = buffered_feats_2

        if target_transform is None:
            self.target_transform = transforms.Compose([torch.as_tensor])
        else:
            self.target_transform = target_transform

        self.xs = xs
        self.ys = ys

    def __getitem__(self, item: int) -> (torch.Tensor, torch.Tensor):
        """
        Retrieves an item from the dataset.

        :param item: Item index.
        :return: Return item in the form (s, t, u, v), where s are the pre-computed ImageNet features,
         t are the pre-computed OIToFER features, u is the image path, and v is the target
        be predicted.
        """
        img_path = self.xs[item]
        return self.buffered_feats_1[img_path], self.buffered_feats_2[img_path], img_path, self.target_transform(self.ys[item])

    def __len__(self) -> int:
        """
        Return the length of the dataset.

        :return: The length of the dataset.
        """
        assert len(self.xs) == len(self.ys)
        return len(self.ys)


if __name__ == '__main__':
    train, test = AnnotationDblBufferedEmoSLDoubleDatasetFactory.get_train_test_datasets(buffered_feats_stream1=BufferINPredictionsDataset,
                                                                                         buffered_feats_stream2=BufferENDataset,
                                                                                         buffered_feats_stream1_params={
                                                                                             'imagenet_model': torchvision.models.vgg16},
                                                                                         max_samples=20)

    for i in range(10):
        print(train.__getitem__(i))
