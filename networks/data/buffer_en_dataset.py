"""
Class to load precomputed EmoNet network features.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import os

import dill

from config import Config
from networks.data.base_buffer_dataset import BaseBufferDataset


class BufferENDataset(BaseBufferDataset):
    """
    Buffer EmoNet Dataset

    """
    def __init__(self, file_feats=None):
        super().__init__()
        print(f"Loading BufferEmoNetDataset...", end='')
        if file_feats is None:
            file_feats = os.path.join(Config.DIR_BUFFER_EMONET, 'buffer_emonet.dill')
        self.feats_per_img = dill.load(open(file_feats, 'rb'))
        print(" done!")

    def get_subset_for_imgs(self, img_paths: list):
        """
        Get a subset of full dataset for image paths provided.

        :param img_paths:
        :return: dictionary containing subset of full dataset
        """
        return {x: self.feats_per_img[x] for x in img_paths}

    def get_feats_for_img(self, img_path: str):
        """
        Return the ImageNet feats for this particular image.

        :param img_path: relative image path, relative to the root image directory.
        :return: buffered features for this image
        """
        return self.feats_per_img[img_path]


if __name__ == '__main__':
    bend = BufferENDataset()
