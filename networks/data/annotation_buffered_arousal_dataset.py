"""
Variant of annotation_arousal_dataset.py that uses pre-computed ImageNet features.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import os
from collections import defaultdict

import PIL
import numpy as np
import torch
from lor_tools.search import binary_search
from torchvision import transforms

from config import Config
from networks.data.base_dataset import BaseDataset
from networks.data.buffer_en_dataset import BufferENDataset
from networks.data.buffer_in_dataset import BufferINDataset
from networks.data.buffer_in_preds_dataset import BufferINPredictionsDataset
from networks.data.buffer_places365_dataset import BufferPlaces365Dataset
from networks.data.split_train_test import SplitTrainTest


class AnnotationBufferedArousalDatasetFactory(BaseDataset):
    """
    Create PyTorch dataset train/test objects, based on "arousal" annotations of scraped images.
    """
    @classmethod
    def get_train_test_datasets(cls,
                                model_name,
                                b_classifier_l1=False,
                                ann_file=None,
                                b_filter=False,
                                base_dir=None,
                                b_no_uncertain=True,
                                b_no_neutral=False,
                                train_test_split=0.8,
                                bins=None,
                                max_samples=-1):
        """
        Read data and put it into datasets.

        :param model_name: name of ImageNet model for which to use precomputed features, 'emonet' for EmoNet, \
         'places365' for Places365 or name ending in '_sin' for Stylized ImageNet
        :param b_classifier_l1: use buffered features of first classifier layer for AlexNet, VGG16 or VGG19, instead\
            of second classifier layer (=default)
        :param ann_file: path to the annotation file to be used
        :param b_filter: filter annotations or not?
        :param base_dir: parent directory containing images to load
        :param b_no_uncertain: by default only images annotated as "keep" are kept for training;
            if this bool is set to "true", also images annotated as "uncertain" will be considered.
        :param b_no_neutral: ignore images that were annotated with no emotion selected (i.e, "neutral" emotion).
        :param train_test_split: ratio of images to be kept as training data
        :param bins: list of bin edges; default = None = each arousal value is a category label
        :param max_samples: maximum number of samples to load
        """
        print(f"AnnotationBufferedArousalDatasetFactory: Reading data...")
        if base_dir is None:
            base_dir = Config.DIR_IMAGES

        ann = cls.get_anns(ann_file=ann_file, b_filter=b_filter)

        data = defaultdict(set)
        for i, row in ann.iterrows():
            # Check image file exists, else ignore
            full_path = os.path.join(base_dir, *row.image_path.split('/'))
            if not os.path.isfile(full_path):
                continue

            if b_no_neutral and row.emotion == "Undefined":
                continue

            aro = row.arousal
            if bins is None:
                data[row.image_path].add(aro)
            else:
                bin_idx = binary_search.BinarySearch.find(bins, aro, b_return_pos=True)
                if bin_idx < 0:
                    bin_idx = (-bin_idx)-1
                data[row.image_path].add(bin_idx)

        data_files, labels = [], []
        for k, v in data.items():
            # Image has been annotated with more than one valence value? Then ignore for now.
            if len(v) > 1:
                continue
            data_files.append(k)
            labels.append(list(v)[0])

        train_img, train_target, test_img, test_target = SplitTrainTest.get_balanced_per_label(data_files, labels,
                                                                                               train_test_split=train_test_split)

        train_target = np.array(train_target)
        test_target = np.array(test_target)

        # Load precomputed features
        if model_name == 'emonet':
            buffered_feats = BufferENDataset().get_subset_for_imgs(img_paths=data_files)
        elif model_name.startswith('places365'):
            parts = model_name.split('-')
            if len(parts) != 2:
                raise ValueError("When using a Places365 model, you should use the parameter format:\n"
                                 "'model_name=places365-xxx', where 'xxx' should replaced by the places365 model to use.\n"
                                 "E.g., 'model_name=places365-feats_alexnet'.")
            buffered_feats = BufferPlaces365Dataset(places365_model=parts[1]).get_subset_for_imgs(img_paths=data_files)
        # Load precomputed ImageNet features
        elif model_name.endswith('-emo8'):
            buffered_feats = (BufferINPredictionsDataset(imagenet_model=model_name[:-5], nb_targets=8, target='emo')
                              .get_subset_for_imgs(img_paths=data_files))
        else:
            buffered_feats = BufferINDataset(in_model_name=model_name).get_subset_for_imgs(img_paths=data_files)

        buffered_feats_train = {x: buffered_feats[x] for x in train_img}
        buffered_feats_test = {x: buffered_feats[x] for x in test_img}

        data_train = AnnotationBufferedArousalDataset(xs=train_img, ys=train_target, buffered_feats=buffered_feats_train,
                                                      base_dir=base_dir)
        data_test = AnnotationBufferedArousalDataset(xs=test_img, ys=test_target, buffered_feats=buffered_feats_test,
                                                     base_dir=base_dir)

        return data_train, data_test


class AnnotationBufferedArousalDataset(torch.utils.data.Dataset):
    """
    PyTorch Dataset containing scraped image data.
    """

    def __init__(self, xs, ys, buffered_feats: dict, base_dir=None):
        """

        :param xs: array containing the paths to the images to use
        :param ys: array containing the target values
        :param base_dir: base dir from which the image paths will be taken
        """
        if base_dir is None:
            self.base_dir = Config.DIR_IMAGES
        else:
            self.base_dir = base_dir

        self.buffered_feats = buffered_feats

        self.target_transform = transforms.Compose([torch.as_tensor])

        self.xs = xs
        self.ys = ys

    def __getitem__(self, item: int) -> (torch.Tensor, torch.Tensor):
        """
        Retrieves an item from the dataset.

        :param item: Item index.
        :return: Return item in the form (x, y), where x is a vector of features, and y are the ground truth values to
        be predicted.
        """
        img_path = self.xs[item]
        return self.buffered_feats[img_path], self.target_transform(self.ys[item])

    def __len__(self) -> int:
        """
        Return the length of the dataset.

        :return: The length of the dataset.
        """
        assert len(self.xs) == len(self.ys)
        return len(self.xs)

    def pil_loader(self, path: str) -> PIL.Image.Image:
        # open path as file to avoid ResourceWarning (https://github.com/python-pillow/Pillow/issues/835)
        full_path = os.path.join(self.base_dir, *path.split('/'))
        with open(full_path, 'rb') as f:
            img = PIL.Image.open(f)
            return img.convert('RGB')
