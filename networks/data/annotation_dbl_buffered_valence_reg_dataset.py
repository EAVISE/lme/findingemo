"""
Dataset to combine 2 feature streams for regression problems.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
from collections import defaultdict

import numpy as np
import torch
import torchvision

from networks.data.base_buffer_dataset import BaseBufferDataset
from networks.data.base_dataset import BaseDataset
from networks.data.buffer_en_dataset import BufferENDataset
from networks.data.buffer_preds_dataset import BufferPredictionsDataset
from networks.data.preprocessing.limit_range_preproc import LimitRange
from networks.data.split_train_test import SplitTrainTest


class AnnotationDblBufferedValenceDoubleDatasetRegFactory(BaseDataset):
    """
    Create PyTorch dataset train/test objects, based on "valence" annotations of scraped images.
    """
    @classmethod
    def get_train_test_datasets(cls,
                                buffered_feats_stream1: BaseBufferDataset,
                                buffered_feats_stream2: BaseBufferDataset,
                                buffered_feats_stream1_params=None,
                                buffered_feats_stream2_params=None,
                                ann_file=None,
                                b_filter=False,
                                b_no_uncertain=True,
                                min=-3.0,
                                max=3.0,
                                train_test_split=0.8,
                                max_samples=-1):
        """
        Read data and put it into datasets.

        :param buffered_feats_stream1: Class that will contain the buffered features for the first data stream
        :param buffered_feats_stream2: Class that will contain the buffered features for the second data stream
        :param buffered_feats_stream1_params: parameters to pass on to buffered_feats_stream1
        :param buffered_feats_stream2_params: parameters to pass on to buffered_feats_stream2
        :param ann_file: path to the annotation file to be used
        :param b_filter: filter annotations or not?
        :param b_no_uncertain: by default only images annotated as "keep" are kept for training;
            if this bool is set to "true", also images annotated as "uncertain" will be considered.
        :param min: minimum range value of labels
        :param min: maximum range value of labels
        :param train_test_split: ratio of images to be kept as training data
        :param max_samples: maximum number of samples to load
        """
        print(f"AnnotationDblBufferedValenceDoubleDatasetRegFactory: Reading data...")

        ann = cls.get_anns(ann_file=ann_file, b_filter=b_filter)

        # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        # We have to be careful when loading the annotations. Some
        # images have been annotated more than once.
        data = defaultdict(set)
        for i, row in ann.iterrows():
            if row.reject == 'Reject' or (not b_no_uncertain and row.reject == 'Uncertain'):
                continue

            val = row.valence
            data[row.image_path].add(val)

        data_files, labels = [], []
        for k, v in data.items():
            # Image has been annotated with more than one valence value? Then ignore.
            if len(v) > 1:
                continue
            data_files.append(k)
            labels.append(list(v)[0])

        train_img, train_target, test_img, test_target = SplitTrainTest.get_balanced_per_label(
            data_files, labels, train_test_split=train_test_split)

        train_target = np.asarray(train_target)
        test_target = np.asarray(test_target)

        # Load precomputed features for first stream
        if buffered_feats_stream1_params is None:
            buffered_feats_stream1_params = dict()
        buffered_feats_stream1 = buffered_feats_stream1(**buffered_feats_stream1_params).get_subset_for_imgs(img_paths=data_files)
        buffered_feats_stream1_train = {x: buffered_feats_stream1[x] for x in train_img}
        buffered_feats_stream1_test = {x: buffered_feats_stream1[x] for x in test_img}

        # Load precomputed features for second stream
        if buffered_feats_stream2_params is None:
            buffered_feats_stream2_params = dict()
        buffered_feats_stream2 = buffered_feats_stream2(**buffered_feats_stream2_params).get_subset_for_imgs(img_paths=data_files)
        buffered_feats_stream2_train = {x: buffered_feats_stream2[x] for x in train_img}
        buffered_feats_stream2_test = {x: buffered_feats_stream2[x] for x in test_img}

        target_transform = LimitRange(min=min, max=max)

        data_train = AnnotationDblBufferedValenceDoubleDataset(xs=train_img, ys=train_target, buffered_feats_1=buffered_feats_stream1_train,
                                                             buffered_feats_2=buffered_feats_stream2_train, target_transform=target_transform)
        data_test = AnnotationDblBufferedValenceDoubleDataset(xs=test_img, ys=test_target, buffered_feats_1=buffered_feats_stream1_test,
                                                            buffered_feats_2=buffered_feats_stream2_test, target_transform=target_transform)

        return data_train, data_test


class AnnotationDblBufferedValenceDoubleDataset(torch.utils.data.Dataset):
    """
    PyTorch Dataset containing scraped image data.
    """

    def __init__(self, xs, ys, buffered_feats_1: dict, buffered_feats_2: dict,
                 transforms=None, target_transform=None):
        """

        :param xs: array containing the paths to the images to use
        :param ys: array containing the target values
        :param buffered_feats_1: dictionary containing buffered features for first stream
        :param buffered_feats_2: dictionary containing buffered features for second stream
        :param transforms: transforms to be applied to images
        :param target_transform: transform to be applied to the target values
        """
        self.buffered_feats_1 = buffered_feats_1
        self.buffered_feats_2 = buffered_feats_2

        if target_transform is None:
            self.target_transform = transforms.Compose([torch.as_tensor])
        else:
            self.target_transform = target_transform

        self.xs = xs
        self.ys = ys

    def __getitem__(self, item: int) -> (torch.Tensor, torch.Tensor):
        """
        Retrieves an item from the dataset.

        :param item: Item index.
        :return: Return item in the form (s, t, u, v), where s are the pre-computed ImageNet features,
         t are the pre-computed OIToFER features, u is the image path, and v is the target
        be predicted.
        """
        img_path = self.xs[item]
        return self.buffered_feats_1[img_path], self.buffered_feats_2[img_path], img_path, self.target_transform(self.ys[item])

    def __len__(self) -> int:
        """
        Return the length of the dataset.

        :return: The length of the dataset.
        """
        assert len(self.xs) == len(self.ys)
        return len(self.ys)


if __name__ == '__main__':
    train, test = AnnotationDblBufferedValenceDoubleDatasetRegFactory.get_train_test_datasets(
        buffered_feats_stream1=BufferPredictionsDataset,
        buffered_feats_stream2=BufferENDataset,
        buffered_feats_stream1_params={'imagenet_model': torchvision.models.vgg16},
        max_samples=20)

    for i in range(10):
        print(train.__getitem__(i))
