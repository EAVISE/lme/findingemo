"""
Class to load precomputed DINOv2 network features.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import os

import dill

from config import Config
from networks.data.base_buffer_dataset import BaseBufferDataset


class BufferDINOv2Dataset(BaseBufferDataset):
    """
    Buffer DINOv2 features dataset

    """
    def __init__(self, file_feats=None, root_dir=None):
        super().__init__()
        print(f"Loading BufferDINOv2Dataset...", end='')
        if root_dir is None:
            root_dir = Config.DIR_BUFFER_DINOv2

        if file_feats is None:
            # file_feats = os.path.join(root_dir, 'buffer_dinov2_vits14_reg_lc_feats.dill')
            file_feats = os.path.join(root_dir, 'buffer_dinov2_vitb14_reg_lc_feats.dill')

        self.feats_per_img = dill.load(open(file_feats, 'rb'))
        print(f" done!\n\tLoaded file: {file_feats}")

    def get_subset_for_imgs(self, img_paths: list):
        """
        Get a subset of full dataset for image paths provided.

        :param img_paths:
        :return: dictionary containing subset of full dataset
        """
        return {x: self.feats_per_img[x] for x in img_paths}

    def get_feats_for_img(self, img_path: str):
        """
        Return the ImageNet feats for this particular image.

        :param img_path: relative image path, relative to the root image directory.
        :return: buffered features for this image
        """
        return self.feats_per_img[img_path]


if __name__ == '__main__':
    dinov2 = BufferDINOv2Dataset()
