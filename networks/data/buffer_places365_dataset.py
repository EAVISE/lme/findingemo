"""
Class to load precomputed Places365 network features.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import os

import dill

from config import Config
from networks.data.base_buffer_dataset import BaseBufferDataset
from networks.nn.places365.places365_model import Places365Model


class BufferPlaces365Dataset(BaseBufferDataset):
    def __init__(self, places365_model: str, root_dir=None):
        super().__init__()
        if root_dir is None:
            root_dir = Config.DIR_BUFFER_PLACES365
        if places365_model is None:
            raise ValueError("Model name not specified.")

        print(f"Loading BufferPlaces365Dataset for model places365_{places365_model}...", end='')
        file_feats = os.path.join(root_dir, f"buffer_places365_{places365_model}.dill")
        self.feats_per_img = dill.load(open(file_feats, 'rb'))
        print(" done!")

    def get_subset_for_imgs(self, img_paths: list):
        """
        Get a subset of full dataset for image paths provided.

        :param img_paths:
        :return: dictionary containing subset of full dataset
        """
        return {x: self.feats_per_img[x] for x in img_paths}

    def get_feats_for_img(self, img_path: str):
        """
        Return the ImageNet feats for this particular image.

        :param img_path: relative image path, relative to the root image directory.
        :return: buffered features for this image
        """
        return self.feats_per_img[img_path]


if __name__ == '__main__':
    boid = BufferPlaces365Dataset(places365_model=Places365Model.ALEXNET)
