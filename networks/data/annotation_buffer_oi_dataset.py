"""
Annotation dataset that loads all annotations, to be used to buffer the YoLo v3 + OpenImages object predictions.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import os

import PIL
import torch
from torchvision import transforms

from config import Config
from networks.data.annotation_dataset_tools import AnnotationDatasetTools
from networks.data.base_dataset import BaseDataset
from networks.data.preprocessing.img_resize_preproc import ImgResize


class AnnotationBufferOIDatasetFactory(BaseDataset):
    @classmethod
    def get_dataset(cls,
                    ann_file=None,
                    b_filter=False,
                    base_dir=None,
                    filter_images: set=None,
                    max_samples=-1,
                    resize_width=608,
                    resize_height=608):
        """
        Read data and put it into datasets.

        :param ann_file: annotation csv file to use
        :param b_filter: filter annotations?
        :param base_dir: parent directory containing images to load
        :param filter_images: a set containing the (relative) paths of images that should not be loaded
        :param max_samples: maximum number of samples to load
        :param resize_width: width to which all images will be resized
        :param resize_height: height to which all images will be resized
        """
        print(f"AnnotationBufferOIDatasetFactory: Reading data...")
        if base_dir is None:
            base_dir = Config.DIR_IMAGES

        if filter_images is None:
            filter_images = set()

        ann = cls.get_anns(ann_file=ann_file, b_filter=b_filter)

        data_files = list(set(ann.image_path.unique()).difference(filter_images))
        AnnotationDatasetTools.filter_missing_images(base_dir=base_dir, data_files=data_files)

        nb_imgs = len(data_files)
        if 0 < max_samples < nb_imgs:
            nb_imgs = max_samples
            data_files = data_files[:nb_imgs]

        img_resize = ImgResize(width=resize_width, height=resize_height)
        transform = transforms.Compose([img_resize])

        data = AnnotationBufferOIDataset(xs=data_files, transform=transform, base_dir=base_dir)

        return data


class AnnotationBufferOIDataset(torch.utils.data.Dataset):
    """
    PyTorch Dataset containing scraped image data.
    """

    def __init__(self, xs, base_dir=None, transform=None):
        """

        :param xs: array containing the paths to the images to use
        :param base_dir: base dir from which the image paths will be taken
        :param transform: transform to be applied to images
        """
        if base_dir is None:
            self.base_dir = Config.DIR_IMAGES
        else:
            self.base_dir = base_dir

        if transform is None:
            self.transform = transforms.Compose([transforms.Resize((608, 608)), transforms.ToTensor()])
        else:
            self.transform = transform

        self.xs = xs

    def __getitem__(self, item: int) -> (torch.Tensor, str):
        """
        Retrieves an item from dataset.

        :param item: Item index.
        :return: Return image as PyTorch tensor.
        """
        img_path = self.xs[item]
        return self.transform(self.pil_loader(img_path)), img_path

    def __len__(self) -> int:
        """
        Return the length of the dataset.

        :return: The length of the dataset.
        """
        return len(self.xs)

    def pil_loader(self, path: str) -> PIL.Image.Image:
        # open path as file to avoid ResourceWarning (https://github.com/python-pillow/Pillow/issues/835)
        full_path = os.path.join(self.base_dir, *path.split('/'))
        with open(full_path, 'rb') as f:
            img = PIL.Image.open(f)
            return img.convert('RGB')


if __name__ == '__main__':
    data = AnnotationBufferOIDatasetFactory.get_dataset(max_samples=0)

    for i in range(100):
        print(data.__getitem__(i))
