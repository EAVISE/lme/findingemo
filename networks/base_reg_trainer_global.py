"""
Contains methods that can be shared across all BaseRegTrainer classes.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import copy
import os
import random
from datetime import datetime

import dill
import numpy as np
import torch

from config import Config
from networks.configs.data_config import DataConfig
from networks.configs.net_config import NetConfig, StopCriterion
from tools.logger import Logger
from tools.print_tools import PrintTools


class BaseRegTrainerGlobal(object):
    def __init__(self, net_config=None, data_config=None, b_default_init=False):
        """

        :param net_config:
        :param data_config:
        :param b_default_init: use default initialization
        """
        if net_config is None:
            self.net_config = NetConfig()
        else:
            self.net_config = net_config

        if data_config is None:
            self.data_config = DataConfig()
        else:
            self.data_config = data_config
        self.device = self.net_config.device

        if self.net_config.model_name is None:
            self.net_config.model_name = self.net_config.network.__name__

        self.logger = Logger(**self.data_config.logger_params)
        self.logger.print(f"Executing {__file__.replace(Config.DIR_CODE, '')}")

        # Make things deterministic
        self.set_random_seed(self.net_config.manual_seed)

        self.b_parallel = False
        self.train_loader, self.test_loader = None, None
        self.model = None

        if b_default_init:
            self.default_init()

    def default_init(self):
        raise NotImplementedError("Inheriting class should implement this method.")

    @staticmethod
    def set_random_seed(seed: int):
        """
        Makes the training deterministic with a fixed random seed.

        :param seed: The seed to use
        :return: none
        """
        if seed is not None:
            torch.manual_seed(seed)
            torch.cuda.manual_seed(seed)
            np.random.seed(seed)
            random.seed(seed)
            torch.backends.cudnn.deterministic = True

    def train(self, b_save_model=False, model_name=None):
        """
        Train the actual network.

        :param b_save_model: True = save model to disk, False = don't. Default = False.
        :param model_name: if the model is saved, this determines the base filename, INCLUDING PATH (not just the filename)!
        """
        self.logger.print("Training model...")
        if b_save_model and model_name is None:
            raise ValueError("Model name should be specified if you want to save the model.")

        # Train the model
        last_avg = 1e6
        best_avg = 1e6
        best_epoch = 0
        best_model = None

        # datetime,model,loss,optimizer,lr,best_epoch,"
        #                                f"#train_samples,train_loss,train_mae,train_spearmanR"
        #                                f"#test_samples,test_loss,test_mae,test_spearmanR,output_file

        best_stats = {'model_name': self.net_config.model_name, 'loss': self.net_config.loss.__class__.__name__,
                      'optimizer': self.net_config.optimizer.__class__.__name__, 'lr': self.net_config.learning_rate,
                      '#train_samples': len(self.train_loader.dataset), '#test_samples': len(self.test_loader.dataset),
                      'output_file': self.logger.logfile.replace(Config.DIR_LOGS, '') if self.logger.logfile is not None else 'None'}
        for epoch in range(1, self.net_config.max_epochs + 1):
            avg_loss, train_mae, train_spearman_r, train_pred_min, train_pred_max,\
                train_pred_avg, train_pred_std = self.train_step(epoch)
            test_avg_loss, test_mae, test_spearman_r, test_pred_min, test_pred_max,\
                test_pred_avg, test_pred_std = self.test_step(epoch, self.test_loader)
            if self.test_loader is not None and self.net_config.stop_criterion == StopCriterion.TEST:
                crit_loss = test_avg_loss
                self.logger.print("Checking stopping criterium using test loss...")
            else:
                crit_loss = avg_loss
                self.logger.print("Checking stopping criterium using train loss...")

            diff = crit_loss - last_avg
            self.logger.print(f"Current avg. loss: {crit_loss:>9.7f}")
            self.logger.print(f"Last avg. loss   : {last_avg:>9.7f}")
            self.logger.print(f"Difference       : {diff:>9.7f}")
            self.logger.print(f"Best avg. loss   : {best_avg:>9.7f}")
            if epoch <= self.net_config.burn_in:
                self.logger.print("Burning in model...")
                best_epoch = epoch
            else:
                if crit_loss < best_avg and -diff > 1e-8:
                    best_avg = crit_loss
                    best_epoch = epoch
                    best_model = copy.deepcopy(self.model)
                    self.logger.print("New best (loss) epoch.")
                    self._update_best_stats(best_stats, epoch,
                                            avg_loss, train_mae, train_spearman_r, train_pred_min, train_pred_max,
                                            train_pred_avg, train_pred_std,
                                            test_avg_loss, test_mae, test_spearman_r, test_pred_min, test_pred_max,
                                            test_pred_avg, test_pred_std)
                elif epoch - best_epoch >= self.net_config.patience:
                    self.logger.print(f"Not doing better for {self.net_config.patience} epochs in a row. Stopping training.")
                    self.model = best_model
                    break
                else:
                    self.logger.print(f"Best epoch was {epoch-best_epoch} of max {self.net_config.patience} epochs ago.")

                last_avg = crit_loss

        self.logger.print(f"Best epoch was epoch {best_epoch} with an average loss of {best_avg:9.7f}.")
        self.logger.print_line(length=90)

        Logger.write_regressor_result_to_csv(best_stats, self.data_config.file_results)
        # Logger.write_conf_mtx(best_stats, self.data_config.file_c_mtxs)

        if b_save_model:
            # Save model
            file_path = os.path.dirname(model_name)
            os.makedirs(file_path, exist_ok=True)

            model_params = model_name + '_' + str(self.net_config.device) + '.pth'
            torch.save(self.model.state_dict(), model_params)
            self.logger.print(f"Model parameters saved to: {model_params}.")

            if hasattr(self.train_loader.dataset, 'transform'):
                transform_path = model_name + '_' + str(self.net_config.device) + '_transform.dill'
                dill.dump(self.train_loader.dataset.transform, open(transform_path, 'wb'))
                self.logger.print(f"Image transform saved to : {transform_path}")

        best_stats['best_model'] = best_model
        best_stats['target_transform'] = self.ttrans
        best_stats['transform'] = None
        if hasattr(self.train_loader.dataset, 'transform'):
            best_stats['transform'] = self.train_loader.dataset.transform

        return best_stats

    @classmethod
    def _update_best_stats(cls, best_stats, epoch, avg_loss, train_mae, train_spearman_r,
                           train_pred_min, train_pred_max, train_pred_avg, train_pred_std,
                           test_avg_loss, test_mae, test_spearman_r,
                           test_pred_min, test_pred_max, test_pred_avg, test_pred_std):
        best_stats['datetime'] = str(datetime.now())
        best_stats['epoch'] = epoch

        best_stats['train_avg'] = avg_loss
        best_stats['train_mae'] = train_mae
        best_stats['train_spearman_r'] = train_spearman_r[0]
        best_stats['train_spearman_r_p'] = train_spearman_r[1]
        best_stats['train_pred_min'] = train_pred_min
        best_stats['train_pred_max'] = train_pred_max
        best_stats['train_pred_avg'] = train_pred_avg
        best_stats['train_pred_std'] = train_pred_std

        best_stats['test_avg'] = test_avg_loss
        best_stats['test_mae'] = test_mae
        best_stats['test_spearman_r'] = test_spearman_r[0]
        best_stats['test_spearman_r_p'] = train_spearman_r[1]
        best_stats['test_pred_min'] = test_pred_min
        best_stats['test_pred_max'] = test_pred_max
        best_stats['test_pred_avg'] = test_pred_avg
        best_stats['test_pred_std'] = test_pred_std

    def test_step(self, epoch: int, loader: torch.utils.data.DataLoader):
        """
        A single epoch in testing.

        :param epoch: The current epoch.
        :param loader: The dataloader to use for testing.
        """
        raise NotImplementedError("Inheriting class should implement this method.")

    def train_step(self, epoch):
        """
        A single epoch in training.

        :param epoch: The current epoch.
        """
        raise NotImplementedError("Inheriting class should implement this method.")
