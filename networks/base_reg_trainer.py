"""
Base Regression trainer.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""

import numpy as np
import sklearn.metrics
import torch
import torch.nn
import torch.optim.lr_scheduler
import torch.utils.data
from scipy.stats import spearmanr
from tqdm import tqdm

from config import Config
from networks.base_reg_trainer_global import BaseRegTrainerGlobal
from networks.nn.class_weights import ClassWeights
from networks.nn.imagenet.imagenet_classifier import get_classifier_for_imagenet
from tools.print_tools import PrintTools
from tools.trainer_tools import TrainerTools


class BaseRegTrainer(BaseRegTrainerGlobal):
    def __init__(self, net_config=None, data_config=None, b_default_init=False):
        """

        :param net_config:
        :param data_config:
        :param b_default_init: use default initialization
        """
        super().__init__(net_config=net_config, data_config=data_config, b_default_init=b_default_init)
        self.logger.print(f"Executing {__file__.replace(Config.DIR_CODE, '')}")

    def default_init(self):
        train_data, test_data = self.data_config.dataset_factory.get_train_test_datasets(**self.data_config.dataset_factory_params)
        self.ttrans = train_data.target_transform

        self.logger.print(f"train_data size: {len(train_data)}")
        self.logger.print(f"test_data size: {len(test_data)}")

        self.train_loader = torch.utils.data.DataLoader(train_data, batch_size=self.net_config.batch_size,
                                                        shuffle=True)
        if self.data_config.dataset_factory_params['train_test_split'] < 1.0:
            self.test_loader = torch.utils.data.DataLoader(test_data, batch_size=self.net_config.batch_size,
                                                           shuffle=False)
        else:
            self.test_loader = None

        # Initialize loss
        if self.net_config.b_set_loss_weights:
            self.logger.print("Using MSELoss Weights...")
            class_weights = ClassWeights.mseloss_weights(dataset=self.train_loader.dataset)
            self.net_config.loss_params['class_weights'] = class_weights
            self.logger.print(f"Class weights: {class_weights}")

        self.net_config.loss = self.net_config.loss(**self.net_config.loss_params)

        if self.net_config.loss.__class__.__name__ == "UnbalancedCrossEntropyLoss":
            raise ValueError("UnbalancedCrossEntropyLoss is not applicable to Regression problems.")

        # Initialize model
        if self.net_config.buffered_in:
            if isinstance(self.net_config.network, str):
                model_name = self.net_config.network
            else:
                model_name = self.net_config.network.__name__

            self.logger.print(f"\tUsing pre-computed ImageNet features...")
            model = get_classifier_for_imagenet(model_name=model_name)
        else:
            model = self.net_config.network
        self.model = model(**self.net_config.network_params).to(self.device)

        self.net_config.set_optimizer(self.model)
        self.net_config.set_lr_scheduler()

        # Freeze convolution weights for param.requires_grad = False
        if self.net_config.b_freeze:
            TrainerTools.freeze_layers(model=self.model,
                                       logger=self.logger,
                                       b_freeze_classifier=self.net_config.freeze_classifier,
                                       depth=self.net_config.imagenet_freeze_depth)

        if self.net_config.change_last_layer:
            # Change the number of output classes, i.e., the last network layer
            TrainerTools.change_last_layer(model=self.model, nb_outputs=self.net_config.nb_outputs, device=self.net_config.device)

        self.logger.print(f"Training model of class: {self.model.__class__.__name__}")

        self.logger.print_line(length=90)
        self.logger.print(f"StopCriterion   : {self.net_config.stop_criterion}")
        self.logger.print(f"Dropout         : {self.net_config.dropout}")
        self.logger.print(f"Loss            : {self.net_config.loss.__class__.__name__}")
        self.logger.print(f"Loss params     : {self.net_config.loss_params}")
        self.logger.print(f"Optimizer       : {self.net_config.optimizer_class.__name__}")
        self.logger.print(f"Optimizer params:")
        for k, v in self.net_config.optimizer.defaults.items():
            self.logger.print(f"\t{k:12s}: {v}")

        self.logger.print(f"\nTraining model of class: {self.model.__class__.__name__}")
        self.logger.print_line(length=90)

    def test_step(self, epoch: int, loader: torch.utils.data.DataLoader):
        """
        A single epoch in testing.

        :param epoch: The current epoch.
        :param loader: The dataloader to use for testing.
        """
        self.model.eval()
        outputs = []
        targets = []
        test_loss = 0.

        with torch.no_grad():
            for batch_index, (data, target) in tqdm(enumerate(loader),
                                                    total=len(loader),
                                                    desc=f'Testing epoch {epoch}:', disable=True):
                data, target = data.to(self.device), target.to(self.device)
                with torch.no_grad():
                    output = self.model(data)

                test_loss += self.net_config.loss(output, target.reshape((-1, 1))).item()

                outputs += output[:, 0].tolist()
                targets += target.tolist()

        test_loss /= len(self.test_loader)
        # self.net_config.lr_scheduler.step(test_loss)

        self.logger.print(f'Testing loss at epoch {epoch}: {test_loss}')
        # trans_avg, trans_std = self._get_trans_avg_std()
        trans_outputs = [self.ttrans.inverse_transform(outputs[i]) for i in range(len(outputs))]
        trans_targets = [self.ttrans.inverse_transform(targets[i]) for i in range(len(targets))]
        avg_per_target, std_per_target =\
            TrainerTools.compute_reg_scores(preds=trans_outputs, targets=trans_targets)
        self.logger.print("Test stats:")
        self.logger.print(PrintTools.print_reg_metrics_mtx(avg_per_target=avg_per_target, std_per_target=std_per_target))
        pred_min = self.ttrans.inverse_transform(np.min(outputs))
        pred_max = self.ttrans.inverse_transform(np.max(outputs))
        pred_avg, pred_std = np.average(trans_outputs), np.std(trans_outputs)
        self.logger.print(f"InvReg Min prediction: {pred_min} (pred: {np.min(outputs):.3f})")
        self.logger.print(f"InvReg Max prediction: {pred_max} (pred: {np.max(outputs):.3f}")
        self.logger.print(f"InvReg Avg./Std.     : {pred_avg} +/- {pred_std}")
        # print(f"Min prediction: {np.min(outputs)}")
        # print(f"Max prediction: {np.max(outputs)}")
        # print(f"Avg./Std.     : {np.average(outputs)} +/- {np.std(outputs)}")
        mae = sklearn.metrics.mean_absolute_error(trans_targets, trans_outputs)
        self.logger.print(f"Mean Absolute Error: {mae}")
        # Confusion matrix with colored diagonal for console printout
        # print(PrintTools.print_confusion_mtx(outputs, targets, nb_classes=self.net_config.nb_outputs, color='green'))
        # Confusion matrix without colored diagonal for logger
        # confusion_mtx = PrintTools.print_confusion_mtx(outputs, targets, nb_classes=self.net_config.nb_outputs, color=None)
        # self.logger.write(confusion_mtx)

        spearman_r = spearmanr(trans_outputs, trans_targets)
        self.logger.print(f"Spearmanr: {spearman_r}")

        return test_loss, mae, spearman_r, pred_min, pred_max, pred_avg, pred_std

    def train_step(self, epoch):
        """
        A single epoch in training.

        :param epoch: The current epoch.
        """
        self.model.train()
        epoch_size = len(self.train_loader)

        outputs, targets = [], []
        total_loss = 0.
        nb_batches = 0
        for batch_index, (data, target) in tqdm(enumerate(self.train_loader),
                                                total=epoch_size,
                                                desc=f'Training epoch {epoch}:', disable=True):
            nb_batches += 1
            data = data.to(self.device)
            # self.logger.print(f"data.shape: {data.shape}")
            # self.logger.print(f"{data[0:,:]}")
            target = target.to(self.device)
            # self.logger.print(f"target.shape: {target.shape}")
            self.net_config.optimizer.zero_grad()
            output = self.model(data)
            # Following is no longer needed since PyTorch v.2... something.
            # if self.net_config.model_name == 'inception_v3':
            #     output = output[0]
            # self.logger.print(f"output.shape: {output.shape}")
            loss = self.net_config.loss(output, target.reshape((-1, 1)))
            loss.backward()
            self.net_config.optimizer.step()

            total_loss += loss.item()

            outputs += output[:, 0].tolist()
            targets += target.tolist()

            if (batch_index+1) % self.net_config.log_interval == 0:
                self.logger.print(f'LR = {self.net_config.optimizer.param_groups[0]["lr"]:.5f}, Current loss = {loss}'
                      f' at batch = {batch_index+1}, epoch = {epoch}.')

        avg_loss = total_loss / nb_batches

        trans_outputs = [self.ttrans.inverse_transform(outputs[i]) for i in range(len(outputs))]
        trans_targets = [self.ttrans.inverse_transform(targets[i]) for i in range(len(targets))]
        mae = sklearn.metrics.mean_absolute_error(trans_targets, trans_outputs)

        self.net_config.lr_scheduler.step()

        self.logger.print(f'Training loss at epoch {epoch}: {avg_loss}')
        # trans_avg, trans_std = self._get_trans_avg_std()
        avg_per_target, std_per_target =\
            TrainerTools.compute_reg_scores(preds=trans_outputs, targets=trans_targets)
        self.logger.print("Train stats:")
        self.logger.print(PrintTools.print_reg_metrics_mtx(avg_per_target=avg_per_target, std_per_target=std_per_target))
        pred_min = self.ttrans.inverse_transform(np.min(outputs))
        pred_max = self.ttrans.inverse_transform(np.max(outputs))
        pred_avg, pred_std = np.average(trans_outputs), np.std(trans_outputs)
        self.logger.print(f"InvReg Min prediction: {pred_min}")
        self.logger.print(f"InvReg Max prediction: {pred_max}")
        self.logger.print(f"InvReg Avg./Std.     : {pred_avg} +/- {pred_std}")
        # print(f"Min prediction: {np.min(outputs)}")
        # print(f"Max prediction: {np.max(outputs)}")
        # print(f"Avg./Std.     : {np.average(outputs)} +/- {np.std(outputs)}")
        self.logger.print(f"Mean Absolute Error: {mae}")
        # self.logger.print()
        # Confusion matrix with colored diagonal for console printout
        # print(PrintTools.print_confusion_mtx(outputs, targets, nb_classes=self.net_config.nb_outputs, color='red'))
        # Confusion matrix without colored diagonal for logger
        # confusion_mtx = PrintTools.print_confusion_mtx(outputs, targets, nb_classes=self.net_config.nb_outputs, color=None)
        # self.logger.write(confusion_mtx)

        spearman_r = spearmanr(trans_outputs, trans_targets)
        self.logger.print(f"Spearmanr: {spearman_r}")

        return avg_loss, mae, spearman_r, pred_min, pred_max, pred_avg, pred_std
