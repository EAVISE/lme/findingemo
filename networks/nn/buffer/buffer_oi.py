"""
Use this script to compute the YoLo v3 + OpenImages features, and store them to disk.
Actually, it is not the features themselves that are stored to disk, as they are too voluminous,
but the post-processed features, resulting in a CSV file containing the detected objects.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import os

import lightnet as ln
import torch
from lightnet.models import YoloV3

from config import Config
from networks.data.annotation_buffer_oi_dataset import AnnotationBufferOIDatasetFactory
from tools.img_tools import ImageTools


class BufferOI:
    @staticmethod
    def buffer_oi(ann_file=None, base_dir=None, device=torch.device('cuda'), out_dir=None, thresh=1e-6):
        """

        :param ann_file: annotation csv file to use
        :param base_dir: base_dir to pass to Annotation...Factory
        :param device:
        :param out_dir:
        :param thresh:
        :return:
        """
        if ann_file is None:
            ann_file = Config.FILE_ANN_SINGLE

        if base_dir is None:
            base_dir = Config.DIR_IMAGES

        if out_dir is None:
            out_dir = Config.DIR_BUFFER_OI

        class_map = []
        with open(Config.FILE_CLASSES_OI, "r") as fin:
            for line in fin:
                line = line.strip()
                class_map.append(line)
        model = YoloV3(601)
        model.load(os.path.join(Config.DIR_PRETRAINED_MODELS, 'DarkNet', 'yolov3-openimages.weights'))
        model.to(device)
        model.eval()

        # Create post-processing pipeline
        post = ln.data.transform.Compose([
            # GetBoxes transformation generates bounding boxes from network output
            ln.data.transform.GetMultiScaleAnchorBoxes(
                conf_thresh=thresh,
                network_stride=model.stride,
                anchors=model.anchors
            ),

            # Filter transformation to filter the output boxes
            ln.data.transform.NMS(
                iou_thresh=thresh
            ),

            # Miscellaneous transformation that transforms the output boxes to a brambox dataframe
            ln.data.transform.TensorToBrambox(
                class_label_map=class_map,
            )
        ])

        dataset = AnnotationBufferOIDatasetFactory.get_dataset(ann_file=ann_file, base_dir=base_dir, max_samples=0)
        # The following dictionary was used initially to store the YoLo output tensors, until it appeared
        # storing these tensors to disc was too expensive, disc space wise, and i opted to store the Pandas DataFrames instead.
        # out_per_img = {}

        if device == torch.device('cuda') and torch.cuda.device_count() > 1:
            print(f"Using {torch.cuda.device_count()} GPUs...")
            model = torch.nn.DataParallel(model)
            model.to(device)

        print(f"Parsing dataset...")
        for i in range(len(dataset)):
            print(f"\rAt image {i+1}/{len(dataset)}...", end='', flush=True)
            try:
                # Save found objects as CSV; mirror dir structure of input image
                img_path = dataset.xs[i]
                out_file = os.path.join(out_dir, ImageTools.get_csv_for_img(img_path[1:]))
                # If file already exists, i.e., image already processed (e.g., multiple annotations), skip
                if os.path.exists(out_file):
                    continue
                # emo = AnnotationEmotionMap.INV_EMOTION_MAP_EXT[target.item()]
                data, img_path = dataset.__getitem__(i)

            except FileNotFoundError:
                continue

            # Q: Actually, i'm not sure the image provided to the model needs to be resized/normalized. It seems
            # the model does this itself.
            with torch.no_grad():
                output_tensor = model(data.unsqueeze(0).to(device))
                output_df = post(output_tensor)
                # Make sure necessary dirs exist
                os.makedirs(os.path.dirname(out_file), exist_ok=True)
                # Write file
                output_df.to_csv(out_file)

                # out_per_img[img_path] = output_tensor

        print()

        # FYI: output size in MB per image = ~53MB
        # with open(os.path.join(Config.DIR_DATA, "test_oi.dill"), 'wb') as fout:
        #     dill.dump(out_per_img, fout)


if __name__ == '__main__':
    BufferOI.buffer_oi()
