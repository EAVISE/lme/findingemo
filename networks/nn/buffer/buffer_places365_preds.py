"""
Use this script to compute Places365 predictions, and store them to disk.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import os

import dill
import torch

from config import Config
from networks.data.annotation_buffer_places365_dataset import AnnotationBufferPlaces365DatasetFactory
from networks.nn.places365.places365_model import Places365Model
from tools.img_tools import ImageTools


class BufferPlaces365Predictions:
    @staticmethod
    def buffer_places365(device=torch.device('cuda'), ann_file=None, base_dir=None, batch_size=5, out_dir=None,
                         b_save_individual=False, b_overwrite=False, places_model=Places365Model.ALEXNET):
        """
        Buffer EmoNet model features. We save the model output to disk.

        :param device:
        :parma ann_file: annotation csv file to use
        :param base_dir:
        :param batch_size:
        :param out_dir:
        :param b_save_individual: save individual files
        :param b_overwrite: overwrite already existing files; if False, image will be skipped if output file already exists
        :return:
        """
        if ann_file is None:
            ann_file = Config.FILE_ANN_SINGLE

        if base_dir is None:
            base_dir = Config.DIR_IMAGES
        if out_dir is None:
            out_dir = Config.DIR_BUFFER_PLACES365

        print(f"Precomputing predictions for Places365: {places_model}...")
        model = Places365Model.load_model(places_model, device=device)
        model.eval()

        per_image_file = os.path.join(out_dir, f'buffer_places365_preds_{places_model}.dill')
        # If b_overwrite, update existing buffered features
        if not b_overwrite and os.path.exists(per_image_file):
            per_image = dill.load(open(per_image_file, 'rb'))
        else:
            per_image = {}

        # Initialize dataset
        dataset = AnnotationBufferPlaces365DatasetFactory.get_dataset(ann_file=ann_file, base_dir=base_dir,
                                                                      filter_images=set(per_image.keys()))

        data_loader = torch.utils.data.DataLoader(dataset, batch_size=batch_size, drop_last=False)

        if device == torch.device('cuda') and torch.cuda.device_count() > 1:
            print(f"Using {torch.cuda.device_count()} GPUs...")
            model = torch.nn.DataParallel(model)
            model.to(device)

        print(f"Parsing data...")
        nb_samples = len(data_loader.dataset)
        nb_batches = nb_samples//data_loader.batch_size
        if nb_samples%data_loader.batch_size != 0:
            nb_batches += 1
        for batch_index, (data, img_paths) in enumerate(data_loader):
            print(f"\rAt batch {batch_index+1}/{nb_batches}; sample {(batch_index+1)*data_loader.batch_size}/{nb_samples}...", end='', flush=True)

            # Filter images that were already processed, if not b_overwrite
            keep_idx = list(range(len(img_paths)))
            filter_rows = set()
            for i, img_path in enumerate(img_paths):
                out_file = os.path.join(out_dir, ImageTools.get_dill_for_img(img_path[1:], model_name='places365-preds'))
                # If file already exists, i.e., image already processed (e.g., multiple annotations), skip
                if not b_overwrite and (img_path in per_image or os.path.exists(out_file)):
                    filter_rows.add(i)
                    continue
            if filter_rows:
                keep_idx = [x for x in keep_idx if x not in filter_rows]
                img_paths = [img_paths[x] for x in range(len(img_paths)) if x not in filter_rows]
                data = data[keep_idx]

            if img_paths:  # Check not all images were filtered out...
                with torch.no_grad():
                    model_out = model(data.to(device))
                    for i in range(model_out.shape[0]):
                        # print(' - '.join([f'{model_out[i, j]:.2f}' for j in range(model_out.shape[1])]))
                        per_image[img_paths[i]] = model_out[i].clone().cpu().detach()

                        if b_save_individual:
                            out_file = os.path.join(out_dir, ImageTools.get_dill_for_img(img_paths[i][1:], model_name='emonet'))
                            # Make sure necessary dirs exist
                            os.makedirs(os.path.dirname(out_file), exist_ok=True)
                            # Write file
                            try:
                                dill.dump(model_out[i], open(out_file, 'wb'))
                            except OSError as e:
                                print(f"!!! Couldn't write file. !!!")
                                print(f"ErrorMsg: {e}")

        print()

        os.makedirs(os.path.dirname(per_image_file), exist_ok=True)
        dill.dump(per_image, open(per_image_file, 'wb'))


if __name__ == '__main__':
    BufferPlaces365Predictions.buffer_places365(b_overwrite=False,
                                                batch_size=25, b_save_individual=False,
                                                places_model=Places365Model.RESNET50)
