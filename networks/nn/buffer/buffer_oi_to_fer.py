"""
Use this script to buffer the OIToFER features, and store them to disk.
The NON-STANDARDIZED features are buffered.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import os

import dill
import torch

from config import Config
from networks.data.annotation_buffer_oi_dataset import AnnotationBufferOIDatasetFactory
from networks.nn.fer.oi_to_fer import BufferedOIToFER
from tools.img_tools import ImageTools


class BufferOIToFER:
    @staticmethod
    def buffer_oi_to_fer(ann_file=None, base_dir=None, device=torch.device('cuda'), out_dir=None, thresh=5e-3,
                         b_save_individual=False, b_overwrite=False):
        if ann_file is None:
            ann_file = Config.FILE_ANN_SINGLE

        if base_dir is None:
            base_dir = Config.DIR_IMAGES

        if out_dir is None:
            out_dir = Config.DIR_BUFFER_OI_TO_FER

        oi_to_fer = BufferedOIToFER(threshold=thresh, device=device)

        per_image_file = os.path.join(out_dir, 'buffer_oitofer.dill')
        # If b_overwrite, update existing buffered features
        if not b_overwrite and os.path.exists(per_image_file):
            per_image = dill.load(open(per_image_file, 'rb'))
        else:
            per_image = {}

        # Initialize dataset
        dataset = AnnotationBufferOIDatasetFactory.get_dataset(ann_file=ann_file, base_dir=base_dir, filter_images=set(per_image.keys()))
        data_loader = torch.utils.data.DataLoader(dataset, batch_size=25, drop_last=False)

        print(f"Parsing data...")
        nb_samples = len(data_loader.dataset)
        nb_batches = nb_samples//data_loader.batch_size
        if nb_samples%data_loader.batch_size != 0:
            nb_batches += 1
        for batch_index, (data, img_paths) in enumerate(data_loader):
            print(f"\rAt batch {batch_index+1}/{nb_batches}; sample {(batch_index+1)*data_loader.batch_size}/{nb_samples}...",
                  end='', flush=True)

            # Filter images that were already processed, if not b_overwrite
            keep_idx = list(range(len(img_paths)))
            filter_rows = set()
            for i, img_path in enumerate(img_paths):
                out_file = os.path.join(out_dir, ImageTools.get_dill_for_img(img_path[1:]))
                # If file already exists, i.e., image already processed (e.g., multiple annotations), skip
                if not b_overwrite and (img_path in per_image or os.path.exists(out_file)):
                    filter_rows.add(i)
                    continue
            if filter_rows:
                keep_idx = [x for x in keep_idx if x not in filter_rows]
                img_paths = [img_paths[x] for x in range(len(img_paths)) if x not in filter_rows]
                data = data[keep_idx]

            if img_paths:  # Check not all images were filtered out...
                with torch.no_grad():
                    model_out = oi_to_fer.process_yolo(data.to(device), img_paths, b_standardize=False)
                    # Output model_out = (tensor(batch_size, 7), tensor(batch_size, 7))
                    for i in range(model_out[0].shape[0]):
                        sample_res = (model_out[0][i].clone().cpu().detach(), model_out[1][i].clone().cpu().detach())
                        per_image[img_paths[i]] = sample_res

                        if b_save_individual:
                            out_file = os.path.join(out_dir, ImageTools.get_dill_for_img(img_paths[i][1:]))
                            # Make sure necessary dirs exist
                            os.makedirs(os.path.dirname(out_file), exist_ok=True)
                            # Write file
                            try:
                                dill.dump(sample_res, open(out_file, 'wb'))
                            except OSError as e:
                                print(f"!!! Couldn't write file. !!!")
                                print(f"ErrorMsg: {e}")

        print()

        os.makedirs(os.path.dirname(per_image_file), exist_ok=True)
        dill.dump(per_image, open(per_image_file, 'wb'))


if __name__ == '__main__':
    BufferOIToFER.buffer_oi_to_fer(device=torch.device('cuda'), b_overwrite=True)
