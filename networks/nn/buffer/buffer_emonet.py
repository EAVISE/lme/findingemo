"""
Use this script to compute EmoNet features, and store them to disk.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import os

import dill
import torch
from emonet_py.emonet import EmoNet

from config import Config
from networks.data.annotation_buffer_en_dataset import AnnotationBufferENDatasetFactory
from tools.img_tools import ImageTools


class BufferEmoNet:
    @staticmethod
    def buffer_emonet(device=torch.device('cuda'), ann_file=None, base_dir=None, batch_size=5, out_dir=None,
                      b_save_individual=False, b_overwrite=False):
        """
        Buffer EmoNet model features. We save the model output to disk.

        :param device:
        :param ann_file: annotation file to use
        :param base_dir:
        :param batch_size:
        :param out_dir:
        :param b_save_individual: save individual files
        :param b_overwrite: overwrite already existing files; if False, image will be skipped if output file already exists
        :return:
        """
        if ann_file is None:
            ann_file = Config.FILE_ANN_SINGLE

        if base_dir is None:
            base_dir = Config.DIR_IMAGES
        if out_dir is None:
            out_dir = Config.DIR_BUFFER_EMONET

        model = EmoNet.get_emonet()
        model = model.to(device)

        print("Precomputing features for EmoNet...")
        model.eval()

        per_image_file = os.path.join(out_dir, 'buffer_emonet.dill')
        # If b_overwrite, update existing buffered features
        if not b_overwrite and os.path.exists(per_image_file):
            per_image = dill.load(open(per_image_file, 'rb'))
        else:
            per_image = {}

        # Initialize dataset
        dataset = AnnotationBufferENDatasetFactory.get_dataset(ann_file=ann_file, base_dir=base_dir, filter_images=set(per_image.keys()))
        # Save dataset input processing, a.k.a. transformer
        transform_path = os.path.join(out_dir, f'transform_{str(device)}.dill')
        if not os.path.isdir(out_dir):
            os.makedirs(out_dir)
        dill.dump(dataset.transform, open(transform_path, 'wb'))
        print(f"Image transform saved to : {transform_path}")

        data_loader = torch.utils.data.DataLoader(dataset, batch_size=batch_size, drop_last=False)

        if device == torch.device('cuda') and torch.cuda.device_count() > 1:
            print(f"Using {torch.cuda.device_count()} GPUs...")
            model = torch.nn.DataParallel(model)
            model.to(device)

        print(f"Parsing data...")
        nb_samples = len(data_loader.dataset)
        nb_batches = nb_samples//data_loader.batch_size
        if nb_samples%data_loader.batch_size != 0:
            nb_batches += 1
        for batch_index, (data, img_paths) in enumerate(data_loader):
            print(f"\rAt batch {batch_index+1}/{nb_batches}; sample {(batch_index+1)*data_loader.batch_size}/{nb_samples}...", end='', flush=True)

            # Filter images that were already processed, if not b_overwrite
            keep_idx = list(range(len(img_paths)))
            filter_rows = set()
            for i, img_path in enumerate(img_paths):
                out_file = os.path.join(out_dir, ImageTools.get_dill_for_img(img_path[1:], model_name='emonet'))
                # If file already exists, i.e., image already processed (e.g., multiple annotations), skip
                if not b_overwrite and (img_path in per_image or os.path.exists(out_file)):
                    filter_rows.add(i)
                    continue
            if filter_rows:
                keep_idx = [x for x in keep_idx if x not in filter_rows]
                img_paths = [img_paths[x] for x in range(len(img_paths)) if x not in filter_rows]
                data = data[keep_idx]

            if img_paths:  # Check not all images were filtered out...
                with torch.no_grad():
                    model_out = model(data.to(device))
                    for i in range(model_out.shape[0]):
                        # print(' - '.join([f'{model_out[i, j]:.2f}' for j in range(model_out.shape[1])]))
                        per_image[img_paths[i]] = model_out[i].clone().cpu().detach()

                        if b_save_individual:
                            out_file = os.path.join(out_dir, ImageTools.get_dill_for_img(img_paths[i][1:], model_name='emonet'))
                            # Make sure necessary dirs exist
                            os.makedirs(os.path.dirname(out_file), exist_ok=True)
                            # Write file
                            try:
                                dill.dump(model_out[i], open(out_file, 'wb'))
                            except OSError as e:
                                print(f"!!! Couldn't write file. !!!")
                                print(f"ErrorMsg: {e}")

        print()

        os.makedirs(os.path.dirname(per_image_file), exist_ok=True)
        dill.dump(per_image, open(per_image_file, 'wb'))


if __name__ == '__main__':
    BufferEmoNet.buffer_emonet(b_overwrite=True, base_dir=os.path.join(Config.DIR_DATA, 'AnnImages'),
                               batch_size=5, b_save_individual=False)
