"""
EZ script to (re-)buffer all things that need to be buffered.

PREDICTIONS ARE NOT BUFFERED BY THIS SCRIPT, AS YOU FIRST NEED TO RETRAIN THE MODELS USED FOR MAKING THE PREDICTIONS!

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import os

import torchvision

from config import Config
from networks.nn.buffer.buffer_emonet import BufferEmoNet
from networks.nn.buffer.buffer_in import BufferImageNet
from networks.nn.buffer.buffer_oi import BufferOI
from networks.nn.buffer.buffer_oi_to_fer import BufferOIToFER
from networks.nn.buffer.buffer_places365_feats import BufferPlaces365Feats
from networks.nn.buffer.buffer_places365_preds import BufferPlaces365Predictions
from networks.nn.places365.places365_model import Places365Model

if __name__ == '__main__':
    b_overwrite = False  # Reparse images that were already previously buffered

    batch_size = 25
    ann_file = Config.FILE_ANN_SINGLE
    base_dir = Config.DIR_IMAGES
    models = [
        torchvision.models.alexnet,
        torchvision.models.vgg16, torchvision.models.vgg19,
        torchvision.models.resnet18, torchvision.models.resnet34,
        torchvision.models.resnet50, torchvision.models.resnet101,
        torchvision.models.googlenet, torchvision.models.inception_v3,
        torchvision.models.densenet121, torchvision.models.densenet161
    ]

    BufferEmoNet.buffer_emonet(b_overwrite=b_overwrite, ann_file=ann_file, base_dir=base_dir,
                               batch_size=250, b_save_individual=False)

    # To buffer the ImageNet features, and NOT the predictions, use the following:
    BufferImageNet.buffer_multiple_models(models=models, b_overwrite=b_overwrite, ann_file=ann_file, base_dir=base_dir,
                                          out_dir=os.path.join(Config.DIR_BUFFER_ROOT, 'Buffer_ImageNet'),
                                          batch_size=batch_size, b_save_individual=False)

    BufferOI.buffer_oi(ann_file=ann_file, base_dir=base_dir)
    BufferOIToFER.buffer_oi_to_fer(ann_file=ann_file, base_dir=base_dir, b_overwrite=b_overwrite)

    for m in [Places365Model.ALEXNET, Places365Model.RESNET18, Places365Model.RESNET50, Places365Model.DENSENET161]:
        BufferPlaces365Predictions.buffer_places365(b_overwrite=b_overwrite, ann_file=ann_file, base_dir=base_dir, batch_size=250,
                                                    b_save_individual=False, places_model=m)

    # Buffer Places365 "features", i.e., output of second-to-last layer
    for places365_model in [Places365Model.ALEXNET, Places365Model.RESNET18,
                            Places365Model.RESNET50, Places365Model.DENSENET161]:
        BufferPlaces365Feats.buffer_places365(ann_file=ann_file,
                                              b_overwrite=b_overwrite,
                                              batch_size=batch_size, b_save_individual=False,
                                              places_model=places365_model)
