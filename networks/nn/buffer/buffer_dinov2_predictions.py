"""
Use this script to compute DINOv2 model predictions, and store them to disk.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import os

import dill
import torch

from config import Config
from networks.data.base_dataset import BaseDataset
from networks.data.buffer_dinov2_dataset import BufferDINOv2Dataset
from networks.data.generic_buffered_feats_dataset import GenericBufferedFeatsDataset
from networks.nn.dinov2.dinov2_models import DINOv2Classifier1, DINOv2Regressor1
from tools.img_tools import ImageTools


class BufferDINOv2Predictions:
    @staticmethod
    def buffer_dinov2_predictions(model,
                                  file_model_weights,
                                  model_params=None,
                                  device=torch.device('cuda'), ann_file=None, batch_size=5, out_dir=None,
                                  out_file='buffer_dinov2_emo8.dill', b_save_individual=False, b_overwrite=False):
        """
        Buffer DINOv2 predictions. We save the model output to disk.

        :param model: model class
        :param file_model_weights: path to the file containing the model weighs to be loaded
        :param model_params: parameters to be passed along to model constructor
        :param device:
        :param ann_file: annotation file to use
        :param batch_size:
        :param out_dir:
        :param out_file: name of the output file to generate
        :param b_save_individual: save individual files
        :param b_overwrite: overwrite already existing files; if False, image will be skipped if output file already exists
        :return:
        """
        if ann_file is None:
            ann_file = Config.FILE_ANN3

        if out_dir is None:
            out_dir = Config.DIR_BUFFER_DINOv2

        model = model(**model_params)
        model.load_state_dict(torch.load(file_model_weights, map_location=device))
        model = model.to(device)
        model.eval()

        print("Precomputing predictions for DINOv2 model...")

        per_image_file = os.path.join(out_dir, out_file)
        # If b_overwrite, update existing buffered features
        if not b_overwrite and os.path.exists(per_image_file):
            per_image = dill.load(open(per_image_file, 'rb'))
        else:
            per_image = {}

        ann = BaseDataset.get_anns(ann_file=ann_file, b_filter=False)
        data_files = ann['image_path'].values.tolist()

        # Initialize dataset
        data = BufferDINOv2Dataset(root_dir=Config.DIR_BUFFER_DINOv2).get_subset_for_imgs(img_paths=data_files)
        dataset = GenericBufferedFeatsDataset(buffered_feats=data)
        data_loader = torch.utils.data.DataLoader(dataset, batch_size=batch_size, drop_last=False)

        if device == torch.device('cuda') and torch.cuda.device_count() > 1:
            print(f"Using {torch.cuda.device_count()} GPUs...")
            model = torch.nn.DataParallel(model)
            model.to(device)

        print(f"Parsing data...")
        nb_samples = len(data_loader.dataset)
        nb_batches = nb_samples//data_loader.batch_size
        if nb_samples%data_loader.batch_size != 0:
            nb_batches += 1
        for batch_index, (data, img_paths) in enumerate(data_loader):
            print(f"\rAt batch {batch_index+1}/{nb_batches}; sample {(batch_index+1)*data_loader.batch_size}/{nb_samples}...", end='', flush=True)

            # Filter images that were already processed, if not b_overwrite
            keep_idx = list(range(len(img_paths)))
            filter_rows = set()
            for i, img_path in enumerate(img_paths):
                out_file = os.path.join(out_dir, ImageTools.get_dill_for_img(img_path[1:], model_name='DINOv2'))
                # If file already exists, i.e., image already processed (e.g., multiple annotations), skip
                if not b_overwrite and (img_path in per_image or os.path.exists(out_file)):
                    filter_rows.add(i)
                    continue
            if filter_rows:
                keep_idx = [x for x in keep_idx if x not in filter_rows]
                img_paths = [img_paths[x] for x in range(len(img_paths)) if x not in filter_rows]
                data = data[keep_idx]

            if img_paths:  # Check not all images were filtered out...
                with torch.no_grad():
                    model_out = model(data.to(device))
                    for i in range(model_out.shape[0]):
                        # print(' - '.join([f'{model_out[i, j]:.2f}' for j in range(model_out.shape[1])]))
                        per_image[img_paths[i]] = model_out[i].clone().cpu().detach()

                        if b_save_individual:
                            out_file = os.path.join(out_dir, ImageTools.get_dill_for_img(img_paths[i][1:], model_name='DINOv2'))
                            # Make sure necessary dirs exist
                            os.makedirs(os.path.dirname(out_file), exist_ok=True)
                            # Write file
                            try:
                                dill.dump(model_out[i], open(out_file, 'wb'))
                            except OSError as e:
                                print(f"!!! Couldn't write file. !!!")
                                print(f"ErrorMsg: {e}")

        print()

        os.makedirs(os.path.dirname(per_image_file), exist_ok=True)
        dill.dump(per_image, open(per_image_file, 'wb'))
        print(f"Saved predictions to {per_image_file}.")


if __name__ == '__main__':
    # Buffer Emo8 predictions
    BufferDINOv2Predictions.buffer_dinov2_predictions(model=DINOv2Classifier1,
                                                  model_params={'nb_outputs': 8},
                                                  file_model_weights=os.path.join(Config.DIR_MODELS, 'BestModels',
                                                                                  '20240515', 'emo8_DINOv2_B_lr=0.0001_loss=UnbalancedCrossEntropyLoss_sc=test_cuda.pth'),
                                                  ann_file=Config.FILE_ANN3_SINGLE,
                                                  out_file='buffer_dinov2_b_emo8.dill',
                                                  b_overwrite=True, batch_size=5, b_save_individual=False)

    # Buffer Arousal regression predictions
    BufferDINOv2Predictions.buffer_dinov2_predictions(model=DINOv2Regressor1,
                                                  model_params={},
                                                  file_model_weights=os.path.join(Config.DIR_MODELS, 'BestModels_Arousal Reg Loss=MSE',
                                                                                  '20240515', 'arousal1_DINOv2_B_lr=0.001_loss=MSELoss_sc=test_cuda:1.pth'),
                                                  ann_file=Config.FILE_ANN3_SINGLE,
                                                  out_file='buffer_dinov2_b_aro1.dill',
                                                  b_overwrite=True, batch_size=5, b_save_individual=False)

    # Buffer Valence regression predictions
    BufferDINOv2Predictions.buffer_dinov2_predictions(model=DINOv2Regressor1,
                                                  model_params={},
                                                  file_model_weights=os.path.join(Config.DIR_MODELS, 'BestModels_Valence Reg Loss=MSE',
                                                                                  '20240515', 'valence1_DINOv2_B_lr=0.001_loss=MSELoss_sc=test_cuda.pth'),
                                                  ann_file=Config.FILE_ANN3_SINGLE,
                                                  out_file='buffer_dinov2_b_val1.dill',
                                                  b_overwrite=True, batch_size=5, b_save_individual=False)
