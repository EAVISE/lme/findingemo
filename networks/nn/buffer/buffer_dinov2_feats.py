"""
Precompute and buffer (i.e., save to disk) DINOv2 features.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import os

import dill
import torch

from config import Config
from networks.data.annotation_buffer_in_dataset import AnnotationBufferINDatasetFactory


if __name__ == '__main__':
    # Load the model
    device = torch.device('cuda' if torch.cuda.is_available() else "cpu")
    # Models: "small" dinov2_vits14_reg_lc and "regular" dinov2_vitb14_reg_lc
    model = torch.hub.load('facebookresearch/dinov2', 'dinov2_vitb14_reg_lc')
    model.to(device)

    # Load the dataset
    dataset = AnnotationBufferINDatasetFactory.get_dataset(resize_height=602, resize_width=798)

    feats = dict()
    with torch.no_grad():
        for idx in range(len(dataset)):
            print(f"\rAt image {idx+1} of {len(dataset)}...", end='')
            img, img_path = dataset.__getitem__(idx)
            f = model(img.unsqueeze(0).to(device)).squeeze(0)

            feats[img_path] = f
    print()

    out_file = os.path.join(Config.DIR_BUFFER_DINOv2, 'buffer_dinov2_vitb14_reg_lc_feats.dill')
    os.makedirs(os.path.dirname(out_file), exist_ok=True)
    dill.dump(feats, open(out_file, 'wb'))
    print(f"Results written to {out_file}.")
