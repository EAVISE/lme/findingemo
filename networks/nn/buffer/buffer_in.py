"""
Use this script to compute ImageNet features, either up until the second-to-last layer, or emo8/emo24 predictions.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import os
import copy
from typing import Iterable

import dill
import torch
import torchvision.models

from config import Config
from networks.data.annotation_buffer_in_dataset import AnnotationBufferINDatasetFactory
from tools.img_tools import ImageTools


class BufferImageNet:
    @classmethod
    def buffer_multiple_models(cls, models: Iterable, device=torch.device('cuda'), ann_file=None, base_dir=None,
                               out_dir=None, batch_size=5, b_overwrite=False, b_save_individual=False, **kwargs):
        """

        :param models:
        :param device:
        :param ann_file: annotation csv file to use
        :param base_dir: base_dir to pass to Annotation...Factory
        :param out_dir:
        :param batch_size:
        :param b_overwrite:
        :param b_save_individual: save individual files
        :return:
        """
        if ann_file is None:
            ann_file = Config.FILE_ANN_SINGLE

        if base_dir is None:
            base_dir = Config.DIR_IMAGES
        if out_dir is None:
            out_dir = Config.DIR_BUFFER_IN

        # Initialize dataset
        dataset = AnnotationBufferINDatasetFactory.get_dataset(max_samples=0, ann_file=ann_file, base_dir=base_dir)
        # Save dataset input processing, a.k.a. transformer
        transform_path = os.path.join(out_dir, f'transform_{str(device)}.dill')
        if not os.path.isdir(out_dir):
            os.makedirs(out_dir)
        dill.dump(dataset.transform, open(transform_path, 'wb'))
        print(f"Image transform saved to : {transform_path}")

        data_loader = torch.utils.data.DataLoader(dataset, batch_size=batch_size, drop_last=False)

        # Cycle through models
        for model in models:
            cls.buffer_in(device=device, data_loader=data_loader, model=model, out_dir=out_dir, b_overwrite=b_overwrite,
                          b_save_individual=b_save_individual, **kwargs)

    @classmethod
    def buffer_in(cls, device=torch.device('cuda'), data_loader=None, model=None, base_dir=None, out_dir=None,
                  model_name_suffix='', b_save_individual=False, b_overwrite=False):
        """
        Buffer ImageNet model features. We remove the last layer (+potential DropOut layer preceding it), and save the model output
        to disk.

        :param device:
        :param data_loader: if None, will initialize AnnotationBufferINDatasetFactory and create data_loader from that
        :param model:
        :param base_dir:
        :param out_dir:
        :param model_name_suffix: generic model name = 'buffer_' + model_name + model_name_suffix + '.dill'
        :param b_save_individual: save individual files
        :param b_overwrite: overwrite already existing files; if False, image will be skipped if output file already exists
        :return:
        """
        if base_dir is None:
            base_dir = Config.DIR_IMAGES

        if out_dir is None:
            out_dir = Config.DIR_BUFFER_IN

        if model is None:
            raise ValueError("No model specified. Parameter 'model' should refer to a torchvision.models.vgg* initialization method.")
        model_name = model.__name__
        print(f"Precomputing features for model {model_name}...")
        model = model(weights=torchvision.models.get_model_weights(model_name).DEFAULT)
        model.to(device)
        model.eval()

        if model_name in {'alexnet', 'vgg16', 'vgg19'}:
            # Remove last layer + dropout layer preceding it
            del model.classifier[6]
            del model.classifier[5]
            # del model.classifier[4]
            # del model.classifier[3]
            # del model.classifier[2]
        elif model_name.startswith('resnet') or model_name in {'googlenet', 'inception_v3'}:
            # Replace last fully connected layer with identity layer
            model.fc = torch.nn.Identity()
        elif model_name.startswith('densenet') or model_name.startswith('squeezenet'):
            del model.classifier
            model.classifier = torch.nn.Identity()
        else:
            raise ValueError(f"Don't know what to do with model of type '{model_name}'.")

        per_image_file = os.path.join(out_dir, 'buffer_' + model_name + model_name_suffix + '.dill')
        # If b_overwrite, update existing buffered features
        if not b_overwrite and os.path.exists(per_image_file):
            per_image = dill.load(open(per_image_file, 'rb'))
        else:
            per_image = {}

        # Initialize dataset
        if data_loader is None:
            dataset = AnnotationBufferINDatasetFactory.get_dataset(base_dir=base_dir,
                                                                   max_samples=0,
                                                                   filter_images=set(per_image.keys()))
            data_loader = torch.utils.data.DataLoader(dataset, batch_size=10, drop_last=False)
        elif not b_overwrite:
            dataset = copy.deepcopy(data_loader.dataset)
            dataset.filter_images(set(per_image.keys()))
            data_loader = torch.utils.data.DataLoader(dataset, batch_size=data_loader.batch_size, drop_last=False)

        if device == torch.device('cuda') and torch.cuda.device_count() > 1:
            print(f"Using {torch.cuda.device_count()} GPUs...")
            model = torch.nn.DataParallel(model)
            model.to(device)

        print(f"Parsing data...")
        nb_samples = len(data_loader.dataset)
        nb_batches = nb_samples//data_loader.batch_size
        if nb_samples%data_loader.batch_size != 0:
            nb_batches += 1
        for batch_index, (data, img_paths) in enumerate(data_loader):
            print(f"\rAt batch {batch_index+1}/{nb_batches}; sample {(batch_index+1)*data_loader.batch_size}/{nb_samples}...", end='', flush=True)

            # Filter images that were already processed, if not b_overwrite
            keep_idx = list(range(len(img_paths)))
            filter_rows = set()
            for i, img_path in enumerate(img_paths):
                out_file = os.path.join(out_dir, ImageTools.get_dill_for_img(img_path[1:], model_name=model_name))
                # If file already exists, i.e., image already processed (e.g., multiple annotations), skip
                if not b_overwrite and (img_path in per_image or os.path.exists(out_file)):
                    filter_rows.add(i)
                    continue
            if filter_rows:
                keep_idx = [x for x in keep_idx if x not in filter_rows]
                img_paths = [img_paths[x] for x in range(len(img_paths)) if x not in filter_rows]
                data = data[keep_idx]

            if img_paths:  # Check not all images were filtered out...
                with torch.no_grad():
                    model_out = model(data.to(device))
                    for i in range(model_out.shape[0]):
                        per_image[img_paths[i]] = model_out[i].clone().cpu().detach()

                        if b_save_individual:
                            out_file = os.path.join(out_dir, ImageTools.get_dill_for_img(img_paths[i][1:], model_name=model_name))
                            # Make sure necessary dirs exist
                            os.makedirs(os.path.dirname(out_file), exist_ok=True)
                            # Write file
                            try:
                                dill.dump(model_out[i], open(out_file, 'wb'))
                            except OSError as e:
                                print(f"!!! Couldn't write file. !!!")
                                print(f"ErrorMsg: {e}")

        print()

        os.makedirs(os.path.dirname(per_image_file), exist_ok=True)
        dill.dump(per_image, open(per_image_file, 'wb'))
        print(f"Results written to {per_image_file}.")


if __name__ == '__main__':
    models = [
        torchvision.models.vgg16, torchvision.models.vgg19,
        torchvision.models.alexnet
        # torchvision.models.squeezenet1_0,
        # torchvision.models.resnet18, torchvision.models.resnet34,
        # torchvision.models.resnet50, torchvision.models.resnet101,
        # torchvision.models.googlenet, torchvision.models.inception_v3,
        # torchvision.models.densenet121, torchvision.models.densenet161
    ]

    # To buffer the ImageNet features, and NOT the predictions, use the following:
    # BufferImageNet.buffer_multiple_models(models=models, b_overwrite=True,
    #                                       base_dir=os.path.join(Config.DIR_DATA, 'AnnImagesAlphaPoseBW', 'vis'),
    #                                       out_dir=os.path.join(Config.DIR_BUFFER_ROOT, 'Buffer_ImageNet_APBW'),
    #                                       batch_size=50, b_save_individual=False)
    BufferImageNet.buffer_multiple_models(models=models, b_overwrite=False, ann_file=Config.FILE_ANN_SINGLE,
                                          base_dir=Config.DIR_IMAGES,
                                          out_dir=os.path.join(Config.DIR_BUFFER_ROOT, 'Buffer_ImageNet'),
                                          model_name_suffix='_layer=class1',
                                          batch_size=25, b_save_individual=False)

    # BufferImageNet.buffer_in(device=torch.device('cuda'), model=torchvision.models.vgg16, b_overwrite=False,
    #                          b_save_individual=False)
