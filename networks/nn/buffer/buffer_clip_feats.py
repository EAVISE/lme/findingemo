"""
Precompute and buffer (i.e., save to disk) CLIP features.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import os

import PIL
import clip
import dill
import torch

from config import Config
from networks.data.annotation_buffer_in_dataset import AnnotationBufferINDatasetFactory


def pil_loader(path: str, base_dir) -> PIL.Image.Image:
    # open path as file to avoid ResourceWarning (https://github.com/python-pillow/Pillow/issues/835)
    full_path = os.path.join(base_dir, *path.split('/'))
    with open(full_path, 'rb') as f:
        try:
            img = PIL.Image.open(f)
            img = img.convert('RGB')
        except (OSError, PIL.UnidentifiedImageError) as e:
            print(f"!!!Couldn't open image:\t{full_path}")
            raise e
        return img


if __name__ == '__main__':
    # Load the model
    device = "cuda" if torch.cuda.is_available() else "cpu"
    model, preprocess = clip.load('ViT-B/32', device)

    # Load the dataset
    dataset = AnnotationBufferINDatasetFactory.get_dataset()

    # Compute image features
    feats = dict()
    for idx in range(len(dataset)):
        if (idx+1) % 10 == 0:
            print(f"\rAt image {idx+1}/{len(dataset)}...", end='', flush=True)
        # Prepare the inputs
        _, img_path = dataset.__getitem__(idx)
        image = pil_loader(img_path, base_dir=dataset.base_dir)
        image_input = preprocess(image).unsqueeze(0).to(device)

        # Calculate features
        with torch.no_grad():
            image_features = model.encode_image(image_input).squeeze(0)
            feats[img_path] = image_features

    print(f"\rAt image {len(dataset)}/{len(dataset)}...")

    # Save to disk
    out_file = os.path.join(Config.DIR_BUFFER_CLIP, 'buffer_clip_vitb32_feats.dill')
    os.makedirs(os.path.dirname(out_file), exist_ok=True)
    dill.dump(feats, open(out_file, 'wb'))
    print(f"Results written to {out_file}.")
