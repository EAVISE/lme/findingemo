"""
Buffer ImageNet predictions.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import os

import dill
import torch
import torchvision.models

from config import Config
from networks.data.annotation_buffer_in_dataset import AnnotationBufferINDatasetFactory
from networks.data.annotation_buffer_in_preds_dataset import AnnotationBufferINPredictionsDatasetFactory
from networks.nn.imagenet.imagenet_classifier import get_classifier_for_imagenet
from tools.img_tools import ImageTools


class BufferImageNetPredictions:
    @classmethod
    def buffer_in_predictions(cls, device=torch.device('cuda'), data_loader=None, model=None, model_name=None,
                              nb_outputs=None, target='emo', out_dir=None, b_save_individual=False, b_overwrite=False):
        """
        Buffer ImageNet model features. We remove the last layer (+potential DropOut layer preceding it), and save the
        model output to disk.

        :param device:
        :param data_loader: if None, will initialize AnnotationBufferINDatasetFactory and create data_loader from that
        :param model:
        :param model_name:
        :param nb_outputs: number of outputs of the network, i.e., the number of emotions to predict
        :param target: prediction target
        :param out_dir:
        :param b_save_individual: save individual files
        :param b_overwrite: overwrite already existing files; if False, image will be skipped if output file already exists
        :return:
        """
        if out_dir is None:
            out_dir = Config.DIR_BUFFER_IN

        if model is None:
            raise ValueError("No model specified. Parameter 'model' should be an initialized ImageNetClassifier* instance.")
        print(f"Precomputing features for model {model_name}...")
        model.to(device)
        model.eval()

        per_image_file = os.path.join(out_dir, 'buffer_' + model_name + f'_{target}{nb_outputs}' + '.dill')
        # If b_overwrite, update existing buffered features
        if not b_overwrite and os.path.exists(per_image_file):
            per_image = dill.load(open(per_image_file, 'rb'))
        else:
            per_image = {}

        if data_loader is None:
            dataset = AnnotationBufferINDatasetFactory.get_dataset(base_dir=os.path.join(Config.DIR_DATA, 'AnnImages'), max_samples=0)
            data_loader = torch.utils.data.DataLoader(dataset, batch_size=10, drop_last=False)

        print(f"Parsing data...")
        nb_samples = len(data_loader.dataset)
        nb_batches = nb_samples // data_loader.batch_size
        if nb_samples % data_loader.batch_size != 0:
            nb_batches += 1
        for batch_index, (data, img_paths) in enumerate(data_loader):
            print(f"\rAt batch {batch_index + 1}/{nb_batches}; sample {(batch_index + 1) * data_loader.batch_size}/{nb_samples}...", end='',
                  flush=True)

            # Filter images that were already processed, if not b_overwrite
            keep_idx = list(range(len(img_paths)))
            filter_rows = set()
            for i, img_path in enumerate(img_paths):
                out_file = os.path.join(out_dir, ImageTools.get_dill_for_img(img_path[1:], model_name=model_name))
                out_file = out_file[:-5] + f'_{target}{nb_outputs}' + out_file[-5:]
                # If file already exists, i.e., image already processed (e.g., multiple annotations), skip
                if not b_overwrite and (img_path in per_image or os.path.exists(out_file)):
                    filter_rows.add(i)
                    continue
            if filter_rows:
                keep_idx = [x for x in keep_idx if x not in filter_rows]
                img_paths = [img_paths[x] for x in range(len(img_paths)) if x not in filter_rows]
                data = data[keep_idx]

            if img_paths:  # Check not all images were filtered out...
                with torch.no_grad():
                    model_out = model(data.to(device))
                    for i in range(model_out.shape[0]):
                        per_image[img_paths[i]] = model_out[i].clone().cpu().detach()

                        if b_save_individual:
                            out_file = os.path.join(out_dir, ImageTools.get_dill_for_img(img_paths[i][1:], model_name=model_name))
                            out_file = out_file[:-5] + f'_{target}{nb_outputs}' + out_file[-5:]
                            # Make sure necessary dirs exist
                            os.makedirs(os.path.dirname(out_file), exist_ok=True)
                            # Write file
                            try:
                                dill.dump(model_out[i], open(out_file, 'wb'))
                            except OSError as e:
                                print(f"!!! Couldn't write file. !!!")
                                print(f"ErrorMsg: {e}")

        print()

        os.makedirs(os.path.dirname(per_image_file), exist_ok=True)
        dill.dump(per_image, open(per_image_file, 'wb'))


if __name__ == '__main__':
    # Create a dictionary that maps each base model onto its corresponding pre-trained classifier weights file.

    # ############################################################
    # Emo8 models
    # ############################################################
    # date = '20231222'
    # loss = 'UnbalancedCrossEntropyLoss'
    # nb_outputs = 8
    # base_models = {
    #     torchvision.models.alexnet:
    #         os.path.join(Config.DIR_MODELS, 'BestModels', date,
    #                      'emo8_alexnet_lr=0.0001_loss=UnbalancedCrossEntropyLoss_sc=test_cuda.pth'),
    #     torchvision.models.vgg16:
    #         os.path.join(Config.DIR_MODELS, 'BestModels', date,
    #                      'emo8_vgg16_lr=0.001_loss=UnbalancedCrossEntropyLoss_sc=test_cuda.pth'),
    #     torchvision.models.vgg19:
    #         os.path.join(Config.DIR_MODELS, 'BestModels', date,
    #                      'emo8_vgg19_lr=0.001_loss=UnbalancedCrossEntropyLoss_sc=test_cuda.pth'),
    #     torchvision.models.resnet18:
    #         os.path.join(Config.DIR_MODELS, 'BestModels', date,
    #                      'emo8_resnet18_lr=0.001_loss=UnbalancedCrossEntropyLoss_sc=test_cuda.pth'),
    #     torchvision.models.resnet34:
    #         os.path.join(Config.DIR_MODELS, 'BestModels', date,
    #                      'emo8_resnet34_lr=0.001_loss=UnbalancedCrossEntropyLoss_sc=test_cuda.pth'),
    #     torchvision.models.resnet50:
    #         os.path.join(Config.DIR_MODELS, 'BestModels', date,
    #                      'emo8_resnet50_lr=0.001_loss=UnbalancedCrossEntropyLoss_sc=test_cuda.pth'),
    #     torchvision.models.resnet101:
    #         os.path.join(Config.DIR_MODELS, 'BestModels', date,
    #                      'emo8_resnet101_lr=0.001_loss=UnbalancedCrossEntropyLoss_sc=test_cuda.pth'),
    #     torchvision.models.googlenet:
    #         os.path.join(Config.DIR_MODELS, 'BestModels', date,
    #                      'emo8_googlenet_lr=0.001_loss=UnbalancedCrossEntropyLoss_sc=test_cuda.pth'),
    #     torchvision.models.inception_v3:
    #         os.path.join(Config.DIR_MODELS, 'BestModels', date,
    #                      'emo8_inception_v3_lr=0.0001_loss=UnbalancedCrossEntropyLoss_sc=test_cuda.pth'),
    #     torchvision.models.densenet121:
    #         os.path.join(Config.DIR_MODELS, 'BestModels', date,
    #                      'emo8_densenet121_lr=0.001_loss=UnbalancedCrossEntropyLoss_sc=test_cuda.pth'),
    #     torchvision.models.densenet161:
    #         os.path.join(Config.DIR_MODELS, 'BestModels', date,
    #                      'emo8_densenet161_lr=0.001_loss=UnbalancedCrossEntropyLoss_sc=test_cuda.pth')
    # }

    # ############################################################
    # Arousal3 Models
    # ############################################################
    # date = '20240118'
    # loss = 'CrossEntropyLoss'
    # nb_outputs = 3
    # base_models = {
    #     torchvision.models.alexnet:
    #         os.path.join(Config.DIR_MODELS, 'BestModels_Arousal', date,
    #                      f'arousal3_alexnet_lr=0.01_loss={loss}_sc=test_cuda.pth'),
    #     torchvision.models.vgg16:
    #         os.path.join(Config.DIR_MODELS, 'BestModels_Arousal', date,
    #                      f'arousal3_vgg16_lr=0.01_loss={loss}_sc=test_cuda.pth'),
    #     torchvision.models.vgg19:
    #         os.path.join(Config.DIR_MODELS, 'BestModels_Arousal', date,
    #                      f'arousal3_vgg19_lr=0.001_loss={loss}_sc=test_cuda.pth'),
    #     torchvision.models.resnet18:
    #         os.path.join(Config.DIR_MODELS, 'BestModels_Arousal', date,
    #                      f'arousal3_resnet18_lr=0.001_loss={loss}_sc=test_cuda.pth'),
    #     torchvision.models.resnet34:
    #         os.path.join(Config.DIR_MODELS, 'BestModels_Arousal', date,
    #                      f'arousal3_resnet34_lr=0.01_loss={loss}_sc=test_cuda.pth'),
    #     torchvision.models.resnet50:
    #         os.path.join(Config.DIR_MODELS, 'BestModels_Arousal', date,
    #                      f'arousal3_resnet50_lr=0.01_loss={loss}_sc=test_cuda.pth'),
    #     torchvision.models.resnet101:
    #         os.path.join(Config.DIR_MODELS, 'BestModels_Arousal', date,
    #                      f'arousal3_resnet101_lr=0.01_loss={loss}_sc=test_cuda.pth'),
    #     torchvision.models.googlenet:
    #         os.path.join(Config.DIR_MODELS, 'BestModels_Arousal', date,
    #                      f'arousal3_googlenet_lr=0.01_loss={loss}_sc=test_cuda.pth'),
    #     torchvision.models.inception_v3:
    #         os.path.join(Config.DIR_MODELS, 'BestModels_Arousal', date,
    #                      f'arousal3_inception_v3_lr=0.001_loss={loss}_sc=test_cuda.pth'),
    #     torchvision.models.densenet121:
    #         os.path.join(Config.DIR_MODELS, 'BestModels_Arousal', date,
    #                      f'arousal3_densenet121_lr=0.01_loss={loss}_sc=test_cuda.pth'),
    #     torchvision.models.densenet161:
    #         os.path.join(Config.DIR_MODELS, 'BestModels_Arousal', date,
    #                      f'arousal3_densenet161_lr=0.001_loss={loss}_sc=test_cuda.pth')
    # }

    # ############################################################
    # Valence3 Models
    # ############################################################
    # date = '20240118'
    # loss = 'CrossEntropyLoss'
    # nb_outputs = 3
    # base_models = {
    #     torchvision.models.alexnet:
    #         os.path.join(Config.DIR_MODELS, 'BestModels_Valence', date,
    #                      f'valence3_alexnet_lr=0.0001_loss={loss}_sc=test_cuda.pth'),
    #     torchvision.models.vgg16:
    #         os.path.join(Config.DIR_MODELS, 'BestModels_Valence', date,
    #                      f'valence3_vgg16_lr=0.001_loss={loss}_sc=test_cuda.pth'),
    #     torchvision.models.vgg19:
    #         os.path.join(Config.DIR_MODELS, 'BestModels_Valence', date,
    #                      f'valence3_vgg19_lr=0.001_loss={loss}_sc=test_cuda.pth'),
    #     torchvision.models.resnet18:
    #         os.path.join(Config.DIR_MODELS, 'BestModels_Valence', date,
    #                      f'valence3_resnet18_lr=0.001_loss={loss}_sc=test_cuda.pth'),
    #     torchvision.models.resnet34:
    #         os.path.join(Config.DIR_MODELS, 'BestModels_Valence', date,
    #                      f'valence3_resnet34_lr=0.001_loss={loss}_sc=test_cuda.pth'),
    #     torchvision.models.resnet50:
    #         os.path.join(Config.DIR_MODELS, 'BestModels_Valence', date,
    #                      f'valence3_resnet50_lr=0.01_loss={loss}_sc=test_cuda.pth'),
    #     torchvision.models.resnet101:
    #         os.path.join(Config.DIR_MODELS, 'BestModels_Valence', date,
    #                      f'valence3_resnet101_lr=0.01_loss={loss}_sc=test_cuda.pth'),
    #     torchvision.models.googlenet:
    #         os.path.join(Config.DIR_MODELS, 'BestModels_Valence', date,
    #                      f'valence3_googlenet_lr=0.001_loss={loss}_sc=test_cuda.pth'),
    #     torchvision.models.inception_v3:
    #         os.path.join(Config.DIR_MODELS, 'BestModels_Valence', date,
    #                      f'valence3_inception_v3_lr=0.001_loss={loss}_sc=test_cuda.pth'),
    #     torchvision.models.densenet121:
    #         os.path.join(Config.DIR_MODELS, 'BestModels_Valence', date,
    #                      f'valence3_densenet121_lr=0.1_loss={loss}_sc=test_cuda.pth'),
    #     torchvision.models.densenet161:
    #         os.path.join(Config.DIR_MODELS, 'BestModels_Valence', date,
    #                      f'valence3_densenet161_lr=0.01_loss={loss}_sc=test_cuda.pth')
    # }

    # # ############################################################
    # # Arousal1 Regression Models
    # # ############################################################
    # folder = 'BestModels_Arousal Reg Loss=MSE'
    # date = '20240515'
    # loss = 'MSELoss'
    # nb_outputs = 1
    # base_models = {
    #     torchvision.models.alexnet:
    #         os.path.join(Config.DIR_MODELS, folder, date,
    #                      f'arousal1_alexnet_lr=0.0001_loss=MSELoss_sc=test_cuda:1.pth'),
    #     torchvision.models.vgg16:
    #         os.path.join(Config.DIR_MODELS, folder, date,
    #                      f'arousal1_vgg16_lr=0.0001_loss=MSELoss_sc=test_cuda:1.pth'),
    #     torchvision.models.vgg19:
    #         os.path.join(Config.DIR_MODELS, folder, date,
    #                      f'arousal1_vgg19_lr=0.0001_loss=MSELoss_sc=test_cuda:1.pth'),
    #     torchvision.models.resnet18:
    #         os.path.join(Config.DIR_MODELS, folder, date,
    #                      f'arousal1_resnet18_lr=0.0001_loss=MSELoss_sc=test_cuda:1.pth'),
    #     torchvision.models.resnet34:
    #         os.path.join(Config.DIR_MODELS, folder, date,
    #                      f'arousal1_resnet34_lr=0.001_loss=MSELoss_sc=test_cuda:1.pth'),
    #     torchvision.models.resnet50:
    #         os.path.join(Config.DIR_MODELS, folder, date,
    #                      f'arousal1_resnet50_lr=0.001_loss=MSELoss_sc=test_cuda:1.pth'),
    #     torchvision.models.resnet101:
    #         os.path.join(Config.DIR_MODELS, folder, date,
    #                      f'arousal1_resnet101_lr=0.001_loss=MSELoss_sc=test_cuda:1.pth'),
    #     torchvision.models.googlenet:
    #         os.path.join(Config.DIR_MODELS, folder, date,
    #                      f'arousal1_googlenet_lr=0.001_loss=MSELoss_sc=test_cuda:1.pth'),
    #     torchvision.models.inception_v3:
    #         os.path.join(Config.DIR_MODELS, folder, date,
    #                      f'arousal1_inception_v3_lr=0.001_loss=MSELoss_sc=test_cuda:1.pth'),
    #     torchvision.models.densenet121:
    #         os.path.join(Config.DIR_MODELS, folder, date,
    #                      f'arousal1_densenet121_lr=0.001_loss=MSELoss_sc=test_cuda:1.pth'),
    #     torchvision.models.densenet161:
    #         os.path.join(Config.DIR_MODELS, folder, date,
    #                      f'arousal1_densenet161_lr=0.001_loss=MSELoss_sc=test_cuda:1.pth')
    # }

    # ############################################################
    # Valence1 Regression Models
    # ############################################################
    folder = 'BestModels_Valence Reg Loss=MSE'
    date = '20240515'
    loss = 'MSELoss'
    nb_outputs = 1
    base_models = {
        torchvision.models.alexnet:
            os.path.join(Config.DIR_MODELS, folder, date,
                         f'valence1_alexnet_lr=0.0001_loss=MSELoss_sc=test_cuda:0.pth'),
        torchvision.models.vgg16:
            os.path.join(Config.DIR_MODELS, folder, date,
                         f'valence1_vgg16_lr=0.001_loss=MSELoss_sc=test_cuda:0.pth'),
        torchvision.models.vgg19:
            os.path.join(Config.DIR_MODELS, folder, date,
                         f'valence1_vgg19_lr=0.001_loss=MSELoss_sc=test_cuda:0.pth'),
        torchvision.models.resnet18:
            os.path.join(Config.DIR_MODELS, folder, date,
                         f'valence1_resnet18_lr=0.001_loss=MSELoss_sc=test_cuda:0.pth'),
        torchvision.models.resnet34:
            os.path.join(Config.DIR_MODELS, folder, date,
                         f'valence1_resnet34_lr=0.001_loss=MSELoss_sc=test_cuda:0.pth'),
        torchvision.models.resnet50:
            os.path.join(Config.DIR_MODELS, folder, date,
                         f'valence1_resnet50_lr=0.001_loss=MSELoss_sc=test_cuda:0.pth'),
        torchvision.models.resnet101:
            os.path.join(Config.DIR_MODELS, folder, date,
                         f'valence1_resnet101_lr=0.001_loss=MSELoss_sc=test_cuda:0.pth'),
        torchvision.models.googlenet:
            os.path.join(Config.DIR_MODELS, folder, date,
                         f'valence1_googlenet_lr=0.001_loss=MSELoss_sc=test_cuda:0.pth'),
        torchvision.models.inception_v3:
            os.path.join(Config.DIR_MODELS, folder, date,
                         f'valence1_inception_v3_lr=0.001_loss=MSELoss_sc=test_cuda:0.pth'),
        torchvision.models.densenet121:
            os.path.join(Config.DIR_MODELS, folder, date,
                         f'valence1_densenet121_lr=0.001_loss=MSELoss_sc=test_cuda:0.pth'),
        torchvision.models.densenet161:
            os.path.join(Config.DIR_MODELS, folder, date,
                         f'valence1_densenet161_lr=0.001_loss=MSELoss_sc=test_cuda:0.pth')
    }

    # To buffer the model predictions, use the following. Note that you first have to train models on
    # a specific task, whose predictions can then be buffered.
    # First, load the trained models.
    for model, path_weights in base_models.items():
        # Create a DataSet with precomputed base model features for this model.
        dataset = AnnotationBufferINPredictionsDatasetFactory.get_dataset(model_name=model.__name__,
                                                                          ann_file=Config.FILE_ANN3,
                                                                          base_dir=Config.DIR_IMAGES,
                                                                          max_samples=-1)
        data_loader = torch.utils.data.DataLoader(dataset, batch_size=10, drop_last=False)

        # Get ImageNetClassifier corresponding to this base model.
        classifier = get_classifier_for_imagenet(model_name=model.__name__ + '_reg')()#(nb_outputs=nb_outputs)
        # Load pre-trained classifier weights.
        try:
            classifier.load_state_dict(torch.load(path_weights, map_location=torch.device('cuda')))
        except RuntimeError:
            w = torch.load(path_weights, map_location=torch.device('cuda'))
            ks = list(w.keys())
            for k in ks:
                w[k.replace('module.', '')] = w[k]
                del w[k]
            classifier.load_state_dict(w)
            print("Successfully replaced weight dictionary keys.")

        classifier.eval()

        BufferImageNetPredictions.buffer_in_predictions(device=torch.device('cuda'), data_loader=data_loader,
                                                        model=classifier, model_name=model.__name__,
                                                        nb_outputs=nb_outputs, target='val',
                                                        out_dir=Config.DIR_BUFFER_IN,
                                                        b_overwrite=True, b_save_individual=False)
