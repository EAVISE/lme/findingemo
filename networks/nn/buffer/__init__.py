"""
A package containing scripts to buffer the output (i.e., class predictions) of certain models.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
