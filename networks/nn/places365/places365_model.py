"""


.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import os

import torch
from torchvision import models

from config import Config
from tools.trainer_tools import TrainerTools


class Places365Model:
    ALEXNET = 'alexnet'
    DENSENET161 = 'densenet161'
    RESNET18 = 'resnet18'
    RESNET50 = 'resnet50'

    @staticmethod
    def load_model(model_name: str, device=torch.device('cuda')):
        if model_name == Places365Model.ALEXNET:
            net = models.alexnet(weights=None)
        elif model_name == Places365Model.DENSENET161:
            net = models.densenet161(weights=None)
        elif model_name == Places365Model.RESNET18:
            net = models.resnet18(weights=None)
        elif model_name == Places365Model.RESNET50:
            net = models.resnet50(weights=None)
        else:
            raise ValueError(f"Model {model_name} is currently not supported.")
        net.to(device)
        TrainerTools.change_last_layer(net, nb_outputs=365, device=torch.device(device))

        statedict = torch.load(os.path.join(Config.DIR_PRETRAINED_MODELS, 'Places365', 'Official',
                                            f'{model_name}_places365.pth.tar'))['state_dict']

        orig_keys = list(statedict.keys())
        for k in orig_keys:
            if model_name == Places365Model.DENSENET161:
                idx = k.find('norm.')
                if idx < 0:
                    idx = k.find('conv.')
                if idx < 0:
                    alt_k = k[7:]
                else:
                    next_ch = k[idx+5]
                    if next_ch.isdigit():
                        alt_k = k[7:].replace('norm.', 'norm').replace('conv.', 'conv')
                    else:
                        alt_k = k[7:]
                statedict[alt_k] = statedict[k]
            else:
                alt_k = k.replace('module.', '')
                statedict[alt_k] = statedict[k]
            if alt_k != k:
                del statedict[k]
        net.load_state_dict(statedict)

        return net
