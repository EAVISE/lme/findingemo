"""
Class that holds several different methods for obtaining class weights when one wants to train a network
using imbalanced data.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import math
from collections import Counter

import torch

from networks.data.annotation_dataset_tools import AnnotationEmotionMap


class ClassWeights:
    @staticmethod
    def classical_weights(dataset: torch.utils.data.Dataset) -> torch.Tensor:
        """
        More conventional way of computing class weights.
        See, e.g., https://www.analyticsvidhya.com/blog/2020/10/improve-class-imbalance-class-weights/

        :param dataset: the dataset containing the training dataset
        :return:
        """
        target_cnts = Counter(dataset.ys)
        nb_classes = len(target_cnts)
        sum_targets = sum(target_cnts.values())
        target_weights = torch.zeros(nb_classes)
        for k, v in target_cnts.items():
            target_weights[k] = sum_targets / (nb_classes * v)
        target_weights /= max(target_weights).item()

        return target_weights


    @staticmethod
    def unbalanced_crossentropy_24emo_weights(b_include_neutral=False,
                                              b_use_classical_weights=False,
                                              **params) -> dict:
        """
        Weight dictionary for Unbalanced Cross Entropy Loss in case of 24 distinct emotions.
        Element (i, j) corresponds to the weight of misclassifying a sample of class i as class j.

        :param b_include_neutral: also include "neutral" emotion, besides the 24 emotions from Plutchik's wheel.
        :param b_use_classical_weights: if 'True', use classical weights as starting point. In this case, it is expected
            that the necessary parameters to send to the classical_weights() are provided as '\*\*params'.
            "Starting point" means the classical weights will make up the diagonal elements, and off-diagonal elements
            will be weighted accordingly.
        :return: dictionary (k, v) with k = tpl(i,j) a tuple containing the matrix entry coordinates, and v its weight value
        """
        if b_use_classical_weights:
            starting_point = ClassWeights.classical_weights(**params)
            weights = {(i, i): starting_point[i].item() for i in range(24)}
            # First, check largest distance, so we can scale afterwards
            temp = [AnnotationEmotionMap.get_emo_distance(
                    AnnotationEmotionMap.INV_EMOTION_MAP_EXT[0],
                    AnnotationEmotionMap.INV_EMOTION_MAP_EXT[i]) for i in range(24)]
            scale_factor = 1./max(temp)

            # Misclassify i as j
            weights.update(
                {(i, j): (AnnotationEmotionMap.get_emo_distance(
                    AnnotationEmotionMap.INV_EMOTION_MAP_EXT[i],
                    AnnotationEmotionMap.INV_EMOTION_MAP_EXT[j]))*weights[(i, i)]*scale_factor
                    for i in range(24) for j in range(24) if i != j}
            )
        else:
            weights = {(i, i): 1 for i in range(24)}
            # First, check largest distance, so we can scale afterwards
            temp = [AnnotationEmotionMap.get_emo_distance(
                AnnotationEmotionMap.INV_EMOTION_MAP_EXT[0],
                AnnotationEmotionMap.INV_EMOTION_MAP_EXT[i]) for i in range(24)]
            scale_factor = 1. / max(temp)

            weights.update(
                {(i, j): 1 + AnnotationEmotionMap.get_emo_distance(
                    AnnotationEmotionMap.INV_EMOTION_MAP_EXT[i],
                    AnnotationEmotionMap.INV_EMOTION_MAP_EXT[j])*scale_factor
                    for i in range(24) for j in range(24) if i != j}
            )

        return weights

    @staticmethod
    def unbalanced_crossentropy_8emo_weights(b_use_classical_weights=False,
                                             **params) -> dict:
        """
        Weight dictionary for Unbalanced Cross Entropy Loss in case of 8 distinct emotions.
        Element (i, j) corresponds to the weight of misclassifying a sample of class i as class j.

        :param b_use_classical_weights: if 'True', use classical weights as starting point. In this case, it is expected
            that the necessary parameters to send to the classical_weights() are provided as '\*\*params'.
            "Starting point" means the classical weights will make up the diagonal elements, and off-diagonal elements
            will be weighted accordingly.
        :return: dictionary (k, v) with k = tpl(i,j) a tuple containing the matrix entry coordinates, and v its weight value
        """
        if b_use_classical_weights:
            starting_point = ClassWeights.classical_weights(**params)
            weights = {(i, i): starting_point[i].item() for i in range(8)}
            # First, check largest distance, so we can scale afterwards
            temp = [AnnotationEmotionMap.get_emo_distance(
                    AnnotationEmotionMap.INV_EMOTION_MAP_EXT[1],
                    AnnotationEmotionMap.INV_EMOTION_MAP_EXT[i]) for i in range(1, 24, 3)]
            scale_factor = 1./max(temp)

            # Misclassify i as j
            # Version 1
            # weights.update(
            #     {((i-1)//3, (j-1)//3): (AnnotationEmotionMap.get_emo_distance(
            #         AnnotationEmotionMap.INV_EMOTION_MAP_EXT[i],
            #         AnnotationEmotionMap.INV_EMOTION_MAP_EXT[j]))
            #                            * weights[((i-1)//3, (i-1)//3)] * scale_factor
            #         for i in range(1, 24, 3) for j in range(1, 24, 3) if i != j}
            # )

            # Version 2
            weights.update(
                {((i-1)//3, (j-1)//3): (AnnotationEmotionMap.get_emo_distance(
                    AnnotationEmotionMap.INV_EMOTION_MAP_EXT[i],
                    AnnotationEmotionMap.INV_EMOTION_MAP_EXT[j])/(1 + weights[((j-1)//3, (j-1)//3)]))
                                       * weights[((i-1)//3, (i-1)//3)] * scale_factor
                    for i in range(1, 24, 3) for j in range(1, 24, 3) if i != j}
            )
        else:
            weights = {(i, i): 1 for i in range(8)}
            # First, check largest distance, so we can scale afterwards
            temp = [AnnotationEmotionMap.get_emo_distance(
                    AnnotationEmotionMap.INV_EMOTION_MAP_EXT[1],
                    AnnotationEmotionMap.INV_EMOTION_MAP_EXT[i]) for i in range(1, 24, 3)]
            scale_factor = 1. / max(temp)

            weights.update(
                {((i-1)//3, (j-1)//3): 1 + AnnotationEmotionMap.get_emo_distance(
                    AnnotationEmotionMap.INV_EMOTION_MAP_EXT[i],
                    AnnotationEmotionMap.INV_EMOTION_MAP_EXT[j])*scale_factor
                    for i in range(1, 24, 3) for j in range(1, 24, 3) if i != j}
            )

        return weights

    @staticmethod
    def unbalanced_crossentropy_aro_val_weights(dataset: torch.utils.data.Dataset) -> dict:
        """
        Weight dictionary for Unbalanced Cross Entropy Loss in for arousal/valence classification.
        Element (i, j) corresponds to the weight of misclassifying a sample of class i as class j.

        :return: dictionary (k, v) with k = tpl(i,j) a tuple containing the matrix entry coordinates, and v its weight value
        """
        nb_classes = len(set(dataset.ys))

        starting_point = ClassWeights.classical_weights(dataset=dataset)
        weights = {(i, i): starting_point[i].item() for i in range(nb_classes)}

        # Misclassify i as j
        weights.update(
            {(i, j): (abs(j-i)/(1+weights[(j, j)]))*weights[(i, i)]
                for i in range(nb_classes) for j in range(nb_classes) if i != j}
        )

        return weights
