"""

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import torch


class DINOv2Classifier1(torch.nn.Module):
    def __init__(self, nb_outputs: int, dropout=None):
        """

        :param nb_outputs:
        :param dropout: for compatibility purposes; not used.
        """
        super().__init__()
        self.classifier = torch.nn.Sequential(
            torch.nn.Linear(in_features=1000, out_features=nb_outputs, bias=True)
        )

    def forward(self, x):
        return self.classifier(x)


class DINOv2Regressor1(torch.nn.Module):
    def __init__(self, dropout=None):
        """

        :param nb_outputs:
        :param dropout: for compatibility purposes; not used.
        """
        super().__init__()
        self.classifier = torch.nn.Sequential(
            torch.nn.Linear(in_features=1000, out_features=1, bias=True),
            torch.nn.Sigmoid()
        )

    def forward(self, x):
        return self.classifier(x)
