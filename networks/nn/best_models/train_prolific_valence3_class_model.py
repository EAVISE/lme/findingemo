"""
This script allows to easily obtain trained Arousal classification models on the FindingEmo dataset.

The script will, for each model type, and for each parameter setting in a grid search, train 5 models, then keep the
best model over all visited parameter settings and save it to disk.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import os
from shutil import copyfile

import dill
import torch.utils.data
from torch.nn import CrossEntropyLoss
from torchvision import models

from config import Config
from networks.base_trainer import BaseTrainer
from networks.configs.data_config import DataConfig
from networks.configs.net_config import NetConfig, StopCriterion
from networks.data.annotation_buffered_valence_dataset import AnnotationBufferedValenceDatasetFactory
from networks.nn.losses.unbalanced_crossentropy_loss import UnbalancedCrossEntropyLoss

if __name__ == '__main__':
    net_config = NetConfig(nb_outputs=3)
    net_config.b_allow_parallel = False
    net_config.b_use_manual_seed = False
    net_config.device = torch.device('cuda:1')
    net_config.max_epochs = 250
    net_config.dropout = 0.25
    net_config.hold_for_epochs = 3
    net_config.patience = 6
    net_config.batch_size = 50
    # net_config.learning_rate = .001
    net_config.log_interval = 5
    net_config.burn_in = 0
    net_config.stop_criterion = StopCriterion.TEST

    net_config.pretrained = True
    # net_config.network = models.vgg16
    net_config.network_params = {'nb_outputs': net_config.nb_outputs}
    # net_config.network_params = {'weights': models.VGG16_Weights.DEFAULT, 'progress': True}
    net_config.buffered_in = True

    net_config.b_freeze = False
    net_config.change_last_layer = False
    net_config.optimizer_class = torch.optim.Adam
    net_config.loss = UnbalancedCrossEntropyLoss
    net_config.loss_params = {}

    data_config = DataConfig()
    data_config.ann_file = Config.FILE_ANN_SINGLE
    data_config.base_dir = Config.DIR_IMAGES
    # data_config.base_dir = os.path.join(Config.DIR_DATA, "AnnImagesAlphaPoseBW", "vis")
    data_config.dataset_factory_params['ann_file'] = data_config.ann_file
    data_config.dataset_factory_params['base_dir'] = data_config.base_dir
    if net_config.buffered_in:
        data_config.dataset_factory = AnnotationBufferedValenceDatasetFactory
        data_config.dataset_factory_params['bins'] = [-2, 1]
        data_config.dataset_factory_params['max_samples'] = -1
    else:
        raise NotImplementedError
    data_config.dataset_factory_params['b_no_neutral'] = True

    # Logger params
    date = '20240118'
    out_dir = os.path.join(Config.DIR_MODELS, 'BestModels_Valence', date)
    os.makedirs(out_dir, exist_ok=True)

    data_config.logger_params['b_overwrite'] = True
    data_config.logger_params['b_create_out_dir'] = True

    models = [
        models.alexnet,
        models.vgg16, models.vgg19,
        models.resnet18, models.resnet34, models.resnet50, models.resnet101,
        models.googlenet,
        models.inception_v3,
        models.densenet121, models.densenet161
    ]

    for net in models:
        if isinstance(net, str):
            in_name = net
        else:
            in_name = net.__name__

        best_stats = None
        best_test_weighted_f1 = 0.
        logger_file = os.path.join(out_dir, f"best_model_log_{in_name}" + ".txt_TEMP")
        data_config.logger_params['logfile'] = logger_file

        for lr in [0.1, 0.01, 0.001, 0.0001]:
            for _ in range(5):
                print(f"At lr {lr}, run {_}...")
                net_config.network = net  # Reset network
                net_config.model_name = in_name
                if net_config.buffered_in:
                    data_config.dataset_factory_params['model_name'] = net_config.model_name

                net_config.learning_rate = lr
                net_config.loss = CrossEntropyLoss  # Reset loss
                net_config.set_default_lr_scheduler()

                print("=" * 60)
                print(f"Training model {net_config.model_name} with starting lr={lr}...")

                gen_file_name = f"valence{net_config.nb_outputs}_{net_config.model_name}_lr={lr}_" + \
                                f"loss={net_config.loss.__name__}_sc={net_config.stop_criterion.value}"

                trainer = BaseTrainer(net_config, data_config, b_set_loss_weights=True, b_default_init=True)
                _best_stats = trainer.train(b_save_model=False)
                _best_stats['model_name'] = gen_file_name
                if _best_stats['test_weighted_f1'] > best_test_weighted_f1:
                    best_test_weighted_f1 = _best_stats['test_weighted_f1']
                    best_stats = _best_stats

                    copyfile(logger_file, logger_file[:-5])

                    print("=" * 60)
                    print(f"Best so far: lr={lr} -- run {_} :: test weighted f1: {best_test_weighted_f1}")
        os.remove(logger_file)

        model_params = os.path.join(out_dir, best_stats['model_name'] + '_' + str(net_config.device) + '.pth')
        torch.save(best_stats['best_model'].state_dict(), model_params)
        print(f"Model parameters saved to: {model_params}.")

        if best_stats['transform'] is not None:
            transform_path = best_stats['model_name'] + '_' + str(net_config.device) + '_transform.dill'
            dill.dump(os.path.join(out_dir, best_stats['transform']), open(transform_path, 'wb'))
            print(f"Image transform saved to : {transform_path}")
