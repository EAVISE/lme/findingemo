"""
This script allows to easily obtain trained Arousal regression models on the FindingEmo dataset.

The script will, for each model type, and for each parameter setting in a grid search, train 5 models, then keep the
best model over all visited parameter settings and save it to disk.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import math
import os
from shutil import copyfile

import dill
import torch.utils.data

from torchvision import models
from torch.nn import MSELoss

from config import Config
from networks.base_reg_trainer import BaseRegTrainer
from networks.configs.data_config import DataConfig
from networks.configs.net_config import NetConfig, StopCriterion
from networks.data.annotation_buffered_arousal_reg_dataset import AnnotationBufferedArousalRegDatasetFactory
from networks.nn.losses.weighted_mse_loss import WeightedMSELoss

if __name__ == '__main__':
    net_config = NetConfig(nb_outputs=1)
    net_config.b_allow_parallel = False
    net_config.b_use_manual_seed = False
    net_config.device = torch.device('cuda')
    net_config.max_epochs = 250
    net_config.dropout = 0.25
    net_config.hold_for_epochs = 5
    net_config.patience = 8
    net_config.batch_size = 50
    net_config.learning_rate = .001
    net_config.log_interval = 5
    net_config.burn_in = 0
    net_config.stop_criterion = StopCriterion.TEST

    net_config.pretrained = True
    net_config.buffered_in = True

    net_config.b_freeze = False
    net_config.change_last_layer = False
    net_config.optimizer_class = torch.optim.Adam

    data_config = DataConfig()
    data_config.ann_file = Config.FILE_ANN
    data_config.base_dir = Config.DIR_IMAGES

    # Logger params
    date = '20240515'
    out_dir = os.path.join(Config.DIR_MODELS, 'BestModels_Arousal Reg Loss=MSE', date)
    os.makedirs(out_dir, exist_ok=True)

    data_config.logger_params['b_overwrite'] = True
    data_config.logger_params['b_create_out_dir'] = True

    models = [
        # 'CLIP',
        # 'DINOv2_1',
        'emonet',
        # models.alexnet,
        # models.vgg16, models.vgg19,
        # models.resnet18, models.resnet34, models.resnet50, models.resnet101,
        # models.googlenet,
        # models.inception_v3,
        # models.densenet121, models.densenet161,
        # 'vgg16_cc', 'alexnet_cc', 'resnet18_cc',
        # 'places365-feats_alexnet', 'places365-feats_resnet18',
        # 'places365-feats_resnet50', 'places365-feats_densenet161'
    ]

    for net in models:
        if isinstance(net, str):
            in_name = net
        else:
            in_name = net.__name__

        best_stats = None
        best_test_mae = math.inf
        best_file = None
        logger_file = os.path.join(out_dir, f"best_model_log_{in_name}" + ".txt_TEMP")
        data_config.logger_params['logfile'] = logger_file

        for lr in [0.001, 0.0001]:
            for _ in range(5):
                print(f"At lr {lr}, run {_}...")
                net_config.network = net  # Reset network
                net_config.model_name = in_name
                if net_config.buffered_in:
                    data_config.dataset_factory_params['model_name'] = net_config.model_name
                net_config.network = net_config.model_name + '_reg'

                net_config.learning_rate = lr
                net_config.loss = MSELoss  # Reset loss
                net_config.network_params = {}
                net_config.b_set_loss_weights = False
                net_config.set_default_lr_scheduler()

                if net_config.buffered_in or net_config.model_name == 'emonet':
                    data_config.dataset_factory = AnnotationBufferedArousalRegDatasetFactory
                    data_config.dataset_factory_params['model_name'] = net_config.model_name
                else:
                    raise NotImplementedError(f"Option isn't available: "
                                              f"buffered_in={net_config.buffered_in}, "
                                              f"model_name={net_config.model_name}")

                print("=" * 60)
                print(f"Training model {net_config.model_name} with starting lr={lr}...")

                gen_file_name = f"arousal{net_config.nb_outputs}_{net_config.model_name}_lr={lr}_" + \
                                f"loss={net_config.loss.__name__}_sc={net_config.stop_criterion.value}"

                trainer = BaseRegTrainer(net_config, data_config, b_default_init=True)
                _best_stats = trainer.train(b_save_model=False)
                _best_stats['model_name'] = gen_file_name
                if _best_stats['test_mae'] < best_test_mae:
                    best_test_mae = _best_stats['test_mae']
                    best_stats = _best_stats

                    if best_file is not None:
                        os.remove(best_file)

                    copyfile(logger_file, logger_file[:-5])
                    best_file = os.path.join(out_dir,
                                             best_stats['model_name'] + '_' + str(net_config.device) + '.pth')
                    torch.save(best_stats['best_model'].state_dict(), best_file)
                    print(f"Temporary best model parameters saved to: {best_file}.")

                    print("=" * 60)
                    print(f"Best so far: lr={lr} -- run {_} :: test MAE: {best_test_mae}")
        os.remove(logger_file)

        model_params = os.path.join(out_dir, best_stats['model_name'] + '_' + str(net_config.device) + '.pth')
        torch.save(best_stats['best_model'].state_dict(), model_params)
        print(f"Model parameters saved to: {model_params}.")

        if best_stats['transform'] is not None:
            transform_path = os.path.join(out_dir, best_stats['model_name'] + '_' + str(net_config.device) + '_transform.dill')
            dill.dump(best_stats['transform'], open(transform_path, 'wb'))
            print(f"Image transform saved to : {transform_path}")

        if best_stats['target_transform'] is not None:
            transform_path = os.path.join(out_dir, best_stats['model_name'] + '_' + str(net_config.device) + '_target_transform.dill')
            dill.dump(best_stats['target_transform'], open(transform_path, 'wb'))
            print(f"Target transform saved to : {transform_path}")
