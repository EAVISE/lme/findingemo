"""
Scripts to train models for several tasks.
The general idea is to essentially do a grid search, and only keep the best performing model from the batch.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
