"""
Script to use the output of YoLo v3 + OpenImages, take the "Human face" regions within that output and send it through
a FER model.
By default, the FER model we use is the one from https://github.com/LetheSec/Fer2013-Facial-Emotion-Recognition-Pytorch

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import os

import lightnet as ln
import numpy as np
import pandas as pd
import torch
from PIL import Image
from lightnet.models import YoloV3
from torch import nn
from torch.nn import functional as F
from torchvision import transforms

from config import Config
from networks.data.buffer_oi_dataset import BufferOIDataset
from networks.nn.fer.resnet import ResNet18
from tools.tensor_tools import TensorTools


class BaseOIToFER(object):
    def __init__(self, threshold=0.005, min_size=32, device=torch.device('cpu')):
        """

        :param threshold: threshold to be applied to YoLo output; regions with confidence value <threshold are ignored
        :param min_size: minimum width/height that face regions should have; if less, the region will be ignored
        :param device: device to load the model on
        :return:
        """
        self.threshold = threshold
        self.device = device

        # Load FER model
        fer_model = ResNet18().cuda()
        checkpoint = torch.load(os.path.join(Config.DIR_PRETRAINED_MODELS, "FER2013", "official", "best_checkpoint.tar"))
        fer_model.load_state_dict(checkpoint['model_state_dict'])
        fer_model.eval()
        self.fer_model = fer_model.to(device)

        # Define FER Transform, as used by LetheSec
        self.fer_transform = transforms.Compose([
            transforms.Grayscale(),
            transforms.TenCrop(40),
            transforms.Lambda(lambda crops: torch.stack(
                [transforms.ToTensor()(crop) for crop in crops])),
            transforms.Lambda(lambda tensors: torch.stack(
                [transforms.Normalize(mean=(0,), std=(255,))(t) for t in tensors])),
        ])

        self.min_size = min_size

    def process_yolo(self, img, img_paths_or_yolo_output, b_standardize=True):
        """
        Depending on the implementation, second argument will contain the image paths, or the YoLo output for the image(s)

        :param img:
        :param img_paths_or_yolo_output:
        :param b_standardize:
        :return:
        """
        raise NotImplementedError

    def _process_img_df(self, output_img_df: pd.DataFrame, img: torch.Tensor):
        avg_emo_scores, std_emo_scores = torch.zeros(7), torch.zeros(7)

        if not output_img_df.empty:
            pil_img = (255 * img.cpu().numpy().transpose(1, 2, 0)).astype(np.uint8)
            pil_img = Image.fromarray(pil_img)

            emo_scores = []  # Container to keep track of the emo scores over the face regions
            for row_idx, row in output_img_df.iterrows():
                # Filter face regions
                if 0.5 < row.width / row.height < 1.5 and \
                        row.width > self.min_size and row.height > self.min_size:
                    # Extract face region from image
                    crop = pil_img.crop(
                        (row.x_top_left, row.y_top_left, row.x_top_left + row.width, row.y_top_left + row.height))
                    # Resize region
                    crop = crop.resize((48, 48))
                    # Apply transforms to transform crop to valid input to FER model; this will generate 10 crops from the image
                    crop = self.fer_transform(crop).to(self.device)
                    # Process crops with model; this will result in 10 outputs, 1 per crop
                    outputs = self.fer_model(crop)
                    # Average outputs
                    outputs = torch.sum(outputs, dim=0) / 10
                    # Take softmax to obtain emo score
                    emo_score = torch.softmax(outputs, dim=0)

                    emo_scores.append(emo_score)

            # If there are emo scores...
            if emo_scores:
                stacked_emo_scores = torch.stack(emo_scores)
                avg_emo_scores = torch.mean(stacked_emo_scores, dim=0)
                if len(emo_scores) > 1:
                    std_emo_scores = torch.std(stacked_emo_scores, dim=0)
                else:
                    std_emo_scores = torch.zeros(7)

        return avg_emo_scores, std_emo_scores


class BufferedOIToFER(BaseOIToFER):
    def __init__(self, threshold=0.005, min_size=32, device=torch.device('cpu')):
        super().__init__(threshold, min_size, device)

        self.boid = BufferOIDataset()

    def process_yolo(self, img, img_paths, b_standardize=True):
        """
        Process YoLo output.

        This method will extract all the "Human face" regions from this output,

        :param img: image(s) from which yolo_output was generated, as torch.Tensor; format (N, C, H, W)... or is it (N, C, W, H)?
        :param img_paths: path to image(s) to process, as list
        :param b_standardize: if True, will standardize the averages and stds tensors
        :return: (averages, stds) of all emotion scores of all "Human face" regions that were parsed
        """
        batch_size = len(img)
        batch_avg_emo_scores, batch_std_emo_scores = torch.zeros((batch_size, 7)), torch.zeros((batch_size, 7))

        with torch.no_grad():
            # We process the output image per image, in case a batch has more than 1 image
            for idx in range(img.shape[0]):
                # Post process YoLo output to get boxed regions + labels
                # Filter output for 'Human face' regions
                output_img_df = self.boid.get_csv_for_img(img_paths[idx], b_rebuffer=True)
                output_img_df = output_img_df[(output_img_df["class_label"] == 'Human face') &
                                              (output_img_df["confidence"] > self.threshold)]

                avg_emo_scores, std_emo_scores = self._process_img_df(output_img_df=output_img_df, img=img[idx])
                batch_avg_emo_scores[idx] = avg_emo_scores
                batch_std_emo_scores[idx] = std_emo_scores

        if b_standardize:
            # TODO: standardize std values using avg statistics! i.e., don't normalize the std values to 1
            # Otherwise, you lose the relative importance compared to avg... or something
            # Think about it, then decide
            batch_avg_emo_scores, batch_std_emo_scores = TensorTools.standardize_2d_tensor(batch_avg_emo_scores), \
                                                         TensorTools.standardize_2d_tensor(batch_std_emo_scores)

        return batch_avg_emo_scores, batch_std_emo_scores


class OIToFER(BaseOIToFER):
    def __init__(self, threshold=0.005, min_size=32, device=torch.device('cpu')):
        """

        :param threshold: threshold to be applied to YoLo output; regions with confidence value <threshold are ignored
        :param min_size: minimum width/height that face regions should have; if less, the region will be ignored
        :param device: device to load the FER model on
        :return:
        """
        super().__init__(threshold, min_size, device)

        # Class map
        class_map = []
        with open(os.path.join(Config.DIR_CODE, "NeuroANNData", "networks", "nn", "object_detection", "openimages.names"), "r") as fin:
            for line in fin:
                line = line.strip()
                class_map.append(line)

        # Create post-processing pipeline
        self.post = ln.data.transform.Compose([
            # GetBoxes transformation generates bounding boxes from network output
            ln.data.transform.GetMultiScaleDarknetBoxes(
                conf_thresh=threshold,
                network_strides=[32, 16, 8],
                anchors=[[(3.625, 2.8125), (4.875, 6.1875), (11.65625, 10.1875)],
                         [(1.875, 3.8125), (3.875, 2.8125), (3.6875, 7.4375)],
                         [(1.25, 1.625), (2.0, 3.75), (4.125, 2.875)]]
            ),

            # Filter transformation to filter the output boxes
            ln.data.transform.NMS(
                nms_thresh=threshold
            ),

            # Miscelaneous transformation that transforms the output boxes to a brambox dataframe
            ln.data.transform.TensorToBrambox(
                class_label_map=class_map,
            )
        ])

    def process_yolo(self, img, yolo_output, b_standardize=False):
        """
        Process YoLo output.

        This method will extract all the "Human face" regions from this output,

        :param img: image(s) from which yolo_output was generated, as torch.Tensor; format (N, C, H, W)... or is it (N, C, W, H)?
        :param yolo_output: output from parsing image with YoLo v3 + OpenImages network
        :param b_standardize: if True, will standardize the averages and stds tensors
        :return: (averages, stds) of all emotion scores of all "Human face" regions that were parsed
        """
        batch_size = img.shape[0]
        batch_avg_emo_scores, batch_std_emo_scores = torch.zeros((batch_size, 7)), torch.zeros((batch_size, 7))

        with torch.no_grad():
            output_df = self.post(yolo_output)
            # We process the output image per image, in case a batch has more than 1 image
            for idx in range(img.shape[0]):
                # Post process YoLo output to get boxed regions + labels
                # Filter output for 'Human face' regions
                output_img_df = output_df[(output_df["image"] == idx) & (output_df["class_label"] == 'Human face')]

                avg_emo_scores, std_emo_scores = self._process_img_df(output_img_df=output_img_df, img=img[idx])
                batch_avg_emo_scores[idx] = avg_emo_scores
                batch_std_emo_scores[idx] = std_emo_scores

        if b_standardize:
            # TODO: standardize std values using avg statistics! i.e., don't normalize the std values to 1
            # Otherwise, you lose the relative importance compared to avg... or something
            # Think about it, then decide
            batch_avg_emo_scores, batch_std_emo_scores = TensorTools.standardize_2d_tensor(batch_avg_emo_scores), \
                                                         TensorTools.standardize_2d_tensor(batch_std_emo_scores)

        return batch_avg_emo_scores, batch_std_emo_scores


class OIToFERModule(nn.Module):
    """
    Use this network to obtain the OIToFER features.
    """
    def __init__(self, nb_outputs=2, dropout=0.25, device=torch.device('cpu')):
        super().__init__()
        # Open Images network
        self.model_oi = YoloV3(601)
        self.model_oi.load(os.path.join(Config.DIR_PRETRAINED_MODELS, 'DarkNet', 'yolov3-openimages.weights'))

        self.oi_to_fer = OIToFER(device=device)

        # Freeze layers
        for param in self.model_oi.parameters():
            param.requires_grad = False

    def forward(self, x):
        x_oi = self.model_oi(x)
        x_avg, x_std = self.oi_to_fer.process_yolo(img=x, yolo_output=x_oi, b_standardize=True)
        x = torch.concat([x_avg, x_std], dim=1).to(x.device)

        return x


class OIToFERClassifier(nn.Module):
    """
    Use this network to use only the OIToFER output to train a classifier.
    """
    def __init__(self, nb_outputs=2, dropout=0.25, device=torch.device('cpu')):
        super().__init__()
        # Open Images network
        self.model_oi = YoloV3(601)
        self.model_oi.load(os.path.join(Config.DIR_PRETRAINED_MODELS, 'DarkNet', 'yolov3-openimages.weights'))

        self.oi_to_fer = OIToFER(device=device)

        # Freeze layers
        for param in self.model_oi.parameters():
            param.requires_grad = False

        self.dropout = dropout
        self.A = F.elu  # Activation

        self.l1 = nn.Linear(in_features=14, out_features=28)
        self.l2 = nn.Linear(in_features=28, out_features=28)
        self.l3 = nn.Linear(in_features=28, out_features=nb_outputs)

    def forward(self, x):
        x_oi = self.model_oi(x)
        x_avg, x_std = self.oi_to_fer.process_yolo(img=x, yolo_output=x_oi, b_standardize=True)
        x = torch.concat([x_avg, x_std], dim=1).to(x.device)

        x = F.dropout(self.A(self.l1(x)), p=self.dropout)
        x = F.dropout(self.A(self.l2(x)), p=self.dropout)
        x = self.l3(x)

        if not self.training:
            x = torch.softmax(x, dim=1)

        return x
