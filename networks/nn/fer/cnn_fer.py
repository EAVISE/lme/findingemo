"""

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import torch.nn as nn
import torch.nn.functional as F


class CNNFERHead(nn.Module):
    def __init__(self, in_channels=1, dropout=0.25):
        super().__init__()
        self.act = F.relu
        self.dropout = nn.Dropout(p=dropout)
        self.dropout2d = nn.Dropout2d(p=dropout)

        self.conv1a = nn.Conv2d(in_channels=in_channels, out_channels=12, kernel_size=5, padding=2)
        self.conv1b = nn.Conv2d(in_channels=12, out_channels=12, kernel_size=5, padding=2)
        self.bn1 = nn.BatchNorm2d(12)
        self.mp1 = nn.MaxPool2d(2, 2)  # 1 48 48 -> 12 24 24

        self.conv2a = nn.Conv2d(in_channels=12, out_channels=24, kernel_size=3, padding=1)
        self.conv2b = nn.Conv2d(in_channels=24, out_channels=24, kernel_size=3, padding=1)
        self.bn2 = nn.BatchNorm2d(24)
        self.mp2 = nn.MaxPool2d(2, 2)  # 12 24 24 -> 24 12 12

        self.l1 = nn.Linear(in_features=24*12*12, out_features=24*12*12)
        self.l2 = nn.Linear(in_features=24*12*12, out_features=24*12*12)

        self.conv3a = nn.Conv2d(in_channels=24, out_channels=48, kernel_size=3, padding=1)
        self.conv3b = nn.Conv2d(in_channels=48, out_channels=48, kernel_size=3, padding=1)
        self.bn3 = nn.BatchNorm2d(48)
        self.mp3 = nn.MaxPool2d(2, 2)  # 24 12 12 -> 48 6 6

        self.conv4a = nn.Conv2d(in_channels=48, out_channels=96, kernel_size=3, padding=1)
        self.conv4b = nn.Conv2d(in_channels=96, out_channels=96, kernel_size=3, padding=1)
        self.bn4 = nn.BatchNorm2d(96)
        self.mp4 = nn.MaxPool2d(2, 2)  # 48 6 6 -> 96 3 3

        self.flat_dim = 96 * 3 * 3  # Dimension when flattening last layer

    def forward(self, x, b_debug=False):
        x = self.mp1(self.act(self.dropout2d(self.bn1(self.conv1b(self.conv1a(x.unsqueeze(1)))))))
        x = self.mp2(self.act(self.dropout2d(self.bn2(self.conv2b(self.conv2a(x))))))
        x = x.view((x.shape[0], -1))
        x = self.l2(self.dropout(self.act(self.l1(x))))
        x = x.view((x.shape[0], 24, 12, 12))
        x = self.mp3(self.act(self.dropout2d(self.bn3(self.conv3b(self.conv3a(x))))))
        x = self.mp4(self.act(self.dropout2d(self.bn4(self.conv4b(self.conv4a(x))))))

        return x


# ############################################################
# Read-out layer
# ############################################################
class CNNFERReadOut(nn.Module):
    def __init__(self, head=None, nb_outputs=7, dropout=0.25):
        """
        Read-out network to be used in conjunction a CNN that serves as head.

        :param head: CNN to use as head
        :param nb_outputs: nb of nodes in the final layer
        :param dropout: dropout rate
        """
        super().__init__()
        if head is None:
            self.head = CNNFERHead(dropout=dropout)
        else:
            self.head = head()
        self.dropout = nn.Dropout(p=dropout)
        self.l1 = nn.Linear(self.head.flat_dim, self.head.flat_dim)
        self.l2 = nn.Linear(self.head.flat_dim, self.head.flat_dim//4)
        self.l3 = nn.Linear(self.head.flat_dim//4, nb_outputs)

    def forward(self, x, b_debug=False):
        if b_debug:
            print(f"input: {x.shape}")
        x = self.head(x)

        x = x.view(-1, self.head.flat_dim)
        if b_debug:
            print(f"view: {x.shape}")
        x = self.l3(F.relu(self.dropout(self.l2(F.relu(self.dropout(self.l1(x)))))))
        # x = self.l2(x)
        if b_debug:
            print(f"l2: {x.shape}")

        if not self.training:
            x = F.softmax(x, dim=1)
            if b_debug:
                print(f"softmax: {x.shape}")

        return x
