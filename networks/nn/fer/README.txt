Check out:
https://github.com/LetheSec/Fer2013-Facial-Emotion-Recognition-Pytorch

They achieve 73+% test accuracy by meticulously retraining an ImageNet network.
Paper: https://arxiv.org/pdf/2105.03588.pdf