"""
Contains a simple single linear layer network that serves as a regression model using EmoNet outputs.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import torch


class EmoNetClassifier1(torch.nn.Module):
    """
    Use pre-computed EmoNet features.
    """
    def __init__(self, nb_outputs=8, dropout=None):
        """

        :param nb_outputs: number of output nodes, i.e., target classes
        :param dropout: does nothing; here for compatibility purposes with ImageNetClassifier models
        """
        super().__init__()
        self.l1 = torch.nn.Linear(in_features=20, out_features=nb_outputs)

    def forward(self, en_feats):
        """

        :param en_feats: EmoNet features
        :return:
        """
        out = self.l1(en_feats)

        if not self.training:
            out = torch.nn.functional.softmax(out, dim=1)

        return out


class EmoNetRegressor1(torch.nn.Module):
    """
    Use precomputed EmoNet features to predict some numerical value.
    """
    def __init__(self, dropout=None):
        """

        :param nb_outputs:
        :param dropout: for compatibility purposes; not used.
        """
        super().__init__()
        self.classifier = torch.nn.Sequential(
            torch.nn.Linear(in_features=20, out_features=1, bias=True),
            torch.nn.Sigmoid()
        )

    def forward(self, x):
        return self.classifier(x)
