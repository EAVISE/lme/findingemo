"""
Unbalanced Cross Entropy loss that also includes off-diagonal elements.
In other words: you can give different weighs to different misclassifications.
Say you have 3 classes, then in regular CrossEntropy, classifying an element
with ground truth class 0 as either class 1 or 2 would result in the same penalty.
With this loss, you can give different weights to both cases, penalizing one
misclassification harder than the other.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import torch


class UnbalancedCrossEntropyLoss(torch.nn.Module):
    def __init__(self, cs: {} = None, nb_classes=3, eps=1e-12):
        """

        :param cs: a dictionary containing the weights for all those terms in the loss that should be altered from the default
            values. By default, these are 1 for the diagonal terms (i.e., "class x examples that were predicted as being of
            class x", and 0 for the cross terms (i.e., "class x examples that were predicted as being of class y").
            This means that when no specific weights are provided, this loss reverts to regular unweighted Cross Entropy.
            Format: key = tuple (i, j) with i and j class indices
                    value = weight for this particular tuple
        :param nb_classes: number of classes, i.e., output nodes of the network
        :param eps: numerical stability term; replace 0-odds with eps
        """
        super().__init__()
        self.nb_classes = nb_classes
        # Default weight for diagonal terms = 1, for cross terms = 0
        self.cs = torch.diag(torch.ones(nb_classes))
        # Update where necessary
        if cs is not None:
            for k, v in cs.items():
                self.cs[k[0], k[1]] = v

        self.eps = torch.tensor(eps)

    def get_weights_as_tbl(self):
        """
        Get table representing weights encoded as string.

        :return: str representing table representing weights
        """
        tbl = "\t"
        for i in range(self.nb_classes):
            tbl += f'{i:>3d}\t\t'
        tbl += '\n'
        for i in range(self.nb_classes):
            tbl += f'{i:3d}\t'
            for j in range(self.nb_classes):
                tbl += f'{self.cs[i, j]:.2f}\t'
            tbl += '\n'

        return tbl

    def print_weights(self):
        tbl = self.get_weights_as_tbl()

        print("UnbalancedCrossEntropy weights:")
        print(tbl)

    def forward(self, input: torch.Tensor, target: torch.Tensor):
        """

        :param input: raw, unnormalized scores in a tensor of size (B, C), with B = batchsize, C = nb. of classes
        :param target: either one-hot encoded target vectors, or integers denoting the target class
        :return:
        """
        # Convert raw input to log of probabilities
        x = torch.nn.functional.log_softmax(input, dim=1)

        # Regular Cross Entropy
        # ce = torch.sum(target * torch.log(x), dim=1)

        weighted_mean_factor = torch.tensor(0.).to(input.device)  # sum of all coefficients, to be used to compute weighted mean
        loss = torch.tensor(0.).to(input.device)
        # Cycle through samples in batch
        for r in range(target.shape[0]):
            row_x = x[r, :]
            argmax_x = torch.argmax(row_x).item()
            if len(target.shape) == 2:
                row_t = target[r, :]
                argmax_t = torch.argmax(row_t).item()
            elif len(target.shape) == 1:
                argmax_t = target[r]
            else:
                raise ValueError(f"Target has {len(target.shape)} dimensions, don't know what to do in this case.")

            # Regular Cross Entropy
            weight_ce = self.cs[(argmax_t, argmax_t)]
            weighted_mean_factor += weight_ce
            ce = weight_ce * row_x[argmax_t]
            loss += ce

            if argmax_x == argmax_t:
                continue

            # Misclassified item of class t as class x; cst = cross term
            weight_cst = self.cs[(argmax_t, argmax_x)]
            weighted_mean_factor += weight_cst
            if weight_cst == 0:
                continue
            # Don't forget we use log_softmax above, so need to take exp(x) here to obtain probability
            if row_x[argmax_x] == 0.:
                cst = weight_cst * torch.log(self.eps)
            else:
                cst = weight_cst * torch.log(1 - torch.exp(row_x[argmax_x]))
            loss += cst

        # TODO: Allow for different ways of averaging
        res = -loss/weighted_mean_factor
        # res = -loss/(2*target.shape[0])

        return res
