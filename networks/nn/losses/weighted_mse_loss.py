"""

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import torch


class WeightedMSELoss(torch.nn.Module):
    def __init__(self, class_weights, device) -> None:
        super().__init__()
        self.class_weights = class_weights
        self.device = device

    def forward(self, input: torch.Tensor, target: torch.Tensor) -> torch.Tensor:
        target_weight = torch.zeros(len(target)).to(self.device)
        for i in range(len(target)):
            target_weight[i] = self.class_weights[target[i][0].item()]
        target_weight = target_weight.unsqueeze(1)

        return (target_weight * (input - target) ** 2).mean()
