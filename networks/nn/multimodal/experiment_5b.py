"""
Network to use output from three input streams, e.g., ImageNet, OIToFER and EmoNet or Places365.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""

import torch
import torch.nn as nn
import torch.nn.functional as F


def _check_feats(feats):
    if isinstance(feats, list):
        # Convert format; for each image, concatenate the average and std values
        feats = torch.stack([torch.cat([feats[0][i], feats[1][i]], dim=0)
                             for i in range(feats[0].shape[0])], dim=0).to(feats[0].device)
    return feats


class Exp5bMergerNetwork1(nn.Module):
    """
    This merger network will combine the output from an ImageNet network and Places365.

    It is assumed that pre-computed features are used, both for ImageNet and for Places365.
    """
    def __init__(self, nb_stream1: int, nb_stream2: int, nb_stream3: int,
                 b_softmax1=False, b_softmax2=False, b_softmax3=False,
                 dropout=0.25, nb_outputs=8):
        """

        :param nb_stream1: nb of features in stream1.
        :param nb_stream2: nb of features in stream2.
        :param nb_stream3: nb of features in stream3.
        :param b_softmax1: apply softmax to stream1 features.
        :param b_softmax2: apply softmax to stream2 features.
        :param b_softmax3: apply softmax to stream3 features.
        :param dropout:
        :param nb_outputs: number of output nodes, i.e., target classes.
        """
        super().__init__()
        nb_in_feats = nb_stream1 + nb_stream2 + nb_stream3

        self.b_softmax_1, self.b_softmax_2, self.b_softmax_3 = b_softmax1, b_softmax2, b_softmax3
        self.l1 = nn.Linear(in_features=nb_in_feats, out_features=nb_outputs)
        self.dropout = nn.Dropout(p=dropout)

    def forward(self, stream1_feats, stream2_feats, stream3_feats, img_path):
        """

        :param stream1_feats: pre-computed ImageNet features
        :param stream2_feats: stream2 features
        :param stream3_feats: stream3 features
        :param img_path: image path; not used, but put there to be compatible with BaseDoubleTrainer
        :return:
        """
        stream1_feats = _check_feats(stream1_feats)
        stream2_feats = _check_feats(stream2_feats)
        stream3_feats = _check_feats(stream3_feats)

        if self.b_softmax_1:
            stream1_feats = torch.softmax(stream1_feats, dim=1)
        if self.b_softmax_2:
            stream2_feats = torch.softmax(stream2_feats, dim=1)
        if self.b_softmax_3:
            stream3_feats = torch.softmax(stream3_feats, dim=1)

        x = torch.cat([stream1_feats, stream2_feats, stream3_feats], dim=1)

        out = self.l1(self.dropout(x))

        if not self.training:
            out = F.softmax(out, dim=1)

        return out


class Exp5bMergerNetwork2(nn.Module):
    """
    Same as Exp5bMergerNetwork1, but with 2 linear layers.
    """
    def __init__(self, nb_stream1: int, nb_stream2: int, nb_stream3: int,
                 b_softmax1=False, b_softmax2=False, b_softmax3=False,
                 nb_outputs=8):
        """

        :param nb_stream1: nb of features in stream1.
        :param nb_stream2: nb of features in stream2.
        :param nb_stream3: nb of features in stream3.
        :param b_softmax1: apply softmax to stream1 features.
        :param b_softmax2: apply softmax to stream2 features.
        :param b_softmax3: apply softmax to stream3 features.
        :param nb_outputs: number of output nodes, i.e., target classes.
        """
        super().__init__()
        nb_in_feats = nb_stream1 + nb_stream2 + nb_stream3

        self.b_softmax_1, self.b_softmax_2, self.b_softmax_3 = b_softmax1, b_softmax2, b_softmax3
        self.l1 = nn.Linear(in_features=nb_in_feats, out_features=nb_in_feats)
        self.l2 = nn.Linear(in_features=nb_in_feats, out_features=nb_outputs)
        self.dropout = nn.Dropout(p=0.2)

    def forward(self, stream1_feats, stream2_feats, stream3_feats, img_path):
        """

        :param stream1_feats: stream1 features
        :param stream2_feats: stream2 features
        :param stream3_feats: stream3 features
        :param img_path: image path; not used, but put there to be compatible with BaseDoubleTrainer
        :return:
        """
        stream1_feats = _check_feats(stream1_feats)
        stream2_feats = _check_feats(stream2_feats)
        stream3_feats = _check_feats(stream3_feats)

        if self.b_softmax_1:
            stream1_feats = torch.softmax(stream1_feats, dim=1)
        if self.b_softmax_2:
            stream2_feats = torch.softmax(stream2_feats, dim=1)
        if self.b_softmax_3:
            stream3_feats = torch.softmax(stream3_feats, dim=1)

        x = torch.cat([stream1_feats, stream2_feats, stream3_feats], dim=1)

        out = self.l2(self.dropout(self.l1(self.dropout(x))))

        if not self.training:
            out = F.softmax(out, dim=1)

        return out


class Exp5bMergerNetwork3(nn.Module):
    """
    Same as Exp5bMergerNetwork1, but with 3 linear layers.
    """
    def __init__(self, nb_stream1: int, nb_stream2: int, nb_stream3: int,
                 b_softmax1=False, b_softmax2=False, b_softmax3=False,
                 nb_outputs=8):
        """

        :param nb_stream1: nb of features in stream1.
        :param nb_stream2: nb of features in stream2.
        :param nb_stream3: nb of features in stream3.
        :param b_softmax1: apply softmax to stream1 features.
        :param b_softmax2: apply softmax to stream2 features.
        :param b_softmax3: apply softmax to stream3 features.
        :param nb_outputs: number of output nodes, i.e., target classes.
        """
        super().__init__()
        nb_in_feats = nb_stream1 + nb_stream2 + nb_stream3

        self.b_softmax_1, self.b_softmax_2, self.b_softmax_3 = b_softmax1, b_softmax2, b_softmax3
        self.l1 = nn.Linear(in_features=nb_in_feats, out_features=nb_in_feats)
        self.l2 = nn.Linear(in_features=nb_in_feats, out_features=nb_in_feats)
        self.l3 = nn.Linear(in_features=nb_in_feats, out_features=nb_outputs)
        self.dropout = nn.Dropout(p=0.2)

    def forward(self, stream1_feats, stream2_feats, stream3_feats, img_path):
        """

        :param in_feats: pre-computed ImageNet features
        :param stream2_feats: stream2 features
        :param stream3_feats: stream3 features
        :param img_path: image path; not used, but put there to be compatible with BaseDoubleTrainer
        :return:
        """
        stream1_feats = _check_feats(stream1_feats)
        stream2_feats = _check_feats(stream2_feats)
        stream3_feats = _check_feats(stream3_feats)

        if self.b_softmax_1:
            stream1_feats = torch.softmax(stream1_feats, dim=1)
        if self.b_softmax_2:
            stream2_feats = torch.softmax(stream2_feats, dim=1)
        if self.b_softmax_3:
            stream3_feats = torch.softmax(stream3_feats, dim=1)

        x = torch.cat([stream1_feats, stream2_feats, stream3_feats], dim=1)

        out = self.l3(self.dropout(self.l2(self.dropout(self.l1(self.dropout(x))))))

        if not self.training:
            out = F.softmax(out, dim=1)

        return out
