"""
Load a trained multimodal model.
The issue with this is that the models have typically been trained with buffered features.
When training and saving a model, and then trying to load the saved model, we can not just
load the saved weights into the multimodal model.
What we need to do, is load the ORIGINAL models (i.e., not the buffered versions), and
replace their last layer(s) with new layers into which we then load the saved weights.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import os

import PIL
import torch
import torchvision.models
from emonet_py.emonet import EmoNet, EmoNetPreProcess

from config import Config
from networks.nn.imagenet.load_buffered_imagenet import LoadBufferedImageNet
from networks.nn.multimodal.experiment_5b import Exp5bMergerNetwork1


class TrainedMultimodalModel(torch.nn.Module):
    # List of available models that can be loaded
    EXP5B_VGG19_VGG16SIN_EMONET_20231207 = "Exp5b_VGG19+VGG16SIN+EmoNet_20231207"

    def __init__(self, multimodal_class, file_multimodal_weights, sub_models: list,
                 nb_stream1=8, nb_stream2=8, nb_stream3=8, b_softmax1=True, b_softmax2=True, b_softmax3=True,
                 device=torch.device('cuda')):
        """

        :param multimodal_class:
        :param file_multimodal_weights:
        :param sub_models: list of tuples containing the ALREADY LOADED submodels (i.e., the models that serve as input\
        to the multimodal model) and their corresponding image preprocessors.\
        The order in the list should reflect the order in which the model outputs should be presented to the multimodal model.\
        So, sub_models = list(tpl(model, image_preprocessor)), such that the model can be called by using\
        out = model(image_preprocessor(image)) for some PIL.Image object 'image'.
        :param nb_stream1:
        :return:
        """
        super().__init__()
        self.sub_models = torch.nn.ModuleList([sub_models[i][0] for i in range(len(sub_models))])
        self.pre_procs = [sub_models[i][1] for i in range(len(sub_models))]
        if multimodal_class == Exp5bMergerNetwork1:
            model = Exp5bMergerNetwork1(nb_stream1=nb_stream1, nb_stream2=nb_stream2, nb_stream3=nb_stream3,
                                        b_softmax1=b_softmax1, b_softmax2=b_softmax2, b_softmax3=b_softmax3, nb_outputs=8)
            model.load_state_dict(torch.load(file_multimodal_weights))
        else:
            raise ValueError(f"Don't know what to do with model of class: {multimodal_class}")

        self.model = model
        self.model.to(device)
        for m in self.sub_models:
            m.to(device)
        self.device = device
        self.eval()

    def forward(self, img: PIL.Image.Image or [torch.Tensor]):
        """

        :param img: can be:\
         - an instance of PIL.Image.Image, in which case the image will be sent through each model's preprocessing pipeline\
         - a list containing torch.Tensor objects, in which case it is assumed the necessary preprocessing has already been\
          performed beforehand, and the tensors will be sent through the models as is. It is assumed in this case that the\
          ordering of the objects in this list reflects the ordering of the 'sub models' in self.sub_models.\
         - a tensor, which will be sent as is to each sub_model
        :return:
        """
        if isinstance(img, PIL.Image.Image):
            sub_outs = [self.sub_models[i](self.pre_procs[i](img).to(self.device).unsqueeze(0)) for i in range(len(self.sub_models))]
        elif isinstance(img, list):
            sub_outs = [self.sub_models[i](img[i]) for i in range(len(self.sub_models))]
        elif isinstance(img, torch.Tensor):
            sub_outs = [self.sub_models[i](img) for i in range(len(self.sub_models))]
        else:
            raise TypeError(f'Provided image was of type {img.__class__}, expected either PIL.Image.Image or list.')
        out = self.model(*sub_outs, None)

        return out

    @staticmethod
    def load_model(multimodal_model: str):
        if multimodal_model == TrainedMultimodalModel.EXP5B_VGG19_VGG16SIN_EMONET_20231207:
            saved_weights = os.path.join(Config.DIR_MODELS, 'Emo8', '20231207',
                                         'emo8_Exp5bMergerNetwork1_vgg19^+vgg16_sin+EmoNet_lr=0.0002_loss=UnbalancedCrossEntropyLoss_sc=test_cuda.pth')
            saved_weights_vgg19 = os.path.join(Config.DIR_MODELS, 'BestModels', '20231207',
                                               'emo8_vgg19_lr=0.001_loss=UnbalancedCrossEntropyLoss_sc=test_cuda.pth')
            saved_weights_vgg16sin = os.path.join(Config.DIR_MODELS, 'BestModels', '20231207',
                                                  'emo8_vgg16_sin_lr=0.001_loss=UnbalancedCrossEntropyLoss_sc=test_cuda.pth')

            sub_models = [LoadBufferedImageNet.load(torchvision.models.vgg19, saved_weights_vgg19),
                          # This returns a tuple (model, pp)!
                          LoadBufferedImageNet.load(torchvision.models.vgg16, saved_weights_vgg16sin),
                          (EmoNet.get_emonet(), EmoNetPreProcess())]

            tmm = TrainedMultimodalModel(multimodal_class=Exp5bMergerNetwork1,
                                         file_multimodal_weights=saved_weights,
                                         sub_models=sub_models,
                                         nb_stream1=8, nb_stream2=8, nb_stream3=20,
                                         b_softmax1=True, b_softmax2=True, b_softmax3=False)
            return tmm
        else:
            raise ValueError(f"Don't know what to do with provided model name: {multimodal_model}")


if __name__ == '__main__':
    tmm = TrainedMultimodalModel.load_model(TrainedMultimodalModel.EXP5B_VGG19_VGG16SIN_EMONET_20231207)
