"""
Network to use output from two input streams.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""

import torch
import torch.nn as nn
import torch.nn.functional as F


class Exp7bMergerNetwork1(nn.Module):
    """
    This merger network will combine the output from two feature streams.

    It is assumed that all features are pre-computed.
    """
    def __init__(self, nb_stream1: int, nb_stream2: int,
                 b_softmax1=False, b_softmax2=False,
                 nb_outputs=8,
                 dropout=0.25):
        """

        :param nb_stream1: nb of features in stream1.
        :param nb_stream2: nb of features in stream2.
        :param b_softmax1: apply softmax to stream1 features.
        :param b_softmax2: apply softmax to stream2 features.
        :param nb_outputs: number of output nodes, i.e., target classes.
        """
        super().__init__()
        nb_in_feats = nb_stream1 + nb_stream2

        self.b_softmax1, self.b_softmax2 = b_softmax1, b_softmax2
        self.l1 = nn.Linear(in_features=nb_in_feats, out_features=nb_outputs)
        self.dropout = nn.Dropout(p=dropout)

    def forward(self, stream1_feats, stream2_feats, img_path):
        """

        :param stream1_feats: stream1 features
        :param stream2_feats: stream2 features
        :param img_path: image path; not used, but put there to be compatible with BaseDoubleTrainer
        :return:
        """
        stream1_feats = self._check_feats(stream1_feats)
        stream2_feats = self._check_feats(stream2_feats)

        if self.b_softmax1:
            stream1_feats = torch.softmax(stream1_feats, dim=1)
        if self.b_softmax2:
            stream2_feats = torch.softmax(stream2_feats, dim=1)

        x = torch.cat([stream1_feats, stream2_feats], dim=1)

        out = self.l1(self.dropout(x))

        if not self.training:
            out = F.softmax(out, dim=1)

        return out

    def _check_feats(self, feats):
        if isinstance(feats, list):
            # Convert format; for each image, concatenate the average and std values
            feats = torch.stack([torch.cat([feats[0][i], feats[1][i]], dim=0)
                                 for i in range(feats[0].shape[0])], dim=0).to(feats[0].device)
        return feats


class Exp7bMergerNetwork2(nn.Module):
    """
    Same as Exp7bMergerNetwork1, but with 2 linear layers.
    """
    def __init__(self, nb_stream1: int, nb_stream2: int,
                 b_softmax1=False, b_softmax2=False,
                 nb_outputs=8, dropout=0.25):
        """

        :param nb_stream1: nb of features in stream1.
        :param nb_stream2: nb of features in stream2.
        :param b_softmax1: apply softmax to stream1 features.
        :param b_softmax2: apply softmax to stream2 features.
        :param nb_outputs: number of output nodes, i.e., target classes.
        :param dropout:
        """
        super().__init__()
        nb_in_feats = nb_stream1 + nb_stream2

        self.b_softmax1, self.b_softmax2 = b_softmax1, b_softmax2
        self.l1 = nn.Linear(in_features=nb_in_feats, out_features=nb_in_feats)
        self.l2 = nn.Linear(in_features=nb_in_feats, out_features=nb_outputs)
        self.dropout = nn.Dropout(p=dropout)

    def forward(self, stream1_feats, stream2_feats, img_path):
        """

        :param stream1_feats: stream1 features
        :param stream2_feats: stream2 features
        :param img_path: image path; not used, but put there to be compatible with BaseDoubleTrainer
        :return:
        """
        stream1_feats = self._check_feats(stream1_feats)
        stream2_feats = self._check_feats(stream2_feats)

        if self.b_softmax1:
            stream1_feats = torch.softmax(stream1_feats, dim=1)
        if self.b_softmax2:
            stream2_feats = torch.softmax(stream2_feats, dim=1)

        x = torch.cat([stream1_feats, stream2_feats], dim=1)

        out = self.l2(self.dropout(self.l1(self.dropout(x))))

        if not self.training:
            out = F.softmax(out, dim=1)

        return out

    def _check_feats(self, feats):
        if isinstance(feats, list):
            # Convert format; for each image, concatenate the average and std values
            feats = torch.stack([torch.cat([feats[0][i], feats[1][i]], dim=0)
                                 for i in range(feats[0].shape[0])], dim=0).to(feats[0].device)
        return feats


class Exp7bMergerNetwork3(nn.Module):
    """
    Same as Exp7bMergerNetwork1, but with 3 linear layers.
    """
    def __init__(self, nb_stream1: int, nb_stream2: int,
                 b_softmax1=False, b_softmax2=False,
                 nb_outputs=8, dropout=0.25):
        """

        :param nb_stream1: nb of features in stream1.
        :param nb_stream2: nb of features in stream2.
        :param b_softmax1: apply softmax to stream1 features.
        :param b_softmax2: apply softmax to stream2 features.
        :param nb_outputs: number of output nodes, i.e., target classes.
        """
        super().__init__()
        nb_in_feats = nb_stream1 + nb_stream2

        self.b_softmax1, self.b_softmax2 = b_softmax1, b_softmax2
        self.l1 = nn.Linear(in_features=nb_in_feats, out_features=nb_in_feats)
        self.l2 = nn.Linear(in_features=nb_in_feats, out_features=nb_in_feats)
        self.l3 = nn.Linear(in_features=nb_in_feats, out_features=nb_outputs)
        self.dropout = nn.Dropout(p=dropout)

    def forward(self, stream1_feats, stream2_feats, img_path):
        """

        :param stream1_feats: stream1 features
        :param stream2_feats: stream2 features
        :param img_path: image path; not used, but put there to be compatible with BaseDoubleTrainer
        :return:
        """
        stream1_feats = self._check_feats(stream1_feats)
        stream2_feats = self._check_feats(stream2_feats)

        if self.b_softmax1:
            stream1_feats = torch.softmax(stream1_feats, dim=1)
        if self.b_softmax2:
            stream2_feats = torch.softmax(stream2_feats, dim=1)

        x = torch.cat([stream1_feats, stream2_feats], dim=1)

        out = self.l3(self.dropout(self.l2(self.dropout(self.l1(self.dropout(x))))))

        if not self.training:
            out = F.softmax(out, dim=1)

        return out

    def _check_feats(self, feats):
        if isinstance(feats, list):
            # Convert format; for each image, concatenate the average and std values
            feats = torch.stack([torch.cat([feats[0][i], feats[1][i]], dim=0)
                                 for i in range(feats[0].shape[0])], dim=0).to(feats[0].device)
        return feats


class Exp7bMergerNetworkRegressor1(nn.Module):
    """
    This merger network will combine the output from two feature streams.

    It is assumed that all features are pre-computed.
    """
    def __init__(self, nb_stream1: int, nb_stream2: int,
                 b_softmax1=False, b_softmax2=False,
                 dropout=0.25):
        """

        :param nb_stream1: nb of features in stream1.
        :param nb_stream2: nb of features in stream2.
        :param b_softmax1: apply softmax to stream1 features.
        :param b_softmax2: apply softmax to stream2 features.
        """
        super().__init__()
        nb_in_feats = nb_stream1 + nb_stream2

        self.b_softmax1, self.b_softmax2 = b_softmax1, b_softmax2
        self.l1 = nn.Linear(in_features=nb_in_feats, out_features=1)
        self.dropout = nn.Dropout(p=dropout)

    def forward(self, stream1_feats, stream2_feats, img_path):
        """

        :param stream1_feats: stream1 features
        :param stream2_feats: stream2 features
        :param img_path: image path; not used, but put there to be compatible with BaseDoubleTrainer
        :return:
        """
        stream1_feats = self._check_feats(stream1_feats)
        stream2_feats = self._check_feats(stream2_feats)

        if self.b_softmax1:
            stream1_feats = torch.softmax(stream1_feats, dim=1)
        if self.b_softmax2:
            stream2_feats = torch.softmax(stream2_feats, dim=1)

        x = torch.cat([stream1_feats, stream2_feats], dim=1)

        out = torch.nn.functional.sigmoid(self.l1(self.dropout(x)))

        return out

    def _check_feats(self, feats):
        if isinstance(feats, list):
            # Convert format; for each image, concatenate the average and std values
            feats = torch.stack([torch.cat([feats[0][i], feats[1][i]], dim=0)
                                 for i in range(feats[0].shape[0])], dim=0).to(feats[0].device)
        return feats

class Exp7bMergerNetworkRegressor2(nn.Module):
    """
    This merger network will combine the output from two feature streams. This version will first reduce the stream2
    features to 1D by means of a linear layer.

    It is assumed that all features are pre-computed.
    """
    def __init__(self, nb_stream1: int, nb_stream2: int,
                 b_softmax1=False, b_softmax2=False,
                 **kwargs):
        """

        :param nb_stream1: nb of features in stream1.
        :param nb_stream2: nb of features in stream2.
        :param b_softmax1: apply softmax to stream1 features.
        :param b_softmax2: apply softmax to stream2 features.
        :param kwargs: unused extra parameters; mainly for compatibility purposes
        """
        super().__init__()

        self.b_softmax1, self.b_softmax2 = b_softmax1, b_softmax2
        self.l_feats2 = nn.Linear(in_features=nb_stream2, out_features=1)
        self.l_out = nn.Linear(in_features=nb_stream1+1, out_features=1)

    def forward(self, stream1_feats, stream2_feats, img_path):
        """

        :param stream1_feats: stream1 features
        :param stream2_feats: stream2 features
        :param img_path: image path; not used, but put there to be compatible with BaseDoubleTrainer
        :return:
        """
        stream1_feats = self._check_feats(stream1_feats)
        stream2_feats = self._check_feats(stream2_feats)

        if self.b_softmax1:
            stream1_feats = torch.softmax(stream1_feats, dim=1)
        if self.b_softmax2:
            stream2_feats = torch.softmax(stream2_feats, dim=1)

        pre_out = torch.nn.functional.sigmoid(self.l_feats2(stream2_feats))
        x = torch.cat([stream1_feats, pre_out], dim=1)

        out = torch.nn.functional.sigmoid(self.l_out(x))

        return out

    def _check_feats(self, feats):
        if isinstance(feats, list):
            # Convert format; for each image, concatenate the average and std values
            feats = torch.stack([torch.cat([feats[0][i], feats[1][i]], dim=0)
                                 for i in range(feats[0].shape[0])], dim=0).to(feats[0].device)
        return feats
