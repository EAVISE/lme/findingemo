"""

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""

from config import Config
from networks.base_triple_trainer import BaseTripleTrainer


class Experiment5bEmoSLTrainer(BaseTripleTrainer):
    def __init__(self, net_config=None, data_config=None, b_set_loss_weights=True):
        """

        :param net_config:
        :param data_config:
        :param b_set_loss_weights: if 'True', will determine the appropriate class weights to be given to the loss function
        """
        super().__init__(net_config, data_config, b_set_loss_weights)
        self.logger.print(f"Executing {__file__.replace(Config.DIR_CODE, '')}")

        # Initialize model
        # ImageNet network
        if not self.net_config.buffered_in:
            raise ValueError("For this trainer, net_config.buffered_in has to be set to 'True'.")

        self.model = self.net_config.network(**self.net_config.network_params).to(self.device)

        self.net_config.set_optimizer(self.model)
        self.net_config.set_lr_scheduler()

        self.logger.print_line(length=90)
        self.logger.print(f"StopCriterion   : {net_config.stop_criterion}")
        self.logger.print(f"Dropout         : {net_config.dropout}")
        self.logger.print(f"Loss            : {net_config.loss.__class__.__name__}")
        self.logger.print(f"Loss params     : {net_config.loss_params}")
        self.logger.print(f"Optimizer       : {net_config.optimizer_class.__name__}")
        self.logger.print(f"Optimizer params:")
        for k, v in net_config.optimizer.defaults.items():
            self.logger.print(f"\t{k:12s}: {v}")

        self.logger.print(f"\nTraining model of class: {self.model.__class__.__name__}")
        self.logger.print_line(length=90)
