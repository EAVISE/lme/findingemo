"""
EmoNet baseline.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import torch.nn as nn
import torch.nn.functional as F


class EmoNetBaseline(nn.Module):
    """
    Use pre-computed EmoNet features.
    """
    def __init__(self, nb_outputs=8, dropout=None):
        """

        :param nb_outputs: number of output nodes, i.e., target classes
        :param dropout: does not; here for compatibility purposes with ImageNetClassifier models
        """
        super().__init__()
        self.l1 = nn.Linear(in_features=20, out_features=nb_outputs)

    def forward(self, en_feats):
        """

        :param en_feats: EmoNet features
        :return:
        """
        out = self.l1(en_feats)

        if not self.training:
            out = F.softmax(out, dim=1)

        return out
