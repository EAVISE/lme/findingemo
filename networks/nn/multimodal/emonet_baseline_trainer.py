"""
Trainer to establish EmoNet baseline.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import os

import torch
from torch.nn import CrossEntropyLoss

from config import Config
from networks.base_trainer import BaseTrainer
from networks.configs.data_config import DataConfig
from networks.configs.net_config import NetConfig, StopCriterion
from networks.data.annotation_buffered_emo_sl_dataset import AnnotationBufferedEmoSLDatasetFactory
from networks.nn.losses.unbalanced_crossentropy_loss import UnbalancedCrossEntropyLoss
from networks.nn.multimodal.emonet_baseline import EmoNetBaseline
from tools.logger import Logger


class EmoNetBaselineTrainer(BaseTrainer):
    def __init__(self, net_config=None, data_config=None, b_set_loss_weights=True, b_default_init=False):
        super().__init__(net_config, data_config, b_set_loss_weights, b_default_init)
        self.default_init()


if __name__ == '__main__':
    net_config = NetConfig(nb_outputs=8)
    net_config.device = torch.device('cuda')
    net_config.max_epochs = 250
    net_config.dropout = 0.25
    net_config.hold_for_epochs = 3
    net_config.patience = 6
    net_config.batch_size = 50
    net_config.learning_rate = .001
    net_config.log_interval = 5
    net_config.burn_in = 0
    net_config.stop_criterion = StopCriterion.TEST
    net_config.network = EmoNetBaseline

    net_config.pretrained = False
    net_config.b_freeze = False
    net_config.freeze_classifier = False
    net_config.change_last_layer = False

    net_config.buffered_in = False
    # net_config.imagenet_params = {'pretrained': net_config.pretrained}
    # net_config.imagenet_freeze_depth = 2

    net_config.network_params = {'nb_outputs': net_config.nb_outputs}
    # net_config.optimizer_class = torch.optim.RMSprop/
    # net_config.optimizer_params = {'momentum': 0.75}
    net_config.optimizer_class = torch.optim.Adam
    # net_config.optimizer_class = torch.optim.SGD
    net_config.loss = CrossEntropyLoss
    net_config.set_default_lr_scheduler()

    net_config.model_name = 'emonet^'
    gen_file_name = f"emo{net_config.nb_outputs}_{net_config.network.__name__}_lr={net_config.learning_rate}_" + \
                    f"loss={net_config.loss.__name__}_sc={net_config.stop_criterion.value}"

    data_config = DataConfig()
    data_config.ann_file = Config.FILE_ANN_SINGLE
    data_config.base_dir = Config.DIR_IMAGES
    data_config.dataset_factory = AnnotationBufferedEmoSLDatasetFactory
    data_config.dataset_factory_params['max_samples'] = 0
    data_config.dataset_factory_params['model_name'] = 'emonet'
    data_config.dataset_factory_params['b_no_neutral'] = True
    data_config.dataset_factory_params['base_dir'] = data_config.base_dir
    data_config.dataset_factory_params['b_only_main_emos'] = True
    # Logger params
    data_config.logger_params['logfile'] = None
    data_config.logger_params['b_overwrite'] = False

    b_single_model = False
    if b_single_model:
        trainer = EmoNetBaselineTrainer(net_config, data_config, b_set_loss_weights=True)
        trainer.train(b_save_model=False, model_name="cnn3_nopeople_model_1")
    else:
        out_dir = os.path.join(Config.DIR_LOGS, "EmoNet Emo8")
        data_config.file_results = os.path.join(out_dir, f"training_emo{net_config.nb_outputs}_results.csv")
        data_config.file_c_mtxs = os.path.join(out_dir, f"training_emo{net_config.nb_outputs}_results_confusion_matrices.txt")
        Logger.open_results_csv(data_config.file_results)
        Logger.open_mtx_file(data_config.file_c_mtxs)

        for lr in [0.1, 0.01, 0.001, 0.0001]:
            for loss in [CrossEntropyLoss, UnbalancedCrossEntropyLoss]:
                for idx_run in range(10):
                    net_config.network = EmoNetBaseline  # Reset network
                    net_config.learning_rate = lr
                    net_config.model_name = 'emonet^'
                    net_config.loss = loss  # Reset loss
                    net_config.set_default_lr_scheduler()

                    print("=" * 60)
                    print(f"Training model {net_config.model_name} with starting lr={lr}...")

                    gen_file_name = f"emo{net_config.nb_outputs}_{net_config.model_name}" + \
                                    f"_optim={net_config.optimizer_class.__name__}" + \
                                    f"_lr={lr}_loss={net_config.loss.__name__}_sc={net_config.stop_criterion.value}"

                    # file_cnts = Logger.get_nb_files(out_dir, gen_file_name)
                    # print(f"Run {idx_run} :: {gen_file_name}: {file_cnts}")
                    # if idx_run < file_cnts:
                    #     continue

                    data_config.logger_params['logfile'] = os.path.join(out_dir, gen_file_name + ".txt")

                    try:
                        trainer = EmoNetBaselineTrainer(net_config, data_config, b_set_loss_weights=True)
                        best_stats = trainer.train(b_save_model=False, model_name="cnn3_nopeople_model_1")
                        print("=" * 60)
                        for k, v in best_stats.items():
                            print(f"{k}: {v}")
                    except Exception as e:
                        print(e)
                        continue
