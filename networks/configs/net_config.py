"""
Configuration file containing all the necessary parameter values relating to a neural net.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import math
from enum import Enum

import torch

from networks.nn.class_weights import ClassWeights


class StopCriterion(Enum):
    TRAIN = 'train'
    TEST = 'test'


class NetConfig:
    def __init__(self, nb_outputs=25):
        """

        :param nb_outputs: number of network outputs; default = 25 = 24 emotion classes + 1 neutral class.
        """
        self.device = torch.device('cuda')
        self.b_allow_parallel = False  # Allow running process over multiple GPUs, if possible
        self.b_use_manual_seed = False
        self.manual_seed = 1

        self.max_epochs = 400
        self.batch_size = 1

        self.model_name = None  # Name you want to give to the model you are training; will default to self.network.__name__
        self.network = None
        self.network_params = {}
        self.imagenet = None  # ImageNet trained network to use, if necessary
        self.imagenet_params = {}
        self.imagenet_load_state = None  # Path to state_dict file to load; None = don't load any weights
        self.imagenet_freeze_depth = 3  # When freezing classifier for ImageNet
        self.pretrained = False  # When using torchvision.models models, use pretrained version
        self.b_classifier_l1 = False
        self.b_freeze = False  # Freeze layers of pretrained models
        self.freeze_classifier = False  # b_freeze_classifier to pass to TrainerTools.freeze_layers if self.freeze = True
        self.change_last_layer = False  # Change last layer of network, as used by BaseTrainer and BaseDoubleTrainer
        self.loss = torch.nn.CrossEntropyLoss
        self.loss_params = {}
        self.class_weights = ClassWeights.classical_weights

        self.buffered_in = False  # Set to True for use with pre-computed ImageNet featuress
        self.buffered_oi = False  # Set to True for use with pre-computed YoLo v3 + OpenImages features
        self.buffered_oitofer = False  # Set to True for use with pre-computed OIToFER features
        self.buffered_places365 = False  # Set to True for use with pre-computed Places365 features
        self.places365_model = None  # Which places365 model to use

        self.nb_outputs = nb_outputs

        self.dropout = 0.25
        self.learning_rate = 0.01
        self.optimizer = None
        self.optimizer_class = torch.optim.Adam
        self.optimizer_params = {}

        self.hold_for_epochs = 5
        self.patience = 16
        self.burn_in = 0  # Number of burn in epochs

        self.lr_scheduler = None
        self.lr_scheduler_params = {}

        self.log_interval = 100

        self.stop_criterion = StopCriterion.TRAIN

    def set_optimizer(self, model, lr=None, weight_decay=1e-6):
        if lr is None:
            lr = self.learning_rate
        self.optimizer = self.optimizer_class(model.parameters(),
                                              lr=lr,
                                              weight_decay=weight_decay,
                                              **self.optimizer_params)

    def set_default_lr_scheduler(self):
        self.lr_scheduler = torch.optim.lr_scheduler.LambdaLR
        self.lr_scheduler_params = {'lr_lambda': [lambda epoch: 1 / math.sqrt(epoch // self.hold_for_epochs + 1)]}

    def set_lr_scheduler(self):
        """
        Initialize the learning rate scheduler.

        :param hold_for_epochs: nb of epochs to keep the learning rate fixed
        :return:
        """
        if self.optimizer is None:
            raise ValueError("Optimizer needs to be initialized first.")
        # self.lr_scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(self.optimizer, 'min',
        #                                                                patience=self.patience)
        # self.lr_scheduler = torch.optim.lr_scheduler.\
        #     LambdaLR(self.optimizer,
        #              lr_lambda=[lambda epoch: 1/math.sqrt(epoch//self.hold_for_epochs + 1)])
        # self.lr_scheduler = torch.optim.lr_scheduler.\
        #     LambdaLR(self.optimizer,
        #              lr_lambda=[lambda epoch: (0.95**epoch)])
        self.lr_scheduler = self.lr_scheduler(self.optimizer, **self.lr_scheduler_params)
