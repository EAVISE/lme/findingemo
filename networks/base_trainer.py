"""
Base trainer class to be used as is or inherited from.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
from collections import Counter

import numpy as np
import torch
import torch.nn
import torch.optim.lr_scheduler
import torch.utils.data
from sklearn.metrics import average_precision_score
from tqdm import tqdm

from config import Config
from networks.base_trainer_global import BaseTrainerGlobal
from networks.nn.class_weights import ClassWeights
from networks.nn.imagenet.imagenet_classifier import get_classifier_for_imagenet
from tools.print_tools import PrintTools
from tools.trainer_tools import TrainerTools


class BaseTrainer(BaseTrainerGlobal):
    def __init__(self, net_config=None, data_config=None, b_set_loss_weights=True, b_default_init=False):
        """

        :param net_config:
        :param data_config:
        :param b_set_loss_weights: if 'True', will determine the appropriate class weights to be given to the loss function
        :param b_default_init: use default initialization
        """
        super().__init__(net_config=net_config, data_config=data_config,
                         b_set_loss_weights=b_set_loss_weights, b_default_init=b_default_init)
        self.logger.print(f"Executing {__file__.replace(Config.DIR_CODE, '')}")

    def default_init(self):
        # Load data
        if hasattr(self.data_config, 'datasets'):
            train_data, test_data = self.data_config.datasets
        else:
            train_data, test_data = self.data_config.dataset_factory.get_train_test_datasets(**self.data_config.dataset_factory_params)

        self.logger.print(f"train_data size: {len(train_data)}")
        self.logger.print(f"test_data size: {len(test_data)}")

        self.train_loader = torch.utils.data.DataLoader(train_data, batch_size=self.net_config.batch_size,
                                                        shuffle=True)
        if self.data_config.dataset_factory_params['train_test_split'] < 1.0:
            self.test_loader = torch.utils.data.DataLoader(test_data, batch_size=self.net_config.batch_size,
                                                           shuffle=False)
        else:
            self.test_loader = None

        # Initialize loss
        # Determine weights for loss function
        if self.b_set_loss_weights:
            if self.net_config.loss.__name__.startswith("UnbalancedCrossEntropyLoss"):
                if not 'b_only_main_emos' in self.data_config.dataset_factory_params:
                    cw = ClassWeights.unbalanced_crossentropy_aro_val_weights
                    self.net_config.loss_params = {'cs': cw(dataset=self.train_loader.dataset),
                                                   'nb_classes': self.net_config.nb_outputs}
                else:
                    if self.data_config.dataset_factory_params['b_only_main_emos']:
                        cw = ClassWeights.unbalanced_crossentropy_8emo_weights
                    else:
                        cw = ClassWeights.unbalanced_crossentropy_24emo_weights
                    self.net_config.loss_params = {'cs': cw(b_use_classical_weights=True,
                                                            dataset=self.train_loader.dataset),
                                                   'nb_classes': self.net_config.nb_outputs}
            else:
                self.logger.print("Using Classical Weights...")
                target_weights = ClassWeights.classical_weights(dataset=self.train_loader.dataset)
                self.net_config.loss_params = {'weight': target_weights.to(self.device)}
                self.logger.print(f"Target weights: {target_weights}")

        self.net_config.loss = self.net_config.loss(**self.net_config.loss_params)
        if self.net_config.loss.__class__.__name__ == "UnbalancedCrossEntropyLoss":
            self.logger.print("UnbalancedCrossEntropyLoss using weights:")
            self.logger.print(self.net_config.loss.get_weights_as_tbl())

        # Initialize model
        if isinstance(self.net_config.network, str):
            in_name = self.net_config.network
        else:
            in_name = self.net_config.network.__name__
        if self.net_config.buffered_in:
            self.logger.print(f"\tUsing pre-computed ImageNet features for model {in_name}...")
            model = get_classifier_for_imagenet(model_name=in_name)
        else:
            model = self.net_config.network
        if self.net_config.b_classifier_l1:
            self.model = model(**self.net_config.network_params, b_classifier_l1=True).to(self.device)
        else:
            self.model = model(**self.net_config.network_params).to(self.device)

        if self.net_config.b_allow_parallel and torch.cuda.device_count() > 1:
            self.b_parallel = True
            print(f"Using {torch.cuda.device_count()} GPUs...")
            self.model = torch.nn.DataParallel(self.model)
            self.model.to(self.net_config.device)
            self.net_config.set_optimizer(self.model.module)
        else:
            self.net_config.set_optimizer(self.model)
        self.net_config.set_lr_scheduler()

        # Freeze convolution weights for param.requires_grad = False
        if self.net_config.b_freeze:
            TrainerTools.freeze_layers(model=self.model,
                                       logger=self.logger,
                                       b_freeze_classifier=self.net_config.freeze_classifier,
                                       depth=self.net_config.imagenet_freeze_depth)

        if self.net_config.change_last_layer:
            # Change the number of output classes, i.e., the last network layer
            TrainerTools.change_last_layer(model=self.model, nb_outputs=self.net_config.nb_outputs, device=self.net_config.device)

        self.logger.print(f"Training model of class: {self.model.__class__.__name__}")

        self.logger.print_line(length=90)
        self.logger.print(f"StopCriterion   : {self.net_config.stop_criterion}")
        self.logger.print(f"Dropout         : {self.net_config.dropout}")
        self.logger.print(f"Loss            : {self.net_config.loss.__class__.__name__}")
        self.logger.print(f"Loss params     : {self.net_config.loss_params}")
        self.logger.print(f"Optimizer       : {self.net_config.optimizer_class.__name__}")
        self.logger.print(f"Optimizer params:")
        for k, v in self.net_config.optimizer.defaults.items():
            self.logger.print(f"\t{k:12s}: {v}")

        self.logger.print(f"\nTraining model of class: {self.model.__class__.__name__}")
        self.logger.print_line(length=90)

    def test_step(self, epoch: int, loader: torch.utils.data.DataLoader):
        """
        A single epoch in testing.

        :param epoch: The current epoch.
        :param loader: The dataloader to use for testing.
        """
        self.model.eval()
        outputs, targets = [], []
        y_scores = []
        test_loss = 0.

        # indices = torch.tensor(range(loader.dataset.ys.shape[1]))
        nb_correct, nb_total = 0, 0
        counts_per_label = Counter()
        preds_per_label = Counter()
        correct_per_label = Counter()
        with torch.no_grad():
            for batch_index, (data, target) in tqdm(enumerate(loader),
                                                    total=len(loader),
                                                    desc=f'Testing epoch {epoch}:', disable=True):
                data, target = data.to(self.device), target.to(self.device)
                with torch.no_grad():
                    output = self.model(data)

                _t = target.cpu().numpy()
                _p = output.argmax(dim=1).cpu().numpy()
                counts_per_label.update(_t)
                preds_per_label.update(_p)
                for t in zip(_t, _p):
                    nb_total += 1
                    targets.append(t[0])
                    outputs.append(t[1])
                    if t[0] == t[1]:
                        nb_correct += 1
                        correct_per_label[t[0]] += 1

                y_scores.append(output.detach().cpu())

                test_loss += self.net_config.loss(output, target).item()

        test_loss /= len(self.test_loader)

        # Compute average precision
        y_true = np.asarray(targets)
        y_scores = torch.cat(y_scores).numpy()
        ap = average_precision_score(y_true, y_scores)

        self.logger.print(f'Testing loss at epoch {epoch}: {test_loss}')
        prec_per_label, rec_per_label, f1_per_label, acc, weighted_acc, macro_f1, weighted_f1 =\
            TrainerTools.compute_class_scores(nb_classes=self.net_config.nb_outputs, counts_per_label=counts_per_label,
                                              preds_per_label=preds_per_label, correct_per_label=correct_per_label)
        self.logger.print("Test stats:")
        self.logger.print(PrintTools.print_class_metrics_mtx(
            prec=prec_per_label, rec=rec_per_label, f1=f1_per_label))
        self.logger.print(f'Accuracy         : {acc:>6.4f} [{nb_correct}/{nb_total}]')
        self.logger.print(f'Weighted accuracy: {weighted_acc:>6.4f}')
        self.logger.print(f'Macro F1         : {macro_f1:>6.4f}')
        self.logger.print(f'Weighted F1      : {weighted_f1:>6.4f}')
        self.logger.print(f'Average precision: {ap:>6.4f}')
        labels = sorted(counts_per_label.keys())
        s_cnts_per_label = 'Counts per label'
        s_preds_per_label = 'Preds per label '
        s_corr_per_label = 'Corr. per label '
        for l in labels:
            s_cnts_per_label += f' ██ {l}: {counts_per_label[l]:4d}'
            s_preds_per_label += f' ██ {l}: {preds_per_label[l]:4d}'
            s_corr_per_label += f' ██ {l}: {correct_per_label[l]:4d}'
        self.logger.print(s_cnts_per_label + '\n' + s_preds_per_label + '\n' + s_corr_per_label)
        # Confusion matrix with colored diagonal for console printout
        print(PrintTools.print_confusion_mtx(outputs, targets, nb_classes=self.net_config.nb_outputs, color='green'))
        # Confusion matrix without colored diagonal for logger
        confusion_mtx = PrintTools.print_confusion_mtx(outputs, targets, nb_classes=self.net_config.nb_outputs, color=None)
        self.logger.write(confusion_mtx)

        return test_loss, prec_per_label, rec_per_label, f1_per_label, acc, weighted_acc, macro_f1, weighted_f1, confusion_mtx, ap

    def train_step(self, epoch):
        """
        A single epoch in training.

        :param epoch: The current epoch.
        """
        self.model.train()
        epoch_size = len(self.train_loader)

        outputs, targets = [], []
        y_scores = []
        total_loss = 0.
        nb_batches, nb_correct, nb_total = 0, 0, 0
        counts_per_label = Counter()
        preds_per_label = Counter()
        correct_per_label = Counter()
        for batch_index, (data, target) in tqdm(enumerate(self.train_loader),
                                                total=epoch_size,
                                                desc=f'Training epoch {epoch}:', disable=True):
            nb_batches += 1
            data = data.to(self.device)
            # self.logger.print(f"data.shape: {data.shape}")
            # self.logger.print(f"{data[0:,:]}")
            target = target.to(self.device)
            # self.logger.print(f"target.shape: {target.shape}")
            self.net_config.optimizer.zero_grad()
            output = self.model(data)
            # if self.net_config.model_name == 'inception_v3':
            #     output = output[0]
            # self.logger.print(f"output.shape: {output.shape}")
            loss = self.net_config.loss(output, target)
            loss.backward()
            self.net_config.optimizer.step()

            total_loss += loss.item()

            if 'b_one_hot_target' in self.data_config.dataset_factory_params and \
                    self.data_config.other_params['b_one_hot_target']:
                _t = target.argmax(dim=1).tolist()
            else:
                _t = target.tolist()
            _p = output.argmax(dim=1).tolist()
            outputs += _p
            targets += _t
            counts_per_label.update(_t)
            preds_per_label.update(_p)
            for t in zip(_t, _p):
                nb_total += 1
                if t[0] == t[1]:
                    nb_correct += 1
                    correct_per_label[t[0]] += 1

            y_scores.append(output.detach().cpu())

            if (batch_index+1) % self.net_config.log_interval == 0:
                self.logger.print(f'LR = {self.net_config.optimizer.param_groups[0]["lr"]:.5f}, Current loss = {loss}'
                      f' at batch = {batch_index+1}, epoch = {epoch}.')
                labels = sorted(counts_per_label.keys())
                s_cnts_per_label = 'Counts per label'
                s_preds_per_label = 'Preds per label '
                s_corr_per_label = 'Corr. per label '
                for l in labels:
                    s_cnts_per_label += f' || {l}: {counts_per_label[l]:4d}'
                    s_preds_per_label += f' || {l}: {preds_per_label[l]:4d}'
                    s_corr_per_label += f' || {l}: {correct_per_label[l]:4d}'
                self.logger.print(s_cnts_per_label + '\n' + s_preds_per_label + '\n' + s_corr_per_label)

        avg_loss = total_loss / nb_batches
        self.net_config.lr_scheduler.step()

        # Compute average precision
        y_true = np.asarray(targets)
        y_scores = torch.cat(y_scores).numpy()
        ap = average_precision_score(y_true, y_scores)

        self.logger.print(f'Training loss at epoch {epoch}: {avg_loss}')
        prec_per_label, rec_per_label, f1_per_label, acc, weighted_acc, macro_f1, weighted_f1 =\
            TrainerTools.compute_class_scores(nb_classes=self.net_config.nb_outputs, counts_per_label=counts_per_label,
                                              preds_per_label=preds_per_label, correct_per_label=correct_per_label)
        self.logger.print("Train stats:")
        self.logger.print(PrintTools.print_class_metrics_mtx(
            prec=prec_per_label, rec=rec_per_label, f1=f1_per_label))
        self.logger.print(f'Accuracy         : {acc:>6.4f} [{nb_correct}/{nb_total}]')
        self.logger.print(f'Weighted accuracy: {weighted_acc:>6.4f}')
        self.logger.print(f'Macro F1         : {macro_f1:>6.4f}')
        self.logger.print(f'Weighted F1      : {weighted_f1:>6.4f}')
        self.logger.print(f'Average precision: {ap:>6.4f}')
        self.logger.print()
        # Confusion matrix with colored diagonal for console printout
        print(PrintTools.print_confusion_mtx(outputs, targets, nb_classes=self.net_config.nb_outputs, color='red'))
        # Confusion matrix without colored diagonal for logger
        confusion_mtx = PrintTools.print_confusion_mtx(outputs, targets, nb_classes=self.net_config.nb_outputs, color=None)
        self.logger.write(confusion_mtx)

        return avg_loss, prec_per_label, rec_per_label, f1_per_label, acc, weighted_acc, macro_f1, weighted_f1, confusion_mtx, ap

    # def _compute_avg_prec(self, output, targets):
    #     y_true = np.asarray(targets)
    #     y_pred = np.as