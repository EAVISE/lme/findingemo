"""
Contains methods that can be shared across all BaseTrainer classes.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import copy
import os
import random
from datetime import datetime

import dill
import numpy as np
import torch

from config import Config
from networks.configs.data_config import DataConfig
from networks.configs.net_config import NetConfig, StopCriterion
from tools.logger import Logger
from tools.print_tools import PrintTools


class BaseTrainerGlobal(object):
    def __init__(self, net_config=None, data_config=None, b_set_loss_weights=True, b_default_init=False):
        """

        :param net_config:
        :param data_config:
        :param b_set_loss_weights: if 'True', will determine the appropriate class weights to be given to the loss function
        :param b_default_init: use default initialization
        """
        if net_config is None:
            self.net_config = NetConfig()
        else:
            self.net_config = net_config

        if data_config is None:
            self.data_config = DataConfig()
        else:
            self.data_config = data_config
        self.device = self.net_config.device

        if self.net_config.model_name is None:
            if isinstance(self.net_config.network, str):
                in_name = self.net_config.network
            else:
                in_name = self.net_config.network.__name__
            self.net_config.model_name = in_name

        self.logger = Logger(**self.data_config.logger_params)
        self.logger.print(f"Executing {__file__.replace(Config.DIR_CODE, '')}")
        self.logger.print(f"Using annotation file: {self.data_config.ann_file}")

        # Make things deterministic
        if self.net_config.b_use_manual_seed:
            self.set_random_seed(self.net_config.manual_seed)

        self.b_set_loss_weights = b_set_loss_weights

        self.b_parallel = False
        self.train_loader, self.test_loader = None, None
        self.model = None

        if b_default_init:
            self.default_init()

    def default_init(self):
        raise NotImplementedError("Inheriting class should implement this method.")

    @staticmethod
    def get_datasets(data_config: DataConfig):
        return data_config.dataset_factory.get_train_test_datasets(**data_config.dataset_factory_params)

    @staticmethod
    def set_random_seed(seed: int):
        """
        Makes the training deterministic with a fixed random seed.

        :param seed: The seed to use
        :return: none
        """
        if seed is not None:
            torch.manual_seed(seed)
            torch.cuda.manual_seed(seed)
            np.random.seed(seed)
            random.seed(seed)
            torch.backends.cudnn.deterministic = True

    def train(self, b_save_model=False, model_name=None):
        """
        Train the actual network.

        :param b_save_model: True = save model to disk, False = don't. Default = False.
        :param model_name: if the model is saved, this determines the base filename, INCLUDING PATH (not just the filename)!
        :return: best_stats = dictionary containing best stats as well as best model
        """
        self.logger.print("Training model...")
        if b_save_model and model_name is None:
            raise ValueError("Model name should be specified if you want to save the model.")

        # Train the model
        last_avg = 1e6
        best_avg = 1e6
        best_ap = 0.
        last_wf1 = 0.
        best_wf1 = 0.
        best_epoch = 0
        wf1_unchanged = 0  # Track number of epochs weighted F1 score hasn't changed.
                           # This will allow us to detect situations where the loss keeps decreasing,
                           # but the predictions don't change anymore. I.e., the model is stuck.
        best_model = None

        best_stats = {'model_name': self.net_config.model_name, 'loss': self.net_config.loss.__class__.__name__,
                      'optimizer': self.net_config.optimizer.__class__.__name__, 'lr': self.net_config.learning_rate,
                      '#train_samples': len(self.train_loader.dataset), '#test_samples': len(self.test_loader.dataset),
                      'output_file': self.logger.logfile.replace(Config.DIR_LOGS, '') if self.logger.logfile is not None else 'None'}
        for epoch in range(1, self.net_config.max_epochs + 1):
            avg_loss, prec_per_label, rec_per_label, f1_per_label, acc, weighted_acc, macro_f1, weighted_f1, c_mtx, ap =\
                self.train_step(epoch)
            test_avg_loss, test_prec_per_label, test_rec_per_label, test_f1_per_label, test_acc, test_weighted_acc,\
                test_macro_f1, test_weighted_f1, test_c_mtx, test_ap =\
                self.test_step(epoch, self.test_loader)
            if self.test_loader is not None and self.net_config.stop_criterion == StopCriterion.TEST:
                crit_loss, crit_acc, crit_weighted_acc, crit_macro_f1, crit_weighted_f1, crit_ap =\
                    test_avg_loss, test_acc, test_weighted_acc, test_macro_f1, test_weighted_f1, test_ap
                self.logger.print("Checking stopping criterium using test loss...")
            else:
                crit_loss, crit_acc, crit_weighted_acc, crit_macro_f1, crit_weighted_f1, crit_ap =\
                    avg_loss, acc, weighted_acc, macro_f1, weighted_f1, ap
                self.logger.print("Checking stopping criterium using train loss...")

            diff = crit_loss - last_avg
            diff_wf1 = crit_weighted_f1 - last_wf1
            self.logger.print(f"Current avg. loss: {crit_loss:>9.7f}  --  Current wF1: {crit_weighted_f1:.3f}")
            self.logger.print(f"Last avg. loss   : {last_avg:>9.7f}  --  Last wF1   : {last_wf1:.3f}")
            self.logger.print(f"Difference       : {diff:>9.7f} --  Difference : {diff_wf1:.3f}")
            self.logger.print(f"Best avg. loss   : {best_avg:>9.7f}  --  Best wF1   : {best_wf1:.3f}")
            self.logger.print(f"Best avg. prec.  : {best_ap:>6.4f}")
            if epoch <= self.net_config.burn_in:
                self.logger.print("Burning in model...")
                best_epoch = epoch
            else:
                if crit_loss < best_avg and -diff > 1e-8:
                    best_avg = crit_loss
                    best_ap = crit_ap
                    best_epoch = epoch
                    best_model = copy.deepcopy(self.model)
                    if crit_weighted_f1 > best_wf1:
                        best_wf1 = crit_weighted_f1
                    self.logger.print("New best (loss) epoch.")
                    self._update_best_stats(best_stats, epoch,
                                            prec_per_label, rec_per_label, f1_per_label,
                                            avg_loss, acc, weighted_acc, macro_f1, weighted_f1, c_mtx, ap,
                                            test_prec_per_label, test_rec_per_label, test_f1_per_label,
                                            test_avg_loss, test_acc, test_weighted_acc,
                                            test_macro_f1, test_weighted_f1, test_c_mtx, test_ap)
                elif crit_weighted_f1 > best_wf1:
                    best_wf1 = crit_weighted_f1
                    best_ap = crit_ap
                    best_epoch = epoch
                    best_model = copy.deepcopy(self.model)
                    self.logger.print("New best (weighted F1) epoch.")
                    self._update_best_stats(best_stats, epoch,
                                            prec_per_label, rec_per_label, f1_per_label,
                                            avg_loss, acc, weighted_acc, macro_f1, weighted_f1, c_mtx, ap,
                                            test_prec_per_label, test_rec_per_label, test_f1_per_label,
                                            test_avg_loss, test_acc, test_weighted_acc,
                                            test_macro_f1, test_weighted_f1, test_c_mtx, test_ap)
                elif epoch - best_epoch >= self.net_config.patience:
                    self.logger.print(f"Not doing better for {self.net_config.patience} epochs in a row. Stopping training.")
                    self.model = best_model
                    break
                else:
                    self.logger.print(f"Best epoch was {epoch-best_epoch} of max {self.net_config.patience} epochs ago.")

                if last_wf1 == crit_weighted_f1:
                    wf1_unchanged += 1
                    if wf1_unchanged >= 3:
                        self.logger.print(f"!!! Weighted F1 hasn't changed for 3 epochs in a row. Aborting training. !!!")
                        best_stats['epoch'] = str(best_stats['epoch']) + '*'
                        self.model = best_model
                        break
                else:
                    wf1_unchanged = 0

                last_avg = crit_loss
                last_wf1 = crit_weighted_f1

        self.logger.print(f"Best epoch was epoch {best_epoch} with an average loss of {best_avg:9.7f}.\n"
                          f"Best metric stats:")
        self.logger.print_line(length=90)
        self.logger.print(f"Train:")
        self.logger.print(PrintTools.print_class_metrics_mtx(
            prec=best_stats['prec_per_label'], rec=best_stats['rec_per_label'], f1=best_stats['f1_per_label']))
        for k in ("train_acc", "train_weighted_acc", "train_macro_f1", "train_weighted_f1", "train_ap"):
            self.logger.print(f"{k:18s}: {best_stats[k]}")
        self.logger.print_line(length=90)
        self.logger.print(f"\nTest:")
        self.logger.print(PrintTools.print_class_metrics_mtx(
            prec=best_stats['test_prec_per_label'], rec=best_stats['test_rec_per_label'], f1=best_stats['test_f1_per_label']))
        for k in ("test_acc", "test_weighted_acc", "test_macro_f1", "test_weighted_f1", "test_ap"):
            self.logger.print(f"{k:18s}: {best_stats[k]}")

        Logger.write_result_to_csv(best_stats, self.data_config.file_results)
        Logger.write_conf_mtx(best_stats, self.data_config.file_c_mtxs)

        if b_save_model:
            # Save model
            file_path = os.path.dirname(model_name)
            os.makedirs(file_path, exist_ok=True)

            model_params = model_name + '_' + str(self.net_config.device) + '.pth'
            torch.save(self.model.state_dict(), model_params)
            self.logger.print(f"Model parameters saved to: {model_params}.")

            if hasattr(self.train_loader.dataset, 'transform'):
                transform_path = model_name + '_' + str(self.net_config.device) + '_transform.dill'
                dill.dump(self.train_loader.dataset.transform, open(transform_path, 'wb'))
                self.logger.print(f"Image transform saved to : {transform_path}")

        best_stats['best_model'] = best_model
        best_stats['transform'] = None
        if hasattr(self.train_loader.dataset, 'transform'):
            best_stats['transform'] = self.train_loader.dataset.transform

        return best_stats

    @classmethod
    def _update_best_stats(cls, best_stats, epoch,
                           prec_per_label, rec_per_label, f1_per_label, avg_loss, acc, weighted_acc,
                           macro_f1, weighted_f1, c_mtx, ap,
                           test_prec_per_label, test_rec_per_label, test_f1_per_label, test_avg_loss,
                           test_acc, test_weighted_acc,
                           test_macro_f1, test_weighted_f1, test_c_mtx, test_ap):
        best_stats['datetime'] = str(datetime.now())
        best_stats['epoch'] = epoch

        best_stats['prec_per_label'] = prec_per_label
        best_stats['rec_per_label'] = rec_per_label
        best_stats['f1_per_label'] = f1_per_label

        best_stats['train_avg'] = avg_loss
        best_stats['train_acc'] = acc
        best_stats['train_weighted_acc'] = weighted_acc
        best_stats['train_macro_f1'] = macro_f1
        best_stats['train_weighted_f1'] = weighted_f1
        best_stats['train_c_mtx'] = c_mtx
        best_stats['train_ap'] = ap

        best_stats['test_prec_per_label'] = test_prec_per_label
        best_stats['test_rec_per_label'] = test_rec_per_label
        best_stats['test_f1_per_label'] = test_f1_per_label

        best_stats['test_avg'] = test_avg_loss
        best_stats['test_acc'] = test_acc
        best_stats['test_weighted_acc'] = test_weighted_acc
        best_stats['test_macro_f1'] = test_macro_f1
        best_stats['test_weighted_f1'] = test_weighted_f1
        best_stats['test_c_mtx'] = test_c_mtx
        best_stats['test_ap'] = test_ap

    def test_step(self, epoch: int, loader: torch.utils.data.DataLoader):
        """
        A single epoch in testing.

        :param epoch: The current epoch.
        :param loader: The dataloader to use for testing.
        """
        raise NotImplementedError("Inheriting class should implement this method.")

    def train_step(self, epoch):
        """
        A single epoch in training.

        :param epoch: The current epoch.
        """
        raise NotImplementedError("Inheriting class should implement this method.")
