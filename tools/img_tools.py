"""
Methods for printing images stored as tensors.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import os

import PIL
import matplotlib.pyplot as plt
import torch


class ImageTools:
    @staticmethod
    def get_csv_for_img(img_path: str):
        """
        Replace image extension with '.csv'.

        :param img_path:
        :return: img_path with file extension replace by '.csv'
        """
        return img_path[:len(img_path) - len(os.path.splitext(img_path)[1])] + '.csv'

    @staticmethod
    def get_dill_for_img(img_path: str, model_name: str = ""):
        """
        Replace image extension with '.dill', possibly including a model name.
        Assuming the model name is vgg19, "image1.jpg" becomes "image1_vgg19.dill".

        :param img_path:
        :param model_name: name of the model to which the dill file should refer
        :return: img_path with file extension replace by '.dill' or "_model_name.dill"
        """
        dill_img_path = img_path[:len(img_path) - len(os.path.splitext(img_path)[1])]
        if model_name:
            dill_img_path += '_' + model_name
        dill_img_path += '.dill'

        return dill_img_path

    @staticmethod
    def print_grayscale_img(t: torch.Tensor):
        """
        Print grayscale image to screen using MatPlotLib.

        :param t: 2D tensor representing the grayscale image; values are expected to be 0 <= x <= 255.n
        :return:
        """
        plt.figure()
        plt.imshow(t.cpu().numpy(), cmap='gray')
        plt.show()

    @staticmethod
    def open_image(img_path):
        # open path as file to avoid ResourceWarning (https://github.com/python-pillow/Pillow/issues/835)
        with open(img_path, 'rb') as f:
            img = PIL.Image.open(f)

        return img.convert('RGB')
