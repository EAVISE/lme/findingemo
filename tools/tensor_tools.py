"""
Collection of handy tensor manipulations.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import torch


class TensorTools:
    @staticmethod
    def standardize_2d_tensor(t: torch.Tensor):
        """
        Standardize 2D tensor, assuming each row represents one sample.

        :param t:
        :return:
        """
        t_min = torch.min(t, dim=1)[0]
        t = t - (t_min.view(t.shape[0], -1).expand(t.shape))
        t_max = torch.max(t, dim=1)[0]
        # Replace 0. values in t_max to avoid nan's.
        t_max[t_max == 0] = 1.
        t = torch.einsum('ij,i->ij', t, 1/t_max)

        return t
