"""

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import pandas as pd

class PandasTools:
    @staticmethod
    def pd_dataframe_append_row(df: pd.DataFrame, row: dict):
        df = pd.concat([df, pd.DataFrame.from_records([row])], ignore_index=True)

        return df
