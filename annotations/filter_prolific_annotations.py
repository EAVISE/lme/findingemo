"""
A script to filter annotations.

Also allows the creation of a copy of the annotation file with all lines that pose problems (e.g., refer to images
that can not be loaded) removed.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import copy

import pandas as pd

from config import Config
from server_prolific.login_api import LoginAPI
from server_prolific.prolific_scorer import ProlificScorer


class FilterProlificAnnotations:
    @ staticmethod
    def process(annotations: pd.DataFrame or str,
                b_remove_incomplete=True,
                b_use_alt_score=False,
                min_score=0.80):
        """

        :param annotations: path to the annotations csv file to load, or already loaded Pandas DataFrame
        :param b_remove_incomplete: remove annotations from users that did not complete the task (i.e., less than 45 'keep' annotations)
        :param min_score: minimum annotation overlap score; annotations by users with less are discarded; if no score is known (for
            users from earliest runs), annotations are kept.
        :return:
        """
        if isinstance(annotations, str):
            annotations = pd.read_csv(annotations, header=0, index_col=0)
            annotations.fillna('Undefined', inplace=True)

        annotations = copy.deepcopy(annotations)

        # Filter users that did not complete the task
        user_db = LoginAPI._get_user_db()

        # !!! We make an exception for the first few annotators, as we did not originally use these fixed overlap images,
        # and hence, no overlap score. Also, using the overlap images, the first run bugged.
        if b_remove_incomplete:
            done_users = set(user_db[user_db.done].prolific_id)
            annotations = annotations[annotations.user.isin(done_users)]

        # keep_users = set(user_db[(user_db.overlap_score >= min_score) |
        #                          (pd.isna(user_db.overlap_score))].prolific_id)
        print(f"Number of users BEFORE filtering: {len(annotations.user.unique())}")
        user_scores = ProlificScorer.score_all(annotations, b_silent=True)
        keep_users = set(user_db[pd.isna(user_db.overlap_score)].prolific_id)  # Keep
        for user, user_scores in user_scores.items():
            if b_use_alt_score and (user_scores[1] >= min_score):  # and (user_scores[2] >= .7):
                keep_users.add(user)
            elif not b_use_alt_score and user_scores[0] >= min_score:
                keep_users.add(user)

        print("SOMETHING WEIRD GOING ON HERE! len(keep_users) != len(annotations.user.unique())")
        annotations = annotations[annotations.user.isin(keep_users)]
        print(f"Number of users AFTER filtering : {len(annotations.user.unique())}")
        print(f"Number of annotations AFTER filtering : {len(annotations)}")

        nb_keep = len(annotations[annotations.reject == 'Keep'])
        nb_keep_uq = len(annotations[annotations.reject == 'Keep'].image_path.unique())
        print(f"Number of 'keep' annotations left       : {nb_keep}")
        print(f"Number of unique 'keep' annotated images: {nb_keep_uq}")
        print(f"Ratio: {nb_keep_uq/nb_keep:.3f}")

        return annotations
