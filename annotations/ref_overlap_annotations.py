"""
Reference overlap annotations used for/by Prolific annotation server.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""


class OverlapAnnotation:
    def __init__(self, path, kr, valence_sign, emotion_leaf, min_arousal):
        """

        :param path:
        :param kr: keep/reject
        :param valence_sign: -1 = neg, 0 = neutral, 1 = pos
        """
        self.path = path
        self.kr = kr
        self.valence_sign = valence_sign
        self.emotion_leaf = emotion_leaf
        self.min_arousal = min_arousal

class RefOverlapAnnotations:
    FIXED_OVERLAP_IMAGES = [
        OverlapAnnotation("/Run_1/Grateful seniors playground/20856-5-full.jpg", 'Reject', 0, 24, 0), # Reject
        OverlapAnnotation("/Run_1/Kind people gathering/christmas-dinner.jpg", 'Keep', 1, 0, 1), # Joy
        OverlapAnnotation("/Run_1/Angry children sports/protesters-outside-police-were-angry-police-inaction.jpg", 'Keep', -1, 6,3), # Anger
        OverlapAnnotation("/Run_1/Violent people sports/trump-rally-california.jpg", 'Keep', -1, 6, 4), # Rage
        OverlapAnnotation("/Run_1/Desiring children work/161dfe2a046ced6d7ef45283e24d63eb.png", 'Keep', -1, 4, 1)  # Sadness
    ]
