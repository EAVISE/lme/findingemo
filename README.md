# FindingEmo

<div align="center">
<hr>
🚨⚠️🚨 NOT FOR COMMERCIAL PURPOSES 🚨⚠️🚨
<hr>
</div>

This repository contains the annotations, URL list, code and additional material relating to the paper ["FindingEmo: A Picture Dataset for Emotion
Recognition in the Wild"](https://arxiv.org/abs/2402.01355) by Laurent Mertens et al., accepted at the [NeurIPS 2024 Datasets and Benchmarks Track](https://nips.cc/virtual/2024/poster/97871).
There are 2 versions of the paper, with some experiments differing between both; this repository contains the code for both version.

<font color='red'>**In case you are only looking for the dataset**</font> (annotations + images), please visit https://gitlab.com/EAVISE/lme/findingemo-light.
Alternatively, simply install the FindingEmo-Light package:
```
pip install findingemo-light
```
Then, in a Python script, do:
```python
from findingemo_light.paper.download_multi import download_data

download_data(target_dir='./Path/To/Where/You/Want/To/Download/The/Images')
```
To get the annotations, use:
```python
from findingemo_light.data.read_annotations import read_annotations

ann_data = read_annotations()
print(ann_data)
```


## Updates
2024/12/04: Created a FindingEmo-Light repository for those who are only looking for the data.\
2024/10/30: URL list has been updated.

## Logo
The ```./data/Logo``` folder contains image files with the dataset logo in various formats. If you find this data useful,
feel free to use the logo on your poster.

<div align="center">
<img src="https://www.laurentmertens.com/FindingEmo/FindingEmo_Color_Logo.png">
</div>

## Dataset
The <u><b>annotations</b></u> are stored in the [```data/annotations_single.ann```](./data/annotations_single.ann) file, which is a simple text file in CSV format.
The [```data/dataset_urls_exploded.json```](./data/dataset_urls_exploded.json) file contains the <u><b>URLs for the annotated images</b></u>, with multiple URLs provided
for a large number of images. We intend to update this file as we obtain backup URLs for more images.

A <u><b>Croissant metadata</b></u> file is also included ([```./croissant-findingemo.json```](./croissant-findingemo.json) to allow loading the dataset through the [Croissant](https://github.com/mlcommons/croissant) framework.

Dataset <u><b>documentation</b></u> can be found at [```datasheet/datasheet.md```](./datasheet/datasheet.md).

## Code
All CSV/TXT files used for generating the results tables and graphs in the paper are contained in the ```data``` folder.
To generate the tables/graphs, use ```paper/create_graphs.py``` and ```paper/create_latex_tables.py```.

<font color='red'><b><u>Importantly</u></b></font>, the root folder contains a ```config.py``` script that defines a number of useful constants representing
paths and file locations. This file should be edited to reflect your system.

**If you think anything is missing to reproduce the results discussed in the paper, or simply need some help in this
regard, please contact the author at [laurent.mertens@kuleuven.be](laurent.mertens@kuleuven.be).**

## Legal Compliance and Privacy
This dataset contains URLs to potentially copyrighted material. If you are a member of a research institution located
within the European Union, you are allowed to use this material for **non-commercial research purposes** by virtue of Title
II, Article 3 of the [InfSoc directive](https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX%3A32019L0790)). If you
are not located within the European Union, it is your responsibility to verify with local legislation whether you are
allowed to use this material or not.

<font color='red'>In case you are the legal copyright holder of any of the images we provide a link to, or are depicted in any of these
images, and you do not wish that your material and/or likeness be used for Machine Learning purposes, you can contact
either [laurent.mertens@kuleuven.be](laurent.mertens@kuleuven.be) or
[joost.vennekens@kuleuven.be](joost.vennekens@kuleuven.be) with the details of the image, and we will immediately remove
it from the dataset.

**IN NO CASE SHALL THIS DATASET BE USED FOR ANY COMMERCIAL PURPOSE.**</font>

## Licensing
All code is licensed under an MIT license (see [LICENSE_code.md](./LICENSE_code.md)). \
All data (annotations + list of URLs) is shared  under a
[Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License][cc-by-nc-sa] (see [LICENSE_data.md](./LICENSE_data.md)).

[cc-by-nc-sa]: http://creativecommons.org/licenses/by-nc-sa/4.0/
[cc-by-nc-sa-shield]: https://img.shields.io/badge/License-CC%20BY--NC--SA%204.0-lightgrey.svg

## Data overview
The ```data``` folder contains the CSV/TXT files containing the results per experiment used to generate the results
reported in the paper.

The ```AnnotationServerAuth``` folder is the default location of the ```id_rsa``` and ```id_rsa.pub``` files you will
need to generate to run the annotation interface. (See [below](#ann_interface)</a>.)

All ```Baseline[...]``` folder contain log files with the results for that particular experiment. E.g., ```Baseline Emo8
Loss=CE``` contains the results for the baseline models for Emo8 classification using CrossEntropyLoss.

The ```BufferedFeatures```  and ```Models``` folders are the default folders where output from the scripts referred to
below will be saved.

The ```Logo``` folder contains image files with the dataset logo.

The ```annotations_single.ann``` file, which is a simple text file in CSV format, contains the image annotations.

The ```dataset_urls.txt``` file contains the original single URL for each annotated images.

The ```dataset_urls_exploded.txt``` file is the extended version of ```dataset_urls.txt```, which list multiple URLs for a
number of images.

## <a name="obtaining_images"></a>Obtaining the annotated images
Execute ```paper/download_multi.py``` and save images to ```Config.DIR_IMAGES``` (which is the
default save location).

(The ```download_images.py``` script is the original version that uses the single-URL-per-image ```dataset_urls.txt``` file.)

## Pretrained models overview
Models used in this code include standard PyTorch models, as well as other models available from:
* EmoNet: [https://gitlab.com/EAVISE/lme/emonet](https://gitlab.com/EAVISE/lme/emonet); included in requirements.txt.
* FER2013: [https://github.com/LetheSec/Fer2013-Facial-Emotion-Recognition-Pytorch](https://github.com/LetheSec/Fer2013-Facial-Emotion-Recognition-Pytorch);
we use the ```official model checkpoint``` linked to in the README. Place the weights under ```Config.DIR_PRETRAINED_MODELS/FER2013/official/best_checkpoint.tar```.
* OpenImages-trained YoLo v3: look for ```YOLOv3 on the Open Images dataset``` on [https://pjreddie.com/darknet/yolo/](https://pjreddie.com/darknet/yolo/)
Place the weights under ```Config.DIR_PRETRAINED_MODELS/DarkNet/yolov3-openimages.weights```.
* Places365: [https://github.com/CSAILVision/places365](https://github.com/CSAILVision/places365); look for "PyTorch Places365 models"
in the README. Place models under ```Config.DIR_PRETRAINED_MODELS/Places365/Official/[model_name]_places365.pth.tar```.
* CLIP:  [https://github.com/openai/CLIP](https://github.com/openai/CLIP); install the Python ```clip``` package following
the instructions provided on this page.
* DINOv2: [https://github.com/facebookresearch/dinov2](https://github.com/facebookresearch/dinov2); the DINOv2 model is
loaded through PyTorch.

## How to obtain the results reported in the paper
Of course, you will not obtain exactly the same results, as the process is stochastic and we did not use a fixed random
seed (we report averages and standard deviations). The steps to redo the experiments are as follows.

### Download the dataset
See [Obtaining the annotated images](#obtaining_images).

### Buffer model activations and predictions
Note that you will have to edit the files referred to below, typically to select all the models you want to generate
results for.

* Execute ```networks/nn/buffer/buffer_all.py``` to pre-compute ImageNet model activations and store to disk.
* Execute ```networks/nn/buffer/buffer_clip_feats.py``` to pre-compute CLIP embeddings and store to disk.
* Execute ```networks/nn/buffer/buffer_dinov2_feats.py``` to pre-compute DINOv2 embeddings and store to disk.
* Execute ```networks/nn/best_models/train_prolific_xxx_yyy_model.py``` scripts (```xxx``` == ```arousal```, ```emo8```,
* ```valence```, ```y``` == ```reg``` for regression, ```class``` for classification) to train models for Arousal3, Emo8
and Valence3 classification or Arousal and Valence regression.
* Use ```networks/nn/buffer/buffer_in_predictions.py``` to pre-compute model predictions.
* Use ```networks/nn/buffer_clip_predictions``` and ```networks/nn/buffer_dinov2_predictions``` to pre-compute model
predictions using CLIP/DINOv2 features.

### Generate results
* Execute the necessary ```paper/generate_..._results.py``` scripts to generate the results

### Generate graphs and tables
* Execute ```paper/create_graphs.py``` and ```paper/create_latex_tables.py```, referring to the
output files generated in the previous step. Check ```main``` clause of these scripts to select which graphs/tables you
wich to generate.

## <a name="ann_interface"></a>Annotation interface
Instructions on how to get the annotation interface up and running are contained in [./AnnotationServerSetup.md](AnnotationServerSetup.md).

## Acknowledgment
This work was funded by KU Leuven grant IDN/21/010.


Author: Laurent Mertens\
Mail: [laurent.mertens@kuleuven.be](laurent.mertens@kuleuven.be)
