"""
A class that controls what random images are selected and shown to users for multi-user annotations.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import os
import random
from collections import Counter

import numpy as np
import pandas as pd
from lor_tools.search.binary_search import BinarySearch

from annotations.filter_prolific_annotations import FilterProlificAnnotations
from config import Config
from tools.annotation_dataset_tools import AnnotationEmotionMap
from tools.pandas_tools import PandasTools


# from login_api import LoginAPI


class OverlapAPI:
    FIXED_OVERLAP_IMAGES = [
        "/Run_1/Grateful seniors playground/20856-5-full.jpg",  # Reject
        "/Run_1/Kind people gathering/christmas-dinner.jpg",  # Joy
        "/Run_1/Angry children sports/protesters-outside-police-were-angry-police-inaction.jpg",  # Anger
        "/Run_1/Violent people sports/trump-rally-california.jpg",  # Rage
        "/Run_1/Desiring children work/161dfe2a046ced6d7ef45283e24d63eb.png"  # Sadness
    ]

    MAX_AROUSAL_DIFF = 3

    def __init__(self):
        """
        Check whether overlap csv file exists;
        If it does, load.
        If it does not, create and initialize with default.
        """
        print("Initializing OverlapAPI...", end="")
        self.min_choice = 10  # Minimum number of valid, i.e., not 'done', images that need to be available in the pool.
        self.rng = random.SystemRandom()

        # If csv file containing overlap image db already exists, load it.
        if os.path.exists(Config.FILE_OVERLAP):
            self.overlap_imgs = pd.read_csv(filepath_or_buffer=Config.FILE_OVERLAP, header=0, index_col=0)
        # If not, create a new db, and see if it can be initialized with existing annotations.
        else:
            self.overlap_imgs = pd.DataFrame(
                columns=["img_path", "nb_anns", "full_agreement", "done"])

            # Load existing annotations, if any, and extract those images for which multiple annotations exist
            annotations = self.get_annotations()
            if annotations is not None:
                overlap_img_paths = Counter()

                for img_path in sorted(annotations.image_path.unique()):
                    # Don't consider the fixed overlap images
                    if img_path in self.FIXED_OVERLAP_IMAGES:
                        continue

                    img_anns = annotations[annotations.image_path == img_path]
                    if len(img_anns) > 1:
                        overlap_img_paths[img_path] += len(img_anns)
                        b_full_agreement = self.check_full_agreement(img_anns)
                        b_done = self.compute_done_status(nb_anns=len(img_anns), b_full_agreement=b_full_agreement)

                        print("="*60)
                        with pd.option_context('display.max_rows', None,
                                               'display.max_columns', None,
                                               'display.precision', 3,
                                               ):
                            print(img_anns)
                        print(f"Full agreement? {b_full_agreement}")
                        new_row = {
                            "img_path": img_path,
                            "nb_anns": len(img_anns),
                            "full_agreement": b_full_agreement,
                            "done": b_done
                        }
                        # self.overlap_imgs = self.overlap_imgs._append(new_row, ignore_index=True)
                        # The "append" method has been deprecated. Use below alternative instead.
                        self.overlap_imgs = pd.concat([self.overlap_imgs, pd.DataFrame.from_records([new_row])],
                                                      ignore_index=True)

                self.save_db()

        self.img_paths = set(self.overlap_imgs.img_path.values)

        print(" done!")

    def save_db(self):
        """
        Save the overlap image database to disk.

        :return:
        """
        self.overlap_imgs.to_csv(path_or_buf=Config.FILE_OVERLAP)

    @classmethod
    def get_annotations(cls):
        annotations = None

        if os.path.exists(Config.FILE_ANN):
            annotations = pd.read_csv(Config.FILE_ANN, header=0, index_col=0)
            annotations.fillna('Undefined', inplace=True)

            annotations = FilterProlificAnnotations.process(annotations, b_remove_incomplete=True,
                                                            b_use_alt_score=True,
                                                            min_score=0.80)
            if annotations.empty:
                annotations = None

        return annotations

    @staticmethod
    def compute_done_status(nb_anns, b_full_agreement):
        """
        Check whether we have all the annotations we need for a given image or not.

        :param nb_anns:
        :param b_full_agreement:
        :return: True is we have what we need, False otherwise.
        """
        return (nb_anns >= 3 and b_full_agreement) or (nb_anns >= 5)

    def update_image(self, img_path, df_img_anns):
        """
        Update database entry for a specific image.

        :param img_path:
        :param df_img_anns:
        :return:
        """
        if not self.is_control_image(img_path):
            print(f"OverlapAPI:: img_path not present in overlap db: {img_path}")
            return False
        print(f"OverlapAPI: Updating entry for image {img_path}")
        rec = self.overlap_imgs[self.overlap_imgs.img_path == img_path]

        b_full_agreement = self.check_full_agreement(df_img_anns)
        b_done = self.compute_done_status(nb_anns=len(df_img_anns), b_full_agreement=b_full_agreement)

        self.overlap_imgs.at[rec.index[0], 'nb_anns'] = len(df_img_anns)
        self.overlap_imgs.at[rec.index[0], 'full_agreement'] = b_full_agreement
        self.overlap_imgs.at[rec.index[0], 'done'] = b_done

        self.save_db()

    def get_random_img(self):
        """
        Randomly select an image (path) from all valid images.

        :return: image_path if an image could be found, None otherwise
        """
        # Get all valid images, i.e., those that are not done yet
        df_valid_imgs = self.overlap_imgs[self.overlap_imgs.done == False]

        # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        # Check if we need to update the db
        # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        if len(df_valid_imgs) < self.min_choice:
            self._update_overlap_images(max_images=100)
            df_valid_imgs = self.overlap_imgs[self.overlap_imgs.done == False]

        # In case there are no valid images, return None
        if df_valid_imgs.empty:
            return None

        # Higher odds for images with more annotations
        nb_anns_vals = df_valid_imgs.nb_anns.values
        odds = [nb_anns_vals[0]]
        for i in range(1, len(nb_anns_vals)):
            odds.append(odds[i-1] + nb_anns_vals[i])

        # Generate random integer that will be converted to index of image in df_valid_imgs
        rnd_idx = self.rng.randint(1, odds[-1])
        img_idx = BinarySearch.find(odds, rnd_idx, b_return_pos=True)
        if img_idx < 0:
            img_idx = -(img_idx + 1)
        rnd_img = df_valid_imgs.iloc[img_idx]

        # The usecase of this method is to be used to select a random overlap image for a Prolific user to annotate.
        # Temporarily set this iamge to "done", so that it can not be picked again to be annotated by a different user,
        # as long as the current user has not finished annotating it.
        self.overlap_imgs.at[rnd_img.name, 'done'] = True

        return rnd_img.img_path

    def is_control_image(self, img_path):
        """
        Check if the provided img_path is known as an overlap image candidate, i.e., is present in the db.

        :param img_path:
        :return: True if img_path is present in the overlap image db, False otherwise.
        """
        return img_path in self.img_paths

    @classmethod
    def check_full_agreement(cls, df_img_anns):
        """
        Check whether all annotations for a given image agree or not.

        :param df_img_anns: DataFrame containing all annotations for a SAME image.
        :return: True if all agree, False if they don't.
        """
        # If only one annotator, return False
        if len(df_img_anns) == 1:
            return False

        ku_uq = df_img_anns.reject.unique()
        # Everyone agrees on Keep/Reject
        if len(ku_uq) == 1:
            # Everyone agrees on Reject; stop here
            if ku_uq[0] == 'Reject':
                return True
            # Everyone agrees on Keep; check other values, namely valence, arousal and emotion_leaf
            elif ku_uq[0] == 'Keep':
                # Check valence; all values should bear same sign
                val = df_img_anns.valence.values
                val_agree = np.all(val >= 0) if val[0] >= 0 else np.all(val < 0)
                if not val_agree:
                    return False
                # Check arousal; check max diff = 3 by default
                arsl = df_img_anns.arousal.values
                if (arsl.max() - arsl.min()) > cls.MAX_AROUSAL_DIFF:
                    return False
                # Check emotions; should come from same emotion leaf
                emos = df_img_anns.emotion.unique()
                if len(emos) == 1:
                    return True
                else:
                    emo_leafs = [AnnotationEmotionMap.EMOTION_LEAF_MAP_EXT[emos[i]] for i in range(len(emos))]
                    if len(emo_leafs) == 1:
                        return True
                    else:
                        return False
            else:
                raise ValueError(f"Unexpected Keep/Reject value: {ku_uq[0]}")
        # Not everyone agrees on Keep/Reject
        else:
            return False

    def _update_overlap_images(self, max_images=500):
        """
        Update the available overlap images, i.e., the pool of images from which random overlap images are picked.
        A maximum number of max_images new images will be added to the pool.

        :param max_images: maximum number of new images to add to the pool.
        :return:
        """
        print("OverlapAPI: Updating overlap image db...", end='')
        # # Request IDs of annotators with certain minimum Prolific score
        # ok_users = LoginAPI.get_users_min_prolific_score(min_score=0.95)
        # if not ok_users:
        #     print(f" done; alas, no ok users were found, so no new images could be added.")
        #     return
        # Load annotations
        annotations = self.get_annotations()
        if annotations is None:
            print(f" done; alas, no existing or valid annotations could be found, so no new images could be added.")
            return
        # Get annotations from ok annotators
        # ok_annotations = annotations[annotations.user.isin(ok_users)]
        # if ok_annotations.empty:
        #     print(f" done; alas, no ok annotations could be found, so no new images could be added.")
        #     return

        # Add images
        uq_img_paths = set(annotations.image_path.unique())
        # Make sure to remove fixed overlap images
        for e in self.FIXED_OVERLAP_IMAGES:
            if e in uq_img_paths:
                uq_img_paths.remove(e)

        remaining_paths = list(uq_img_paths.difference(self.img_paths))
        if not remaining_paths:
            print(f" done; alas, no sufficient valid images could be found, so no new images could be added.")
            return

        # Shuffle images around so as to mingle annotators as well
        self.rng.shuffle(remaining_paths)
        # If max_images > len(remaining_paths), this will just return remaining_paths
        remaining_paths = remaining_paths[:max_images]

        nb_added_images = len(remaining_paths)
        for img_path in remaining_paths:
            # Check to be sure, but there should be no images with more than 1 annotation in this list
            img_anns = annotations[annotations.image_path == img_path]
            if len(img_anns) > 1:
                print(f"OverlapAPI: !!! ERROR WHEN UPDATING OVERLAP IMAGE DB !!!\nImage has {len(img_anns)} annotations: {img_path}")
                max_images -= 1
                continue

            new_row = {
                "img_path": img_path,
                "nb_anns": 1,
                "full_agreement": False,
                "done": False
            }
            # self.overlap_imgs = self.overlap_imgs.append(new_row, ignore_index=True)
            self.overlap_imgs = PandasTools.pd_dataframe_append_row(self.overlap_imgs, new_row)

        self.save_db()
        print(f" done! Added {nb_added_images} new images to the db.")


if __name__ == '__main__':
    overlap_api = OverlapAPI()
    overlap_api.get_random_img()
    # overlap_api._update_overlap_images()
