"""
CherryPy server_main instance that controls the login process.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import datetime
import os
from enum import Enum
from io import RawIOBase, StringIO

import cherrypy
import cherrypy_cors
import jwt
import pandas as pd
from cryptography.fernet import Fernet
from cryptography.hazmat.primitives import serialization
from jwt import ExpiredSignatureError
from termcolor import cprint

from config import Config
from tools.pandas_tools import PandasTools


class DecryptedDB(RawIOBase):
    def __init__(self, file, decoder):
        with open(file, 'rb') as fin:
            self.content = decoder.decrypt(fin.read())

    def read(self, size=-1):
        return self.content.decode()


class Roles(Enum):
    ADMIN = 'admin'
    ANNOTATOR = 'annotator'


class LoginAPI(object):
    FERNET = None

    @staticmethod
    def get_wsgi_config():
        cherrypy_cors.install()
        return {
            '/': {
                'cors.expose.on': True,
            }
        }

    def __init__(self):
        print("Initializing Login API...", end='')

        self.private_key = serialization.load_ssh_private_key(
            open(os.path.join(Config.DIR_AUTH, 'id_rsa'), 'rb').read(), password=b'')
        self.public_key = serialization.load_ssh_public_key(
            open(os.path.join(Config.DIR_AUTH, 'id_rsa.pub'), 'rb').read())

        # Completion URL provided by Prolific; should be updated with your own completion code
        self.completion_url = "https://app.prolific.co/submissions/complete?cc=XXXXXX"

        print(' done!')

    @cherrypy.expose
    def index(self):
        return "I'm Login API (3), I'm alive!"

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def login(self, param_name, param_value):
        """

        :param param_name: parameter name, should equal "PROLIFIC_ID"
        :param param_value: the Prolific ID of the user accessing the website
        :return: either a >0 fail code if login was unsuccessful, or fail code 0 + a valid JWT token if successful
        """
        # Check param_name has correct value
        if not param_name == 'PROLIFIC_ID':
            return {'fail_code': 1}

        # print(f"param_name : {param_name}")
        # print(f"param_value: {param_value}")

        user_db = self._get_user_db()

        # 1. Check user is known; if not, add to db
        b_exists = (param_value in user_db.prolific_id.values)
        if not b_exists:
            user_db = self._add_user_to_db(prolific_id=param_value)

        rec = user_db[user_db.prolific_id == param_value]
        # Generate a JWT token to log user in.
        # JWT token is valid for 20 minutes
        role = rec.iloc[0].role
        b_consented = bool(rec.iloc[0].consented)
        b_passed_tuto = bool(rec.iloc[0].passed_tuto)
        jwt_token = self._generate_jwt(prolific_id=param_value, role=role, b_consented=b_consented, b_passed_tuto=b_passed_tuto)

        return {'fail_code': 0,
                'jwt_token': jwt_token,
                'prolific_id': param_value,
                'role': role,
                'consented': b_consented,
                'passed_tuto': b_passed_tuto}

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def mark_consent(self, jwt_token):
        """
        Mark this user as having consented in the user database.

        :param jwt_token:
        :return:
        """
        b_is_valid = False
        new_token, prolific_id, role, b_consented, b_passed_tuto = None, None, None, False, False
        try:
            payload = jwt.decode(
                jwt=jwt_token,
                key=self.public_key,
                algorithms=['RS256', ],
                options={'verify_exp': True}
            )
            b_is_valid = True
            prolific_id = payload['prolific_id']
            # Update record in user_db
            user_db = self._get_user_db()
            rec = user_db[user_db.prolific_id == prolific_id]
            if rec.empty or len(rec) > 1:
                print("User not present, or multiple identical users present, in account; can not update.")
                return
            user_db.at[rec.index[0], 'consented'] = True

            with open(Config.FILE_USER_DB, 'wb') as fout:
                fout.write(self._get_fernet().encrypt(user_db.to_csv().encode(encoding='utf-8')))

            role = rec.iloc[0].role
            b_consented = True
            b_passed_tuto = bool(rec.iloc[0].passed_tuto)

            # Generate new JWT with updated expiration date
            new_token = self._generate_jwt(prolific_id=prolific_id, role=role, b_consented=b_consented, b_passed_tuto=b_passed_tuto)

        except ExpiredSignatureError as e:
            prolific_id = self._get_username(jwt_token)
            print(f'[{prolific_id}] mark_consent(): Unable to decode the token, error: {e}')

        return {'is_valid': b_is_valid,
                'jwt_token': new_token,
                'username': prolific_id,
                'role': role,
                'consented': b_consented,
                'passed_tuto': b_passed_tuto}

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def set_passed_tuto(self, jwt_token):
        """
        Update database entry for this particular user to reflect that (s)he passed the tuto.

        :param jwt_token:
        :return:
        """
        res = {'success': False}
        try:
            payload = jwt.decode(
                jwt=jwt_token,
                key=self.public_key,
                algorithms=['RS256', ],
                options={'verify_exp': True}
            )
            # print(f"Successfully decoded jwt; payload:\n{payload}")
            # print(int(datetime.datetime.now().timestamp()))
            # Generate new JWT with updated expiration date
            prolific_id = payload['prolific_id']
            role = payload['role']
            b_consented = payload['consented']
            b_passed_tuto = self.set_user_passed_tuto(prolific_id)  # Will be False if record doesn't exist or can't be updated somehow.
            new_token = self._generate_jwt(prolific_id=prolific_id, role=role, b_consented=b_consented, b_passed_tuto=b_passed_tuto)

            res['success'] = True
            res['jwt_token'] = new_token
            res['prolific_id'] = prolific_id
            res['role'] = role
            res['consented'] = b_consented
            res['passed_tuto'] = b_passed_tuto

        except ExpiredSignatureError as e:
            print(f'Unable to decode the token, error: {e}')

        return res

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def check_jwt_is_valid(self, jwt_token):
        """
        Verify whether the provided JWT has expired or not. If not, generate a new token with the same email address
        and updated expiration date. The expiration date will be updated to "current time + 20 minutes".

        :param jwt_token: an encoded JSON Web Token, as provided by the client side.
        :return: dict() containing boolean indicating whether JWT is still valid + field for JWT; if provided JWT
            was valid, an updated JWT will be returned with updated expiration time, otherwise it will just be 'None'.
        """
        b_is_valid = False
        new_token = None
        prolific_id = None
        role = None
        consented = None
        passed_tuto = None
        try:
            payload = jwt.decode(
                jwt=jwt_token,
                key=self.public_key,
                algorithms=['RS256', ],
                options={'verify_exp': True}
            )
            # print(f"Successfully decoded jwt; payload:\n{payload}")
            # print(int(datetime.datetime.now().timestamp()))
            b_is_valid = True
            # Generate new JWT with updated expiration date
            prolific_id = payload['prolific_id']
            role = payload['role']
            b_consented = payload['consented']
            b_passed_tuto = bool(payload['passed_tuto'])
            new_token = self._generate_jwt(prolific_id=prolific_id, role=role, b_consented=b_consented, b_passed_tuto=b_passed_tuto)

        except ExpiredSignatureError as e:
            print(f'Unable to decode the token, error: {e}')

        return {'is_valid': b_is_valid,
                'jwt_token': new_token,
                'prolific_id': prolific_id,
                'role': role,
                'consented': consented,
                'passed_tuto': passed_tuto}

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def get_done_url(self, jwt_token):
        """

        :param jwt_token:
        :return:
        """
        res = {'success': False}
        try:
            payload = jwt.decode(
                jwt=jwt_token,
                key=self.public_key,
                algorithms=['RS256', ],
                options={'verify_exp': True}
            )

        except Exception as e:
            print(f'Unable to decode the token, error: {e}')
            return res

        user_db = self._get_user_db()

        prolific_id = payload['prolific_id']
        b_exists = (prolific_id in user_db.prolific_id.values)
        if b_exists:
            rec = user_db[user_db.prolific_id == prolific_id]
            if rec.iloc[0].done:
                res['success'] = True
                res['url'] = self.completion_url

        return res

    # #####
    # Here be Dragons!
    # #####
    @classmethod
    def _add_user_to_db(cls, prolific_id: str):
        """
        Add user to user db.

        :param prolific_id:
        :return:
        """
        user_db = cls._get_user_db()

        # 1. Check prolific_id is not yet known
        b_exists = (prolific_id in user_db.prolific_id.values)
        if b_exists:
            return user_db
        # 2. If ID is not yet known, create new entry in user database
        else:
            creation_time = str(datetime.datetime.now())
            new_row = {
                "prolific_id": prolific_id,
                "role": Roles.ANNOTATOR.value,
                "creation_time": creation_time,
                "consented": False,
                "passed_tuto": False,
                "done": False,
                "overlap_idx": 0,
                "overlap_score": -1
            }
            # user_db = user_db.append(new_row, ignore_index=True)
            user_db = PandasTools.pd_dataframe_append_row(user_db, new_row)

            with open(Config.FILE_USER_DB, 'wb') as fout:
                fout.write(cls._get_fernet().encrypt(user_db.to_csv().encode(encoding='utf-8')))

        return user_db

    @classmethod
    def set_user_consented(cls, prolific_id):
        """

        :param prolific_id:
        :return:
        """
        user_db = cls._get_user_db()

        b_exists = (prolific_id in user_db.prolific_id.values)
        if not b_exists:
            return False
        else:
            rec = user_db[user_db.prolific_id == prolific_id]
            user_db.at[rec.index[0], 'consented'] = True

            with open(Config.FILE_USER_DB, 'wb') as fout:
                fout.write(cls._get_fernet().encrypt(user_db.to_csv().encode(encoding='utf-8')))

            return True

    @classmethod
    def set_user_passed_tuto(cls, prolific_id):
        """

        :param prolific_id:
        :return:
        """
        user_db = cls._get_user_db()

        b_exists = (prolific_id in user_db.prolific_id.values)
        if not b_exists:
            return False
        else:
            rec = user_db[user_db.prolific_id == prolific_id]
            user_db.at[rec.index[0], 'passed_tuto'] = True

            with open(Config.FILE_USER_DB, 'wb') as fout:
                fout.write(cls._get_fernet().encrypt(user_db.to_csv().encode(encoding='utf-8')))

            return True

    @classmethod
    def get_user_is_done(cls, prolific_id):
        """

        :param prolific_id:
        :return: True if successful, False otherwise
        """
        user_db = cls._get_user_db()

        b_exists = (prolific_id in user_db.prolific_id.values)
        if not b_exists:
            return False
        else:
            rec = user_db[user_db.prolific_id == prolific_id]

            return rec.iloc[0].done

    @classmethod
    def set_user_to_done(cls, prolific_id, user_score):
        """
        Set

        :param prolific_id:
        :param user_score: user_score over fixed overlap images
        :return: True if successful, False otherwise
        """
        user_db = cls._get_user_db()

        b_exists = (prolific_id in user_db.prolific_id.values)
        if not b_exists:
            return False
        else:
            rec = user_db[user_db.prolific_id == prolific_id]
            user_db.at[rec.index[0], 'done'] = True
            user_db.at[rec.index[0], 'overlap_score'] = user_score

            with open(Config.FILE_USER_DB, 'wb') as fout:
                fout.write(cls._get_fernet().encrypt(user_db.to_csv().encode(encoding='utf-8')))

            return True

    @classmethod
    def get_user_overlap_idx(cls, prolific_id):
        """

        :param prolific_id:
        :return: True if successful, False otherwise
        """
        user_db = cls._get_user_db()

        b_exists = (prolific_id in user_db.prolific_id.values)
        if not b_exists:
            return False
        else:
            rec = user_db[user_db.prolific_id == prolific_id]
            return int(rec['overlap_idx'].values[0])

    @classmethod
    def inc_user_overlap_idx(cls, prolific_id):
        """
        Increment user overlap index.

        :param prolific_id:
        :return: True if successful, False otherwise
        """
        user_db = cls._get_user_db()

        b_exists = (prolific_id in user_db.prolific_id.values)
        if not b_exists:
            return False
        else:
            rec = user_db[user_db.prolific_id == prolific_id]
            user_db.at[rec.index[0], 'overlap_idx'] = rec['overlap_idx'].values[0] + 1

            with open(Config.FILE_USER_DB, 'wb') as fout:
                fout.write(cls._get_fernet().encrypt(user_db.to_csv().encode(encoding='utf-8')))

            return True

    @classmethod
    def get_users_min_prolific_score(cls, min_score=1.0):
        """
        Get IDs of users with a minimum Prolific score.

        :param min_score: minimum score users should have.
        :return: list of user IDs fitting the criterium.
        """
        user_db = cls._get_user_db()
        ok_users = list(user_db[user_db.overlap_score >= min_score].prolific_id.values)

        return ok_users

    @classmethod
    def _get_user_db(cls):
        """
        Load user db into Pandas DataFrame if it already exists, otherwise create an empty database.

        :return: Pandas DataFrame
        """
        # Does user database already exist? If not, create.
        if os.path.exists(Config.FILE_USER_DB):
            with open(Config.FILE_USER_DB, 'rb') as fin:
                raw_csv = cls._get_fernet().decrypt(fin.read())
            user_db = pd.read_csv(StringIO(raw_csv.decode('utf-8')),
                                  header=0,
                                  index_col=0)
        else:
            user_db = pd.DataFrame(columns=["prolific_id", "role", "creation_time", "consented", "passed_tuto", "done", "overlap_idx", "overlap_score"])

        return user_db

    def _generate_jwt(self, prolific_id: str, role: str, b_consented: bool, b_passed_tuto: bool):
        """
        Generate an encrypted JSON Web Token encoding the provided prolific_id and role, and that contains an expiration time 20 minutes
        from the current timestamp.

        :param prolific_id:
        :param role: user role
        :return: JWT
        """
        # exp = expiry
        exp = int((datetime.datetime.now() + datetime.timedelta(minutes=20)).timestamp())
        payload_data = {
            'prolific_id': prolific_id,
            'role': role,
            'consented': b_consented,
            'passed_tuto': b_passed_tuto,
            'logged_in': 'True',
            'exp': exp
        }

        jwt_token = jwt.encode(
            payload=payload_data,
            key=self.private_key,
            algorithm='RS256'
        )

        return jwt_token

    @classmethod
    def _get_fernet(cls):
        if cls.FERNET is None:
            cprint("Initializing cls.FERNET", color='red')
            # Does Fernet key for encrypting the user database already exist?
            # If not, create and save to filepath specified in Config.
            if os.path.exists(Config.FILE_FERNET_KEY):
                with open(Config.FILE_FERNET_KEY, 'rb') as fin:
                    fernet_key = fin.read()
            else:
                fernet_key = Fernet.generate_key()

                # Check directory to write key to exists; if not, create
                abs_dir = os.path.dirname(os.path.abspath(Config.FILE_FERNET_KEY))
                if not os.path.exists(abs_dir):
                    os.makedirs(abs_dir)
                # Write generate key to file
                with open(Config.FILE_FERNET_KEY, 'wb') as fout:
                    fout.write(fernet_key)
            cls.FERNET = Fernet(fernet_key)
        # else:
        #     cprint("cls.FERNET already initialized!", color='blue')

        return cls.FERNET

    def _get_username(self, jwt_token):
        try:
            payload = jwt.decode(
                jwt=jwt_token,
                key=self.public_key,
                algorithms=['RS256', ],
                options={'verify_exp': False}
            )
            username = payload['username']
        except Exception as e:
            username = 'User UKN'

        return username


if __name__ == '__main__':
    lapi = LoginAPI()
    print("haha")