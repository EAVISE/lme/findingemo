"""
Precompute Keep/Reject/Uncertain labels for all scraped images.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import os
from torchvision import models

from PIL import Image
from config import Config
import torch
from networks.nn.imagenet.load_buffered_imagenet import LoadBufferedImageNet


class PrecomputeRKUDataset(torch.utils.data.Dataset):
    """
    PyTorch Dataset containing scraped image data.
    """
    def __init__(self, image_files, base_dir, preprocess_chain):
        """

        :param image_files: dictionary containing buffered features for first stream
        """
        self.image_files = image_files
        self.base_dir = base_dir
        self.preprocess_chain = preprocess_chain

    def __getitem__(self, item: int) -> (torch.Tensor, str):
        """
        Retrieves an item from the dataset.

        :param item: Item index.
        :return:
        """
        rel_path = self.image_files[item]
        img_path = os.path.join(self.base_dir, *rel_path.split(os.sep))
        try:
            img = self.preprocess_chain(self._pil_loader(img_path))
        except:
            print(f"Error opening image: {img_path}")
            img = torch.zeros((3,600,800))
            rel_path = '!' + rel_path

        return img, rel_path

    def _pil_loader(self, path: str):
        # open path as file to avoid ResourceWarning (https://github.com/python-pillow/Pillow/issues/835)
        with open(path, 'rb') as f:
            img = Image.open(f).convert('RGB')

        return img

    def __len__(self) -> int:
        """
        Return the length of the dataset.

        :return: The length of the dataset.
        """
        return len(self.image_files)


class PrecomputeRKU:
    def __init__(self, base_dir=None, out_file=None, batch_size=20, device=torch.device('cpu'), b_allow_parallel=True):
        if base_dir is None:
            base_dir = Config.DIR_IMAGES
        if out_file is None:
            out_file = os.path.join(Config.DIR_AUTH, Config.FILE_TSV_RK_NAME)
        self.out_file = out_file
        self.batch_size = batch_size
        self.device = device

        # model_name = "rku_googlenet_new_cuda"
        # self.model, preprocess = \
        #     LoadBufferedImageNet.load(model=models.googlenet, weights_file=os.path.join(Config.DIR_MODELS, f'{model_name}.pth'))
        model_name = 'rku_resnet50_lr=0.001_loss=CrossEntropyLoss_sc=test_cuda.pth'
        self.model, preprocess = \
            LoadBufferedImageNet.load(model=models.resnet50, weights_file=os.path.join(Config.DIR_MODELS, 'RKU', '20230630', model_name))
        self.model.eval()
        self.model.to(device)

        if b_allow_parallel:
            print(f"Using {torch.cuda.device_count()} GPUs...")
            self.model = torch.nn.DataParallel(self.model)

        self.valid_exts = {'.jpg', '.jpeg', '.png'}
        image_files = self._parse_dir(base_dir)

        dataset = PrecomputeRKUDataset(image_files=image_files, base_dir=base_dir, preprocess_chain=preprocess)
        self.dataloader = torch.utils.data.DataLoader(dataset, batch_size=batch_size)

    def parse_images(self):
        with open(self.out_file, 'w') as fout:
            fout.write("image_path\treject\tkeep\n")

            at_batch, at_image = 0, 0
            nb_files = len(self.dataloader.dataset)
            nb_batches = nb_files/self.batch_size
            with torch.no_grad():
                for data, paths in self.dataloader:
                    at_batch += 1
                    at_image += len(paths)
                    if at_batch % 5 == 0:
                        print(f"\rAt batch {at_batch}/{nb_batches} -- file {at_image}/{nb_files}", end='', flush=True)
                    pred = self.model(data.to(self.device))
                    pred = torch.softmax(pred, dim=1)

                    for i in range(len(paths)):
                        fout.write(f"{paths[i]}\t{pred[i][0].item()}\t{pred[i][1].item()}\n")
                print(f"\rAt batch {at_batch}/{nb_batches} -- file {at_image}/{nb_files}")


    def _parse_dir(self, _dir):
        images = []
        for e in os.listdir(_dir):
            full_path = os.path.join(_dir, e)
            if os.path.isdir(full_path):
                images += self._parse_dir(full_path)
            else:
                if os.path.splitext(e)[1] in self.valid_exts:
                    images.append(full_path.replace(Config.DIR_IMAGES, ''))

        return images

    def _get_preds_for_image(self, tensor_img):
        with torch.no_grad():
            pred = self.model.forward(tensor_img.unsqueeze(0))[0]
            pred = torch.softmax(pred, dim=0)
            pred = pred.detach().cpu().numpy()

        return pred


if __name__ == '__main__':
    processor = PrecomputeRKU(batch_size=150, device=torch.device('cuda'))
    processor.parse_images()
