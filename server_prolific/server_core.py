"""
Main server_main script. Run this to start up the server_main.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import cherrypy

from server_prolific.img_api import ImageAPI
from server_prolific.login_api import LoginAPI


class HelloApiWsgi:
    def __call__(self, environ, start_response):
        start_response('200 OK', [('Content-Type', 'text/html')])
        return [bytes('CherryPy is up and running!', 'UTF-8')]


if __name__ == '__main__':
    # Mount the application
    cherrypy.tree.graft(HelloApiWsgi(), '/')
    # ! Underscores are not allowed by the Apache server_main in hostnames !
    # See, e.g., https://ma.ttias.be/apache-httpd-2-2-15-60-underscores-hostnames-now-blocked/
    cherrypy.tree.mount(ImageAPI(), '/image-api', config=ImageAPI.get_wsgi_config())
    cherrypy.tree.mount(LoginAPI(), '/login-api', config=LoginAPI.get_wsgi_config())

    # Unsubscribe frm default server_main
    cherrypy.server.unsubscribe()

    # Instantiate a new server_main object
    server = cherrypy._cpserver.Server()

    # Configure the server_main object
    server.socket_host = "127.0.0.1"
    server.socket_port = 8083
    server.socket_queue_size = 32
    server.thread_pool = 4

    # Subscribe to this server_main
    server.subscribe()

    cherrypy.engine.start()
    cherrypy.engine.block()
