"""
Script to generate email files necessary for encoding the smtp password needed to be able to send emails.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import os

from cryptography.fernet import Fernet

from config import Config

if __name__ == '__main__':
    # Create Fernet key file

    email_key = os.path.join(Config.DIR_AUTH, 'email.key')
    email_enc = os.path.join(Config.DIR_AUTH, 'email.enc')

    # Overwrite if files already exist?
    b_overwrite = True

    if not b_overwrite and os.path.exists(email_key):
        raise FileExistsError("Files already exist.")

    fernet_key = Fernet.generate_key()

    # Check directory to write key to exists; if not, create
    abs_dir = os.path.dirname(os.path.abspath(email_key))
    if not os.path.exists(abs_dir):
        os.makedirs(abs_dir)
    # Write generate key to file
    with open(email_key, 'wb') as fout:
        fout.write(fernet_key)

    fernet = Fernet(key=fernet_key)
    print("Please enter the password to be encoded: ")
    pwd = input()
    with open(email_enc, 'wb') as fout:
        fout.write(fernet.encrypt(pwd.encode(encoding='utf-8')))
    print("All done!")
