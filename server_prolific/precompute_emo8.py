"""
Precompute Keep/Reject/Uncertain labels for all scraped images.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import os
from collections import Counter
from datetime import datetime

import pandas as pd
import torch
from PIL import Image

from config import Config
from networks.nn.multimodal.trained_multimodal_model import TrainedMultimodalModel


class PrecomputeEmo8Dataset(torch.utils.data.Dataset):
    """
    PyTorch Dataset containing scraped image data.
    """
    def __init__(self, image_files, base_dir, preprocess_chain):
        """

        :param image_files: list of relative image paths corresponding to the images to be loaded
        :param base_dir: base dir
        :param preprocess_chain: preprocess image transform(s) to be applied to images
        """
        self.image_files = image_files
        self.base_dir = base_dir
        self.preprocess_chain = preprocess_chain

    def __getitem__(self, item: int) -> (torch.Tensor, str):
        """
        Retrieves an item from the dataset.

        :param item: Item index.
        :return:
        """
        rel_path = self.image_files[item]
        img_path = os.path.join(self.base_dir, *rel_path.split(os.sep))
        try:
            img = self._pil_loader(img_path)
        except:
            print(f"Error opening image: {img_path}")
            # img = torch.zeros((3, 600, 800))
            img = Image.new(mode="RGB", size=(800, 600), color=(0, 0, 0))
            rel_path = '!' + rel_path

        processed_imgs = []
        for pc in self.preprocess_chain:
            pc_img = pc(img)
            processed_imgs.append(pc_img)

        return processed_imgs, rel_path

    def _pil_loader(self, path: str):
        # open path as file to avoid ResourceWarning (https://github.com/python-pillow/Pillow/issues/835)
        with open(path, 'rb') as f:
            img = Image.open(f).convert('RGB')

        return img

    def __len__(self) -> int:
        """
        Return the length of the dataset.

        :return: The length of the dataset.
        """
        return len(self.image_files)


class PrecomputeEmo8:
    def __init__(self, base_dir=None, out_file=None, trained_model=TrainedMultimodalModel.EXP5B_VGG16_VGG16SIN_EMONET_20230921,
                 batch_size=20, device=torch.device('cpu'), b_only_keep=False, b_allow_parallel=True):
        """

        :param base_dir:
        :param out_file:
        :param trained_model:
        :param batch_size:
        :param device:
        :param b_only_keep: only process images that were predicted to be "keep" by precompute_rk.py
        :param b_allow_parallel: allow usage of multiple GPUs, if present
        """
        if base_dir is None:
            base_dir = Config.DIR_IMAGES
        if out_file is None:
            out_file = os.path.join(Config.DIR_AUTH3, Config.FILE_TSV_EMO8_NAME)

        self.out_file = out_file
        self.batch_size = batch_size
        self.device = device

        print(f"Precomputing Emo8 labels using model {trained_model}")
        self.model = TrainedMultimodalModel.load_model(trained_model)
        self.model.eval()

        self.model.to(device)
        if b_allow_parallel:
            # raise NotImplementedError("Couldn't get parallel running to work, for now...")
            print(f"Using {torch.cuda.device_count()} GPUs...")
            self.model = torch.nn.DataParallel(self.model)
            # self.model.to(device)

        filter_paths = None
        if b_only_keep:
            if os.path.exists(Config.FILE_TSV_RK):
                df_rk_buffer = pd.read_csv(filepath_or_buffer=Config.FILE_TSV_RK, sep='\t')
                # 'Keep' images
                df_rk_buffer_ok = df_rk_buffer[df_rk_buffer['keep'] > 0.5]
                # Image names of 'Keep' images
                filter_paths = set(df_rk_buffer_ok.image_path.values)
            else:
                raise FileNotFoundError(f"Couldn't fine {Config.FILE_TSV_RK}. Did you run precompute_rk.py already?")

        self.valid_exts = {'.jpg', '.jpeg', '.png'}
        image_files = self._parse_dir(base_dir, _base_dir=base_dir, _filter_paths=filter_paths)

        if b_allow_parallel:
            dataset = PrecomputeEmo8Dataset(image_files=image_files, base_dir=base_dir,
                                            preprocess_chain=self.model.module.pre_procs)
        else:
            dataset = PrecomputeEmo8Dataset(image_files=image_files, base_dir=base_dir,
                                            preprocess_chain=self.model.pre_procs)
        self.dataloader = torch.utils.data.DataLoader(dataset, batch_size=batch_size)

    def parse_images(self):
        t_start = datetime.now()
        avg_t = 0

        cnts_per_emo = Counter()
        with open(self.out_file, 'w') as fout:
            fout.write("image_path\tjoy\ttrust\tfear\tsurprise\tsadness\tdisgust\tanger\tanticipation\tmax_emo\n")

            at_batch, at_image = 0, 0
            nb_files = len(self.dataloader.dataset)
            nb_batches = nb_files/self.batch_size
            s_cnts = ''
            with torch.no_grad():
                for data, paths in self.dataloader:
                    at_batch += 1
                    at_image += len(paths)
                    if at_batch % 5 == 0:
                        t_lap = datetime.now()
                        t_diff = t_lap - t_start
                        t_avg = t_diff.total_seconds()/at_batch
                        t_left = (nb_batches - at_batch)*t_avg
                        t_left_h = int(t_left//3600)
                        t_left_m = int((t_left - 3600*t_left_h)//60)
                        t_left_s = int(t_left - 3600*t_left_h - 60*t_left_m)

                        s_cnts = '\t'
                        for i in range(8):
                            s_cnts += f"{i} :: {cnts_per_emo[i]}"
                            if i < 7:
                                s_cnts += f" -- "
                        print(f"\rAt batch {at_batch}/{nb_batches} -- avg.t={t_avg:.2f} -- left: {t_left_h:02d}:{t_left_m:02d}:{t_left_s:02d} -- file {at_image}/{nb_files}{s_cnts}", end='', flush=True)
                    if isinstance(data, torch.Tensor):
                        pred = self.model(data.to(self.device))
                    else:
                        pred = self.model(data)

                    pred = torch.softmax(pred, dim=1)
                    max_pred = torch.argmax(pred, dim=1)
                    cnts_per_emo.update([max_pred[i].item() for i in range(len(max_pred))])

                    for i in range(len(paths)):
                        fout.write(f"{paths[i]}")
                        for j in range(len(pred[i])):
                            fout.write(f"\t{pred[i][j].item()}")
                        fout.write(f"\t{max_pred[i]}")
                        fout.write("\n")

                print(f"\rAt batch {at_batch}/{nb_batches} -- file {at_image}/{nb_files}{s_cnts}")

    def _parse_dir(self, _dir, _base_dir, _filter_paths):
        images = []
        for e in os.listdir(_dir):
            full_path = os.path.join(_dir, e)
            if os.path.isdir(full_path):
                images += self._parse_dir(full_path, _base_dir=_base_dir, _filter_paths=_filter_paths)
            else:
                if os.path.splitext(e)[1] in self.valid_exts:
                    rel_path = full_path.replace(_base_dir, '')
                    if _filter_paths is None or rel_path in _filter_paths:
                        images.append(rel_path)

        return images

    def _get_preds_for_image(self, tensor_img):
        with torch.no_grad():
            pred = self.model.forward(tensor_img.unsqueeze(0))[0]
            pred = torch.softmax(pred, dim=0)
            pred = pred.detach().cpu().numpy()

        return pred


if __name__ == '__main__':
    processor = PrecomputeEmo8(base_dir=Config.DIR_IMAGES,
                               trained_model=TrainedMultimodalModel.EXP5B_VGG19_VGG16SIN_EMONET_20231207,
                               batch_size=50, device=torch.device('cuda'),
                               b_only_keep=True, b_allow_parallel=True)
    processor.parse_images()
