"""
CherryPy server_main instance that controls the image requests coming from the annotator interface.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import os
import random
from collections import Counter
from datetime import datetime

import cherrypy
import cherrypy_cors
import jwt
import pandas as pd
import torch
from PIL import Image, UnidentifiedImageError
from cryptography.hazmat.primitives import serialization
from lor_tools.search.binary_search import BinarySearch
from termcolor import cprint
from torchvision import models

from config import Config
from networks.imagenet.load_buffered_imagenet import LoadBufferedImageNet
from server_prolific.login_api import LoginAPI
from server_prolific.overlap_api import OverlapAPI
from server_prolific.prolific_scorer import ProlificScorer
from tools.annotation_dataset_tools import AnnotationEmotionMap
from tools.pandas_tools import PandasTools


class ImageAPI(object):
    @staticmethod
    def get_wsgi_config():
        cherrypy_cors.install()
        return {
            '/': {
                'cors.expose.on': True,
            },
            '/images': {
                'tools.staticdir.on': True,
                'tools.staticdir.dir': Config.DIR_IMAGES
            }
        }

    def __init__(self):
        print("Initializing Image API...")

        # Collect all images located within (subdirectories of) the base dir
        self.valid_exts = {'.jpg', '.jpeg', '.png'}
        self.images = self._parse_dir(Config.DIR_IMAGES)
        # Public key used for decoding the JWT
        self.max_anns = 50  # Max number of images to be annotated by a single user
        self.public_key = serialization.load_ssh_public_key(
            open(os.path.join(Config.DIR_AUTH, 'id_rsa.pub'), 'rb').read())

        print(f"Loaded {len(self.images)} images...")

        # Load non-fixed overlap images API
        self.overlap_api = OverlapAPI()

        # Load annotations
        self.seen_name = set()  # Here, we keep track of filenames, rather than full paths
        # The idea is to be able to avoid duplicates; same image present within several folders.
        # Hence, whilst storing the full path in the annotations file, we only put the file name
        # into this container, so that we can check if a file with a particular name has already
        # been annotated, regardless of what folder it is present in.
        if os.path.exists(Config.FILE_ANN):
            self.annotations = pd.read_csv(Config.FILE_ANN, header=0, index_col=0)
            self.annotations.fillna('Undefined', inplace=True)

            # Check all annotated files are present; useful in case annotation files get moved around different systems
            nb_found, not_found = 0, []
            for i, row in self.annotations.iterrows():
                # Example of image_path: /Happy seniors competition/IMG_0999-e1471384138366.jpg
                parts = row.image_path.split('/')[1:]
                if os.path.exists(os.path.join(Config.DIR_IMAGES, *parts)):
                    nb_found += 1
                else:
                    not_found.append(row.image_path)
                self.seen_name.add(parts[-1])
            print(f"Found    : {nb_found}")
            print(f"Not found: {len(not_found)}")
            for e in not_found:
                print(f"\t{e}")
        else:
            self.annotations = pd.DataFrame(columns=["user", "image_path", "reject", "tags", "age",
                                                     "valence", "arousal", "emotion", "dec_factors", "ambiguity",
                                                     "fmri_candidate, datetime"])

        # The following dataframe is used to compute the status of the overlap images
        # self.annotations_filtered = FilterProlificAnnotations.process(self.annotations, b_remove_incomplete=True, min_score=0.80)
        self.annotations_filtered = OverlapAPI.get_annotations()

        # Get number of annotations per emotion
        self.cnts_per_emo = Counter([AnnotationEmotionMap.EMOTION_LEAF_MAP_EXT[e] for e in self.annotations.emotion.values])
        # Create list that contains "limits", that serve the following purpose:
        # When randomly sampling the next image to be annotated, the system will generate a random number between 0 and 1.
        # It will then look in what bucket in this "limits list" this number falls, and the bucket idx will server as emo idx.
        # It will then randomly sample an image that has a precomputed emo prediction that equals emo idx.
        self.nb_anns = len(self.annotations)  # Includes "reject" annotations
        self.nb_emo_anns = sum([self.cnts_per_emo[i] for i in range(24)])  # Ignore "Neutral"

        self._compute_emo8_limits()
        print(f"emo8_limits: {self.emo8_limits}")

        self.b_buffered_rk = False
        self.idx_rk_buffer = {}
        if os.path.exists(Config.FILE_TSV_RK):
            self.b_buffered_rk = True
            self.df_rk_buffer = pd.read_csv(filepath_or_buffer=Config.FILE_TSV_RK, sep='\t')
            # Create index
            self.idx_rk_buffer = {k:i for i,k in enumerate(self.df_rk_buffer.image_path.values)}
            # Filter out "good" images
            df_rk_buffer_ok = self.df_rk_buffer[self.df_rk_buffer['keep'] > 0.75]
            self.idxs_rk_buffer_ok = df_rk_buffer_ok.index.to_list()

        self.b_buffered_emo8 = False
        # self.emo8_buffered_imgs = set()
        if os.path.exists(Config.FILE_TSV_EMO8):
            self.b_buffered_emo8 = True
            self.df_emo8_buffer = pd.read_csv(filepath_or_buffer=Config.FILE_TSV_EMO8, sep='\t')
            # Add column to indicate whether image is still available for annotation or not
            self.df_emo8_buffer['image_name'] = ''
            self.df_emo8_buffer['available'] = True
            print("Initializing df_emo8_buffer...")
            for i, row in self.df_emo8_buffer.iterrows():
                print(f"\r\tAt row {i}...", end="")
                img_name = row.image_path.split('/')[1:][-1]
                self.df_emo8_buffer.at[i, 'image_name'] = img_name
                if img_name in self.seen_name:
                    self.df_emo8_buffer.at[i, 'available'] = False
            print()

        model_name = 'rku_resnet50_lr=0.001_loss=CrossEntropyLoss_sc=test_cuda.pth'
        self.model, self.preprocess = \
            LoadBufferedImageNet.load(model=models.resnet50, weights_file=os.path.join(Config.DIR_MODELS, 'RKU', '20230630', model_name))

        self.model.eval()

        # "Control percentage", expressed as a fraction 0<=x<=1.
        # This are the odds that, when a user requests a new image using "get_image_cnn_good", (s)he
        # will actually get to see an image that has already been annotated by someone else.
        # This way, we create the necessary overlap to determine inter-annotator agreement.
        self.pc_control = 0.15  # 0.20 up until and including run 26. Lowered afterwards to compensate for the fact
                                # that up until run 24 or something, due to a bug, the PC was in actuality higher
                                # ... It was supposed to be run 26, but it's propably up until and including run 27,
                                # as it appears that after pulling the code on the server, it was still running the
                                # previous version.

        # Define random number generator
        self.rng = random.SystemRandom()

        print()

    def _parse_dir(self, _dir):
        images = []
        for e in os.listdir(_dir):
            full_path = os.path.join(_dir, e)
            if os.path.isdir(full_path):
                images += self._parse_dir(full_path)
            else:
                if os.path.splitext(e)[1] in self.valid_exts:
                    images.append(full_path.replace(Config.DIR_IMAGES, ''))

        return images

    def _pil_loader(self, path: str):
        # open path as file to avoid ResourceWarning (https://github.com/python-pillow/Pillow/issues/835)
        full_path = os.path.join(Config.DIR_IMAGES, *path.split('/'))
        with open(full_path, 'rb') as f:
            img = Image.open(f)
            return img.convert('RGB')

    @cherrypy.expose
    def index(self):
        return "I'm Image API, I'm alive!"

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def get_image(self, jwt_token):
        res = {'success': False}
        try:
            payload = jwt.decode(
                jwt=jwt_token,
                key=self.public_key,
                algorithms=['RS256', ],
                options={'verify_exp': True}
            )

        except Exception as e:
            print(f'Unable to decode the token, error: {e}')
            return res

        b_seen = True
        while b_seen:
            idx = self.rng.randint(0, len(self.images)-1)
            fp = self.images[idx]

            # This image has already been annotated?
            b_seen = self._already_seen(fp)
            if b_seen:
                print(f"This image has already been annotated: {fp}")

        res = {'success': True, 'path': fp}
        return res

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def get_image_cnn(self, jwt_token):
        res = {'success': False}
        try:
            payload = jwt.decode(
                jwt=jwt_token,
                key=self.public_key,
                algorithms=['RS256', ],
                options={'verify_exp': True}
            )

        except Exception as e:
            print(f'Unable to decode the token, error: {e}')
            return res

        b_seen = True
        while b_seen:
            idx = self.rng.randint(0, len(self.images)-1)
            fp = self.images[idx]

            # This image has already been annotated?
            b_seen = self._already_seen(fp)
            if b_seen:
                print(f"This image has already been annotated: {fp}")
                continue

            b_process_img = True
            if self.b_buffered_rk:
                try:
                    img_idx = self.idx_rk_buffer[fp]
                    pred = [self.df_rk_buffer.iloc[img_idx].reject, self.df_rk_buffer.iloc[img_idx].keep]
                    b_process_img = False
                except KeyError:
                    pass
            if b_process_img:
                try:
                    img = self.preprocess(self._pil_loader(fp))
                    pred = self._get_preds_for_image(img)
                except UnidentifiedImageError:
                    continue

            pred = {'Reject': f'{pred[0]:4.2f}',
                    # 'Uncertain': f'{pred[1]:4.2f}',
                    'Keep': f'{pred[1]:4.2f}'}

        res = {'success': True, 'path': fp, 'pred': pred}
        return res

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def get_image_cnn_good(self, jwt_token):
        """
        Get next random image with a 'Keep' score > 0.75.

        :return:
        """
        res = {'success': False}
        try:
            payload = jwt.decode(
                jwt=jwt_token,
                key=self.public_key,
                algorithms=['RS256', ],
                options={'verify_exp': True}
            )

        except Exception as e:
            print(f'Unable to decode the token, error: {e}')
            return res

        prolific_id = payload['prolific_id']

        # user_anns = self.annotations[self.annotations.user == prolific_id]
        # # ann_imgs = user_anns.image_path.values
        # nb_keep = len(user_anns[user_anns.reject == 'Keep'])
        # if nb_keep >= self.max_anns:
        if LoginAPI.get_user_is_done(prolific_id=prolific_id):
            print(f"User {prolific_id} is all done, no more images to serve.")
            return {'success': True, 'path': 'ALL_DONE'}

        # Check if we need to fetch a control image.
        b_ok = False
        if self.rng.random() < self.pc_control:
            # We do need to fetch a control image.
            print(f"Fetching control image for user [{prolific_id}]...")

            # First, check if we need to present a fixed control image
            overlap_idx = LoginAPI.get_user_overlap_idx(prolific_id=prolific_id)
            if overlap_idx < len(OverlapAPI.FIXED_OVERLAP_IMAGES):
                fp = OverlapAPI.FIXED_OVERLAP_IMAGES[overlap_idx]
                LoginAPI.inc_user_overlap_idx(prolific_id=prolific_id)
                print(f"Selected fixed overlap image for user: {prolific_id}, idx {overlap_idx}")
            # If not, randomly pick an image
            else:
                fp = self._get_control_image(prolific_id)
            if fp is not None:
                if fp in self.idx_rk_buffer:
                    fp_idx = self.idx_rk_buffer[fp]
                    pred = {'Reject': f'{self.df_rk_buffer.iloc[fp_idx].reject:4.2f}',
                            # 'Uncertain': f'{pred[1]:4.2f}',
                            'Keep': f'{self.df_rk_buffer.iloc[fp_idx].keep:4.2f}'}
                    b_ok = True
                else:
                    try:
                        img = self.preprocess(self._pil_loader(fp))
                        pred = self._get_preds_for_image(img)
                        pred = {'Reject': f'{pred[0]:4.2f}',
                                # 'Uncertain': f'{pred[1]:4.2f}',
                                'Keep': f'{pred[1]:4.2f}'}
                        b_ok = True
                    except (UnidentifiedImageError, FileNotFoundError):
                        print("There was an error parsing the control image. Reverting to regular method.")

        if not b_ok:
            at_iter = 0
            while True:
                # Build in some anti-eternal-loop mechanism
                at_iter += 1
                if at_iter > 1000:
                    pred = {'Reject': 'N/A',
                            'Uncertain': 'N/A',
                            'Keep': 'N/A'}
                    break

                fp = None
                if self.b_buffered_emo8:
                    # Generate random integer that will be mapped onto emo
                    dummy_emo_idx = self.rng.uniform(0., self.emo8_limits[-1])
                    emo_idx = BinarySearch.find(self.emo8_limits, dummy_emo_idx, b_return_pos=True)
                    if emo_idx < 0:
                        emo_idx = (-emo_idx)-1
                    print(f"Random emo_idx: {emo_idx}")
                    # Randomly choose image for this emo
                    df_emo = self.df_emo8_buffer[(self.df_emo8_buffer.max_emo == emo_idx) &
                                                 (self.df_emo8_buffer.available == True)]
                    if df_emo.empty:
                        continue
                    # Else, pick random image from this DataFrame, and check it is a 'Keep'
                    # Ideally, the 'Reject' images should already be filtered
                    img_idx = self.rng.randint(0, len(df_emo)-1)
                    fp = df_emo.iloc[img_idx].image_path
                    row_idx = df_emo.iloc[img_idx].name
                    self.df_emo8_buffer.at[row_idx, 'available'] = False
                # We didn't get a buffered emo8 image, pick random image from "Keep" images
                if fp is None:
                    if self.b_buffered_rk:
                        idx = self.rng.randint(0, len(self.idxs_rk_buffer_ok)-1)
                        idx = self.idxs_rk_buffer_ok[idx]
                        fp = self.df_rk_buffer.iloc[idx].image_path
                    else:
                        idx = self.rng.randint(0, len(self.images)-1)
                        fp = self.images[idx]

                # Make sure the image exists; in case of buffered Keep/Reject, this might happen if, e.g., the used buffered
                # scores come from a different set of images.
                if not os.path.exists(os.path.join(Config.DIR_IMAGES, *fp.split('/'))):
                    continue
                elif fp.startswith("/Run_1"):
                    print("Image from run 1, skipping...")
                    continue


                # This image has already been annotated?
                if self._already_seen(fp):
                    print(f"This image has already been annotated: {fp}")
                    continue

                b_process_img = True
                pred = None
                if self.b_buffered_rk:
                    try:
                        img_idx = self.idx_rk_buffer[fp]
                        pred = [self.df_rk_buffer.iloc[img_idx].reject, self.df_rk_buffer.iloc[img_idx].keep]
                        b_process_img = False
                        # print("Buffered!")
                    except KeyError:
                        print("Homeboy has not been approved.")
                        continue
                if b_process_img:
                    try:
                        img = self.preprocess(self._pil_loader(fp))
                        pred = self._get_preds_for_image(img)

                    except UnidentifiedImageError:
                        continue

                # img = self.preprocess(self._pil_loader(fp))
                # pred = self._get_preds_for_image(img)
                if pred is not None and pred[1] > 0.75:
                    pred = {'Reject': f'{pred[0]:4.2f}',
                            # 'Uncertain': f'{pred[1]:4.2f}',
                            'Keep': f'{pred[1]:4.2f}'}
                    break

        res = {'success': True, 'path': fp, 'pred': pred}
        return res

    # Get control image
    def _get_control_image_OLD(self, for_user: str):
        """
        OLD implementation of selecting a control image, that does not use the OverlapAPI.

        :param for_user: the (ID of the) user for which a control image should be fetched.
        :return: the path to the control image.
        """
        # Get subset of annotations that are eligible as control annotations/images.
        ann_subset =\
            self.annotations[self.annotations.user != for_user]
        # Check there are valid annotations
        if ann_subset.empty:
            return None

        while True:
            # Get random index in subset
            rnd_idx = self.rng.randint(0, len(ann_subset)-1)
            # Get corresponding image and check it hasn't been annotated by this particular user.
            control_image = ann_subset.iloc[rnd_idx].image_path
            # This image has already been annotated by this user?
            # If so, remove it from the pool, then chose different entry.
            if self._already_seen(control_image, for_user):
                print(f"This image has already been annotated by user [{for_user}]: {control_image}")
                ann_subset = ann_subset.drop(ann_subset.index[[rnd_idx]])
                # Probably won't happen, but check that after removing the invalid control image,
                # the remaining subset isn't empty.
                if ann_subset.empty:
                    return None
            else:
                return control_image

    def _get_control_image(self, for_user: str):
        """
        Call the OverlapAPI to select a random control image.

        :param for_user: the (ID of the) user for which a control image should be fetched.
        :return: the path to the control image.
        """
        # Build in some anti-eternal loop thing
        max_tries = 50
        at_try = 0
        while True:
            control_image = self.overlap_api.get_random_img()

            # This image has already been annotated by this user?
            # If so, choose different entry.
            # Note that in theory the only way a control image could already have been annotated by a (new) user
            # is if the same random control image is chosen more than once for this specific user.
            if control_image is None:
                print(f"Control image API returned None (user [{for_user}])")
                return None
            elif self._already_seen(control_image, for_user):
                print(f"This image has already been annotated by user [{for_user}]: {control_image}")
                at_try += 1
                if at_try == max_tries:
                    return None
            else:
                print(f"Overlap API selected for user [{for_user}]: {control_image}")
                return control_image

    # ############################################################
    # Annotations
    # ############################################################
    @cherrypy.expose
    @cherrypy.tools.json_out()
    def save_annotation(self, jwt_token, img, rej_value, tags, age, valence, arousal, emotion,
                        dec_factors, ambiguity, fmri_cand):
        res = {'success': False, 'done': False}
        try:
            payload = jwt.decode(
                jwt=jwt_token,
                key=self.public_key,
                algorithms=['RS256', ],
                options={'verify_exp': True}
            )

        except Exception as e:
            print(f'Unable to decode the token, error: {e}')
            return res

        prolific_id = payload['prolific_id']

        # Example of 'img': ./api-phd/image-api/images/Proud seniors work/d0f2e7bcfed5b3ef23f15e094ce19ad5.jpg
        img = img[26:]
        # print(f"{img}\n{rej_value}\n{tags}")
        new_row = {"user": prolific_id,
                   "image_path": img,
                   "reject": rej_value,
                   "tags": tags,
                   "age": age,
                   "valence": int(valence),
                   "arousal": int(arousal),
                   "emotion": emotion,
                   "dec_factors": dec_factors,
                   "ambiguity": int(ambiguity),
                   "fmri_candidate": (fmri_cand == 'true'),
                   "datetime": datetime.now().__str__()}

        # self.annotations = self.annotations.append(new_row, ignore_index=True)  # No longer works since Pandas v.2
        self.annotations = PandasTools.pd_dataframe_append_row(self.annotations, new_row)
        self.annotations.to_csv(path_or_buf=Config.FILE_ANN)
        # Also update filtered annotations DataFrame, but don't save changes to disk
        self.annotations_filtered = PandasTools.pd_dataframe_append_row(self.annotations_filtered, new_row)
        res['success'] = True

        user_anns = self.annotations[self.annotations.user == prolific_id]
        nb_user_anns_keep = len(user_anns[user_anns.reject == 'Keep'])

        if nb_user_anns_keep >= self.max_anns:
            # compute the overlap score and put into table
            user_score = ProlificScorer.score_user(user_anns)

            LoginAPI.set_user_to_done(prolific_id=prolific_id, user_score=user_score[0])
            res['done'] = True

        img_name = img.split('/')[-1]
        if img_name in self.seen_name:
            cprint(f"WARNING! Filename was ALREADY present in 'seen_name'! --> {img_name}")
        self.seen_name.add(img_name)

        # Update emo weights
        self._update_emo8_limits(emotion)

        # Update OverlapAPI if necessary; use filtered annotations DataFrame for this
        if self.overlap_api.is_control_image(img):
            self.overlap_api.update_image(img_path=img,
                                          df_img_anns=self.annotations_filtered[self.annotations_filtered.image_path == img])

        return res

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def get_stats(self, jwt_token):
        res = {'success': False}
        try:
            payload = jwt.decode(
                jwt=jwt_token,
                key=self.public_key,
                algorithms=['RS256', ],
                options={'verify_exp': True}
            )

        except Exception as e:
            print(f'Unable to decode the token, error: {e}')
            return res

        user_anns = self.annotations[self.annotations.user == payload['prolific_id']]
        nb_user_anns = len(user_anns)

        for k in ['Keep', 'Uncertain', 'Reject']:
            res[k] = len(user_anns[user_anns.reject == k])

        res['Done'] = res['Keep']  # nb_user_anns
        res['Left'] = self.max_anns - res['Done']

        res['success'] = True
        return res

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def get_annotations(self, jwt_token, sorted_by=None, ascending='true'):
        res = []
        try:
            payload = jwt.decode(
                jwt=jwt_token,
                key=self.public_key,
                algorithms=['RS256', ],
                options={'verify_exp': True}
            )

        except Exception as e:
            print(f'Unable to decode the token, error: {e}')
            return res

        if payload['role'] != 'admin':
            return res

        anns = self.annotations
        if sorted_by is not None:
            if sorted_by == 'index':
                anns = anns.sort_index(ascending=(ascending == 'true'))
            else:
                anns = anns.sort_values(by=sorted_by, ascending=(ascending == 'true'))

        # if payload['role'] == 'admin':
        for i, row in anns.iterrows():
            res.append((i, row.image_path, row.reject, row.tags, row.age,
                        row.valence, row.arousal, row.emotion,
                        row.dec_factors, row.ambiguity, row.fmri_candidate, row.user))
        #
        # else:
        # for i, row in anns[anns == payload['prolific_id']].iterrows():
        #     res.append((i, row.image_path, row.reject, row.tags, row.age,
        #                 row.valence, row.arousal, row.emotion,
        #                 row.dec_factors, row.ambiguity, row.fmri_candidate))

        return res

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def get_annotations_per_image(self, jwt_token):
        """
        Returns the annotations grouped per image.

        :param jwt_token:
        :param sorted_by:
        :param ascending:
        :return:
        """
        res = []
        try:
            payload = jwt.decode(
                jwt=jwt_token,
                key=self.public_key,
                algorithms=['RS256', ],
                options={'verify_exp': True}
            )

        except Exception as e:
            print(f'Unable to decode the token, error: {e}')
            return res

        # User isn't an admin
        if payload['role'] != 'admin':
            return res

        anns = self.annotations.copy(deep=True)

        # Compute number of annotations per image and store in extra column
        for img in anns['image_path'].unique():
            anns_for_img = anns[anns['image_path'] == img]
            nb_anns = len(anns_for_img)
            for idx in anns_for_img.index:
                anns.at[idx, 'nb_anns'] = nb_anns

        # Sort
        anns = anns.sort_values(by=["nb_anns", "image_path", "user"], ascending=[False, True, True])

        for i, row in anns.iterrows():
            res.append((i, row.image_path, row.reject, row.tags, row.age,
                        row.valence, row.arousal, row.emotion,
                        row.dec_factors, row.ambiguity, row.fmri_candidate, row.user))

        return res

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def get_prev_annotation(self, jwt_token, idx):
        res = {'fail_code': 1}
        try:
            payload = jwt.decode(
                jwt=jwt_token,
                key=self.public_key,
                algorithms=['RS256', ],
                options={'verify_exp': True}
            )

        except Exception as e:
            print(f'Unable to decode the token, error: {e}')
            return res

        # If user is admin, just give previous annotation, regardless of who made that annotation
        if payload['role'] == 'admin':
            idx = int(idx)-1
            if idx < 0:
                idx = len(self.annotations)-1
            elif idx >= len(self.annotations):
                idx = 0
        # Else, get previous annotation by this particular user
        else:
            user_anns = self.annotations[self.annotations.user == payload['prolific_id']]
            idx = int(idx)
            idx_loc = user_anns.index.get_loc(idx)
            idx_loc -= 1
            if idx_loc < 0:
                idx_loc = len(user_anns)-1
            idx = int(user_anns.index[idx_loc])

        row = self.annotations.iloc[idx]
        return [idx, row.image_path, row.reject, row.tags, row.age, int(row.valence), int(row.arousal),
                row.emotion, row.dec_factors, int(row.ambiguity), str(row.fmri_candidate)]

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def get_prev_own_annotation(self, jwt_token, idx):
        """
        Specifically for admins.

        :param jwt_token:
        :param idx:
        :return:
        """
        res = {'fail_code': 1}
        try:
            payload = jwt.decode(
                jwt=jwt_token,
                key=self.public_key,
                algorithms=['RS256', ],
                options={'verify_exp': True}
            )

        except Exception as e:
            print(f'Unable to decode the token, error: {e}')
            return res

        # Get previous annotation by this particular user
        user_anns = self.annotations[self.annotations.user == payload['prolific_id']]
        idx = int(idx)
        idx_loc = user_anns.index.get_loc(idx)
        idx_loc -= 1
        if idx_loc < 0:
            idx_loc = len(user_anns)-1
        idx = int(user_anns.index[idx_loc])

        row = self.annotations.iloc[idx]
        return [idx, row.image_path, row.reject, row.tags, row.age, int(row.valence), int(row.arousal),
                row.emotion, row.dec_factors, int(row.ambiguity), str(row.fmri_candidate)]

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def get_prev_keep_annotation(self, jwt_token, idx):
        res = {'fail_code': 1}
        try:
            payload = jwt.decode(
                jwt=jwt_token,
                key=self.public_key,
                algorithms=['RS256', ],
                options={'verify_exp': True}
            )

        except Exception as e:
            print(f'Unable to decode the token, error: {e}')
            return res

        # If user is admin, just give next annotation, regardless of who made that annotation
        if payload['role'] == 'admin':
            keep_anns = self.annotations[self.annotations.reject == 'Keep']
        # Else, get next annotation by this particular user
        else:
            keep_anns = self.annotations[(self.annotations.user == payload['prolific_id']) &
                                         (self.annotations.reject == 'Keep')]

        idx = int(idx)
        # Beware! We have no guarantee that the current annotation is of type 'keep'!
        idx_loc = BinarySearch.find(keep_anns.index.values, idx, b_return_pos=True)
        if idx_loc < 0:
            idx_loc = (-idx_loc) - 1
        idx_loc -= 1
        if idx_loc < 0:
            idx_loc = len(keep_anns) - 1
        idx = int(keep_anns.index[idx_loc])

        row = self.annotations.iloc[idx]
        return [idx, row.image_path, row.reject, row.tags, row.age, int(row.valence), int(row.arousal),
                row.emotion, row.dec_factors, int(row.ambiguity), str(row.fmri_candidate)]

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def get_prev_uncertain_annotation(self, jwt_token, idx):
        res = {'fail_code': 1}
        try:
            payload = jwt.decode(
                jwt=jwt_token,
                key=self.public_key,
                algorithms=['RS256', ],
                options={'verify_exp': True}
            )

        except Exception as e:
            print(f'Unable to decode the token, error: {e}')
            return res

        # If user is admin, just give next annotation, regardless of who made that annotation
        if payload['role'] == 'admin':
            keep_anns = self.annotations[self.annotations.reject == 'Uncertain']
        # Else, get next annotation by this particular user
        else:
            keep_anns = self.annotations[(self.annotations.user == payload['prolific_id']) &
                                         (self.annotations.reject == 'Uncertain')]

        idx = int(idx)
        # Beware! We have no guarantee that the current annotation is of type 'keep'!
        idx_loc = BinarySearch.find(keep_anns.index.values, idx, b_return_pos=True)
        if idx_loc < 0:
            idx_loc = (-idx_loc) - 1
        idx_loc -= 1
        if idx_loc < 0:
            idx_loc = len(keep_anns) - 1
        idx = int(keep_anns.index[idx_loc])

        row = self.annotations.iloc[idx]
        return [idx, row.image_path, row.reject, row.tags, row.age, int(row.valence), int(row.arousal),
                row.emotion, row.dec_factors, int(row.ambiguity), str(row.fmri_candidate)]

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def get_next_own_annotation(self, jwt_token, idx):
        """
        Specifically for admins.

        :param jwt_token:
        :param idx:
        :return:
        """
        res = {'fail_code': 1}
        try:
            payload = jwt.decode(
                jwt=jwt_token,
                key=self.public_key,
                algorithms=['RS256', ],
                options={'verify_exp': True}
            )

        except Exception as e:
            print(f'Unable to decode the token, error: {e}')
            return res

        # Get next annotation by this particular user
        user_anns = self.annotations[self.annotations.user == payload['prolific_id']]
        idx = int(idx)
        idx_loc = user_anns.index.get_loc(idx)
        idx_loc += 1
        if idx_loc >= len(user_anns):
            idx_loc = 0
        idx = int(user_anns.index[idx_loc])

        row = self.annotations.iloc[idx]
        return [idx, row.image_path, row.reject, row.tags, row.age, int(row.valence), int(row.arousal),
                row.emotion, row.dec_factors, int(row.ambiguity), str(row.fmri_candidate)]

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def get_next_annotation(self, jwt_token, idx):
        res = {'fail_code': 1}
        try:
            payload = jwt.decode(
                jwt=jwt_token,
                key=self.public_key,
                algorithms=['RS256', ],
                options={'verify_exp': True}
            )

        except Exception as e:
            print(f'Unable to decode the token, error: {e}')
            return res

        # If user is admin, just give next annotation, regardless of who made that annotation
        if payload['role'] == 'admin':
            idx = int(idx)+1
            if idx < 0:
                idx = len(self.annotations)-1
            elif idx >= len(self.annotations):
                idx = 0
        # Else, get next annotation by this particular user
        else:
            user_anns = self.annotations[self.annotations.user == payload['prolific_id']]
            idx = int(idx)
            idx_loc = user_anns.index.get_loc(idx)
            idx_loc += 1
            if idx_loc >= len(user_anns):
                idx_loc = 0
            idx = int(user_anns.index[idx_loc])

        row = self.annotations.iloc[idx]
        return [idx, row.image_path, row.reject, row.tags, row.age, int(row.valence), int(row.arousal),
                row.emotion, row.dec_factors, int(row.ambiguity), str(row.fmri_candidate)]

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def get_next_keep_annotation(self, jwt_token, idx):
        res = {'fail_code': 1}
        try:
            payload = jwt.decode(
                jwt=jwt_token,
                key=self.public_key,
                algorithms=['RS256', ],
                options={'verify_exp': True}
            )

        except Exception as e:
            print(f'Unable to decode the token, error: {e}')
            return res

        # If user is admin, just give next annotation, regardless of who made that annotation
        if payload['role'] == 'admin':
            keep_anns = self.annotations[self.annotations.reject == 'Keep']
        # Else, get next annotation by this particular user
        else:
            keep_anns = self.annotations[(self.annotations.user == payload['prolific_id']) &
                                         (self.annotations.reject == 'Keep')]

        idx = int(idx)
        # Beware! We have no guarantee that the current annotation is of type 'keep'!
        idx_loc = BinarySearch.find(keep_anns.index.values, idx, b_return_pos=True)
        if idx_loc < 0:
            idx_loc = (-idx_loc)-1
        idx_loc += 1
        if idx_loc >= len(keep_anns):
            idx_loc = 0
        idx = int(keep_anns.index[idx_loc])

        row = self.annotations.iloc[idx]
        return [idx, row.image_path, row.reject, row.tags, row.age, int(row.valence), int(row.arousal),
                row.emotion, row.dec_factors, int(row.ambiguity), str(row.fmri_candidate)]

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def get_next_uncertain_annotation(self, jwt_token, idx):
        res = {'fail_code': 1}
        try:
            payload = jwt.decode(
                jwt=jwt_token,
                key=self.public_key,
                algorithms=['RS256', ],
                options={'verify_exp': True}
            )

        except Exception as e:
            print(f'Unable to decode the token, error: {e}')
            return res

        # If user is admin, just give next annotation, regardless of who made that annotation
        if payload['role'] == 'admin':
            keep_anns = self.annotations[self.annotations.reject == 'Uncertain']
        # Else, get next annotation by this particular user
        else:
            keep_anns = self.annotations[(self.annotations.user == payload['prolific_id']) &
                                         (self.annotations.reject == 'Uncertain')]

        idx = int(idx)
        # Beware! We have no guarantee that the current annotation is of type 'keep'!
        idx_loc = BinarySearch.find(keep_anns.index.values, idx, b_return_pos=True)
        if idx_loc < 0:
            idx_loc = (-idx_loc)-1
        idx_loc += 1
        if idx_loc >= len(keep_anns):
            idx_loc = 0
        idx = int(keep_anns.index[idx_loc])

        row = self.annotations.iloc[idx]
        return [idx, row.image_path, row.reject, row.tags, row.age, int(row.valence), int(row.arousal),
                row.emotion, row.dec_factors, int(row.ambiguity), str(row.fmri_candidate)]

    # ############################################################
    # Here be Dragons!
    # ############################################################
    def _already_seen(self, img_path, for_user=None):
        """
        Check either whether an image, defined by its image name, has already been annotated or not, OR whether
        a specific image (path) has already been annotated by a specific user.

        :param img_path:
        :param for_user: if specified, will check whether the image has been annotated by this specific user
        :return:
        """
        if for_user is None:
            img_name = img_path.split("/")[-1]
            # print(f"Not seen before: {img_name}")
            return img_name in self.seen_name
        else:
            ann_subset = self.annotations[(self.annotations.image_path == img_path) &
                                          (self.annotations.user == for_user)]
            return not ann_subset.empty

    def _get_preds_for_image(self, tensor_img):
        with torch.no_grad():
            pred = self.model.forward(tensor_img.unsqueeze(0))[0]
            pred = torch.softmax(pred, dim=0)
            pred = pred.detach().cpu().numpy()

        return pred

    def _update_emo8_limits(self, emo):
        try:
            idx_emo = AnnotationEmotionMap.EMOTION_LEAF_MAP_EXT[emo]
        except KeyError:
            return

        self.nb_anns += 1
        self.nb_emo_anns += 1
        self.cnts_per_emo[idx_emo] += 1
        self._compute_emo8_limits()

    def _compute_emo8_limits(self):
        self.emo8_pre_limits = [self.nb_emo_anns/(self.cnts_per_emo[i] if self.cnts_per_emo[i] > 0 else 1) for i in range(8)]
        self.emo8_limits = [self.emo8_pre_limits[0]]
        for i in range(1, 8):
            self.emo8_limits.append(self.emo8_pre_limits[i] + self.emo8_limits[i-1])
        # print(self.emo8_limits)
