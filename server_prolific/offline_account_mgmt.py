"""

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import datetime
import hashlib
import math
import os
from io import StringIO

import numpy as np
import pandas as pd
from cryptography.fernet import Fernet

from config import Config
from server_prolific.login_api import Roles
from server_prolific.prolific_scorer import ProlificScorer


class OfflineAccountMgmt:
    def __init__(self):
        print("Initializing Offline Account Management...")

        # Does Fernet key for encrypting the user database already exist?
        # If not, create and save to filepath specified in Config.
        if os.path.exists(Config.FILE_FERNET_KEY):
            with open(Config.FILE_FERNET_KEY, 'rb') as fin:
                fernet_key = fin.read()
        else:
            fernet_key = Fernet.generate_key()

            # Check directory to write key to exists; if not, create
            abs_dir = os.path.dirname(os.path.abspath(Config.FILE_FERNET_KEY))
            if not os.path.exists(abs_dir):
                os.makedirs(abs_dir)
            # Write generate key to file
            with open(Config.FILE_FERNET_KEY, 'wb') as fout:
                fout.write(fernet_key)
        self.fernet = Fernet(fernet_key)

        # Does user database already exist? If not, create.
        if os.path.exists(Config.FILE_USER_DB):
            with open(Config.FILE_USER_DB, 'rb') as fin:
                raw_csv = self.fernet.decrypt(fin.read())
            self.user_db = pd.read_csv(StringIO(raw_csv.decode('utf-8')),
                                       header=0,
                                       index_col=0)
        else:
            self.user_db = pd.DataFrame(columns=["prolific_id", "role", "creation_time", "consented", "passed_tuto",
                                                 "done", "overlap_idx", "overlap_score"])

    def print_users(self):
        # print(self.user_db)
        with pd.option_context('display.max_rows', None,
                               'display.max_columns', None,
                               'display.precision', 3,
                               ):
            print(self.user_db)

    def delete_account(self, user: str):
        user_idx = self.user_db[self.user_db['prolific_id'] == user].index
        if user_idx.empty:
            print("User not present in account; can not delete.")
        self.user_db.drop(index=user_idx, inplace=True)
        with open(Config.FILE_USER_DB, 'wb') as fout:
            fout.write(self.fernet.encrypt(self.user_db.to_csv().encode(encoding='utf-8')))

        print("Account deleted successfully.")

    def change_role(self, user: str, role: Roles):
        user_idx = self.user_db[self.user_db['prolific_id'] == user].index
        if user_idx.empty:
            print("User not present in account; can not update.")
        self.user_db.at[user_idx, 'role'] = role.value
        with open(Config.FILE_USER_DB, 'wb') as fout:
            fout.write(self.fernet.encrypt(self.user_db.to_csv().encode(encoding='utf-8')))

        print("Account updated successfully.")

    def toggle_passed_tuto(self, user: str):
        user_idx = self.user_db[self.user_db['prolific_id'] == user].index
        if user_idx.empty:
            print("User not present in account; can not update.")
        b_passed_tuto = self.user_db.iloc(user_idx[0])[0].passed_tuto
        self.user_db.at[user_idx, 'passed_tuto'] = not b_passed_tuto
        with open(Config.FILE_USER_DB, 'wb') as fout:
            fout.write(self.fernet.encrypt(self.user_db.to_csv().encode(encoding='utf-8')))

        print("Account updated successfully.")

    def toggle_done(self, user: str):
        user_idx = self.user_db[self.user_db['prolific_id'] == user].index
        if user_idx.empty:
            print("User not present in account; can not update.")
        b_done = self.user_db.iloc[user_idx[0]].done
        self.user_db.at[user_idx[0], 'done'] = not b_done
        with open(Config.FILE_USER_DB, 'wb') as fout:
            fout.write(self.fernet.encrypt(self.user_db.to_csv().encode(encoding='utf-8')))

        print(f"Account updated successfully. User {user} set to {not b_done}")

    def reset_password(self, user: str, pwd: str):
        """
        Reset password for the provided user.

        :param user: the user
        :param pwd: the password, in plain string
        """
        # 1. Check an account with this user exists
        b_exists = (user in self.user_db.user.values)
        if not b_exists:
            print("No account with this user seems to exist.")
            return
        # 2. If user is known, reset password
        else:
            pwd_hash = hashlib.sha512()
            pwd_hash.update(pwd.encode())
            pwd_hash = pwd_hash.hexdigest()  # This immediately unhooks the hashlib instance

            user_idx = self.user_db[self.user_db['user'] == user].index
            if user_idx.empty:
                print("User not present in account; can not update.")
            self.user_db.at[user_idx, 'hash'] = pwd_hash
            with open(Config.FILE_USER_DB, 'wb') as fout:
                fout.write(self.fernet.encrypt(self.user_db.to_csv().encode(encoding='utf-8')))

            print("Password changed successfully.")

    def check_user_scores(self, ann_file=None, b_update=False):
        """
        Compare registered user overlap scores with newly computed ones.

        :param ann_file: annotation file to be used.
        :param b_update: update scores if registered score is NaN or -1, and newly computed score >0.
        :return:
        """
        if ann_file is None:
            ann_file = Config.FILE_ANN

        anns = pd.read_csv(ann_file, header=0, index_col=0)
        anns.fillna('Undefined', inplace=True)

        # Load annotations
        for idx, row in self.user_db.iterrows():
            user_anns = anns[anns.user == row.prolific_id]
            scores = ProlificScorer.score_user(user_anns, b_strict=True)
            if row.overlap_score != scores[0]:
                print(f"Score mismatch for user {row.prolific_id}: db={row.overlap_score}, fresh computation={scores[0]}")
            if b_update:
                if (math.isnan(row.overlap_score) or row.overlap_score == -1) and scores[0] > 0:
                    self.user_db.at[idx, 'overlap_score'] = scores[0]
                    print(f"Overlap score updated for user {row.prolific_id}: {row.overlap_score} --> {scores[0]}")

        if b_update:
            with open(Config.FILE_USER_DB, 'wb') as fout:
                fout.write(self.fernet.encrypt(self.user_db.to_csv().encode(encoding='utf-8')))


if __name__ == '__main__':
    oam = OfflineAccountMgmt()
    oam.print_users()
