var viewTopBtnsAdmin = `
        <input id="prev_image" type="button" value="Prev" onclick="prevImage();" />
        <input id="next_image" type="button" value="Next" onclick="nextImage();" />
        <input id="next_image_cnn" type="button" value="Next (CNN)" onclick="nextImageCNN();" />
        <input id="next_image_border" type="button" value="Next (Border)" onclick="nextImageBorder();" />
        <input id="next_image_good" type="button" value="Next (Good)" onclick="nextImageGood();" />
        <input id="save_next_border" type="button" value="Save & Border" onclick="saveAndNextBorder();" />
        <input id="save_next_good" type="button" value="Save & Good" onclick="saveAndNextGood();" />
`

var viewTopBtnsAnn = `
        <input id="next_image_good" type="button" value="Skip" onclick="nextImageGood();" />
        <input id="save_next_good" type="button" value="Save" />
`

var viewLogIn = `
<div id="div_login">
    <h1>You need to log in to access this page.</h1>
    <form id="login" action="" onsubmit="">
        <label for="email" class="label_login">Email:</label>
        <input type="email" id="email" name="email" class="input_login"/><br>
        <label for="password" class="label_login">Password:</label>
        <input type="password" id="password" name="password" class="input_login"/>
    </form>
    <div class="button_login">
        <input id="process_login" type="button" value="Login" onclick="processLogin();" />
    </div>
    <div>
        No account yet? Create one <a href="./create_account.html">here</a>.
    </div>
    <div>
        <span id="login_result"></span>
    </div>
</div>
`;

var viewAnnotation = `
<div id="div_img"></div>
<div id="div_annotation_1">
    <p><img class="icon_info" src="./images/icon_info.png" height="20px" onclick="toggleInfoBox('div_info_keepreject');"> Keep/reject image?:</p>
    <div id="div_info_keepreject" class="div_info">
        <p>Regardless of the emotional content, all photographs should adhere to the following criteria:<p>
        <ol>
            <li>Each photograph must display a realistic situation, e.g., no drawings, no watermark, no fantasy content (i.e., digitally manipulated photos), no horror, etc.</li>
            <li>The formal quality of the photograph should be sufficient, i.e., no fuzzy/blurry photographs.</li>
            <li>Each picture must display at least 2 people that are clearly visible. Alternatively, if only one person is shown, but this person is clearly a part of a larger context, the image can also be suitable. (See further down for an example.)</li>
            <li>The main feature of the photograph must not consist of a textual element. For instance, if a cardboard displaying ‘stop racism’ is a central feature of the picture, the picture is not suitable.</li>
        </ol>
        <!--
        <p>If an image does not adhere to each of these criteria, or you are not certain, please rate it as not suitable by choosing the "Reject" option (selected by default). If it is not quite clear whether the image adheres to the criteria, please mark the photo as "Uncertain". If does adhere, mark it as "Keep".</p>
        -->
        <p>If an image does not adhere to each of these criteria, or you are not certain, please rate it as not suitable by choosing the "Reject" option (selected by default). Else, mark it as "Keep".</p>
    </div>
    <input type="radio" id="keep_keep" name="ann_keep" value="Keep" onclick="checkBoxes(this);">
    <label for="keep_keep">Keep</label><br>
    <!--
    <input type="radio" id="keep_unc" name="ann_keep" value="Uncertain" onclick="checkBoxes(this);">
    <label for="keep_unc">Uncertain</label><br>
    -->
    <input type="radio" id="keep_rej" name="ann_keep" value="Reject" onclick="checkBoxes(this);">
    <label for="keep_rej">Reject</label><br>

    <span class="span_hor"></span>

    <p><img class="icon_info" src="./images/icon_info.png" height="20px" onclick="toggleInfoBox('div_info_tags');"> Tags:</p>
    <div id="div_info_tags" class="div_info">
        <p>Images can further be described by a number of tags:<p>
        <ul>
            <li><b>Bad quality photo</b>: when a picture is too blocky/blurry.</li>
            <li><b>Copyright</b>: a copyright, contrary to a watermark, is not repeated but appears only once. Typically, this leads to the picture being rejected, unless possibly the copyright is only small in size and could be cropped out without losing the essence of the picture.</li>
            <li><b>Watermark</b>: a watermark is a specific pattern, typically containing the name of the copyright holder, that is repeated over an entire image.</li>
            <li><b>No interaction</b>: the people in the picture don't have a direct interaction.</li>
            <li><b>No people</b>: the picture does not depict any people.</li>
            <li><b>Text</b>: the image contains a lot of text, either typeset on top of it, or present on, e.g., banners held by subjects depicted in the picture. If the text is typeset, this is disqualifying (i.e., the picture is rejected). If the text is present in the picture itself, it is disqualifying if it is too prominent. Use your own discretion to determine what is "too prominent" and what is not. A good rule of thumb is: if your attention is immediately drawn to the textual elements when viewing the picture, then it is too prominent and the picture is disqualified.</li>
            <li><b>Not Applicable</b>: typically used for images that are actually a collage of more than one photo, or that are rejected but don't fit any of the other tags.</li>
            <!--
            <li><b>Card-like</b>: admittedly, a vague descriptor that indicates the image contains features reminiscent of either a postcard, such as the prominent presence of some inspirational quote, or an advert, such as promotional text. Alternatively, photos of real (post)cards also fall under this tag.</li>
            <li><b>Fantasy/Unrealistic</b>: a photo that clearly has been digitally altered and displays a fantastical image. Note that people being disguised DOES NOT COUNT towards this tag!</li>
            <li><b>No face(s)</b>: self-explanatory. Note that this is not necessarily grounds for rejecting the photo.</li>
            <li><b>No photo</b>: the image isn’t a photo proper, but rather typically a (digital) drawing or painting. Note that screenshots from series or films are considered to be "photos". Photos of drawings, if the drawing is clearly the subject of the photo, also fall under this tag.</li>
            <li><b>Portrait</b>: a typical portrait photo, i.e., were people are posing, either of a single or multiple individuals (e.g., a couple or a sports team). These pictures contain no meaningful social interaction, and are hence disqualified.</li>
            <li><b>Single person</b>: the photo contains only a single person. If a photo displays only a single person that yet is clearly a part of a larger context containing other people, and as such is part of a larger "social context", it can still be a potential "Keep" or certainly "Uncertain".</li>
            <li><b>Small people</b>: a photo that shows typically a crowd of people from a distance, such that all people on the image are depicted very small and show no or barely discernible features. Note that you should use this tag ONLY when ALL people in the picture are small; if there are small people in the background, but the foreground shows one or more people that are clearly discernible, that please don’t use this tag.</li>
            -->
        </ul>
    </div>
    <input type="checkbox" id="rej_bad_photo" name="ann_tags" value="BadQualityPhoto">
    <label for="rej_bad_photo">Bad quality photo</label><br>
    <!--
    <input type="checkbox" id="rej_card_like" name="ann_tags" value="CardLike">
    <label for="rej_card_like">Card-like</label><br>
    -->
    <input type="checkbox" id="rej_copyright" name="ann_tags" value="Copyright">
    <label for="rej_copyright">Copyright</label><br>
    <input type="checkbox" id="rej_watermark" name="ann_tags" value="Watermark">
    <label for="rej_watermark">Watermark</label><br>
    <!--
    <input type="checkbox" id="rej_fantasy" name="ann_tags" value="Fantasy/Unrealistic">
    <label for="rej_fantasy">Fantasy/Unrealistic</label><br>
    <input type="checkbox" id="rej_no_face" name="ann_tags" value="NoFace">
    <label for="rej_no_face">No face(s)</label><br>
    -->
    <input type="checkbox" id="rej_no_interaction" name="ann_tags" value="NoInteraction">
    <label for="rej_no_interaction">No Interaction</label><br>
    <input type="checkbox" id="rej_no_people" name="ann_tags" value="NoPeople">
    <label for="rej_no_people">No people</label><br>
    <!--
    <input type="checkbox" id="rej_no_photo" name="ann_tags" value="NoPhoto">
    <label for="rej_no_photo">No photo</label><br>
    <input type="checkbox" id="rej_portrait" name="ann_tags" value="Portrait">
    <label for="rej_portrait">Portrait</label><br>
    <input type="checkbox" id="rej_single_person" name="ann_tags" value="SinglePerson">
    <label for="rej_single_person">Single person</label><br>
    <input type="checkbox" id="rej_small_people" name="ann_tags" value="SmallPeople">
    <label for="rej_small_people">Small people</label><br>
    -->
    <input type="checkbox" id="rej_text" name="ann_tags" value="Text">
    <label for="rej_text">Text</label><br>
    <input type="checkbox" id="rej_na" name="ann_tags" value="NA">
    <label for="rej_na">Not Applicable</label><br>

    <span class="span_hor"></span>

    <p><img class="icon_info" src="./images/icon_info.png" height="20px" onclick="toggleInfoBox('div_info_age');"> Age group:</p>
    <div id="div_info_age" class="div_info">
        <p>If a photograph is not rated as suitable (i.e., "Reject"), no further assessment is required; click "Save" to proceed to the next paragraph. Else, for "Keep" or "Uncertain" photos, you are also expected to annotate the age group of the main participants in the picture.<p>
        <p>These labels are of course not clear cut; feel free to use your own discretion as to which label applies best.</p>
    </div>
    <input type="checkbox" id="age_children" name="ann_age" value="Children" onclick="checkBoxes(this);">
    <label for="age_children">Children</label><br>
    <input type="checkbox" id="age_youth" name="ann_age" value="Youth" onclick="checkBoxes(this);">
    <label for="age_youth">Youth</label><br>
    <input type="checkbox" id="age_ya" name="ann_age" value="Young Adults" onclick="checkBoxes(this);">
    <label for="age_ya">Young Adults</label><br>
    <input type="checkbox" id="age_adults" name="ann_age" value="Adults" onclick="checkBoxes(this);">
    <label for="age_adults">Adults</label><br>
    <input type="checkbox" id="age_seniors" name="ann_age" value="Seniors" onclick="checkBoxes(this);">
    <label for="age_seniors">Seniors</label><br>

    <hr>

    <p><img class="icon_info" src="./images/icon_info.png" height="20px" onclick="toggleInfoBox('div_info_decide');"> Deciding factor(s) for emotion:</p>
    <div id="div_info_decide" class="div_info">
        <p>We ask which aspects of the photo influenced you most when assessing the emotion, i.e., facial expressions, bodily expressions, the type of interaction (‘Staging’) among the persons (e.g., fighting, dancing, talking), type of context (e.g., wedding, funeral, protest, etc.), objects in the photograph (e.g., gun, chocolate) or a possible conflict between context and person(s) (i.e., somebody exuberantly laughing at a funeral). If none of these apply, and/or the emotion is rather neutral, the "Neutral" tag can be used, although just as for the emotion case, we expect these occasions to be rare.</p>
    </div>
    <input type="checkbox" id="dec_neutral" name="ann_decisive_factor" value="Neutral" onclick="checkBoxes(this);">
    <label for="dec_neutral">Neutral</label><br>
    <input type="checkbox" id="dec_body_lang" name="ann_decisive_factor" value="BodyLanguage" onclick="checkBoxes(this);">
    <label for="dec_body_lang">Body language</label><br>
    <input type="checkbox" id="dec_conflixt_ctxt_person" name="ann_decisive_factor" value="ConflictCtxtPerson" onclick="checkBoxes(this);">
    <label for="dec_conflixt_ctxt_person">Conflict context vs. person</label><br>
    <input type="checkbox" id="dec_context" name="ann_decisive_factor" value="Context" onclick="checkBoxes(this);">
    <label for="dec_context">Context</label><br>
    <input type="checkbox" id="dec_face_ex" name="ann_decisive_factor" value="FacialExpression" onclick="checkBoxes(this);">
    <label for="dec_face_ex">Facial expression</label><br>
    <input type="checkbox" id="dec_staging" name="ann_decisive_factor" value="Staging" onclick="checkBoxes(this);">
    <label for="dec_staging">Staging</label><br>

    <hr class="class_fmri_candidate">

    <input type="checkbox" id="fmri_candidate" class="class_fmri_candidate" name="ann_fmri" value="fMRICandidate">
    <label class="class_fmri_candidate" for="fmri_candidate">fMRI Candidate</label>

</div>
<div id="div_annotation_2">
    <p><img class="icon_info" src="./images/icon_info.png" height="20px" onclick="toggleInfoBox('div_info_negpos');"> Negative/Positive:</p>
    <div id="div_info_negpos" class="div_info">
        <p>We ask you to indicate the emotional characteristic of the ENTIRE SCENE displayed in the photograph, independent of your own political/religious/sexual orientation. So a black lives matter protest is typically negative (= the participants are not happy) independent of whether you support BLM.</p>
        <p>Specifically, we ask you to rate the <b>valence</b> ("Negative/Positive") of the overall emotional gist of the photograph on a 7-point Likert scale from negative (-3) over neutral (0) to positive (+3), and also the <b>intensity</b>, ranging from not intense at all (0) to very intense (6) by using the appropriate sliders.</p>
    </div>
    <input type="range" min="-3" max="3" value="0" class="slider" id="slide_valence" list="valence_marks" onclick="toggleSliderThumb('slide_valence');">
    <datalist id=valence_marks>
    <option>-3</option>
    <option>-2</option>
    <option>-1</option>
    <option>0</option>
    <option>1</option>
    <option>2</option>
    <option>3</option>
    </datalist>
    <p><img class="icon_info" src="./images/icon_info.png" height="20px" onclick="toggleInfoBox('div_info_intensity');"> Intensity:</p>
    <div id="div_info_intensity" class="div_info">
        <p>We ask you to indicate the emotional characteristic of the ENTIRE SCENE displayed in the photograph, independent of your own political/religious/sexual orientation. So a black lives matter protest is typically negative (= the participants are not happy) independent of whether you support BLM.</p>
        <p>Specifically, we ask you to rate the <b>valence</b> ("Negative/Positive") of the overall emotional gist of the photograph on a 7-point Likert scale from negative (-3) over neutral (0) to positive (+3), and also the <b>intensity</b>, ranging from not intense at all (0) to very intense (6) by using the appropriate sliders.</p>
    </div>
    <input type="range" min="0" max="6" value="0" class="slider" id="slide_arousal" list="arousal_marks" onclick="toggleSliderThumb('slide_arousal');">
    <datalist id=arousal_marks>
    <option>0</option>
    <option>1</option>
    <option>2</option>
    <option>3</option>
    <option>4</option>
    <option>5</option>
    <option>6</option>
    </datalist>
    <p><img class="icon_info" src="./images/icon_info.png" height="20px" onclick="toggleInfoBox('div_info_emotion');"> Main emotion:</p>
    <div id="div_info_emotion" class="div_info">
        <p>We also ask to indicate an <b>emotional label</b> by means of a mouse click on an emotion wheel called "Plutchik's Wheel of Emotions". If you can’t find the perfect emotional label then you choose the ‘next best thing’, i.e., the one that reflects it most. In case no particular emotion fits, i.e., the participants all display a neutral expression, you can opt to select no emotion, although such cases are expected to be rare.</p>
        <p>For a more detailed description of each emotion depicted in this wheel, see, e.g., <a href="https://www.6seconds.org/2020/08/11/plutchik-wheel-emotions/" target="_blank">this page</a>.</p>
    </div>
    <div id="div_svg_lt_box">
        <p><b>Similar to:</b> <span id="span_similar_to"></span></p>
        <p><b>Typical sensations:</b> <span id="span_typical_sensations"></span></p>
        <p><b>Telling you:</b> <span id="span_telling_you"></span></p>
        <p><b>Help you:</b> <span id="span_help_you"></span></p>
    </div>
    <div id="div_svg">
        <svg
       version="1.1"
       viewBox="0 0 715 725"
       preserveAspectRatio="xMinYMin meet"
       id="svg315"
       sodipodi:docname="test.svg"
       inkscape:version="1.1.1 (1:1.1+202109281949+c3084ef5ed)"
       xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
       xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
       xmlns="http://www.w3.org/2000/svg"
       xmlns:svg="http://www.w3.org/2000/svg">
      <defs
         id="defs319" />
      <sodipodi:namedview
         id="namedview317"
         pagecolor="#ffffff"
         bordercolor="#666666"
         borderopacity="1.0"
         inkscape:pageshadow="2"
         inkscape:pageopacity="0.0"
         inkscape:pagecheckerboard="0"
         showgrid="false"
         inkscape:zoom="1.1048276"
         inkscape:cx="173.33021"
         inkscape:cy="362.5"
         inkscape:window-width="1920"
         inkscape:window-height="1016"
         inkscape:window-x="0"
         inkscape:window-y="27"
         inkscape:window-maximized="1"
         inkscape:current-layer="svg315" />
      <g
         id="circles"
         fill="none"
         stroke="#000"
         stroke-width="2">
        <circle
           id="outer-ring"
           stroke-dasharray="6"
           cx="357.5"
           cy="362.5"
           r="250" />
        <circle
           id="inner-ring"
           stroke-dasharray="4"
           cx="357.5"
           cy="362.5"
           r="188" />
      </g>
      <path
         fill="#ffc5c5"
         d="M 110.303,325.048 C 84.749,332.483 58.82,344.254 33,362.488 c 24.984,18.157 51.076,29.983 77.296,37.476 -3.834,-25.173 -3.732,-50.409 0.007,-74.916 z"
         id="path191"
         style="stroke:#000000;stroke-width:2" />
      <path
         fill="#c5e2c5"
         d="m 604.723,399.955 c 26.213,-7.488 52.3,-19.314 77.277,-37.47 -25.812,-18.227 -51.728,-29.997 -77.269,-37.432 3.832,25.168 3.728,50.4 -0.008,74.902 z"
         id="path193"
         style="stroke:#000000;stroke-width:2" />
      <path
         fill="#feffdd"
         d="M 394.948,115.287 C 387.517,89.74 375.741,63.818 357.512,38 c -18.155,24.979 -29.98,51.066 -37.471,77.284 25.165,-3.835 50.4,-3.732 74.907,0.003 z"
         id="path204"
         style="stroke:#000000;stroke-width:2" />
      <path
         fill="#c5c5ff"
         d="m 320.038,609.707 c 7.489,26.219 19.315,52.309 37.474,77.293 18.229,-25.813 30,-51.732 37.436,-77.277 -25.168,3.83 -50.404,3.726 -74.91,-0.016 z"
         id="path206"
         style="stroke:#000000;stroke-width:2" />
      <path
         fill="#ffe2ff"
         d="m 156.228,510.84 c -12.811,23.32 -22.829,49.961 -28.192,81.107 30.508,-4.828 57.32,-14.916 81.155,-28.16 -19.965,-14.679 -37.879,-32.447 -52.963,-52.947 z"
         id="path214"
         style="stroke:#000000;stroke-width:2" />
      <path
         fill="#d5eeff"
         d="m 558.778,510.85 c -14.687,19.963 -32.447,37.871 -52.946,52.951 23.829,13.238 50.635,23.32 81.135,28.146 -5.365,-31.14 -15.381,-57.777 -28.189,-81.097 z"
         id="path219"
         style="stroke:#000000;stroke-width:2" />
      <path
         fill="#c5ffc5"
         d="m 558.801,214.167 c 13.237,-23.829 23.319,-50.634 28.146,-81.132 -31.144,5.362 -57.778,15.38 -81.103,28.188 19.967,14.683 37.877,32.446 52.957,52.944 z"
         id="path224"
         style="stroke:#000000;stroke-width:2" />
      <path
         fill="#ffe1c5"
         d="m 209.157,161.228 c -23.319,-12.811 -49.961,-22.827 -81.104,-28.189 4.828,30.509 14.916,57.321 28.161,81.157 14.682,-19.967 32.445,-37.883 52.943,-52.968 z"
         id="path229"
         style="stroke:#000000;stroke-width:2" />
      <g
         id="g248"
         style="font-weight:bold;font-size:16px;font-family:'DejaVu Sans'">
        <text
           transform="translate(200,117.1)"
           id="text232">optimism</text>
        <text
           transform="translate(440,117.1)"
           id="text234">love</text>
        <text
           transform="translate(10,262.6)"
           id="text236">aggressiveness</text>
        <text
           transform="translate(600,262.6)"
           id="text238">submission</text>
        <text
           transform="translate(30,470.6)"
           id="text240">contempt</text>
        <text
           transform="translate(600,470.6)"
           id="text242">awe</text>
        <text
           transform="translate(220,619.4)"
           id="text244">remorse</text>
        <text
           transform="translate(432,619.4)"
           id="text246">disapproval</text>
      </g>
      <g
         id="Annoyance" class="g_emotion">
        <path
           fill="#ff8c8c"
           d="m 175.847,314.122 c -21.354,1.277 -43.308,4.456 -65.545,10.926 -3.737,24.507 -3.842,49.743 -0.008,74.913 21.939,6.268 43.971,9.496 65.518,10.844 -8.674,-32.467 -8.234,-65.725 0.035,-96.683 z"
           style="stroke:#000000;stroke-width:2" />
        <text
           id="text250"
           x="90"
           y="354"
           style="font-weight:bold;font-size:16px;font-family:'DejaVu Sans'">annoyance</text>
      </g>
      <g
         id="Anger" class="g_emotion">
        <path
           fill="#ff0000"
           d="m 242.571,410.092 0.011,-0.004 c -6.076,-14.66 -9.438,-30.729 -9.438,-47.588 0,-16.866 3.363,-32.943 9.445,-47.609 l -0.021,-0.008 c -21.271,-1.306 -43.646,-2.141 -66.725,-0.761 -8.269,30.958 -8.71,64.216 -0.036,96.683 23.044,1.437 45.531,0.726 66.764,-0.713 z"
           style="stroke:#000000;stroke-width:2" />
        <text
           id="text252"
           x="176"
           y="377.20001"
           style="font-weight:bold;font-size:16px;font-family:'DejaVu Sans'"><tspan
             style="fill:#ffffff"
             id="tspan65584">anger</tspan></text>
      </g>
      <g
         id="Rage" class="g_emotion">
        <path
           fill="#d40000"
           d="m 242.589,314.899 c -12.583,30.409 -12.583,64.797 0,95.205 L 357.512,362.5 Z"
           style="stroke:#000000;stroke-width:2" />
        <text
           id="text254"
           x="251"
           y="367.20001"
           style="font-weight:bold;font-size:16px;font-family:'DejaVu Sans'"><tspan
             style="fill:#f2f2f2"
             id="tspan23482">rage</tspan></text>
      </g>
      <g
         id="Vigilance" class="g_emotion">
        <path
           fill="#ff7d00"
           d="M 242.589,314.899 357.512,362.5 309.908,247.577 c -30.399,12.606 -54.714,36.92 -67.319,67.322 z"
           style="stroke:#000000;stroke-width:2" />
        <text
           transform="rotate(45)"
           id="text258"
           x="395.9798"
           y="7.0710673"
           style="font-weight:bold;font-size:16px;font-family:'DejaVu Sans'">vigilance</text>
      </g>
      <g
         id="Anticipation" class="g_emotion">
        <path
           fill="#ffa854"
           d="m 242.58,314.886 0.011,0.004 c 12.606,-30.396 36.919,-54.708 67.317,-67.313 l -0.006,-0.018 c -14.125,-15.968 -29.368,-32.383 -46.669,-47.729 -29.093,16.813 -52.301,40.631 -68.354,68.356 15.272,17.312 31.673,32.707 47.701,46.7 z"
           style="stroke:#000000;stroke-width:2" />
        <text
           id="text260"
           x="133.63078"
           y="310.30438"
           style="font-weight:bold;font-size:16px;font-family:'DejaVu Sans'"
           transform="rotate(-13)">anticipation</text>
      </g>
      <g
         id="Interest" class="g_emotion">
        <path
           fill="#ffc48c"
           d="m 263.233,199.834 c -16.001,-14.192 -33.779,-27.459 -54.076,-38.606 -20.498,15.085 -38.263,33 -52.943,52.968 11.085,19.946 24.38,37.813 38.667,54 16.053,-27.731 39.261,-51.547 68.352,-68.362 z"
           style="stroke:#000000;stroke-width:2" />
        <text
           id="text262"
           x="-38.834263"
           y="297.33197"
           style="font-weight:bold;font-size:16px;font-family:'DejaVu Sans'"
           transform="rotate(-45)">interest</text>
      </g>
      <g
         id="Ecstasy" class="g_emotion">
        <path
           fill="#ffe854"
           d="M 309.908,247.577 357.512,362.5 405.116,247.577 c -30.414,-12.582 -64.797,-12.582 -95.208,0 z"
           style="stroke:#000000;stroke-width:2" />
        <text
           transform="rotate(90)"
           id="text266"
           style="font-weight:bold;font-size:16px;font-family:'DejaVu Sans'"
           x="260"
           y="-353.60938">ecstasy</text>
      </g>
      <g
         id="Joy" class="g_emotion">
        <path
           fill="#ffff54"
           d="m 309.907,247.573 0.002,0.006 c 14.664,-6.081 30.739,-9.444 47.604,-9.444 16.854,0 32.922,3.359 47.58,9.437 l 0.003,-0.005 c 0.008,-0.011 0.018,-0.021 0.021,-0.026 1.304,-21.269 2.138,-43.636 0.757,-66.704 -30.959,-8.268 -64.214,-8.705 -96.679,-0.026 -1.438,23.036 -0.727,45.525 0.712,66.762 z"
           style="stroke:#000000;stroke-width:2" />
        <text
           id="text268"
           style="font-weight:bold;font-size:16px;font-family:'DejaVu Sans'"
           x="344.45703"
           y="214">joy</text>
      </g>
      <g
         id="Serenity" class="g_emotion">
        <path
           fill="#ffffb1"
           d="m 405.872,180.831 c -1.273,-21.354 -4.456,-43.308 -10.929,-65.544 -24.504,-3.734 -49.738,-3.837 -74.904,-0.003 -6.268,21.939 -9.5,43.971 -10.845,65.519 32.466,-8.677 65.722,-8.236 96.678,0.028 z"
           style="stroke:#000000;stroke-width:2" />
        <text
           id="text270"
           style="font-weight:bold;font-size:16px;font-family:'DejaVu Sans'"
           x="320.19531"
           y="149">serenity</text>
      </g>
      <g
         id="Admiration" class="g_emotion">
        <path
           fill="#00b400"
           d="m 357.512,362.5 114.925,-47.604 c -12.604,-30.397 -36.92,-54.715 -67.319,-67.318 z"
           style="stroke:#000000;stroke-width:2" />
        <text
           transform="rotate(-45)"
           id="text274"
           style="font-weight:bold;font-size:16px;font-family:'DejaVu Sans'"
           x="17.677668"
           y="512.6524">admiration</text>
      </g>
      <g
         id="Trust" class="g_emotion">
        <path
           fill="#54ff54"
           d="m 405.116,247.535 c 0,0.012 0,0.021 -0.002,0.035 l -0.003,0.006 c 30.396,12.604 54.706,36.908 67.313,67.302 16.034,-13.999 32.44,-29.401 47.719,-46.714 -16.823,-29.086 -40.647,-52.286 -68.385,-68.329 -17.287,15.338 -32.522,31.741 -46.642,47.7 z"
           style="stroke:#000000;stroke-width:2" />
        <text
           id="text276"
           style="font-weight:bold;font-size:16px;font-family:'DejaVu Sans'"
           x="444"
           y="254.10001">trust</text>
      </g>
      <g
         id="Acceptance" class="g_emotion">
        <path
           fill="#8cff8c"
           d="m 520.146,268.164 c 14.28,-16.188 27.571,-34.053 38.653,-53.998 -15.08,-20.498 -32.99,-38.264 -52.955,-52.942 -20.301,11.147 -38.08,24.417 -54.084,38.611 27.735,16.043 51.561,39.243 68.386,68.329 z"
           style="stroke:#000000;stroke-width:2" />
        <text
           id="text278"
           style="font-weight:bold;font-size:16px;font-family:'DejaVu Sans'"
           x="486.53854"
           y="103.01565"
           transform="rotate(11.724569)">acceptance</text>
      </g>
      <g
         id="Terror" class="g_emotion">
        <path
           fill="#008000"
           d="m 472.438,410.104 c 12.582,-30.408 12.582,-64.796 0,-95.204 l -114.926,47.6 z"
           style="stroke:#000000;stroke-width:2" />
        <text
           id="text282"
           style="font-weight:bold;font-size:16px;font-family:'DejaVu Sans'"
           x="410"
           y="367.20001">terror</text>
      </g>
      <g
         id="Fear" class="g_emotion">
        <path
           fill="#009600"
           d="m 472.43,314.882 c 6.084,14.668 9.449,30.749 9.449,47.618 0,16.859 -3.359,32.932 -9.438,47.594 21.231,1.439 43.719,2.148 66.756,0.709 8.258,-30.959 8.687,-64.215 -0.002,-96.678 -23.091,-1.385 -45.482,-0.549 -66.765,0.757 z"
           style="stroke:#000000;stroke-width:2" />
        <text
           id="text284"
           style="font-weight:bold;font-size:16px;font-family:'DejaVu Sans'"
           x="496"
           y="367.20001">fear</text>
      </g>
      <g
         id="Apprehension" class="g_emotion">
        <path
           fill="#8cc68c"
           d="m 539.196,410.803 c 21.552,-1.346 43.584,-4.578 65.524,-10.848 3.736,-24.502 3.843,-49.734 0.013,-74.897 -22.233,-6.471 -44.188,-9.651 -65.539,-10.933 8.687,32.463 8.258,65.719 0.002,96.678 z"
           style="stroke:#000000;stroke-width:2" />
        <text
           id="text286"
           style="font-weight:bold;font-size:16px;font-family:'DejaVu Sans'"
           x="550"
           y="367.20001">apprehension</text>
      </g>
      <g
         id="Amazement" class="g_emotion">
        <path
           fill="#0089e0"
           d="M 472.438,410.104 357.512,362.5 405.116,477.424 c 30.402,-12.603 54.713,-36.92 67.322,-67.32 z"
           style="stroke:#000000;stroke-width:2" />
        <text
           transform="rotate(45)"
           id="text290"
           x="526.08783"
           y="8.4852829"
           style="font-weight:bold;font-size:16px;font-family:'DejaVu Sans'"><tspan
             style="fill:#f2f2f2"
             id="tspan30876">amazement</tspan></text>
      </g>
      <g
         id="Surprise" class="g_emotion">
        <path
           fill="#59bdff"
           d="m 472.438,410.096 c -12.604,30.402 -36.921,54.723 -67.32,67.328 14,16.035 29.399,32.441 46.716,47.719 29.088,-16.818 52.288,-40.645 68.336,-68.377 -15.349,-17.301 -31.763,-32.545 -47.732,-46.67 z"
           style="stroke:#000000;stroke-width:2" />
        <text
           id="text292"
           x="-39.841366"
           y="670.48773"
           style="font-weight:bold;font-size:16px;font-family:'DejaVu Sans'"
           transform="rotate(-45)">surprise</text>
      </g>
      <g
         id="Distraction" class="g_emotion">
        <path
           fill="#a5dbff"
           d="m 558.778,510.85 c -11.147,-20.301 -24.416,-38.08 -38.608,-54.084 -16.048,27.732 -39.248,51.559 -68.336,68.377 16.188,14.283 34.051,27.576 53.997,38.658 20.498,-15.08 38.263,-32.99 52.947,-52.951 z"
           style="stroke:#000000;stroke-width:2" />
        <text
           id="text294"
           x="-52.028603"
           y="731.50464"
           style="font-weight:bold;font-size:16px;font-family:'DejaVu Sans'"
           transform="rotate(-45)">distraction</text>
      </g>
      <g
         id="Pensiveness" class="g_emotion">
        <path
           fill="#8c8cff"
           d="m 309.195,544.182 c 1.346,21.553 4.576,43.582 10.844,65.525 24.506,3.738 49.741,3.846 74.907,0.016 6.472,-22.232 9.647,-44.188 10.931,-65.535 -32.468,8.681 -65.722,8.256 -96.682,-0.006 z"
           style="stroke:#000000;stroke-width:2" />
        <text
           id="text298"
           x="301.57031"
           y="583"
           style="font-weight:bold;font-size:16px;font-family:'DejaVu Sans'">pensiveness</text>
      </g>
      <g
         id="Sadness" class="g_emotion">
        <path
           fill="#5151ff"
           d="m 405.116,477.428 -0.002,-0.004 c -14.664,6.08 -30.739,9.443 -47.604,9.443 -16.863,0 -32.938,-3.363 -47.604,-9.443 l -0.002,0.004 c -1.438,21.23 -2.149,43.721 -0.712,66.754 30.96,8.262 64.216,8.689 96.679,0.006 1.387,-23.09 0.551,-45.477 -0.755,-66.76 z"
           style="stroke:#000000;stroke-width:2" />
        <text
           id="text300"
           x="320.93359"
           y="520"
           style="font-weight:bold;font-size:16px;font-family:'DejaVu Sans'"><tspan
             style="fill:#f2f2f2"
             id="tspan37192">sadness</tspan></text>
      </g>
      <g
         id="Grief" class="g_emotion">
        <path
           fill="#0000c8"
           d="M 405.116,477.424 357.512,362.5 309.908,477.424 c 30.411,12.582 64.794,12.582 95.208,0 z"
           style="stroke:#000000;stroke-width:2" />
        <text
           fill="#ffffff"
           id="text302"
           x="335.75"
           y="459"
           style="font-weight:bold;font-size:16px;font-family:'DejaVu Sans'">grief</text>
      </g>
      <g
         id="Boredom" class="g_emotion">
        <path
           fill="#ffc6ff"
           d="m 194.837,456.762 c -14.194,16.002 -27.461,33.779 -38.609,54.078 15.083,20.5 32.998,38.268 52.963,52.947 19.948,-11.084 37.812,-24.379 53.999,-38.666 -27.727,-16.05 -51.544,-39.261 -68.353,-68.359 z"
           style="stroke:#000000;stroke-width:2" />
        <text
           id="text306"
           style="font-weight:bold;font-size:16px;font-family:'DejaVu Sans'"
           x="467.95432"
           y="226.75453"
           transform="rotate(45)">boredom</text>
      </g>
      <g
         id="Disgust" class="g_emotion">
        <path
           fill="#ff54ff"
           d="m 309.886,477.42 0.003,-0.006 c -30.396,-12.609 -54.704,-36.926 -67.307,-67.326 l -0.02,0.008 c -15.967,14.123 -32.381,29.367 -47.726,46.666 16.811,29.098 40.626,52.309 68.354,68.359 17.304,-15.271 32.702,-31.672 46.696,-47.701 z"
           style="stroke:#000000;stroke-width:2" />
        <text
           id="text308"
           style="font-weight:bold;font-size:16px;font-family:'DejaVu Sans'"
           x="476.02985"
           y="163.89915"
           transform="rotate(45)">disgust</text>
      </g>
      <g
         id="Loathing" class="g_emotion">
        <path
           fill="#de00de"
           d="M 309.908,477.424 357.512,362.5 242.589,410.104 c 12.605,30.4 36.92,54.717 67.319,67.32 z"
           style="stroke:#000000;stroke-width:2" />
        <text
           id="text310"
           style="font-weight:bold;font-size:16px;font-family:'DejaVu Sans'"
           x="-112.57636"
           y="515.63843"
           transform="rotate(-45)"><tspan
             style="fill:#f2f2f2"
             id="tspan6900">loathing</tspan></text>
      </g>
    </svg>
    </div>
    <p><img class="icon_info" src="./images/icon_info.png" height="20px" onclick="toggleInfoBox('div_info_ambiguity');"> Ambiguity:</p>
    <div id="div_info_ambiguity" class="div_info">
        <p>Please also rate how straightforward the emotional content that is exhibited by the entire photograph is using the scale indicated with <b>"Ambiguity"</b>.</p>
        <p>For instance, if there are approximately as much emotionally positive as emotionally negative cues in the photograph, the emotional content would not be clear (6), while only positive cues or only negative cues would result in a very high clarity (0).</p>
    </div>
    <input type="range" min="0" max="6" value="0" class="slider" id="slide_ambiguity" list="ambiguity_marks" onclick="toggleSliderThumb('slide_ambiguity');">
    <datalist id=ambiguity_marks>
    <option>0</option>
    <option>1</option>
    <option>2</option>
    <option>3</option>
    <option>4</option>
    <option>5</option>
    <option>6</option>
    </datalist>
</div>
`

var viewWelcomeMsg = `
<div id='div_welcome_part1'>
    <h1>Welcome</h1>
    <p class='text_red'>It is recommended to set your browser to "full-screen" mode. Typically, this mode can be toggled by using the
    'F11' key.</p>
    <p>This interface was designed for screen resolutions with a width of 1920 pixels. In case your screen has a higher/lower resolution,
    the interface should automatically resize itself so as to fully fit on your screen, but this might come at the price of reduced
    image sharpness.</p>
    <hr>
    <h3><b>Thank you for your willingness to participate in this annotation task!</b></h3>
    <p>In this experiment, you will be expected to annotate
    50 "good" images, i.e., annotated as "Keep", after which you will receive a URL that will direct you to the Prolific completion page
    for this task. Please take the time to read these annotation instructions before continuing.</p>
    <p class='text_red'>Note that if for any reason you get logged out at some point, you should be able to log back in using the same
    URL provided to you by Prolific, and pick up right where you left.</p>

    <p>We want to build a <b>database of photographs with an emotional content</b>. You will be shown randomly selected images from a
    large corpus, and we ask you to evaluate photographs regarding 2 consecutive issues.</p>
    <p><b>First</b>, regardless of the emotional content, all photographs should adhere to the following criteria:</p>
    <ol>
        <li>Each photograph must display a realistic situation, e.g., no drawings, no watermark, no fantasy content (i.e., digitally manipulated photos), no horror, etc.</li>
        <li>The formal quality of the photograph should be sufficient, i.e., no fuzzy/blurry photographs.</li>
        <li>Each picture must display at least 2 people that are clearly visible. Alternatively, if only one person is shown, but this person is clearly a part of a larger context, the image can also be suitable.</li>
        <li>The main feature of the photograph must not consist of a textual element. For instance, if a cardboard displaying ‘stop racism’ is a central feature of the picture, the picture is not suitable.</li>
    </ol>
    <p>If an image does not adhere to each of these criteria, or you are not certain, please rate it as not suitable by choosing the
    "Reject" option. Else, mark it as "Keep", <b>in which case all other dimensions, except for "tags", need to be annotated before you
    can proceed</b>! Even if you want to keep the default value of a slider, you still need to click the slider first.</p>

    <p>Images can further be described by a number of <b>tags</b>:</p><p>
    </p><ul>
        <!--
        <li><b>Not Applicable</b>: typically used for images that are actually a collage of more than one photo, or that don't fit any of the other tags.</li>
        <li><b>Bad quality photo</b>: when a picture is too blocky/blurry.</li>
        <li><b>Card-like</b>: admittedly, a vague descriptor that indicates the image contains features reminiscent of either a postcard, such as the prominent presence of some inspirational quote, or an advert, such as promotional text. Alternatively, photos of real (post)cards also fall under this tag.</li>
        <li><b>Copyright</b>: a copyright, contrary to a watermark, is not repeated but appears only once. Typically, this leads to the picture being rejected, unless possibly the copyright is only small in size and could be cropped out without losing the essence of the picture.</li>
        <li><b>Fantasy/Unrealistic</b>: a photo that clearly has been digitally altered and displays a fantastical image. Note that people being disguised DOES NOT COUNT towards this tag!</li>
        <li><b>No face(s)</b>: self-explanatory. Note that this is not necessarily grounds for rejecting the photo.</li>
        <li><b>No people</b>: self-explanatory.</li>
        <li><b>No photo</b>: the image isn’t a photo proper, but rather typically a (digital) drawing or painting. Note that screenshots from series or films are considered to be "photos". Photos of drawings, if the drawing is clearly the subject of the photo, also fall under this tag.</li>
        <li><b>Portrait</b>: a typical portrait photo, i.e., were people are posing, either of a single or multiple individuals (e.g., a couple or a sports team). These pictures contain no meaningful social interaction, and are hence disqualified.</li>
        <li><b>Single person</b>: the photo contains only a single person. If a photo displays only a single person that yet is clearly a part of a larger context containing other people, and as such is part of a larger "social context", it can still be a potential "Keep" or certainly "Uncertain".</li>
        <li><b>Small people</b>: a photo that shows typically a crowd of people from a distance, such that all people on the image are depicted very small and show no or barely discernible features. Note that you should use this tag ONLY when ALL people in the picture are small; if there are small people in the background, but the foreground shows one or more people that are clearly discernible, that please don’t use this tag.</li>
        <li><b>Text</b>: the image contains a lot of text, either typeset on top of it, or present on, e.g., banners held by subjects depicted in the picture. If the text is typeset, this is disqualifying (i.e., the picture is rejected). If the text is present in the picture itself, it is disqualifying if it is too prominent. Use your own discretion to determine what is "too prominent" and what is not. A good rule of thumb is: if your attention is immediately drawn to the textual elements when viewing the picture, then it is too prominent and the picture is disqualified.</li>
        <li><b>Watermark</b>: a watermark is a specific pattern, typically containing the name of the copyright holder, that is repeated over an entire image.</li>
        -->
        <li><b>Bad quality photo</b>: when a picture is too blocky/blurry.</li>
        <li><b>Copyright</b>: a copyright, contrary to a watermark, is not repeated but appears only once. Typically, this leads to the picture being rejected, unless possibly the copyright is only small in size and could be cropped out without losing the essence of the picture.</li>
        <li><b>Watermark</b>: a watermark is a specific pattern, typically containing the name of the copyright holder, that is repeated over an entire image.</li>
        <li><b>No interaction</b>: the people in the picture don't have a direct interaction.</li>
        <li><b>No people</b>: the picture does not depict any people.</li>
        <li><b>Text</b>: the image contains a lot of text, either typeset on top of it, or present on, e.g., banners held by subjects depicted in the picture. If the text is typeset, this is disqualifying (i.e., the picture is rejected). If the text is present in the picture itself, it is disqualifying if it is too prominent. Use your own discretion to determine what is "too prominent" and what is not. A good rule of thumb is: if your attention is immediately drawn to the textual elements when viewing the picture, then it is too prominent and the picture is disqualified.</li>
        <li><b>Not Applicable</b>: typically used for images that are actually a collage of more than one photo, or that are rejected but don't fit any of the other tags.</li>
    </ul>

    <p>If a photograph is not rated as suitable (i.e., "Reject"), no further assessment is required; click "Save" to proceed to the next
    paragraph. Else, for "Keep" or "Uncertain" photos, you are also expected to annotate the <b>age group</b> of the main participants in the
    picture. These labels are of course not clear cut; feel free to use your own discretion as to which label applies best.</p>

    <p><b>Second</b>, we want you to focus on the <b>emotional labelling</b> of the photographs. Concretely, we ask you to annotate the image
    on a number of dimensions</p>

    <p>We ask you to indicate the emotional characteristic of the ENTIRE SCENE displayed in the photograph, independent of your own
    political/religious/sexual orientation. So a black lives matter protest is typically negative (= the participants are not happy)
    independent of whether you support BLM. Specifically, we ask you to rate the <b>valence</b> ("Negative/Positive") of the overall
    emotional gist of the photograph on a 7-point Likert scale from negative (-3) over neutral (0) to positive (+3), and also the
    <b>intensity</b>, ranging from not intense at all (0) to very intense (6) by using the appropriate sliders.</p>

    <p>We also ask to indicate an <b>emotional label</b> by means of a mouse click on an emotion wheel called "Plutchik's Wheel of Emotions".
    If you can’t find the perfect emotional label then you choose the ‘next best thing’, i.e., the one that reflects it most. In case
    no particular emotion fits, i.e., the participants all display a neutral expression, you can opt to select no emotion, although
    such cases are expected to be rare. For a more detailed description of each emotion depicted in this wheel, see, e.g.,
    <a href="https://www.6seconds.org/2020/08/11/plutchik-wheel-emotions/" target="_blank">this page</a>. Additional info for each
    emotion will be displayed when hovering over its corresponding cell.</p>

    <p>Please also rate how straightforward the emotional content that is exhibited by the entire photograph is using the scale
    indicated with <b>"Ambiguity"</b>. For instance, if there are approximately as much emotionally positive as emotionally negative
    cues in the photograph, the emotional content would not be clear (6), while only positive cues or only negative cues would result
    in a very high clarity (0).</p>

    <p>Finally, the options under the <b>"Deciding factor(s) for emotion"</b> header ask which aspects of the photo influenced you
    most when assessing the emotion, i.e., facial expressions, bodily expressions, the type of interaction (‘Staging’) among the
    persons (e.g., fighting, dancing, talking), type of context (e.g., wedding, funeral, protest, etc.), objects in the photograph
    (e.g., gun, chocolate) or a possible conflict between context and person(s) (i.e., somebody exuberantly laughing at a funeral).
    If none of these apply, and/or the emotion is rather neutral, the "Neutral" tag can be used, although just as for the emotion case,
    we expect these occasions to be rare.</p>

    <p>If for some reason you would rather not annotate the current image being served to you, you can press the <b>"Skip"</b> button to
    be served a new picture and have the annotation interface be reset, without your current settings being saved.</p>
    <p>If on the other hand you are happy with your current annotation, press <b>"Save"</b> to let it be saved and move on to the next
    image. If this button is greyed out, this means you have not yet annotated all necessary dimensions. Once you have reached the
    required number of annotations, you will automatically get to see the URL that will direct you to the Prolific completion page
    for this task.</p>
    <p>At the top of this screen, you can see your annotation statistics: "Rejected/Accepted" = how many images you marked
    "Reject" and "Keep" respectively, and "Left" = number of "Keep" images left to annotate.</p>

    <p class='text_red'>You can always check these instructions again whilst annotating by clicking the
    <img class="icon_info" src="./images/icon_info.png">-icon next to each criterium. (Click once more to close the infobox again.)</p>
</div>
<div>
    <input type='button' class='btn_start' value='Ok!' onclick='setPassedTuto();' />
</div>
`
