function get_done_url() {
    const token = localStorage.getItem('jwt_token');

    if (token == null) {
        // Verify token hasn't expired yet
        redirectNoPermission();
    } else {
        var xhr = new XMLHttpRequest();

        xhr.addEventListener('readystatechange', function(event)
        {
            if (xhr.readyState == 4) {
                ret_val = JSON.parse(xhr.response); // return value
//                console.log(ret_val);

                target_span = document.getElementById('span_completion_url');
                if (ret_val.success) {
                    let a = document.createElement('a');
                    a.href = ret_val.url;
                    a.innerText = ret_val.url;

                    target_span.innerText = '';
                    target_span.appendChild(a);
                } else {
                    target_span.innerText = '...there was an error retrieving the completion URL. Please contact the study author through Prolific to request your code manually.';
                    target_span.style.color = 'red';
                }
            }
        });

        xhr.open('POST', './api-phd/login-api/get_done_url', true);
        xhr.setRequestHeader('X-Requested-With','XMLHttpRequest');

        var formData = new FormData();
        formData.append('jwt_token', token);

        xhr.send(formData);
    }
}

window.onload = function() {
    get_done_url();
}