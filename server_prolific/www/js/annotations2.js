function getAnnotationsPerImage() {
    // Only for adminss
    const token = localStorage.getItem('jwt_token');

    var xhr = new XMLHttpRequest();

    xhr.addEventListener('readystatechange', function(event)
    {
        // When response has been received
        if (xhr.readyState == 4) {
            ret_val = JSON.parse(xhr.response); // return value

            let role = sessionStorage.getItem('role');
            // Previous header
            let prev_header = document.getElementById('tbl_ann_tr_header');
            // Return value = array where each element contains (annotation index, image path, reject value, reject reason)
            document.getElementById('table_annotations').innerHTML = '';
            let tr_header = document.createElement('tr');
            tr_header.id = 'tbl_ann_tr_header';
            header_labels = []
            let td_head_0 = document.createElement('td');
            td_head_0.innerHTML = "idx";
            header_labels.push(td_head_0);
            let td_head_email = document.createElement('td');
            td_head_email.innerHTML = "user";
            header_labels.push(td_head_email);
            let td_head_1 = document.createElement('td');
            td_head_1.innerHTML = "k/u/r";
            header_labels.push(td_head_1);
            let td_head_2 = document.createElement('td');
            td_head_2.innerHTML = "tags";
            header_labels.push(td_head_2);
            let td_head_3 = document.createElement('td');
            td_head_3.innerHTML = "age";
            header_labels.push(td_head_3);
            let td_head_4 = document.createElement('td');
            td_head_4.innerHTML = "valence";
            header_labels.push(td_head_4);
            let td_head_5 = document.createElement('td');
            td_head_5.innerHTML = "arousal";
            header_labels.push(td_head_5);
            let td_head_6 = document.createElement('td');
            td_head_6.innerHTML = "emotion";
            header_labels.push(td_head_6);
            let td_head_7 = document.createElement('td');
            td_head_7.innerHTML = "dec.fact.";
            header_labels.push(td_head_7);
            let td_head_8 = document.createElement('td');
            td_head_8.innerHTML = "ambiguity";
            header_labels.push(td_head_8);
            let td_head_9 = document.createElement('td');
            td_head_9.innerHTML = "fmri cand.";
            header_labels.push(td_head_9);
            let td_head_10 = document.createElement('td');
            td_head_10.innerHTML = "verify";
            header_labels.push(td_head_10);

            for(let i=0; i<header_labels.length; i++) {
                tr_header.appendChild(header_labels[i]);
            }

            document.getElementById('table_annotations').appendChild(tr_header);

            var prev_img = '';
            var at_image = 0;
            for (let i=0; i<ret_val.length; i++) {
                row = ret_val[i];
                img = row[1];

                // Start of new image?
                if (prev_img != img) {
                    prev_img = img;
                    at_image += 1
                    let tr = document.createElement('tr');
                    if (at_image%2 == 0) {
                        tr.classList.add("tr_even");
                    } else {
                        tr.classList.add("tr_odd");
                    }
                    let td = document.createElement('td');
                    td.classList.add("td_top");
                    td.colSpan = "12";
                    let a = document.createElement('a');
                    a.href = './api-phd/image-api/images' + img;
                    a.innerHTML = img;
                    td.appendChild(a);
                    tr.appendChild(td);
                    document.getElementById('table_annotations').appendChild(tr);
                }

                // Append annotation
                let tr = document.createElement('tr');
                if (at_image%2 == 0) {
                    tr.classList.add("tr_even");
                } else {
                    tr.classList.add("tr_odd");
                }

                let td_ann_idx = document.createElement('td');
                td_ann_idx.innerHTML = i;

                let td_ann_user = document.createElement('td');
                td_ann_user.innerHTML = ret_val[i][ret_val[i].length -1];

                let td_ann_value = document.createElement('td');
                td_ann_value.innerHTML = ret_val[i][2];

                let td_ann_tags = document.createElement('td');
                const ann_tags = ret_val[i][3].split(',');
                let ann_tags_text = '';
                for (let i=0; i<ann_tags.length; i++) {
                    ann_tags_text += ann_tags[i];
                    if (i < ann_tags_text.length -1) {
                        ann_tags_text += '<br>';
                    }
                }
                td_ann_tags.innerHTML = ann_tags_text;

                let td_ann_age = document.createElement('td');
                const ann_age = ret_val[i][4].split(',');
                let ann_age_text = '';
                for (let i=0; i<ann_age.length; i++) {
                    ann_age_text += ann_age[i];
                    if (i < ann_age_text.length -1) {
                        ann_age_text += '<br>';
                    }
                }
                td_ann_age.innerHTML = ann_age_text;

                let td_ann_valence = document.createElement('td');
                td_ann_valence.innerHTML = ret_val[i][5];

                let td_ann_arousal = document.createElement('td');
                td_ann_arousal.innerHTML = ret_val[i][6];

                let td_ann_emotion = document.createElement('td');
                td_ann_emotion.innerHTML = ret_val[i][7];

                let td_ann_dec_factors = document.createElement('td');
                const dec_factors = ret_val[i][8].split(',');
                let dec_factors_text = '';
                for (let i=0; i<dec_factors.length; i++) {
                    dec_factors_text += dec_factors[i];
                    if (i < dec_factors_text.length -1) {
                        dec_factors_text += '<br>';
                    }
                }
                td_ann_dec_factors.innerHTML = dec_factors_text;

                let td_ann_obviousness = document.createElement('td');
                td_ann_obviousness.innerHTML = ret_val[i][9];

                let td_ann_fmri_candidate = document.createElement('td');
                td_ann_fmri_candidate.innerHTML = ret_val[i][10];

                let td_img_link = document.createElement('td');
                td_img_link.innerHTML = "<a href ='./view_annotation.html?"
                + "idx=" + ret_val[i][0]
                + "&img=" + ret_val[i][1]
                + "&reject=" + ret_val[i][2]
                + "&tags=" + ret_val[i][3]
                + "&age=" + ret_val[i][4]
                + "&valence=" + ret_val[i][5]
                + "&arousal=" + ret_val[i][6]
                + "&emotion=" + ret_val[i][7]
                + "&dec_factors=" + ret_val[i][8]
                + "&obviousness=" + ret_val[i][9]
                + "&fmri_candidate=" + ret_val[i][10]
                + "'>view image</a>";

                tr.appendChild(td_ann_idx);
                tr.appendChild(td_ann_user);
                tr.appendChild(td_ann_value);
                tr.appendChild(td_ann_tags);
                tr.appendChild(td_ann_age);
                tr.appendChild(td_ann_valence);
                tr.appendChild(td_ann_arousal);
                tr.appendChild(td_ann_emotion);
                tr.appendChild(td_ann_dec_factors);
                tr.appendChild(td_ann_obviousness);
                tr.appendChild(td_ann_fmri_candidate);
                tr.appendChild(td_img_link);
                document.getElementById('table_annotations').appendChild(tr);
            }
        }
    });

    xhr.open('POST', './api-phd/image-api/get_annotations_per_image', true);
    xhr.setRequestHeader('X-Requested-With','XMLHttpRequest');

    var formData = new FormData();
    formData.append('jwt_token', token);

    xhr.send(formData);
}

window.onload = function() {
    isLoggedIn();

    document.getElementById('div_script').style.display = 'contents';
    getAnnotationsPerImage();
}