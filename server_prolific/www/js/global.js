/*
Contains functions that can come in handy on several pages.
*/
function toggleInfoBox(boxId) {
    let div_info = document.getElementById(boxId);
    if (div_info.style.display == 'block') {
        div_info.style.display = 'none';
    } else {
        div_info.style.display = 'block';
    }
}

function showMenu() {
    document.getElementById("span_user").innerText = sessionStorage.getItem("prolific_id");
    document.getElementById("topMenu").classList.toggle("show");
}

function setImage(img_path) {
    // Clear element
    document.getElementById('div_img').innerText = '';
    // Append image
    var img = document.createElement('img');
    img.setAttribute('src', './api-phd/image-api/images' + img_path);
    img.setAttribute('width', '1100px');
    img.setAttribute('id', 'rand_img');
    document.getElementById('div_img').appendChild(img);
}

function getAnnotationStats() {
    const token = localStorage.getItem('jwt_token');

    var xhr = new XMLHttpRequest();

    xhr.addEventListener('readystatechange', function(event)
    {
        // When file has been successfully transmitted
        if (xhr.readyState == 4) {
//            console.log(xhr.response);
            ret_val = JSON.parse(xhr.response); // return value
            if (ret_val.success) {
                for (let k in ret_val) {
                    if (k == 'Reject') {
                        document.getElementById('span_nb_rejected').innerText = ret_val[k];
                    } // else if (k == 'Uncertain') {
//                        document.getElementById('span_nb_uncertain').innerText = ret_val[k];
//                    }
                    else if (k == 'Keep') {
                        document.getElementById('span_nb_accepted').innerText = ret_val[k];
                    } //else if (k == 'Done') {
//                        document.getElementById('span_nb_done').innerText = ret_val[k];
//                    }
                    else if (k == 'Left') {
                        document.getElementById('span_nb_left').innerText = ret_val[k];
                    }
                }
            } else {
                redirectNoPermission();
            }
        }
    });

    xhr.open('POST', './api-phd/image-api/get_stats', true);
    xhr.setRequestHeader('X-Requested-With','XMLHttpRequest');

    var formData = new FormData();
    formData.append('jwt_token', token);

    xhr.send(formData);
}

// Automatically set browser zoom so that full interface can be displayed on screen
function applyZoom() {
    let width = window.screen.width;
    if (width == 1920) {
        return;
    }

    let zoom = 0.01*Math.floor(100*width/1920);

    document.getElementById("div_script").style.transform = "scale(" + zoom + ")";
    document.getElementById("div_script").style.transformOrigin = "top left";
}