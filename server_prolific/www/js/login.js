function checkUserParams() {
    let parameters = location.search.substring(1).split("&");

    if (parameters.length != 1) {
        redirectNoPermission();
    }

    // Extract parameter name and value, and send to login API
    param1 = parameters[0].split('=');
    for (let i=0; i<param1.length; i++) {
        param1[i] = unescape(param1[i]);
    }

    var xhr = new XMLHttpRequest();

    xhr.addEventListener('readystatechange', function(event)
    {
        if (xhr.readyState == 4) {
            ret_val = JSON.parse(xhr.response); // return value
//            console.log(ret_val);
            if (ret_val.fail_code == 0) {
                localStorage.setItem('jwt_token', ret_val.jwt_token);
                sessionStorage.setItem("prolific_id", ret_val.prolific_id);
                sessionStorage.setItem("role", ret_val.role);
                sessionStorage.setItem("consented", ret_val.consented);
                sessionStorage.setItem("passed_tuto", ret_val.passed_tuto);
                redirectIndex();
            } else {
                redirectNoPermission();
            }
        }
    });

    xhr.open('POST', './api-phd/login-api/login', true);
    xhr.setRequestHeader('X-Requested-With','XMLHttpRequest');

    var formData = new FormData();
    formData.append('param_name', param1[0]);
    formData.append('param_value', param1[1]);

    xhr.send(formData);
}

function isLoggedIn() {
    const token = localStorage.getItem('jwt_token');

    if (token != null) {
        // Verify token hasn't expired yet
        _verifyToken(token);
    } else {
        redirectNoPermission();
//        console.log("No token present.");
        return false;
    }
}

function _verifyToken(token) {
    var xhr = new XMLHttpRequest();

    xhr.addEventListener('readystatechange', function(event)
    {
        // When file has been successfully transmitted
        if (xhr.readyState == 4) {
            ret_val = JSON.parse(xhr.response); // return value
//            console.log(ret_val);
            if (!ret_val.is_valid) {
                redirectLogin();
            } else {
                // Update token with new expiration date
                localStorage.setItem('jwt_token', ret_val.jwt_token);
                sessionStorage.setItem("prolific_id", ret_val.prolific_id);
                sessionStorage.setItem("role", ret_val.role);
            }
        }
    });

    xhr.open('POST', './api-phd/login-api/check_jwt_is_valid', true);
    xhr.setRequestHeader('X-Requested-With','XMLHttpRequest');

    var formData = new FormData();
    formData.append('jwt_token', token);

    xhr.send(formData);
}

function _setStorageAndRedirect(ret_val, b_stay_on_page) {
//    console.log(ret_val);
    localStorage.setItem('jwt_token', ret_val.jwt_token);
    sessionStorage.setItem("username", ret_val.username);
    sessionStorage.setItem("role", ret_val.role);
    sessionStorage.setItem("consented", ret_val.consented);
    sessionStorage.setItem("passed_tuto", ret_val.passed_tuto);
    let at_consent = (location.href.split("/").slice(-1) == 'consent_en.html');
    if (!ret_val.consented) {
        if (!at_consent) {
            redirectConsent();
        }
    } else if (!b_stay_on_page || at_consent) { // No access to "consent" page if you already consented and filled in info
        redirectIndex();
    }
}

function logOut() {
    localStorage.clear();
    redirectNoPermission();
}

// Check: https://stackoverflow.com/questions/8988855/include-another-html-file-in-a-html-file
function redirectDone() {
    location.href="./done.html";
}
function redirectIndex() {
    if (sessionStorage.getItem('consented') == 'false') {
        location.href="./consent_en.html";
    } else {
        location.href="./index.html";
    }
}
function redirectNoPermission() {
    location.href="./no_permission.html";
}
