function changePassword() {
    let email = document.getElementById('email').value;
    let old_pwd = document.getElementById('old_password').value;
    document.getElementById('old_password').value = '';
    let new_pwd = document.getElementById('password').value;

    // Don't allow empty password
    if (new_pwd == '') {
        alert("You did not specify a password. Please choose one.");
        return;
    }

    document.getElementById('password').value = '';
    document.getElementById('verify_password').value = '';

    if (!validateEmail(email)) {
        alert('Invalid email.');
        return;
    }

    old_pwd = pwd_crypt.encrypt(old_pwd);
    new_pwd = pwd_crypt.encrypt(new_pwd);

    var xhr = new XMLHttpRequest();

    xhr.addEventListener('readystatechange', function(event)
    {
        // When file has been successfully transmitted
        if (xhr.readyState == 4) {
            ret_val = JSON.parse(xhr.response); // return value
//            console.log(ret_val);
            let msg = 'Password changed successfully!';
            if (ret_val.fail_code == 0) {
                localStorage.setItem('jwt_token', ret_val.jwt_token);
                hideOverlay();
            } else if (ret_val.fail_code == 1) {
                msg = 'Unknown email. Please create an account first.';
            } else if (ret_val.fail_code == 2) {
                msg = "This account hasn't been activated yet. Please check your emails.";
            } else {
                msg = 'Invalid password.';
            }

            // span to contain return message only exists if return value was not 0, i.e., login was unsuccessfull
            let span_msg = document.getElementById('change_result');
            if (span_msg != null) {
                span_msg.innerText = msg;
            }
        }
    });

    xhr.open('POST', './api-phd/login-api/change_password', true);
    xhr.setRequestHeader('X-Requested-With','XMLHttpRequest');

    var formData = new FormData();
    formData.append('email', email);
    formData.append('old_enc_pwd', old_pwd);
    formData.append('new_enc_pwd', new_pwd);

    xhr.send(formData);
}

window.onload = function() {
    // Check if user is logged in. Otherwise, prompt for login, or account creation.
    isLoggedIn();

    document.getElementById('email').value = sessionStorage.getItem('email');
    document.getElementById('role').value = sessionStorage.getItem('role');
    document.getElementById('div_script').style.display = 'contents';
};
