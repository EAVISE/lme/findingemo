function getAnnotationAnalysis() {
    const token = localStorage.getItem('jwt_token');

    var xhr = new XMLHttpRequest();

    xhr.addEventListener('readystatechange', function(event)
    {
        // When file has been successfully transmitted
        if (xhr.readyState == 4) {
            ret_val = JSON.parse(xhr.response); // return value
            console.log(ret_val);

            // Table for Keep/Uncertain/Reject counts
            {
                let sum_counts = 0;
                for (let k in ret_val.reject) {
                    sum_counts += ret_val.reject[k];
                }
                // Fill table containing counts for reject/uncertain/keep.
                document.getElementById('table_ctr_reject').innerText = '';
                let tr_header = document.createElement('tr');
                tr_header.classList.add('tbl_header');
                let td_empty = document.createElement('td');
                tr_header.appendChild(td_empty);

                let tr_counts = document.createElement('tr');
                let td_idx_counts = document.createElement('td');
                td_idx_counts.innerText = 'Counts'
                tr_counts.appendChild(td_idx_counts);

                let tr_pcs = document.createElement('tr');
                let td_idx_pcs = document.createElement('td');
                td_idx_pcs.innerText = 'Pc. (%)'
                tr_pcs.appendChild(td_idx_pcs);

                let counts_sum = 0;
                let pcs_sum = 0;
                for (let k in ret_val.reject) {
                    let td_label = document.createElement('td');
                    td_label.innerText = k;
                    let td_count = document.createElement('td');
                    td_count.innerText = ret_val.reject[k];
                    counts_sum += ret_val.reject[k];
                    let td_pc = document.createElement('td');
                    let pc = (100*ret_val.reject[k]/sum_counts);
                    td_pc.innerText = pc.toLocaleString(undefined, {
                        minimumFractionDigits: 2,
                        maximumFractionDigits: 2
                    });
                    pcs_sum += pc;

                    tr_header.appendChild(td_label);
                    tr_counts.appendChild(td_count);
                    tr_pcs.appendChild(td_pc);
                }
                let td_header_sum = document.createElement('td');
                td_header_sum.innerText = 'Sum';
                tr_header.appendChild(td_header_sum);

                let td_counts_sum = document.createElement('td');
                td_counts_sum.innerText = counts_sum;
                tr_counts.appendChild(td_counts_sum);

                let td_pcs_sum = document.createElement('td');
                td_pcs_sum.innerText = pcs_sum.toLocaleString(undefined, {
                        minimumFractionDigits: 0,
                        maximumFractionDigits: 2
                    });;
                tr_pcs.appendChild(td_pcs_sum);

                document.getElementById('table_ctr_reject').appendChild(tr_header);
                document.getElementById('table_ctr_reject').appendChild(tr_counts);
                document.getElementById('table_ctr_reject').appendChild(tr_pcs);
            }

            // Table for Tags
            // Return format:  res['tags']['reject_value'] = x
            {
                // Fill table containing counts for tags.
                tbl_ctr_tags = document.getElementById('table_ctr_tags');
                tbl_ctr_tags.innerText = '';
                let tr_header = document.createElement('tr');
                tr_header.classList.add('tbl_header');
                let td_empty = document.createElement('td');
                tr_header.appendChild(td_empty);
                tbl_ctr_tags.appendChild(tr_header);

                let b_first = 1;
                let sorted_keys = Object.keys(ret_val.tags).sort();
                let rv_keys = ['Reject', 'Uncertain', 'Keep']
                for (let tag_idx=0; tag_idx<sorted_keys.length; tag_idx++) {
                    let tag = sorted_keys[tag_idx];
                    let tr_cts_tag = document.createElement('tr');
                    let td_tag = document.createElement('td');
                    td_tag.innerText = tag;
                    td_tag.classList.add('tbl_index');
                    tr_cts_tag.appendChild(td_tag);
                    let sum_tag = 0;
                    // rv = reject value
                    // BEWARE!!! Order of keys in Object can change between different tags!
                    // So ideally, cycle over an external array containing the keys.
                    for (let rv_idx=0; rv_idx<rv_keys.length; rv_idx++) {
                        rv = rv_keys[rv_idx];
                        if (b_first == 1) {
                            let td_index = document.createElement('td');
                            td_index.innerText = rv;
                            tr_header.appendChild(td_index);
                        }
                        let td_count = document.createElement('td');
                        td_count.innerText = ret_val.tags[tag][rv];
                        sum_tag += ret_val.tags[tag][rv];
                        tr_cts_tag.appendChild(td_count);
                    }
                    if (b_first == 1) {
                        let td_label = document.createElement('td');
                        td_label.innerText = "Sum";
                        tr_header.appendChild(td_label);
                        b_first = 0;
                    }
                    let td_sum = document.createElement('td');
                    td_sum.innerText = sum_tag;
                    tr_cts_tag.appendChild(td_sum);
                    tbl_ctr_tags.appendChild(tr_cts_tag);
                }
            }

            // Table for Emotion
            // Return format:  res['emotion']['emotion']['annotations', 'valence_avg', 'valence_std',
            //                      'arousal_avg', 'arousal_std', 'ambiguity_avg', 'ambiguity_std']
            {
                tbl_ctr_emotion = document.getElementById('table_ctr_emotion');
                tbl_ctr_emotion.innerText = "";
                let tr_header = document.createElement('tr');
                tr_header.classList.add('tbl_header');
                let td_empty = document.createElement('td');
                tr_header.appendChild(td_empty);
                tbl_ctr_emotion.appendChild(tr_header);

                let b_first = 1;
                let emo_keys = Object.keys(ret_val.emotion);
                let metrics = ['annotations', 'valence_avg', 'valence_std', 'arousal_avg',
                 'arousal_std', 'ambiguity_avg', 'ambiguity_std']
                for (let emo_idx=0; emo_idx<emo_keys.length; emo_idx++) {
                    let emo = emo_keys[emo_idx];
                    let tr_cts_emo = document.createElement('tr');
                    // Create first td element of the row, i.e., the row index
                    let td_emo = document.createElement('td');
                    td_emo.innerText = emo;
                    td_emo.classList.add('tbl_index');
                    if ((emo_idx + 1) % 4 == 0) {
                        td_emo.classList.add('tbl_index_axis');
                    }
                    tr_cts_emo.appendChild(td_emo);
                    // Create other td elements that make up this row
                    let sum_tag = 0;
                    // First key is an int, other values are floats
                    b_first_key = 1;
                    for (let metric_idx=0; metric_idx<metrics.length; metric_idx++) {
                        metric = metrics[metric_idx];
                        if (b_first == 1) {
                            let td_index = document.createElement('td');
                            td_index.innerText = metric;
                            tr_header.appendChild(td_index);
                        }
                        let td_value = document.createElement('td');
                        if ((emo_idx + 1) % 4 == 0) {
                            td_value.classList.add('tbl_value_bold');
                        }
                        // If it's the first time we see this key, add it to the header as well.
                        if (b_first_key) {
                            td_value.innerText = ret_val.emotion[emo][metric];
                            b_first_key = 0;
                        } else {
                            td_value.innerText = ret_val.emotion[emo][metric].toLocaleString(undefined, {
                                minimumFractionDigits: 2,
                                maximumFractionDigits: 2
                            });
                        }

                        tr_cts_emo.appendChild(td_value);
                    }
                    if (b_first == 1) {
                        b_first = 0;
                    }
                    tbl_ctr_emotion.appendChild(tr_cts_emo);
                }
            }

            // Table for Valence sign per Annotator
            // Return format:  res['valence_sign_per_ann']['user_email']['+'/'-'/'0']
            {
                tbl_valence_sign_per_ann = document.getElementById('table_valence_sign_per_ann');
                tbl_valence_sign_per_ann.innerText = "";

                let user_keys = Object.keys(ret_val.valence_sign_per_ann).sort();
                let sign_keys = ['-', '0', '+'];
                let tr_header = document.createElement('tr');
                tr_header.classList.add('tbl_header');
                let td_empty = document.createElement('td');
                tr_header.appendChild(td_empty);
                tbl_valence_sign_per_ann.appendChild(tr_header);

                let b_first = 1;
                sum_per_sign = [0, 0, 0];
                for (let user_idx=0; user_idx<user_keys.length; user_idx++) {
                    let user = user_keys[user_idx];
                    // Get sum value from other return value
                    let user_sum = ret_val.emo_per_ann[user]['Sum'];
                    let tr_cts_user = document.createElement('tr');
                    // Create first td element of the row, i.e., the row index
                    let td_user = document.createElement('td');
                    td_user.innerText = user;
                    td_user.classList.add('tbl_index');
                    td_user.classList.add('tbl_index_email');
                    tr_cts_user.appendChild(td_user);
                    // Create other td elements that make up this row
                    for (let sign_idx=0; sign_idx<3; sign_idx++) {
                        let sign = sign_keys[sign_idx];
                        let user_val = ret_val.valence_sign_per_ann[user][sign];
                        sum_per_sign[sign_idx] += user_val;
                        // If it's the first time we see this key, add it to the header as well.
                        if (b_first == 1) {
                            let td_index = document.createElement('td');
                            td_index.innerText = sign;
                            tr_header.appendChild(td_index);
                            if (sign_idx == 2) {
                                let td_index = document.createElement('td');
                                td_index.innerText = 'Sum';
                                tr_header.appendChild(td_index);
                            }
                        }
                        let td_value = document.createElement('td');
                        td_value.innerText = user_val;
                        let span_value_pc = document.createElement('span');
                        span_value_pc.classList.add('span_pc');
                        span_value_pc.innerText = (100*user_val/user_sum).toLocaleString(undefined, {
                            minimumFractionDigits: 1,
                            maximumFractionDigits: 1
                        });
                        td_value.appendChild(span_value_pc);
                        tr_cts_user.appendChild(td_value);
                    }
                    let td_user_sum = document.createElement('td');
                    td_user_sum.innerText = user_sum;
                    tr_cts_user.appendChild(td_user_sum);
                    if (b_first == 1) {
                        b_first = 0;
                    }
                    tbl_valence_sign_per_ann.appendChild(tr_cts_user);
                }
                // Add row with sums
                {
                    let tr_cts_sum = document.createElement('tr');
                    let td_label_sum = document.createElement('td');
                    td_label_sum.innerText = 'Total';
                    td_label_sum.classList.add('tbl_index');
                    td_label_sum.classList.add('tbl_index_email');
                    tr_cts_sum.appendChild(td_label_sum);
                    total_sum = 0;
                    for (let sign_idx=0; sign_idx<3; sign_idx++) {
                        total_sum += sum_per_sign[sign_idx];
                        let td_value = document.createElement('td');
                        td_value.innerText = sum_per_sign[sign_idx];
                        td_value.classList.add('tbl_value_bold');
                        tr_cts_sum.appendChild(td_value);
                    }
                    let td_total_sum = document.createElement('td');
                    td_total_sum.innerText = total_sum;
                    td_total_sum.classList.add('tbl_value_bold');
                    tr_cts_sum.appendChild(td_total_sum);
                    tbl_valence_sign_per_ann.appendChild(tr_cts_sum);
                }
            }

            // Table for Emotion per Annotator
            // Return format:  res['emo_per_ann']['user_email']['emotion']
            {
                tbl_emo_per_ann = document.getElementById('table_emo_per_ann');
                tbl_emo_per_ann.innerText = "";

                let user_keys = Object.keys(ret_val.emo_per_ann).sort();
                for (let tbl_part=0; tbl_part<2; tbl_part++) {
                    let tr_header = document.createElement('tr');
                    tr_header.classList.add('tbl_header');
                    let td_empty = document.createElement('td');
                    tr_header.appendChild(td_empty);
                    tbl_emo_per_ann.appendChild(tr_header);

                    let b_first = 1;
                    for (let user_idx=0; user_idx<user_keys.length; user_idx++) {
                        let user = user_keys[user_idx];
                        let user_sum = ret_val.emo_per_ann[user]['Sum'];
                        emo_keys = Object.keys(ret_val.emo_per_ann[user]);
                        let tr_cts_user = document.createElement('tr');
                        // Create first td element of the row, i.e., the row index
                        let td_user = document.createElement('td');
                        td_user.innerText = user;
                        td_user.classList.add('tbl_index');
                        td_user.classList.add('tbl_index_email');
                        tr_cts_user.appendChild(td_user);
                        // Create other td elements that make up this row
                        for (let emo_idx=0; emo_idx<13; emo_idx++) {
                            emo = emo_keys[13*tbl_part + emo_idx]
                            // If it's the first time we see this key, add it to the header as well.
                            if (b_first == 1) {
                                let td_index = document.createElement('td');
                                td_index.innerText = emo;
                                tr_header.appendChild(td_index);
                            }
                            let td_value = document.createElement('td');
                            td_value.innerText = ret_val.emo_per_ann[user][emo]
                            if (13*tbl_part + emo_idx != 25) {
                                let span_value_pc = document.createElement('span');
                                span_value_pc.classList.add('span_pc');
                                span_value_pc.innerText = (100*ret_val.emo_per_ann[user][emo]/user_sum).toLocaleString(undefined, {
                                    minimumFractionDigits: 1,
                                    maximumFractionDigits: 1
                                });
                                td_value.appendChild(span_value_pc);
                            }
                            tr_cts_user.appendChild(td_value);
                        }
                        if (b_first == 1) {
                            b_first = 0;
                        }
                        tbl_emo_per_ann.appendChild(tr_cts_user);
                    }
                }
            }

            document.getElementById('ta_iaa_overlap').value = ret_val.iaa_overlap;
            document.getElementById('ta_iaa_reject').value = ret_val.iaa_reject;
            document.getElementById('ta_iaa_emotion').value = ret_val.iaa_emotion;
            document.getElementById('ta_iaa_emotion_leaf').value = ret_val.iaa_emotion_leaf;
            document.getElementById('ta_iaa_valence').value = ret_val.iaa_valence;
            document.getElementById('ta_iaa_valence_sign_strict').value = ret_val.iaa_valence_sign_strict;
            document.getElementById('ta_iaa_valence_sign_loose').value = ret_val.iaa_valence_sign_loose;
            document.getElementById('ta_iaa_arousal').value = ret_val.iaa_arousal;
            document.getElementById('ta_iaa_ambiguity').value = ret_val.iaa_ambiguity;
        }
    });

    xhr.open('POST', './api-phd/ann-analysis-api/analyze_annotations', true);
    xhr.setRequestHeader('X-Requested-With','XMLHttpRequest');

    var formData = new FormData();
    formData.append('jwt_token', token);

    xhr.send(formData);
}

window.onload = function() {
    // Check if user is logged in. Otherwise, prompt for login, or account creation.
    isLoggedIn();  // see login.js

    document.getElementById('div_script').style.display = 'contents';

    getAnnotationAnalysis();
};
