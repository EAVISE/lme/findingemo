// Method to get the "ascending" value for the sort column selected to sort the annotation data by.
function getAscValue(sort_idx, td_idx, prev_header) {
    if (sort_idx != td_idx) {
        return 'false';
    } else {
        if (prev_header == null) {
            return 'true';
        } else {
            prev_val = prev_header.childNodes[td_idx].getAttribute('ascending');
            return (prev_val == 'true') ? 'false' : 'true';
        }
    }
}

function getAnnotations(sorted_by=null) {
    if (sorted_by != null) {
        let sort_parts = sorted_by.split(',');
        var sort_val = sort_parts[0];
        var sort_idx = Number(sort_parts[1]);
    } else {
        sort_idx = 0;
    }

    const token = localStorage.getItem('jwt_token');

    var xhr = new XMLHttpRequest();

    xhr.addEventListener('readystatechange', function(event)
    {
        // When response has been received
        if (xhr.readyState == 4) {
            ret_val = JSON.parse(xhr.response); // return value

            let role = sessionStorage.getItem('role');
            // Previous header
            let prev_header = document.getElementById('tbl_ann_tr_header');
            // Return value = array where each element contains (annotation index, image path, reject value, reject reason)
            document.getElementById('table_annotations').innerText = '';
            let tr_header = document.createElement('tr');
            tr_header.id = 'tbl_ann_tr_header';
            header_labels = []
            let td_head_0 = document.createElement('td');
            td_head_0.innerText = "idx";
            td_head_0.setAttribute("onclick", "getAnnotations(sorted_by='index," + header_labels.length +"');");
            td_head_0.setAttribute("ascending", getAscValue(sort_idx, header_labels.length, prev_header));
            header_labels.push(td_head_0);
            if (role === 'admin') {
                var td_head_email = document.createElement('td');
                td_head_email.innerText = "user";
                td_head_email.setAttribute("onclick", "getAnnotations(sorted_by='email," + header_labels.length +"');");
                td_head_email.setAttribute("ascending", getAscValue(sort_idx, header_labels.length, prev_header));
                header_labels.push(td_head_email);
            }
            let td_head_1 = document.createElement('td');
            td_head_1.innerText = "k/u/r";
            td_head_1.setAttribute("onclick", "getAnnotations(sorted_by='reject," + header_labels.length +"');");
            td_head_1.setAttribute("ascending", getAscValue(sort_idx, header_labels.length, prev_header));
            header_labels.push(td_head_1);
            let td_head_2 = document.createElement('td');
            td_head_2.innerText = "tags";
            td_head_2.setAttribute("onclick", "getAnnotations(sorted_by='tags," + header_labels.length +"');");
            td_head_2.setAttribute("ascending", getAscValue(sort_idx, header_labels.length, prev_header));
            header_labels.push(td_head_2);
            let td_head_3 = document.createElement('td');
            td_head_3.innerText = "age";
            td_head_3.setAttribute("onclick", "getAnnotations(sorted_by='age," + header_labels.length +"');");
            td_head_3.setAttribute("ascending", getAscValue(sort_idx, header_labels.length, prev_header));
            header_labels.push(td_head_3);
            let td_head_4 = document.createElement('td');
            td_head_4.innerText = "valence";
            td_head_4.setAttribute("onclick", "getAnnotations(sorted_by='valence," + header_labels.length +"');");
            td_head_4.setAttribute("ascending", getAscValue(sort_idx, header_labels.length, prev_header));
            header_labels.push(td_head_4);
            let td_head_5 = document.createElement('td');
            td_head_5.innerText = "arousal";
            td_head_5.setAttribute("onclick", "getAnnotations(sorted_by='arousal," + header_labels.length +"');");
            td_head_5.setAttribute("ascending", getAscValue(sort_idx, header_labels.length, prev_header));
            header_labels.push(td_head_5);
            let td_head_6 = document.createElement('td');
            td_head_6.innerText = "emotion";
            td_head_6.setAttribute("onclick", "getAnnotations(sorted_by='emotion," + header_labels.length +"');");
            td_head_6.setAttribute("ascending", getAscValue(sort_idx, header_labels.length, prev_header));
            header_labels.push(td_head_6);
            let td_head_7 = document.createElement('td');
            td_head_7.innerText = "dec.fact.";
            td_head_7.setAttribute("onclick", "getAnnotations(sorted_by='dec_factors," + header_labels.length +"');");
            td_head_7.setAttribute("ascending", getAscValue(sort_idx, header_labels.length, prev_header));
            header_labels.push(td_head_7);
            let td_head_8 = document.createElement('td');
            td_head_8.innerText = "ambiguity";
            td_head_8.setAttribute("onclick", "getAnnotations(sorted_by='ambiguity," + header_labels.length +"');");
            td_head_8.setAttribute("ascending", getAscValue(sort_idx, header_labels.length, prev_header));
            header_labels.push(td_head_8);
            let td_head_9 = document.createElement('td');
            td_head_9.innerText = "fmri cand.";
            td_head_9.setAttribute("onclick", "getAnnotations(sorted_by='fmri_candidate," + header_labels.length +"');");
            td_head_9.setAttribute("ascending", getAscValue(sort_idx, header_labels.length, prev_header));
            header_labels.push(td_head_9);
            let td_head_10 = document.createElement('td');
            td_head_10.innerText = "verify";
            header_labels.push(td_head_10);

            for(let i=0; i<header_labels.length; i++) {
                tr_header.appendChild(header_labels[i]);
            }

            document.getElementById('table_annotations').appendChild(tr_header);

            for (let i=0; i<ret_val.length; i++) {
                // Append image
                let tr = document.createElement('tr');
                if (i%2 == 0) {
                    tr.classList.add("tr_even");
                } else {
                    tr.classList.add("tr_odd");
                }

                let td_ann_idx = document.createElement('td');
                td_ann_idx.innerHTML = i;

                if (role === 'admin') {
                    var td_ann_user = document.createElement('td');
                    td_ann_user.innerText = ret_val[i][ret_val[i].length -1];
                }

                let td_ann_value = document.createElement('td');
                td_ann_value.innerHTML = ret_val[i][2];

                let td_ann_tags = document.createElement('td');
                const ann_tags = ret_val[i][3].split(',');
                let ann_tags_text = '';
                for (let i=0; i<ann_tags.length; i++) {
                    ann_tags_text += ann_tags[i];
                    if (i < ann_tags.length -1) {
                        ann_tags_text += '<br>';
                    }
                }
                td_ann_tags.innerHTML = ann_tags_text;

                let td_ann_age = document.createElement('td');
                const ann_age = ret_val[i][4].split(',');
                let ann_age_text = '';
                for (let i=0; i<ann_age.length; i++) {
                    ann_age_text += ann_age[i];
                    if (i < ann_age.length -1) {
                        ann_age_text += '<br>';
                    }
                }
                td_ann_age.innerHTML = ann_age_text;

                let td_ann_valence = document.createElement('td');
                td_ann_valence.innerHTML = ret_val[i][5];

                let td_ann_arousal = document.createElement('td');
                td_ann_arousal.innerHTML = ret_val[i][6];

                let td_ann_emotion = document.createElement('td');
                td_ann_emotion.innerHTML = ret_val[i][7];

                let td_ann_dec_factors = document.createElement('td');
                const dec_factors = ret_val[i][8].split(',');
                let dec_factors_text = '';
                for (let i=0; i<dec_factors.length; i++) {
                    dec_factors_text += dec_factors[i];
                    if (i < dec_factors.length -1) {
                        dec_factors_text += '<br>';
                    }
                }
                td_ann_dec_factors.innerHTML = dec_factors_text;

                let td_ann_obviousness = document.createElement('td');
                td_ann_obviousness.innerHTML = ret_val[i][9];

                let td_ann_fmri_candidate = document.createElement('td');
                td_ann_fmri_candidate.innerHTML = ret_val[i][10];

                let td_img_link = document.createElement('td');
                td_img_link.innerHTML = "<a href ='./view_annotation.html?"
                + "idx=" + ret_val[i][0]
                + "&img=" + ret_val[i][1]
                + "&reject=" + ret_val[i][2]
                + "&tags=" + ret_val[i][3]
                + "&age=" + ret_val[i][4]
                + "&valence=" + ret_val[i][5]
                + "&arousal=" + ret_val[i][6]
                + "&emotion=" + ret_val[i][7]
                + "&dec_factors=" + ret_val[i][8]
                + "&obviousness=" + ret_val[i][9]
                + "&fmri_candidate=" + ret_val[i][10]
                + "'>view image</a>";

                tr.appendChild(td_ann_idx);
                if (role === 'admin') {
                    tr.appendChild(td_ann_user);
                }
                tr.appendChild(td_ann_value);
                tr.appendChild(td_ann_tags);
                tr.appendChild(td_ann_age);
                tr.appendChild(td_ann_valence);
                tr.appendChild(td_ann_arousal);
                tr.appendChild(td_ann_emotion);
                tr.appendChild(td_ann_dec_factors);
                tr.appendChild(td_ann_obviousness);
                tr.appendChild(td_ann_fmri_candidate);
                tr.appendChild(td_img_link);
                document.getElementById('table_annotations').appendChild(tr);
            }
        }
    });

    xhr.open('POST', './api-phd/image-api/get_annotations', true);
    xhr.setRequestHeader('X-Requested-With','XMLHttpRequest');

    var formData = new FormData();
    formData.append('jwt_token', token);
    if (sorted_by != null) {
        formData.append('sorted_by', sort_val);
        let asc = !(document.getElementById('tbl_ann_tr_header').childNodes[sort_idx].getAttribute('ascending') == 'true');
//        console.log('compare value: ' + document.getElementById('tbl_ann_tr_header').childNodes[sort_idx].ascending);
//        console.log('asc --> ' + asc);
        formData.append('ascending', asc);
    }

    xhr.send(formData);
}

window.onload = function() {
    isLoggedIn();

    document.getElementById('div_script').style.display = 'contents';
    getAnnotations();
}