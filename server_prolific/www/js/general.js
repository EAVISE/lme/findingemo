/* 'annotated' is used to keep track of how many dimensions were annotated by the user so far. Until the
user has annotated everything that needs to be annotated, they will not be able to move on to the next picture. */
var annotated = new Set();
var not_annotated = new Set();

function resetAnnotation() {
    annotated = new Set();
    not_annotated = new Set(["ann_keep", "ann_age", "ann_decisive_factor", "slide_ambiguity", "emotion",
     "slide_arousal", "slide_valence"]);
//    document.getElementById('save_next_good').disabled = true;
//    document.getElementById('save_next_good').onclick = function() { console.log("HALLO?") };
    document.getElementById('save_next_good').classList.add('disabled');
    document.getElementById('save_next_good').onclick = function() { alertUnAnnotatedDimensions(); };

    document.getElementById('keep_keep').checked = false;
    document.getElementById('keep_rej').checked = false;

    let checked_reasons = document.querySelectorAll('input[name=ann_tags]:checked');
    for (var i=0; i<checked_reasons.length; i++) {
        checked_reasons[i].checked = false;
    }

    let checked_age = document.querySelectorAll('input[name=ann_age]:checked');
    for (var i=0; i<checked_age.length; i++) {
        checked_age[i].checked = false;
    }

    document.getElementById('slide_valence').value = 0;
    document.getElementById('slide_arousal').value = 0;
    var els = document.getElementById('div_svg').getElementsByClassName('clicked');
    for (var i=0; i<els.length; i++) {
        els[i].classList.remove('clicked');
    }

    let checkedBoxes = document.querySelectorAll('input[name=ann_decisive_factor]:checked');
    for (var i=0; i<checkedBoxes.length; i++) {
        checkedBoxes[i].checked = false;
    }
    document.getElementById('slide_ambiguity').value = 0;

    document.getElementById('fmri_candidate').checked = false;
}

function alertUnAnnotatedDimensions() {
    let msg = "Before you can save this annotation, you first need to either 'Reject' this image, or 'Keep' it and "
              + "further annotate the following:\n";
    for(let val of not_annotated) {
        let dim;
        switch (val) {
            case "ann_age":
                dim = "\nAge group";
                break;
            case "ann_decisive_factor":
                dim = "\nDeciding factor(s) for emotion";
                break;
            case "slide_ambiguity":
                dim = "\nAmbiguity";
                break;
            case "emotion":
                dim = "\nMain emotion";
                break;
            case "slide_arousal":
                dim = "\nIntensity";
                break;
            case "slide_valence":
                dim = "\nNegative/Positive";
                break;
            default:
                dim = "";
                break;
        }
        msg = msg + dim;
    }
    console.log(msg);

    alert(msg);
}

// Update counter that keeps track of what dimensions are annotated.
function updateAnnCounter(dimension, add) {
    if (add) {
        annotated.add(dimension);
        not_annotated.delete(dimension);
    } else {
        annotated.delete(dimension);
        not_annotated.add(dimension);
    }

    // Check value of Keep/Reject
    var radios_keep = document.getElementsByName('ann_keep');
    var rej_value;
    for (var i = 0; i < radios_keep.length; i++) {
        if (radios_keep[i].checked) {
            rej_value = radios_keep[i].value;
            break;
        }
    }

    if ((rej_value == 'Keep' && annotated.size == 7) || (rej_value == 'Reject')) {
//        document.getElementById('save_next_good').disabled = false;
        document.getElementById('save_next_good').classList.remove('disabled');
        document.getElementById('save_next_good').onclick = function() { saveAndNextGood(); };
    } else {
//        document.getElementById('save_next_good').disabled = true;
        document.getElementById('save_next_good').classList.add('disabled');
        document.getElementById('save_next_good').onclick = function() { alertUnAnnotatedDimensions(); };
    }

//     console.log(annotated);
}

// Request a random image from the server; no neural network is used
function nextImage() {
    const token = localStorage.getItem('jwt_token');

    disableNextBtns();

    var xhr = new XMLHttpRequest();

    xhr.addEventListener('readystatechange', function(event)
    {
        // When file has been successfully transmitted
        if (xhr.readyState == 4) {
            ret_val = JSON.parse(xhr.response); // return value
            //console.log(ret_val);
            if (ret_val.success) {
                setImage(ret_val.path);

//                document.getElementById('span_score_r').innerText = '-.--';
//                document.getElementById('span_score_u').innerText = '-.--';
//                document.getElementById('span_score_a').innerText = '-.--';

                enableNextBtns();
            } else {
                redirectNoPermission();
            }
        }
    });

    xhr.open('POST', './api-phd/image-api/get_image', true);
    xhr.setRequestHeader('X-Requested-With','XMLHttpRequest');

    var formData = new FormData();
    formData.append('jwt_token', token);

    xhr.send(formData);
}

// Request a random image from the server with neural network categorization scores
function nextImageCNN() {
    const token = localStorage.getItem('jwt_token');

    disableNextBtns();

    var xhr = new XMLHttpRequest();

    xhr.addEventListener('readystatechange', function(event)
    {
        // When file has been successfully transmitted
        if (xhr.readyState == 4) {
            ret_val = JSON.parse(xhr.response); // return value
            //console.log(ret_val);
            if (ret_val.success) {
                setImage(ret_val.path);

//                document.getElementById('span_score_r').innerText = ret_val.pred.Reject;
//                document.getElementById('span_score_u').innerText = ret_val.pred.Uncertain;
//                document.getElementById('span_score_a').innerText = ret_val.pred.Keep;

                enableNextBtns();
            } else {
                redirectNoPermission();
            }
        }
    });

    xhr.open('POST', './api-phd/image-api/get_image_cnn', true);
    xhr.setRequestHeader('X-Requested-With','XMLHttpRequest');

    var formData = new FormData();
    formData.append('jwt_token', token);

    xhr.send(formData);
}

// Request a random image from the server with neural network categorization scores.
// The returned image has an 'keep' (or 'accept' if you prefer) score 0.30<x<0.70.
function nextImageBorder() {
    const token = localStorage.getItem('jwt_token');

    disableNextBtns();

    var xhr = new XMLHttpRequest();

    xhr.addEventListener('readystatechange', function(event)
    {
        // When file has been successfully transmitted
        if (xhr.readyState == 4) {
            ret_val = JSON.parse(xhr.response); // return value
            //console.log(ret_val);
            if (ret_val.success) {
                setImage(ret_val.path);

//                document.getElementById('span_score_r').innerText = ret_val.pred.Reject;
//                document.getElementById('span_score_u').innerText = ret_val.pred.Uncertain;
//                document.getElementById('span_score_a').innerText = ret_val.pred.Keep;

                enableNextBtns();
            } else {
                redirectNoPermission();
            }
        }
    });

    xhr.open('POST', './api-phd/image-api/get_image_cnn_border', true);
    xhr.setRequestHeader('X-Requested-With','XMLHttpRequest');

    var formData = new FormData();
    formData.append('jwt_token', token);

    xhr.send(formData);
}

// Request a random image from the server with neural network categorization scores.
// The returned image has an 'keep' (or 'accept' if you prefer) score >0.75.
function nextImageGood() {
//    console.log("calling nextImageGood()");
    const token = localStorage.getItem('jwt_token');

    disableNextBtns();

    var xhr = new XMLHttpRequest();

    xhr.addEventListener('readystatechange', function(event)
    {
        // When file has been successfully transmitted
        if (xhr.readyState == 4) {
//            console.log(xhr);
            ret_val = JSON.parse(xhr.response); // return value
            if (ret_val.success) {
                if (ret_val.path == 'ALL_DONE') {
                    redirectDone();
                } else {
                    setImage(ret_val.path);

//                    document.getElementById('span_score_r').innerText = ret_val.pred.Reject;
//                    document.getElementById('span_score_u').innerText = ret_val.pred.Uncertain;
//                    document.getElementById('span_score_a').innerText = ret_val.pred.Keep;

                    enableNextBtns();
                }
            } else {
//                redirectNoPermission();
            }
        }
    });

    xhr.open('POST', './api-phd/image-api/get_image_cnn_good', true);
    xhr.setRequestHeader('X-Requested-With','XMLHttpRequest');

    var formData = new FormData();
    formData.append('jwt_token', token);

    xhr.send(formData);
}

function saveAnnotation() {
    isLoggedIn();
    const token = localStorage.getItem('jwt_token');

    // Get selected keep/reject value
    var radios_keep = document.getElementsByName('ann_keep');
    var rej_value;
    for (var i = 0; i < radios_keep.length; i++) {
        if (radios_keep[i].checked) {
            rej_value = radios_keep[i].value;
            break;
        }
    }

    // Get selected tags
    let checked_reasons = document.querySelectorAll('input[name=ann_tags]:checked');
    var ann_tags = [];
    if (checked_reasons.length != 0) {
        for (var i=0; i<checked_reasons.length; i++) {
            ann_tags.push(checked_reasons[i].value);
        }
    } else {
        ann_tags.push('Undefined');
    }

    // Get selected age value(s)
    let checked_age = document.querySelectorAll('input[name=ann_age]:checked');
    var ann_age = [];
    if (checked_age.length != 0) {
        for (var i=0; i<checked_age.length; i++) {
            ann_age.push(checked_age[i].value);
        }
    } else {
        ann_age.push('Undefined');
    }

    // Get valence value
    let valence = document.getElementById('slide_valence').value;

    // Get arousal value
    let arousal = document.getElementById('slide_arousal').value;

    // Get Plutchik emotion
    var emotion = 'Undefined';
    var els = document.getElementById('div_svg').getElementsByClassName('clicked');
    if (els.length == 1) {
        emotion = els[0].id;
    }

    // Get decisive factors
    let checkedBoxes = document.querySelectorAll('input[name=ann_decisive_factor]:checked');
    var dec_factors = [];
    if (checkedBoxes.length != 0) {
        for (var i=0; i<checkedBoxes.length; i++) {
            dec_factors.push(checkedBoxes[i].value);
        }
    } else {
        dec_factors.push('Undefined');
    }

    // Get ambiguity
    let ambiguity = document.getElementById('slide_ambiguity').value;

    // Get fMRI candidate value
    let fmri_cand = document.getElementById('fmri_candidate').checked;

    var xhr = new XMLHttpRequest();

    xhr.addEventListener('readystatechange', function(event)
    {
        // When file has been successfully transmitted
        if (xhr.readyState == 4) {
            ret_val = JSON.parse(xhr.response); // return value; {'success': true/false}
            if (ret_val.success) {
                if (ret_val.done) {
                    redirectDone();
                } else {
                getAnnotationStats();
                }
            } else {
                redirectNoPermission();
            }
        }
    });

    xhr.open('POST', './api-phd/image-api/save_annotation', true);
    xhr.setRequestHeader('X-Requested-With','XMLHttpRequest');

    var formData = new FormData();
    formData.append('jwt_token', token);
    formData.append('img', document.getElementById('rand_img').getAttribute('src'));
    formData.append('rej_value', rej_value);
    formData.append('tags', ann_tags);
    formData.append('age', ann_age);
    formData.append('valence', valence);
    formData.append('arousal', arousal);
    formData.append('emotion', emotion);
    formData.append('dec_factors', dec_factors);
    formData.append('ambiguity', ambiguity);
    formData.append('fmri_cand', fmri_cand);

    xhr.send(formData);
}

function saveAndNextBorder() {
    saveAnnotation();
    nextImageBorder();
}

function saveAndNextGood() {
    saveAnnotation();
    nextImageGood();
}

function disableNextBtns() {
    let div_btns = document.getElementById('div_top_btns').querySelectorAll('input');
    for (var i=0; i<div_btns.length; i++) {
        div_btns[i].disabled=true;
    }
}
function enableNextBtns() {
    let div_btns = document.getElementById('div_top_btns').querySelectorAll('input');
    for (var i=0; i<div_btns.length; i++) {
        div_btns[i].disabled=false;
    }

    resetAnnotation();
}

function setPassedTuto() {
    const token = localStorage.getItem('jwt_token');

    var xhr = new XMLHttpRequest();

    xhr.addEventListener('readystatechange', function(event)
    {
        // When file has been successfully transmitted
        if (xhr.readyState == 4) {
            ret_val = JSON.parse(xhr.response); // return value; {'success': true/false}
            if (ret_val.success) {
                document.getElementsByTagName('body')[0].style.backgroundColor = '#FFFFFF';

                localStorage.setItem('jwt_token', ret_val.jwt_token);
                sessionStorage.setItem("prolific_id", ret_val.username);
                sessionStorage.setItem("role", ret_val.role);
                sessionStorage.setItem("passed_tuto", ret_val.passed_tuto);

                document.getElementById('keep_keep').disabled = false;
                document.getElementById('keep_rej').disabled = false;

                nextImageGood();
            } else {
                redirectNoPermission();
            }
        }
    });

    xhr.open('POST', './api-phd/login-api/set_passed_tuto', true);
    xhr.setRequestHeader('X-Requested-With','XMLHttpRequest');

    var formData = new FormData();
    formData.append('jwt_token', token);

    xhr.send(formData);
}

function toggleSliderThumb(sliderId) {
    updateAnnCounter(sliderId, true);

    if (document.getElementById(sliderId).classList.contains('thumb_unclicked')) {
        document.getElementById(sliderId).classList.remove('thumb_unclicked');
        document.getElementById(sliderId).classList.add('thumb_clicked');
    }
}

function checkBoxes(ann_dimension) {
    let checkedBoxes = document.querySelectorAll('input[name=' + ann_dimension.name + ']:checked');
    if (checkedBoxes.length > 0) {
        updateAnnCounter(ann_dimension.name, true);
    } else {
        updateAnnCounter(ann_dimension.name, false);
    }
}

window.onload = function() {
    // Check if user is logged in. Otherwise, prompt for login, or account creation.
    isLoggedIn();  // see login.js

    document.getElementById('div_main').innerHTML = viewAnnotation;
    document.getElementById('div_script').style.display = 'contents';

    // Add top navigation buttons
    if (sessionStorage.getItem('role') === 'admin') {
        document.getElementById('div_top_btns').innerHTML = viewTopBtnsAdmin;
        let a = document.createElement('a');
        a.href = "annotations2.html";
        a.innerText = "Goto 'Per Image'";
        document.getElementById('topMenu').appendChild(a);
        let a2 = document.createElement('a');
        a2.href = "ann_analysis.html";
        a2.innerText = "Annotation analysis";
        document.getElementById('topMenu').appendChild(a2);
        // Show fMRI-Candidate checkbox
        let fmri_elems = document.getElementsByClassName('class_fmri_candidate');
        for (let i=0; i<fmri_elems.length; i++) {
            if (fmri_elems[i].tagName == 'HR') {
                fmri_elems[i].style.display = 'block';
            } else {
                fmri_elems[i].style.display = 'inline';
            }
        }
    } else {
        document.getElementById('div_top_btns').innerHTML = viewTopBtnsAnn;
    }

    // Add mouse events to emotion paths in Plutchik wheel
    let groups = document.getElementById('div_svg').getElementsByClassName('g_emotion');
    for (var i=0; i<groups.length; i++) {
        groups[i].onclick = function() {toggleClicked(this)};
        groups[i].onmouseenter = function() {displayEmoDesc()};
        groups[i].onmouseleave = function() {hideEmoDesc()};
    }

    if (sessionStorage.getItem('passed_tuto') == 'false') {
        // Disable Skip/Save buttons
        document.getElementById('next_image_good').disabled = true;
        document.getElementById('save_next_good').disabled = true;
        // Show instructions
        document.getElementById('div_img').innerHTML = viewWelcomeMsg;
        document.getElementById('keep_keep').disabled = true;
        document.getElementById('keep_rej').disabled = true;
        // document.getElementById('div_welcome_part2').style.display = 'block';
        document.getElementsByTagName('body')[0].style.backgroundColor = 'rgb(237, 239, 255)';
    } else {
        nextImageGood();
    }
    getAnnotationStats();

    applyZoom();
};
