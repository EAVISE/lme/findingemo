function parseParams() {
    let parameters = location.search.substring(1).split("&");

    // Get individual parameters
    let ann_idx = unescape(parameters[0].split('=')[1]);
    let ann_img = unescape(parameters[1].split('=')[1]);
    let ann_reject = unescape(parameters[2].split('=')[1]);
    let ann_tags = unescape(parameters[3].split('=')[1]);
    let ann_age = unescape(parameters[4].split('=')[1]);
    let ann_valence = unescape(parameters[5].split('=')[1]);
    let ann_arousal = unescape(parameters[6].split('=')[1]);
    let ann_emotion = unescape(parameters[7].split('=')[1]);
    let ann_dec_factors = unescape(parameters[8].split('=')[1]);
    let ann_ambiguity = unescape(parameters[9].split('=')[1]);
    let ann_fmri_candidate = unescape(parameters[10].split('=')[1]);

    return [ann_idx, ann_img, ann_reject, ann_tags, ann_age, ann_valence, ann_arousal, ann_emotion,
     ann_dec_factors, ann_ambiguity, ann_fmri_candidate];
}

function applyParams(params) {
    // Set image
    setImage(params[1]);

    // Set reject value
    let radios_reject = document.getElementsByName('ann_keep');
    for (let i = 0; i < radios_reject.length; i++) {
        if (radios_reject[i].value == params[2]) {
            radios_reject[i].checked = true;
            break;
        }
    }

    // Set tags
    if (params[3] != 'Undefined') {
        const ann_tags = params[3].split(',');
        const tags_boxes = document.querySelectorAll('input[name=ann_tags]');
        for (let i=0; i<tags_boxes.length; i++) {
            if (ann_tags.includes(tags_boxes[i].value)) {
                tags_boxes[i].checked = true;
            }
        }
    }

    // Set age value
    if (params[4] != "Undefined") {
        const ann_age = params[4].split(',');
        const age_boxes = document.querySelectorAll('input[name=ann_age]');
        for (let i=0; i<age_boxes.length; i++) {
            if (ann_age.includes(age_boxes[i].value)) {
                age_boxes[i].checked = true;
            }
        }
    }

    // Set valence
    document.getElementById('slide_valence').value = params[5];

    // Set arousal
    document.getElementById('slide_arousal').value = params[6];

    // Set emotion
    if (params[7] != "Undefined") {
        let g = document.getElementById(params[7]).classList.add('clicked');
    }

    // Set decisive factors
    if (params[8] != 'Undefined') {
        const dec_factors = params[8].split(',');
        const dec_factors_boxes = document.querySelectorAll('input[name=ann_decisive_factor]');
        for (let i=0; i<dec_factors_boxes.length; i++) {
            if (dec_factors.includes(dec_factors_boxes[i].value)) {
                dec_factors_boxes[i].checked = true;
            }
        }
    }

    // Set ambiguity
    document.getElementById('slide_ambiguity').value = params[9];

    // Set fMRI candidate value
    document.getElementById('fmri_candidate').checked = (params[10] === 'true');
}

function prevAnnotation() {
    const token = localStorage.getItem('jwt_token');

    let params = parseParams();

    var xhr = new XMLHttpRequest();

    xhr.addEventListener('readystatechange', function(event)
    {
        // When file has been successfully transmitted
        if (xhr.readyState == 4) {
            ret_val = JSON.parse(xhr.response); // return value

            new_url = "./view_annotation.html?"
                + "idx=" + ret_val[0]
                + "&img=" + ret_val[1]
                + "&reject=" + ret_val[2]
                + "&tags=" + ret_val[3]
                + "&age=" + ret_val[4]
                + "&valence=" + ret_val[5]
                + "&arousal=" + ret_val[6]
                + "&emotion=" + ret_val[7]
                + "&dec_factors=" + ret_val[8]
                + "&ambiguity=" + ret_val[9]
                + "&fmri_candidate=" + ret_val[10];

            document.location.href = new_url;
        }
    });

    xhr.open('POST', './api-phd/image-api/get_prev_annotation', true);
    xhr.setRequestHeader('X-Requested-With','XMLHttpRequest');

    var formData = new FormData();
    formData.append('jwt_token', token);
    formData.append('idx', params[0]);

    xhr.send(formData);
}

function prevOwnAnnotation() {
    const token = localStorage.getItem('jwt_token');

    let params = parseParams();

    var xhr = new XMLHttpRequest();

    xhr.addEventListener('readystatechange', function(event)
    {
        // When file has been successfully transmitted
        if (xhr.readyState == 4) {
            ret_val = JSON.parse(xhr.response); // return value

            new_url = "./view_annotation.html?"
                + "idx=" + ret_val[0]
                + "&img=" + ret_val[1]
                + "&reject=" + ret_val[2]
                + "&tags=" + ret_val[3]
                + "&age=" + ret_val[4]
                + "&valence=" + ret_val[5]
                + "&arousal=" + ret_val[6]
                + "&emotion=" + ret_val[7]
                + "&dec_factors=" + ret_val[8]
                + "&ambiguity=" + ret_val[9]
                + "&fmri_candidate=" + ret_val[10];

            document.location.href = new_url;
        }
    });

    xhr.open('POST', './api-phd/image-api/get_prev_own_annotation', true);
    xhr.setRequestHeader('X-Requested-With','XMLHttpRequest');

    var formData = new FormData();
    formData.append('jwt_token', token);
    formData.append('idx', params[0]);

    xhr.send(formData);
}

function prevKeepAnnotation() {
    const token = localStorage.getItem('jwt_token');

    let params = parseParams();

    var xhr = new XMLHttpRequest();

    xhr.addEventListener('readystatechange', function(event)
    {
        // When file has been successfully transmitted
        if (xhr.readyState == 4) {
            ret_val = JSON.parse(xhr.response); // return value

            new_url = "./view_annotation.html?"
                + "idx=" + ret_val[0]
                + "&img=" + ret_val[1]
                + "&reject=" + ret_val[2]
                + "&tags=" + ret_val[3]
                + "&age=" + ret_val[4]
                + "&valence=" + ret_val[5]
                + "&arousal=" + ret_val[6]
                + "&emotion=" + ret_val[7]
                + "&dec_factors=" + ret_val[8]
                + "&ambiguity=" + ret_val[9]
                + "&fmri_candidate=" + ret_val[10];

            document.location.href = new_url;
        }
    });

    xhr.open('POST', './api-phd/image-api/get_prev_keep_annotation', true);
    xhr.setRequestHeader('X-Requested-With','XMLHttpRequest');

    var formData = new FormData();
    formData.append('jwt_token', token);
    formData.append('idx', params[0]);

    xhr.send(formData);
}

function prevUncertainAnnotation() {
    const token = localStorage.getItem('jwt_token');

    let params = parseParams();

    var xhr = new XMLHttpRequest();

    xhr.addEventListener('readystatechange', function(event)
    {
        // When file has been successfully transmitted
        if (xhr.readyState == 4) {
            ret_val = JSON.parse(xhr.response); // return value

            new_url = "./view_annotation.html?"
                + "idx=" + ret_val[0]
                + "&img=" + ret_val[1]
                + "&reject=" + ret_val[2]
                + "&tags=" + ret_val[3]
                + "&age=" + ret_val[4]
                + "&valence=" + ret_val[5]
                + "&arousal=" + ret_val[6]
                + "&emotion=" + ret_val[7]
                + "&dec_factors=" + ret_val[8]
                + "&ambiguity=" + ret_val[9]
                + "&fmri_candidate=" + ret_val[10];

            document.location.href = new_url;
        }
    });

    xhr.open('POST', './api-phd/image-api/get_prev_uncertain_annotation', true);
    xhr.setRequestHeader('X-Requested-With','XMLHttpRequest');

    var formData = new FormData();
    formData.append('jwt_token', token);
    formData.append('idx', params[0]);

    xhr.send(formData);
}

function nextAnnotation() {
    const token = localStorage.getItem('jwt_token');

    let params = parseParams();

    var xhr = new XMLHttpRequest();

    xhr.addEventListener('readystatechange', function(event)
    {
        // When file has been successfully transmitted
        if (xhr.readyState == 4) {
            ret_val = JSON.parse(xhr.response); // return value

            new_url = "./view_annotation.html?"
                + "idx=" + ret_val[0]
                + "&img=" + ret_val[1]
                + "&reject=" + ret_val[2]
                + "&tags=" + ret_val[3]
                + "&age=" + ret_val[4]
                + "&valence=" + ret_val[5]
                + "&arousal=" + ret_val[6]
                + "&emotion=" + ret_val[7]
                + "&dec_factors=" + ret_val[8]
                + "&ambiguity=" + ret_val[9]
                + "&fmri_candidate=" + ret_val[10];

            document.location.href = new_url;
        }
    });

    xhr.open('POST', './api-phd/image-api/get_next_annotation', true);
    xhr.setRequestHeader('X-Requested-With','XMLHttpRequest');

    var formData = new FormData();
    formData.append('jwt_token', token);
    formData.append('idx', params[0]);

    xhr.send(formData);
}

function nextOwnAnnotation() {
    const token = localStorage.getItem('jwt_token');

    let params = parseParams();

    var xhr = new XMLHttpRequest();

    xhr.addEventListener('readystatechange', function(event)
    {
        // When file has been successfully transmitted
        if (xhr.readyState == 4) {
            ret_val = JSON.parse(xhr.response); // return value

            new_url = "./view_annotation.html?"
                + "idx=" + ret_val[0]
                + "&img=" + ret_val[1]
                + "&reject=" + ret_val[2]
                + "&tags=" + ret_val[3]
                + "&age=" + ret_val[4]
                + "&valence=" + ret_val[5]
                + "&arousal=" + ret_val[6]
                + "&emotion=" + ret_val[7]
                + "&dec_factors=" + ret_val[8]
                + "&ambiguity=" + ret_val[9]
                + "&fmri_candidate=" + ret_val[10];

            document.location.href = new_url;
        }
    });

    xhr.open('POST', './api-phd/image-api/get_next_own_annotation', true);
    xhr.setRequestHeader('X-Requested-With','XMLHttpRequest');

    var formData = new FormData();
    formData.append('jwt_token', token);
    formData.append('idx', params[0]);

    xhr.send(formData);
}

function nextKeepAnnotation() {
    const token = localStorage.getItem('jwt_token');

    let params = parseParams();

    var xhr = new XMLHttpRequest();

    xhr.addEventListener('readystatechange', function(event)
    {
        // When file has been successfully transmitted
        if (xhr.readyState == 4) {
            ret_val = JSON.parse(xhr.response); // return value

            new_url = "./view_annotation.html?"
                + "idx=" + ret_val[0]
                + "&img=" + ret_val[1]
                + "&reject=" + ret_val[2]
                + "&tags=" + ret_val[3]
                + "&age=" + ret_val[4]
                + "&valence=" + ret_val[5]
                + "&arousal=" + ret_val[6]
                + "&emotion=" + ret_val[7]
                + "&dec_factors=" + ret_val[8]
                + "&ambiguity=" + ret_val[9]
                + "&fmri_candidate=" + ret_val[10];

            document.location.href = new_url;
        }
    });

    xhr.open('POST', './api-phd/image-api/get_next_keep_annotation', true);
    xhr.setRequestHeader('X-Requested-With','XMLHttpRequest');

    var formData = new FormData();
    formData.append('jwt_token', token);
    formData.append('idx', params[0]);

    xhr.send(formData);
}

function nextUncertainAnnotation() {
    const token = localStorage.getItem('jwt_token');

    let params = parseParams();

    var xhr = new XMLHttpRequest();

    xhr.addEventListener('readystatechange', function(event)
    {
        // When file has been successfully transmitted
        if (xhr.readyState == 4) {
            ret_val = JSON.parse(xhr.response); // return value

            new_url = "./view_annotation.html?"
                + "idx=" + ret_val[0]
                + "&img=" + ret_val[1]
                + "&reject=" + ret_val[2]
                + "&tags=" + ret_val[3]
                + "&age=" + ret_val[4]
                + "&valence=" + ret_val[5]
                + "&arousal=" + ret_val[6]
                + "&emotion=" + ret_val[7]
                + "&dec_factors=" + ret_val[8]
                + "&ambiguity=" + ret_val[9]
                + "&fmri_candidate=" + ret_val[10];

            document.location.href = new_url;
        }
    });

    xhr.open('POST', './api-phd/image-api/get_next_uncertain_annotation', true);
    xhr.setRequestHeader('X-Requested-With','XMLHttpRequest');

    var formData = new FormData();
    formData.append('jwt_token', token);
    formData.append('idx', params[0]);

    xhr.send(formData);
}

function reSaveAnnotation() {
    const token = localStorage.getItem('jwt_token');

    // Get annotation index
    let params = parseParams();
    let idx = params[0];

    // Get selected keep/reject value
    let radios_reject = document.getElementsByName('ann_keep');
    var rej_value;
    for (var i = 0; i < radios_reject.length; i++) {
        if (radios_reject[i].checked) {
            rej_value = radios_reject[i].value;
            break;
        }
    }

    // Get selected tags
    let checkedReasons = document.querySelectorAll('input[name=ann_tags]:checked');
    var ann_tags = [];
    if (checkedReasons.length != 0) {
        for (var i=0; i<checkedReasons.length; i++) {
            ann_tags.push(checkedReasons[i].value);
        }
    } else {
        ann_tags.push('Undefined');
    }

    // Get selected age value(s)
    let checked_age = document.querySelectorAll('input[name=ann_age]:checked');
    var ann_age = [];
    if (checked_age.length != 0) {
        for (var i=0; i<checked_age.length; i++) {
            ann_age.push(checked_age[i].value);
        }
    } else {
        ann_age.push('Undefined');
    }

    // Get valence value
    let valence = document.getElementById('slide_valence').value;

    // Get arousal value
    let arousal = document.getElementById('slide_arousal').value;

    // Get Plutchik emotion
    var emotion = 'Undefined';
    var els = document.getElementById('div_svg').getElementsByClassName('clicked');
    if (els.length == 1) {
        emotion = els[0].id;
    }

    // Get decisive factors
    let checkedBoxes = document.querySelectorAll('input[name=ann_decisive_factor]:checked');
    var dec_factors = [];
    if (checkedBoxes.length != 0) {
        for (var i=0; i<checkedBoxes.length; i++) {
            dec_factors.push(checkedBoxes[i].value);
        }
    } else {
        dec_factors.push('Undefined');
    }

    // Get ambiguity
    let ambiguity = document.getElementById('slide_ambiguity').value;

    // Get fMRI candidate value
    let fmri_cand = document.getElementById('fmri_candidate').checked;

    var xhr = new XMLHttpRequest();

    xhr.addEventListener('readystatechange', function(event)
    {
        // When file has been successfully transmitted
        if (xhr.readyState == 4) {
            ret_val = JSON.parse(xhr.response); // return value

            if (ret_val.fail_code == 0) {
                document.getElementById('re_save_result').innerText = 'Annotation saved successfully!';
            } else {
                document.getElementById('re_save_result').innerText = "Oops, something went wrong...";
            }

        }
    });

    xhr.open('POST', './api-phd/image-api/re_save_annotation', true);
    xhr.setRequestHeader('X-Requested-With','XMLHttpRequest');

    var formData = new FormData();
    formData.append('jwt_token', token);
    formData.append('idx', idx);
    formData.append('img', document.getElementById('rand_img').getAttribute('src'));
    formData.append('rej_value', rej_value);
    formData.append('tags', ann_tags);
    formData.append('age', ann_age);
    formData.append('valence', valence);
    formData.append('arousal', arousal);
    formData.append('emotion', emotion);
    formData.append('dec_factors', dec_factors);
    formData.append('ambiguity', ambiguity);
    formData.append('fmri_cand', fmri_cand);

    xhr.send(formData);
}

window.onload = function() {
    // Check if user is logged in. Otherwise, prompt for login, or account creation.
    isLoggedIn();  // see login.js

    document.getElementById('div_script').style.display = 'contents';
    document.getElementById('div_main').innerHTML = viewAnnotation;
    document.getElementById('div_annotation_1').innerHTML =
        document.getElementById('div_annotation_1').innerHTML
        + `<br>
        <br>

        <input id="re_save" type="button" value="Save changes" onclick="reSaveAnnotation();" /><br>
        <span id="re_save_result"></span>`

    // Add mouse events to emotion paths in Plutchik wheel
    let groups = document.getElementById('div_svg').getElementsByClassName('g_emotion');
    for (var i=0; i<groups.length; i++) {
        groups[i].onclick = function() {toggleClicked(this)};
        groups[i].onmouseenter = function() {displayEmoDesc()};
        groups[i].onmouseleave = function() {hideEmoDesc()};
    }

    applyParams(parseParams());
    getAnnotationStats();

    if (sessionStorage.getItem('role') === 'admin') {
        // Show fMRI-Candidate checkbox
        let fmri_elems = document.getElementsByClassName('class_fmri_candidate');
        for (let i=0; i<fmri_elems.length; i++) {
            if (fmri_elems[i].tagName == 'HR') {
                fmri_elems[i].style.display = 'block';
            } else {
                fmri_elems[i].style.display = 'inline';
            }
        }

        // Show "Prev/Next (Own)" navigation buttons
        document.getElementById("prev_own_annotation").style.display = 'inline';
        document.getElementById("next_own_annotation").style.display = 'inline';
    }
};
