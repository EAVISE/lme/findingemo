function toggleClicked(e) {
//    console.log("You clicked path " + e.id);
    // Remove class "clicked" from all elements (should be at most one, but whatever) that contain it.
    let els = document.getElementById('div_svg').getElementsByClassName('clicked');
    // Check if element that was clicked already has class 'clicked', in which case it should not be re-added.
    var b_skip = false;
    for (var i=0; i<els.length; i++) {
        if (els[i].id == e.id) {
            b_skip = true;
        }
        els[i].classList.remove('clicked');
        updateAnnCounter('emotion', false);
    }

    // Add class "clicked" to clicked element.
    if (!b_skip) {
        e.classList.add('clicked');
        updateAnnCounter('emotion', true);
    }
}

function displayEmoDesc() {
    let callerEmo = event.srcElement.id;
    switch (callerEmo) {
		case 'Serenity':
			_setEmoDesc(true, 'Calm, Peaceful', 'Relaxed, open hearted', 'Something is happening that\'s essential, pure, or purposeful', 'Renew energy, make connections, reflect to learn');
			break;
		case 'Joy':
			_setEmoDesc(true, 'Excited, Pleased', 'Sense of energy and possibility', 'Life is going well', 'Sparks creativity, connection, gives energy');
			break;
		case 'Ecstasy':
			_setEmoDesc(true, 'Delighted, Giddy', 'Abundance of energy', 'This is better than I imagined', 'Strengthens relationship, increases creativity, builds memories');
			break;

		case 'Acceptance':
			_setEmoDesc(true, 'Open, Welcoming', 'Peaceful', 'We are in this together', 'Create relationships, community');
			break;
		case 'Trust':
			_setEmoDesc(true, 'Accepting, Safe', 'Warm', 'This is safe', 'Be open, connect, build alliance');
			break;
		case 'Admiration':
			_setEmoDesc(true, 'Connected, Proud', 'Glowing', 'I want to support the person or thing', 'Strengthen commitment to person or idea');
			break;

		case 'Apprehension':
			_setEmoDesc(true, 'Worried, Anxious', 'Can\'t relax', 'There could be a problem', 'Seek out potential risks, don\'t ignore problem');
			break;
		case 'Fear':
			_setEmoDesc(true, 'Stressed, Scared', 'Agitated', 'Something I care about is at risk', 'Protect what we care about');
			break;
		case 'Terror':
			_setEmoDesc(true, 'Alarmed, Petrified', 'Hard to breathe', 'There is big danger', 'Seek safety for self/others');
			break;

		case 'Distraction':
			_setEmoDesc(false, 'Scattered, Uncertain', 'Unfocused', 'I don\'t know what to prioritize', 'Consider what to prioritize');
			break;
		case 'Surprise':
			_setEmoDesc(false, 'Shocked, Unexpected', 'Heart pounding', 'Something new happened', 'Pay attention to what\'s right here');
			break;
		case 'Amazement':
			_setEmoDesc(false, 'Inspired, WOWed', 'Heart stopping', 'Something is totally unexpected', 'Remember this moment');
			break;

		case 'Pensiveness':
			_setEmoDesc(false, 'Blue, Unhappy', 'Slow & disconnected', 'Love is distant', 'Remembering people, things that are important');
			break;
		case 'Sadness':
			_setEmoDesc(false, 'Bummed, Loss', 'Heavy', 'Love is going away', 'Focus on what\'s important to us');
			break;
		case 'Grief':
			_setEmoDesc(false, 'Heartbroken, Distraught', 'Hard to get up', 'Love is lost', 'To know what we truly want');
			break;

		case 'Boredom':
			_setEmoDesc(false, 'Tired, Uninterested', 'Drained, low energy', 'The potential for this situation isn\'t being met', 'Take a rest, learn something new, refocus on what I can control about situation');
			break;
		case 'Disgust':
			_setEmoDesc(false, 'Distrust, Rejecting', 'Bitter & unwanted', 'Wrong; rules are violated', 'Notice something unsafe or wrong');
			break;
		case 'Loathing':
			_setEmoDesc(false, 'Disturbed, Horrified', 'Bileous & vehement', 'Fundamental values are violated', 'Energize to block something vile');
			break;

		case 'Annoyance':
			_setEmoDesc(false, 'Frustrated, Prickly', 'Slightly agitated', 'Something is unresolved', 'Notice minor issues');
			break;
		case 'Anger':
			_setEmoDesc(false, 'Mad, Fierce', 'Strong and heated', 'Something is in the way', 'Energize to break through a barrier');
			break;
		case 'Rage':
			_setEmoDesc(false, 'Overwhelmed, Furious', 'Pounding heart, see red', 'I\'m blocked from something vital', 'Attack an obstacle');
			break;

		case 'Interest':
			_setEmoDesc(true, 'Open, Looking', 'Mild sense of curiosity', 'Something useful might come', 'Pay attention, explore');
			break;
		case 'Anticipation':
			_setEmoDesc(true, 'Curious, Considering', 'Alert and exploring', 'Change is happening', 'Look ahead, look at what might be coming');
			break;
		case 'Vigilance':
			_setEmoDesc(true, 'Intense, Focused', 'Highly focused', 'Something big is coming', 'Get ready, look carefully, stay alert');
			break;

        default:
    }
}

function hideEmoDesc() {
    document.getElementById('div_svg_lt_box').style.display = 'none';
}

function _setEmoDesc(top_offset, similar, sens, telling, help) {
    document.getElementById('span_similar_to').innerText = similar;
    document.getElementById('span_typical_sensations').innerText = sens;
    document.getElementById('span_telling_you').innerText = telling;
    document.getElementById('span_help_you').innerText = help;

    document.getElementById('div_svg_lt_box').style.marginTop = (top_offset) ? '320px' : '155px';
    document.getElementById('div_svg_lt_box').style.display = 'block';
}
