function consentAccept() {
    const token = localStorage.getItem('jwt_token');

    var xhr = new XMLHttpRequest();

    xhr.addEventListener('readystatechange', function(event)
    {
        // When file has been successfully transmitted
        if (xhr.readyState == 4) {
            ret_val = JSON.parse(xhr.response); // return value
//            console.log(ret_val);
            if (!ret_val.is_valid) {
                redirectLogin();
            } else {
                document.getElementById("btn_consent_accept").disabled = true;
                document.getElementById("btn_consent_refuse").disabled = true;
                _setStorageAndRedirect(ret_val, false);
            }
        }
    });

    xhr.open('POST', './api-phd/login-api/mark_consent', true);
    xhr.setRequestHeader('X-Requested-With','XMLHttpRequest');

    var formData = new FormData();
    formData.append('jwt_token', token);

    xhr.send(formData);
}

function consentRefuse() {
    logOut();
}

window.onload = function() {
    // Check if user is logged in. Otherwise, prompt for login, or account creation.
    isLoggedIn();  // see login.js
}
