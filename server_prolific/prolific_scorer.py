"""
Score Prolific annotations; remove users that did not annotate 50 images (i.e., returned the task) and stuff like that.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""

import numpy as np
import pandas as pd

from annotations.ref_overlap_annotations import RefOverlapAnnotations
from tools.annotation_dataset_tools import AnnotationEmotionMap


class ProlificScorer:
    REF_ANNS = RefOverlapAnnotations.FIXED_OVERLAP_IMAGES

    @classmethod
    def score_all(cls, anns: pd.DataFrame or str, b_silent=False):
        """

        :param anns: Pandas DataFrame or path to the *.ann file to use
        :param b_silent: don't print stuff, just return the scores
        :return: (strict_score, alt_score, only_keep_score): strict_score compares everything to the ref, alt_score\
         ignores entries where the ref is "Keep" but the user selected "Reject", only_keep only looks at entries where\
         the user selected "Keep" takes
        """
        score_per_user = {}

        all_scores = []
        if isinstance(anns, str):
            anns = pd.read_csv(anns, header=0, index_col=0)
            anns.fillna('Undefined', inplace=True)

        # Get unique annotators
        uq_users = anns.user.unique()

        # For each user, see if all ref images have been annotated
        # If so, score; if not, skip user
        incomplete_user = set()
        skipping_user = set()  # Users that never selected an emotion
        skip_user = set()
        at_non_skip = 1
        for user in uq_users:
            user_anns = anns[anns.user == user]
            # Filter incomplete jobs; runs required either 45 of 50 'keep' annotations
            user_keep = user_anns[user_anns.reject == 'Keep']
            b_incomplete = False
            if len(user_keep) < 45:
                b_incomplete = True
                incomplete_user.add(user)
            # Filter users that never selected an emotion;
            # we check for this by checking that their unique emotion values only contain 1 value
            user_uq_emos = user_keep.emotion.unique()
            if len(user_uq_emos) == 1:
                skipping_user.add(user)
                continue

            user_score, alt_user_score, only_keep_score = cls.score_user(user_anns)
            if user_score == -1:
                skip_user.add(user)

            if user not in skip_user:
                all_scores.append(user_score)
                at_non_skip += 1

            if not b_silent:
                print(f"[{at_non_skip:02d}] User score: {user} == {user_score:.2f} -- "
                      f"{alt_user_score:.2f} / {only_keep_score:.2f}", end="")
                print(" :: INCOMPLETE" if b_incomplete else "")

            score_per_user[user] = (user_score, alt_user_score, only_keep_score)

        if not b_silent:
            print(f"Incomplete users: {len(incomplete_user)}")
            for u in incomplete_user:
                print("\t" + u)
            print(f"Skipping users  : {len(incomplete_user)}")
            for u in skipping_user:
                print("\t" + u)
            print(f"Total users: {len(uq_users)}")
            print(f"Avg./std. score: {np.mean(all_scores):.3f} +/- {np.std(all_scores):.3f}")

        return score_per_user

    @classmethod
    def score_user_by_name(cls, df_anns, user_name):
        df_user = df_anns[df_anns.user == user_name]
        return cls.score_user(df_user)

    @classmethod
    def score_user(cls, user_anns, b_strict=False):
        user_score, nb_overlap = 0, 0
        alt_user_score, alt_nb_overlap = 0, 0
        only_keep_score, only_keep_score_overlap = 0, 0

        for e in cls.REF_ANNS:
            b_alt_keep = True
            row = user_anns[user_anns.image_path == e.path]
            if row.empty:
                if b_strict:
                    return -1, -1, -1
                else:
                    continue
            nb_overlap += 1

            b_only_keep = False
            if row.reject.iloc[0] == 'Keep':
                b_only_keep = True
                only_keep_score_overlap += 1
            # print(f"{e.path} -- {user}")
            # Compare user Keep/Reject annotation to reference
            if row.reject.iloc[0] == e.kr:
                user_score += 1
                alt_user_score += 1
                alt_nb_overlap += 1
                if b_only_keep:
                    only_keep_score += 1
            # See what happens when we remove those cases where a user reject a fixed image that should be "Keep".
            # I suspect some people reject images instead of skipping them.
            elif e.kr == 'Keep':  # Since user annotation differs, user selected "Reject"
                b_alt_keep = False

            # Compare user valence annotation; do this irrespective of KR annotation,
            # in case user accidentally picked wrong KR
            if row.valence.iloc[0] * e.valence_sign > 0 or (row.valence.iloc[0] == 0 and e.valence_sign == 0):
                user_score += 1
                if b_only_keep:
                    only_keep_score += 1
                if b_alt_keep:
                    alt_user_score += 1
            # else:
            #     print(f"valence: user vs. ref: {row.valence.iloc[0]} vs {e.valence_sign}")
            # Compare user arousal annotation; do this irrespective of KR annotation,
            # in case user accidentally picked wrong KR
            if row.arousal.iloc[0] >= e.min_arousal:
                user_score += 1
                if b_only_keep:
                    only_keep_score += 1
                if b_alt_keep:
                    alt_user_score += 1
            # else:
            #     print(f"arousal: user vs. ref: {row.arousal.iloc[0]} vs {e.min_arousal}")
            # Compare user emotion(leaf) annotation; do this irrespective of KR annotation,
            # in case user accidentally picked wrong KR
            user_emo_leaf = AnnotationEmotionMap.EMOTION_LEAF_MAP_EXT[row.emotion.iloc[0]]
            if user_emo_leaf == e.emotion_leaf:
                user_score += 1
                if b_only_keep:
                    only_keep_score += 1
                if b_alt_keep:
                    alt_user_score += 1
            # else:
            #     print(f"emotion: user vs. ref: {user_emo_leaf} vs {e.emotion_leaf}")

        return (user_score/(4.*nb_overlap) if nb_overlap > 0 else -1,
                alt_user_score/(4.*alt_nb_overlap) if alt_nb_overlap > 0 else -1,
                only_keep_score/(4.*only_keep_score_overlap) if only_keep_score_overlap > 0 else -1)

