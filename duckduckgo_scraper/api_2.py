"""
Updated image scraper that:
1) first parses the image(s) by means of a CNN used as a filter network, to determine whether the image should be kept or not
2) also stores the URL of the downloaded images
3) base words from which to form queries are taken from CSV file

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import itertools
import json
import logging
import os
import random
import re
import time
from urllib import request, parse

import dill
import pandas as pd
import torch
from PIL import Image
from termcolor import cprint
from torchvision import models

from config import Config
from tools.trainer_tools import TrainerTools

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class DDGImgScraper2:
    EXTENSIONS = {'jpg', 'JPG', 'jpeg', 'JPEG', 'png', 'PNG'}  # What extensions do we accept?
    IGNORE_DOMAINS = {
        'alamy.com',
        'colourbox.com',
        'depositphotos.com',
        'dreamstime.com',
        'istockphoto.com',
        'gettyimages.com',
        'lovepik.com',
        'photocase.com',
        'sellercloud.com',
        'shutterstock',
        'getfreshnews.com',
        'wmmks.com'
    }

    def __init__(self, model_name: str, max_retries=3, sec_sleep: int = 1):
        """

        :param model_name: (base)path to saved PyTorch model to be used as filter network
        :param max_retries:
        :param sec_sleep: time to sleep between queries, in seconds
        """
        if model_name is None:
            raise ValueError(f"You need to specify a filter network.")
        self.max_retries = max_retries
        self.base_url = 'https://duckduckgo.com/'
        self.sec_sleep = sec_sleep

        model_name = "rku_googlenet_cuda"
        self.model = models.googlenet(pretrained=True)
        TrainerTools.change_last_layer(self.model, nb_outputs=2, device=torch.device('cpu'))
        self.model.load_state_dict(torch.load(os.path.join(Config.DIR_MODELS, f'{model_name}.pth'), map_location=torch.device('cpu')))
        self.model.eval()
        self.preprocess = dill.load(open(os.path.join(Config.DIR_MODELS, f'{model_name}_transform.dill'), 'rb'))

        # Set that will contain all URLs already checked; can be used to avoid downloading the same image twice
        # Note that an URL can have been checked but not saved.
        # At initialization, this set will be populated with the previously saved images, but note we don't keep track
        # of which URLs where all **checked**, so images that were previously (during a previous run) downloaded, checked
        # and then rejected can be downloaded and rejected again...
        self.checked_urls = None

    def run(self, csv_file: str, base_out_path: str, ref_path=None, min_width=800, max_width=1600, max_results=500,
            b_no_duplicates=True, b_recheck_queries=False):
        """
        Method to be called to start a scraping job.

        :param csv_file: path to the csv file containing the query keywords
        :param base_out_path: root out path; images will stored in directories created relative to this path
        :param ref_path: path in which to verify whether a certain query has already been processed or not
        :param min_width: minimum image width
        :param max_width: maximum image width
        :param max_results: maximum number of query results to retrieve
        :param b_no_duplicates: don't download an image again if it has already been downloaded using a different query
        :param b_recheck_queries: if True, will run a query again even if the corresponding output directory already
            exists (suggesting the query has already been run previously)
        :return:
        """
        if ref_path is None:
            ref_path = base_out_path

        # Reset downloaded_urls
        self.checked_urls = set()

        # The URLs of downloaded images will be saved to this text file
        log_file = os.path.join(base_out_path, 'url_list.txt')
        # Format: "{url} --> {full_path}"
        # If file already exists, load existing data
        if os.path.isfile(log_file):
            with open(log_file, 'r') as fin:
                for line in fin:
                    line = line.strip()
                    parts = line.split(' --> ')
                    self.checked_urls.add(parts[0])

        query_terms = pd.read_csv(csv_file)
        query_terms.fillna('', inplace=True)
        terms_emotion = set(query_terms.Emotion.unique())
        terms_subject = set(query_terms.Subject.unique())
        terms_setting = set(query_terms.Setting.unique())
        for s in [terms_emotion, terms_subject, terms_setting]:
            if '' in s:
                s.remove('')

        all_queries = list(itertools.product(terms_emotion, terms_setting, terms_subject))
        random.shuffle(all_queries)
        for p in all_queries:
            emo, setting, subj = p
            query = f"{emo} {subj} {setting}".capitalize()
            # Check output directory for query doesn't already exist; if it does, skip
            if not b_recheck_queries and \
                (os.path.isdir(os.path.join(ref_path, query)) or os.path.isdir(os.path.join(base_out_path, query))):
                print(f"Query seems to already have been processed, skipping: [{query}]")
                continue

            # Get image search results --> the URLs to the original images
            try:
                res = self.search(query, max_results=max_results)
            except Exception as e:
                cprint(f"{e}", color='cyan')
                cprint(f"Couldn't resolve query: [{query}]", color='cyan')
                continue

            # Create output path if it doesn't exist already
            out_path = os.path.join(base_out_path, query)
            if not os.path.isdir(out_path):
                os.makedirs(out_path)

            # Go over fetched URLs, and save images if they match all constraints
            for item in res:
                img_url = item['img']

                # Check URL hasn't already been checked
                if b_no_duplicates and img_url in self.checked_urls:
                    cprint(f"Image previously checked. Skipping...\n\t{img_url}", color='green')
                    continue
                # Else, add to set
                else:
                    self.checked_urls.add(img_url)

                # Check url doesn't contain a domain we want to ignore
                b_ign_dom = False
                for dom in self.IGNORE_DOMAINS:
                    if dom in img_url:
                        b_ign_dom = True
                        break
                if b_ign_dom:
                    cprint(f"Image from blacklisted domain. Skipping...\n\t{img_url}", color='yellow')
                    continue

                # Check file extension
                file_ext = ''
                for ext in self.EXTENSIONS:
                    if '.' + ext in img_url:
                        file_ext = '.' + ext
                        break
                if not file_ext:
                    print(f"Image seems to have invalid extensions. Skipping...\n\t{img_url}")
                    continue

                if min_width <= item['width'] <= max_width:
                    try:
                        print(f"imgurl: {img_url}")
                        # Try to get name from url
                        fname = img_url.split('/')[-1]
                        fname = fname[:fname.index(file_ext)+4]
                        # print(f"fname : {fname}")
                        if not fname:
                            print(f"Empty filename, skipping url [{img_url}]")
                            continue

                        if os.path.isfile(os.path.join(out_path, fname)):
                            print(f"Skipping {fname}, already downloaded...")
                            continue

                        self.download_img(img_url, out_path, fname, log_file)
                    except Exception as e:
                        cprint(f"{e}", color='cyan')
                        cprint(f"Couldn't download image from url: [{img_url}]", color='cyan')

    def search(self, query, max_results=0):
        """

        :param query: the search query
        :param max_results: maximum search results to be returned
        :return: None if unsuccessful, else a list with the found search results
        """
        params = parse.urlencode({'q': query}).encode()

        logger.debug(f"Searching images for query '{query}'.")
        logger.debug("Hitting DuckDuckGo for Token")

        #   First make a request to above URL, and parse out the 'vqd'
        #   This is a special token, which should be used in the subsequent request
        req = request.Request(
            url=self.base_url,
            data=params,
            headers={
                'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'
            }
        )
        res = request.urlopen(req, timeout=10)
        res_text = res.read().decode('utf-8')
        searchObj = re.search(r'vqd=([\d-]+)\&', res_text, re.M|re.I)

        if not searchObj:
            logger.error("Token Parsing Failed !")
            return -1

        logger.debug("Obtained Token")

        headers = {
            'authority': 'duckduckgo.com',
            'accept': 'application/json, text/javascript, */*; q=0.01',
            'sec-fetch-dest': 'empty',
            'x-requested-with': 'XMLHttpRequest',
            'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36',
            'sec-fetch-site': 'same-origin',
            'sec-fetch-mode': 'cors',
            'referer': 'https://duckduckgo.com/',
            'accept-language': 'en-US,en;q=0.9',
        }

        params = {
            'l': 'us-en',
            'o': 'json',
            'q': query,
            'vqd': searchObj.group(1),
            'f': ',,,',
            'p': '1',
            'v7exp': 'a'
        }
        params = parse.urlencode(params).encode()

        requestUrl = self.base_url + "i.js"

        logger.debug("Hitting Url : %s", requestUrl)

        results = []
        while True:
            at_try = 0
            data = None
            while at_try < self.max_retries:
                at_try += 1
                try:
                    req = request.Request(
                        url=requestUrl,
                        data=params,
                        headers=headers
                    )
                    res = request.urlopen(req, timeout=10)
                    data = json.loads(res.read().decode('utf-8'))
                    break
                except ValueError as e:
                    logger.debug("Hitting Url Failure - Sleep and Retry: %s", requestUrl)
                    time.sleep(5)
                    continue

            if at_try == self.max_retries:
                logger.debug("Max tries reached. Stopping.")
                break

            logger.debug("Hitting Url Success : %s", requestUrl)
            get_imgs = self.results_to_list(data["results"])

            results += get_imgs
            if max_results and len(results) >= max_results:
                logger.debug("Hit maximum number of results. Stopping the search.")
                results = results[:max_results]
                return results

            if "next" not in data:
                logger.debug(f"No Next Page - Exiting\nFound {len(results)} results.")
                return results

            requestUrl = self.base_url + data["next"]
            time.sleep(self.sec_sleep)  # Sleeping a couple of seconds in hopes of getting blocked less fast

    @classmethod
    def results_to_list(cls, objs):
        res = []
        for obj in objs:
            item = {
                'width': obj["width"],
                'height': obj["height"],
                'thumbnail': obj["thumbnail"],
                'url': obj["url"],
                'title': obj["title"].encode('utf-8'),
                'img': obj["image"]
            }
            res.append(item)

        return res

    def download_img(self, url: str, path: str, img_name: str, log_file: str = None):
        """

        :param url: url to the image to be downloaded
        :param path: directory wherein the image will be downloaded
        :param img_name: file name
        :param log_file:
        :return:
        """
        req = request.Request(
            url=url,
            headers={
                'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'
            }
        )
        res = request.urlopen(req, timeout=10)

        full_path = os.path.join(path, img_name)
        # Download image
        with open(full_path, 'wb') as fout:
            fout.write(res.read())
        # Pass through network; if result is too bad, delete image
        img = self.pil_loader(full_path)
        img = self.preprocess(img)
        res = self.model(img.unsqueeze(0)).squeeze()
        # Checking score for 'Keep':
        if res[1] < 0.5:
            cprint(f"Image failed the CNN test; 'keep'-score = {res[1]: .3f}. Deleting downloaded file.", color='red')
            os.remove(full_path)
        # Score is Ok, we keep the image. Also store URL!
        elif log_file is not None:
            cprint(f"Image passed the CNN test; 'keep'-score= {res[1]: .3f}. Keeping image.", color='magenta')
            with open(log_file, 'a') as fout:
                fout.write(f"{url} --> {full_path}\n")

    @classmethod
    def pil_loader(cls, full_path: str) -> Image.Image:
        # open path as file to avoid ResourceWarning (https://github.com/python-pillow/Pillow/issues/835)
        with open(full_path, 'rb') as f:
            img = Image.open(f)
            return img.convert('RGB')


if __name__ == '__main__':
    scraper = DDGImgScraper2(model_name="googlenet_pretrained_model_1_cuda")
    scraper.run(csv_file="./Scraping_Part1_keywords_extended.csv",
                base_out_path=os.path.join(Config.DIR_IMAGES, "Run_2"),
                min_width=800,
                max_width=3200,
                max_results=1000,
                b_no_duplicates=True,
                b_recheck_queries=False)
