"""
Based on https://github.com/LaurentMertens/duckduckgo-images-api
"""
import itertools
import json
import logging
import os
import re
import time
from urllib import request, parse

from termcolor import cprint

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class DDGImgScraper:
    def __init__(self, max_retries=3):
        self.max_retries = max_retries
        self.base_url = 'https://duckduckgo.com/'

    def search(self, query, max_results=0):
        """

        :param query: the search query
        :param max_results: maximum search results to be returned
        :return: None if unsuccessful, else a list with the found search results
        """
        params = parse.urlencode({'q': query}).encode()

        logger.debug(f"Searching images for query '{query}'.")
        logger.debug("Hitting DuckDuckGo for Token")

        #   First make a request to above URL, and parse out the 'vqd'
        #   This is a special token, which should be used in the subsequent request
        req = request.Request(
            url=self.base_url,
            data=params,
            headers={
                'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'
            }
        )
        res = request.urlopen(req, timeout=10)
        res_text = res.read().decode('utf-8')
        searchObj = re.search(r'vqd=([\d-]+)\&', res_text, re.M|re.I)

        if not searchObj:
            logger.error("Token Parsing Failed !")
            return -1

        logger.debug("Obtained Token")

        headers = {
            'authority': 'duckduckgo.com',
            'accept': 'application/json, text/javascript, */*; q=0.01',
            'sec-fetch-dest': 'empty',
            'x-requested-with': 'XMLHttpRequest',
            'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36',
            'sec-fetch-site': 'same-origin',
            'sec-fetch-mode': 'cors',
            'referer': 'https://duckduckgo.com/',
            'accept-language': 'en-US,en;q=0.9',
        }

        params = {
            'l': 'us-en',
            'o': 'json',
            'q': query,
            'vqd': searchObj.group(1),
            'f': ',,,',
            'p': '1',
            'v7exp': 'a'
        }
        params = parse.urlencode(params).encode()

        requestUrl = self.base_url + "i.js"

        logger.debug("Hitting Url : %s", requestUrl)

        results = []
        while True:
            at_try = 0
            data = None
            while at_try < self.max_retries:
                at_try += 1
                try:
                    req = request.Request(
                        url=requestUrl,
                        data=params,
                        headers=headers
                    )
                    res = request.urlopen(req, timeout=10)
                    data = json.loads(res.read().decode('utf-8'))
                    break
                except ValueError as e:
                    logger.debug("Hitting Url Failure - Sleep and Retry: %s", requestUrl)
                    time.sleep(5)
                    continue

            if at_try == self.max_retries:
                logger.debug("Max tries reached. Stopping.")
                break

            logger.debug("Hitting Url Success : %s", requestUrl)
            get_imgs = self.results_to_list(data["results"])

            results += get_imgs
            if max_results and len(results) >= max_results:
                logger.debug("Hit maximum number of results. Stopping the search.")
                results = results[:max_results]
                return results

            if "next" not in data:
                logger.debug(f"No Next Page - Exiting\nFound {len(results)} results.")
                return results

            requestUrl = self.base_url + data["next"]
            time.sleep(1)  # Sleeping 1s in hopes of getting blocked less fast

    @classmethod
    def results_to_list(cls, objs):
        res = []
        for obj in objs:
            item = {
                'width': obj["width"],
                'height': obj["height"],
                'thumbnail': obj["thumbnail"],
                'url': obj["url"],
                'title': obj["title"].encode('utf-8'),
                'img': obj["image"]
            }
            res.append(item)

        return res

    @classmethod
    def download_img(cls, url: str, path: str, img_name: str):
        """

        :param url: url to the image to be downloaded
        :param path: directory wherein the image will be downloaded
        :param img_name: file name
        :return:
        """
        req = request.Request(
            url=url,
            headers={
                'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'
            }
        )
        res = request.urlopen(req, timeout=10)

        with open(os.path.join(path, img_name), 'wb') as fout:
            fout.write(res.read())


if __name__ == '__main__':
    # reading mind in the eyes test --> s baron cohen

    min_width = 800
    max_width = 1600
    extensions = ['jpg', 'JPG', 'jpeg', 'JPEG', 'png', 'PNG']  # What extensions do we accept?
    ignore_domains = [
        'alamy.com',
        'colourbox.com',
        'depositphotos.com',
        'dreamstime.com',
        'istockphoto.com',
        'gettyimages.com',
        'lovepik.com',
        'photocase.com',
        'sellercloud.com',
        'shutterstock'
    ]

    base_out_path = os.path.join(os.path.expanduser('~'), 'Work', 'Projects', 'ID-N', 'Data', 'ScrapedImages')

    emotions = [#'Happiness',
#'Pride',
'Excitement',
'Peace',
'Satisfaction',
'Acceptance',
'Affection',
'Joy',
'Compassion',
'Adoration',
'Desire',
'Grateful',
'Love',
'Humble',
'Contentment',
'Empathetic',
'Amusement',
'Appreciative',
'Confident',
'Optimistic',
'Cheerful',
'Carefree',
'Sweet',
'Kind']

    emotions_adj = [
        'Happy',
        'Proud',
        'Excited',
        'Peaceful',
        'Satisfied',
        'Accepting',
        'Affectionate',
        'Joyful',
        'Compassionate',
        'Adoring',
        'Desiring',
        'Grateful',
        'Loving',
        'Humble',
        'Content',
        'Empathetic',
        'Amused',
        'Appreciative',
        'Confident',
        'Optimistic',
        'Cheerful',
        'Carefree',
        'Sweet',
        'Kind'
    ]

    subjects = ['children', 'youth', 'adults', 'seniors', 'people']

    settings = ['forest', 'wedding', 'beach', 'museum', 'circus',
                'playground', 'school', 'work', 'workplace', 'sports',
                'anniversary', 'birthday', 'nature', 'party', 'festival',
                'competition', 'protest', 'rally', 'gathering', 'conference']
    # outdoors, indoors, home, school

    # ############################################################
    # Part 2
    # ############################################################

    scraper = DDGImgScraper()

    for p in itertools.product(emotions_adj, settings, subjects):
        emo, setting, subj = p
        query = f"{emo} {subj} {setting}"
        try:
            res = scraper.search(query, max_results=500)
        except Exception as e:
            cprint(f"{e}", color='cyan')
            cprint(f"Couldn't resolve query: [{query}]", color='cyan')
            continue

        out_path = os.path.join(base_out_path, query)
        if not os.path.isdir(out_path):
            os.makedirs(out_path)

        for item in res:
            img_url = item['img']

            # Check url doesn't contain a domain we want to ignore
            b_ign_dom = False
            for dom in ignore_domains:
                if dom in img_url:
                    b_ign_dom = True
                    break
            if b_ign_dom:
                cprint(f"Image from blacklisted domain. Skipping...\n\t{img_url}", color='yellow')
                continue

            # Check file extension
            file_ext = ''
            for ext in extensions:
                if '.' + ext in img_url:
                    file_ext = '.' + ext
                    break
            if not file_ext:
                print(f"Image seems to have invalid extensions. Skipping...\n\t{img_url}")
                continue

            if min_width <= item['width'] <= max_width:
                try:
                    print(f"imgurl: {img_url}")
                    # Try to get name from url
                    fname = img_url.split('/')[-1]
                    fname = fname[:fname.index(file_ext)+4]
                    print(f"fname : {fname}")
                    if not fname:
                        print(f"Empty filename, skipping url [{img_url}]")
                        continue

                    if os.path.isfile(os.path.join(out_path, fname)):
                        print(f"Skipping {fname}, already downloaded...")
                        continue

                    scraper.download_img(img_url, out_path, fname)
                except Exception as e:
                    cprint(f"{e}", color='cyan')
                    cprint(f"Couldn't download image from url: [{img_url}]", color='cyan')
