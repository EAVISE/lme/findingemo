"""
Define some system settings.

.. codeauthor:: Laurent Mertens <laurent.mertens@kuleuven.be>
"""
import os


class Config:
    #: Users system home dir
    HOME_DIR = os.path.expanduser('~')

    #: Parent folder containing the codebase; MAKE SURE TO UPDATE THIS VALUE TO REFLECT YOUR SETUP!
    # DIR_CODE = None  # os.path.join(HOME_DIR, 'Path', 'To', 'Repo', 'Folder')
    DIR_CODE = os.path.join(HOME_DIR, 'Work', 'Projects', 'NeuroANN', 'Code', 'FindingEmo')
    DIR_DATA = os.path.abspath(os.path.join(DIR_CODE, 'data'))
    # DIR_DATA = os.path.join(HOME_DIR, 'Work', 'Projects', 'NeuroANN', 'Data')
    #: Directory to which log files (e.g., output during training of models) will be written
    DIR_LOGS = os.path.join(HOME_DIR, 'Logs')

    # Website interface authentication
    DIR_AUTH = os.path.join(DIR_DATA, "AnnotationServerAuth")
    # Scraped images
    DIR_IMAGES = os.path.join(DIR_DATA, "AnnImagesProlific")
    #: Logs from training models
    # DIR_LOGS = os.path.join(DIR_DATA, "ExpLogs")
    #: Trained models
    DIR_MODELS = os.path.join(DIR_DATA, "Models")
    #: Pre-trained models
    DIR_PRETRAINED_MODELS = os.path.join(DIR_DATA, "Models")
    #: Default directory containing pre-computed features
    DIR_BUFFER_ROOT = os.path.join(DIR_DATA, "BufferedFeatures")
    DIR_BUFFER_CLIP = os.path.join(DIR_BUFFER_ROOT, 'Buffer_CLIP')
    DIR_BUFFER_DINOv2 = os.path.join(DIR_BUFFER_ROOT, "Buffer_DINOv2")
    DIR_BUFFER_EMONET = os.path.join(DIR_BUFFER_ROOT, "Buffer_EmoNet")
    DIR_BUFFER_IN = os.path.join(DIR_BUFFER_ROOT, "Buffer_ImageNet")
    DIR_BUFFER_OI = os.path.join(DIR_BUFFER_ROOT, "Buffer_YoLov3+OI")
    DIR_BUFFER_OI_TO_FER = os.path.join(DIR_BUFFER_ROOT, "Buffer_OIToFER")
    DIR_BUFFER_PLACES365 = os.path.join(DIR_BUFFER_ROOT, "Buffer_Places365")
    #: Default annotation file
    FILE_ANN_SINGLE = os.path.join(DIR_DATA, "annotations_single.ann")
    #: Overlap images for multi-user annotations file
    FILE_OVERLAP_NAME = "overlap_images.csv"
    FILE_OVERLAP = os.path.join(DIR_AUTH, FILE_OVERLAP_NAME)
    #: Key file for encrypting/decrypting user
    FILE_FERNET_KEY = os.path.join(DIR_AUTH, "filekey.key")
    #: (Encrypted) User database
    FILE_USER_DB = os.path.join(DIR_AUTH, "user_db.csv.enc")
    #: Pre-computed Keep/Reject scores for scraped images
    FILE_TSV_RK_NAME = "precomputed_rku.tsv"
    FILE_TSV_RK = os.path.join(DIR_AUTH, FILE_TSV_RK_NAME)
    FILE_TSV_EMO8_NAME = "precomputed_emo8.tsv"
    FILE_TSV_EMO8 = os.path.join(DIR_AUTH, FILE_TSV_EMO8_NAME)
